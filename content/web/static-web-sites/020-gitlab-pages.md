+++
title = "- gitlab pages"
weight = 20
prev = "/web/static-web-sites/10-github-pages"
next = "/web/100-lets-encrypt"
toc = true
date = "2017-03-12T15:36:35Z"

+++

https://about.gitlab.com/2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/

Originally `2016-04-05`


[Gitlab Pages][link-1]

Need to add some content here?  Although gitlab blog recently in June 2016 also has a series on static websites and gitlab pages.

The web page http://pages.gitlab.io/ has good directions on how to setup gitlab pages.


This is also interesting:  [Hugo Tutorial on Gitlab Pages with Gitlab CI][link-2]

(Optional) Follow steps listed [here][link-5]
```
If you skip this step, your website will be available at https://username.gitlab.io/projectname, where username your username on GitLab and projectname the name of the project you forked in the first step. This is what we call a project Page.  https://docs.gitlab.com/ee/pages/README.html?&_ga=1.116501702.1501937781.1474373742#project-pages

GitLab User or Group Pages
To use this project as your user/group website, you will need one additional step: just rename your project to namespace.gitlab.io, where namespace is your username or groupname. This can be done by navigating to your project's Settings.
You'll need to configure your site too: change this line in your config.toml, from "https://pages.gitlab.io/hugo/" to baseurl = "https://namespace.gitlab.io". Proceed equaly if you are using a custom domain: baseurl = "http(s)://example.com".
Read more about user/group Pages and project Pages.
```


### How to set up GitLab Pages in a repository where there's also actual code

Remember that GitLab Pages are by default branch/tag agnostic and their deployment relies solely on what you specify in .gitlab-ci.yml. You can limit the pages job with the only parameter, whenever a new commit is pushed to a branch that will be used specifically for your pages.

That way, you can have your project's code in the master branch and use an orphan branch (let's name it pages) that will host your static generator site.

You can create a new empty branch like this:

```
git checkout --orphan pages
```
The first commit made on this new branch will have no parents and it will be the root of a new history totally disconnected from all the other branches and commits. Push the source files of your static generator in the pages branch.

Below is a copy of .gitlab-ci.yml where the most significant line is the last one, specifying to execute everything in the pages branch:

```
image: ruby:2.1

pages:
  script:
  - gem install jekyll
  - jekyll build -d public/
  artifacts:
    paths:
    - public
  only:
  - pages
```

https://about.gitlab.com/2016/04/07/gitlab-pages-setup/

https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/

https://about.gitlab.com/2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/

https://about.gitlab.com/2016/06/24/secure-gitlab-pages-with-startssl/

https://about.gitlab.com/2016/08/19/posting-to-your-gitlab-pages-blog-from-ios/


https://about.gitlab.com/2016/12/24/were-bringing-gitlab-pages-to-community-edition/



[link-1]: https://about.gitlab.com/2016/04/04/gitlab-pages-get-started/
[link-2]: https://gohugo.io/tutorials/hosting-on-gitlab/
[link-3]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/
[link-4]: https://gitlab.com/groups/themes-templates
[link-5]: https://gitlab.com/themes-templates/hugo
