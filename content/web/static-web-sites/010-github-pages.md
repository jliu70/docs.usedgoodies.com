+++
toc = true
date = "2017-03-12T15:36:28Z"
title = "- github pages"
weight = 10
prev = "/web/static-web-sites/"
next = "/web/static-web-sites/020-gitlab-pages"
+++

Originally `2016-01-30`


[GitHub Pages][link-1] is a great way to have a website hosted directly from your GitHub repository.  You get one site per GitHub account and organization, and unlimited project sites.

For the user or organization, the website is hosted at `http://username.github.io`

For a project site, the URL is `http://username.github.io/projectRepository`

There are options to allow you to set up a custom domain with GitHub Pages.

GitHub provides step-by-step directions on how to set everything up at https://pages.github.com.

### Pricing
GitHub provides a free tier  (no private repositories - public repos only).  This can be a very inexpensive way for you to host a static website.

See https://github.com/pricing for more information.


Additional documentation:

- https://help.github.com/categories/github-pages-basics/
- https://gohugo.io/tutorials/github-pages-blog/

- https://help.github.com/articles/user-organization-and-project-pages/

### Project Pages

Unlike User and Organization Pages, Project Pages are kept in the same repository as their project. Both personal accounts and organizations can create Project Pages. The URL for a personal account's Project Page will be 'http(s)://<username>.github.io/<projectname>' , while an organization's URL will be 'http(s)://<orgname>.github.io/<projectname>' . The steps for creating Project Pages are the same for both.


Project Pages are similar to User and Organization Pages, with a few slight differences:

You can build and publish Project Pages sites from the master or gh-pages branch. You can also publish your site from a /docs folder on your master branch. For more information, see "Configuring a publishing source for GitHub Pages."

If no custom domain is used, the Project Pages sites are served under a subpath of the User Pages site: username.github.io/projectname




[link-1]: https://pages.github.com
[link-2]: https://help.github.com/articles/user-organization-and-project-pages/
