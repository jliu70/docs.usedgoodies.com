+++
date = "2017-03-12T15:26:48Z"
title = "lets encrypt"
weight = 100
toc = true
+++

Originally `2016-06-14`


[Https is not a magic bullet][link-2]

Let's Encrypt  https://letsencrypt.org

So, I've heard about this for little bit, but I finally got around to using it today.

No more paying $$ to Go Daddy or whatever CA you used before.
The only draw-back is that the certs are good for 90 days only.

They list their reasons for 90 days here:  https://letsencrypt.org/2015/11/09/why-90-days.html

It's a pain in the ass, but I guess that's one way to push for automation.  ;)

Until recently it wasn't part of the firefox CA cert bundles. I think they were signing with another root CA, but it was second level and needed a cert chain.



I'd recommend the "lego" (go-based) client for ease of use.
The native "certbot" or "lets encrypt" client is not as simple to setup and use.


https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/

A good comparison of StartSSL and Let's Encrypt here: https://about.gitlab.com/2016/06/24/secure-gitlab-pages-with-startssl/


Some references below:

https://www.metachris.com/2015/12/comparison-of-10-acme-lets-encrypt-clients/

https://blog.gopheracademy.com/advent-2015/generate-free-tls-certs-with-lego/

Let’s Encrypt and the ACME protocol make free, automatic TLS more accessible than ever. It’s our responsibility as programmers to keep our applications secure, private, and reliable, and now we have no excuse not to use TLS for this purpose.

TLS (formerly “SSL”) certificates cost money and require manual labor to obtain, install, and maintain. Besides, there’s no reason to encrypt unless you collect or send sensitive data, right? Wrong. Encryption not only prevents eavesdropping and surveillance, it also protects packets from being modified in flight—modifications that could break your API or track your users. Essentially, TLS adds a layer of privacy and integrity to your application.

This post will guide you through a free, easy way to obtain real, trusted TLS certificates using Go. Thanks to the efforts of the Internet Security Research Group (ISRG) and, in particular, Let’s Encrypt, the ACME protocol makes it possible to do this. Sebastian Erhart has done an excellent job building an ACME client in Go called lego that we can use to get free, valid TLS certificates in seconds.

Definitions

There’s been a lot of confusion about ACME, Let’s Encrypt, and this whole “free certificates” thing, so first, a few clarifications:

ACME is the protocol that facilitates the automatic issuance, renewal, and revocation of x.509 certificates between certificate authorities and applicants. At time of writing, the spec is still a working draft at the IETF.

ISRG is the non-profit organization behind Let’s Encrypt.

Let’s Encrypt is the first certificate authority (CA) to implement the ACME protocol.

Domain Validation (DV) Certificates are issued once a CA is convinced you own the domain you are requesting a certificate for. Let’s Encrypt issues DV certs. Make no mistake: all DV certificates are technically the same. A free, automated DV cert offers no fewer benefits than one costing $10 or $20.

Currently, the only ACME-based CA is Let’s Encrypt, so for now, the terms “ACME client” and “Let’s Encrypt client” are mostly interchangeable. This may not always be the case, however, so pay attention to library docs and implementation details in the future. (For example, Let’s Encrypt’s CA server software is Boulder, but not all Boulder features are defined in the ACME spec.)

https://www.simplify.ba/articles/2016/05/22/lets-encrypt-renewal-with-lego/
May 22, 2016

Three months ago I created Let's Encrypt certificate using Lego. Today was the time to renew it.

Lego is now even better than before. At the time of certificate creation, renew option was not working, but now is fully supported. This time I didn't build Lego from source, I just downloaded binary and replaced old one. Renewal is easy as creation:

$ ./lego --email="[my e-mail]" --domains="simplify.ba" --domains="www.simplify.ba" --dns="route53" renew
Again, Lego did two ACME challenges, for both domains and I got certificates for both domains in .logo/certificatesand used aws cli to install certificate on CloudFront CDN (this require AWS_SECRET_ACCESS_KEY, AWS_ACCESS_KEY_ID and AWS_REGION environment variables set):

$ aws iam upload-server-certificate --server-certificate-name simplify.ba-ssl-20160522 --certificate-body file://simplify.ba.crt --private-key file://simplify.ba.key --path /cloudfront/prod/
After changing certificate for CloudFront distribution on AWS console and confirming that certificate work, I removed old one:

$ aws iam delete-server-certificate --server-certificate-name simplify.ba-ssl
I'm definitively sticking with Lego for any work with Let's Encrypt certificates.



https://community.letsencrypt.org/t/will-this-service-supply-ucc-certificates/105
https://letsencrypt.org/docs/faq/
Can I get a certificate for multiple domain names (SAN certificates)?
Yes, the same certificate can apply to several different names using the Subject Alternative Name (SAN) mechanism. The resulting certificates will be accepted by browsers for any of the domain names listed in them.
Certificates that use Subject Alternative Names (SAN) are powerful tools that you can use to secure multiple domain names inexpensively and efficiently.  SAN certificates are capable of securing up to 25 fully qualified domain names with a single certificate. Certificates that use SAN can also be referred to as Unified Communications (UC) certificates.


https://w3techs.com/blog/entry/the_impact_of_lets_encrypt_on_the_ssl_certificate_market   Interesting note: "France loves Let's Encrypt. [Let's Encrypt] is the  market leader in several countries, most notably in France with 46.3% market share. "  


[link-1]:https://letsencrypt.org
[link-2]: http://arstechnica.com/security/2016/07/https-is-not-a-magic-bullet-for-web-security/?comments=1
