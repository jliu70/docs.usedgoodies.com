+++
date = "2019-11-19T15:26:48Z"
title = "web resources"
weight = 900
toc = true
+++



### 1/9/2020
#curl
# Using Curl to query IBM PAS REST API

## Just some examples of what can be queried 
NOTE:  maybe the next 'interesting' report - generate for /resources/ipGroups

### /resources/virtualApplications (Pattern instances) - implemented in pasQuery.go
```
curl -k -H "Authorization: Basic amxpdTpWUDg3NTV6TjVzQT81bSQ="  -H "Accept: application/json" -H "X-IBM-PureSystem-API-Version: 2.0" https://10.68.114.1/resources/virtualApplications

#curl -k --user name:password -H "X-IBM-PureSystem-API-Version: 2.0" https://10.68.114.2/resources/virtualApplications
```

### Example output
```
$ curl -k -H "Authorization: Basic amxpdTpWUDg3NTV6TjVzQT81bSQ="  -H "Accept: application/json" -H "X-IBM-PureSystem-API-Version: 2.0" https://10.68.114.1/resources/virtualApplications | ~/Downloads/gron.exe
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  3812  100  3812    0     0   2292      0  0:00:01  0:00:01 --:--:--  2293json = [];
json[0] = {};
json[0].access_rights = {};
json[0].access_rights.ZU1NA011549662490 = "F";
json[0].access_rights["d-8d324d84-668e-4e64-bce1-936940e3023f"] = "R";
json[0].app_id = "a-2310b6e7-3db9-4b57-9ee7-0ec880b714d3";
json[0].app_type = "application";
json[0].appmodel = "https://127.0.0.1:9444/storehouse/user/deployments/d-8d324d84-668e-4e64-bce1-936940e3023f/appmodel.json";
json[0].create_time = "2019-12-02T21:35:09Z";
json[0].creator = "ZU1NA011549662490";
json[0].creator_name = "ZU1NA011549662490";
json[0].deployment = "https://127.0.0.1:9444/storehouse/user/deployments/d-8d324d84-668e-4e64-bce1-936940e3023f/deployment.json";
json[0].deployment_name = "ICO-PREP-F-DZQA8005";
json[0].desired = "RUNNING";
json[0].environment_profile_uuid = "ep-d9f87dfb-7439-48d4-9dfc-f3cbe313454c";
json[0].expirationTime = -1;
json[0].health = "NORMAL";
json[0].id = "d-8d324d84-668e-4e64-bce1-936940e3023f";
json[0].instances = [];
json[0].instances[0] = {};
json[0].instances[0].activated = true;
json[0].instances[0].activation_status = "RUNNING";
json[0].instances[0].cpucount = 1;
json[0].instances[0].displayname = "LINUX_RH7.11575322557897";
json[0].instances[0].health = "NORMAL";
json[0].instances[0].hypervisorHostname = "10.68.114.1";
json[0].instances[0].id = "LINUX_RH7.11575322557897";
json[0].instances[0].last_update = "2019-12-08T11:47:56.863Z";
json[0].instances[0].location = {};
json[0].instances[0].location.cloud_group = {};
json[0].instances[0].location.cloud_group.id = "1";
json[0].instances[0].location.cloud_group.name = "BNR02";
json[0].instances[0].location.name = "PASPRELOADED";
json[0].instances[0].location.placement = {};
json[0].instances[0].location.placement.cloud_group = {};
json[0].instances[0].location.placement.cloud_group.name = "BNR02";
json[0].instances[0].location.placement.ip_groups = [];
json[0].instances[0].location.placement.ip_groups[0] = {};
json[0].instances[0].location.placement.ip_groups[0].address = "10.68.64.72";
json[0].instances[0].location.placement.ip_groups[0].hostname = "DZQA8005.wwmgmt.intraxa";
json[0].instances[0].location.placement.ip_groups[0].name = "BNR02_ATS_MGMT_ZU1_IPG";
json[0].instances[0].location.placement.ip_groups[0].nic = "management";
json[0].instances[0].location.placement.ip_groups[0].purpose = "data";
json[0].instances[0].location.placement.ip_groups[1] = {};
json[0].instances[0].location.placement.ip_groups[1].address = "10.79.132.13";
json[0].instances[0].location.placement.ip_groups[1].hostname = "DZQA8005.ppprivmgmt.intraxa";
json[0].instances[0].location.placement.ip_groups[1].name = "BNR02_ZU1_DEVS_INT_IPG";
json[0].instances[0].location.placement.ip_groups[1].nic = "data_2";
json[0].instances[0].location.placement.ip_groups[1].purpose = "data";
json[0].instances[0].location.placement.location = "PASPRELOADED";
json[0].instances[0].master = true;
json[0].instances[0].memory = 4096;
json[0].instances[0].name = "LINUX_RH7.11575322557897";
json[0].instances[0].previousStatus = "RUNNING";
json[0].instances[0].previous_status = "RUNNING";
json[0].instances[0].private_ip = "10.68.64.72";
json[0].instances[0].public_hostname = "DZQA8005.wwmgmt.intraxa";
json[0].instances[0].public_ip = "10.68.64.72";
json[0].instances[0].runtimeid = "http://10.68.114.1:5006/deployment/resources/instances/55a1050b-a7a1-4527-b332-90c784cf446d";
json[0].instances[0].start_time = "2019-12-02T21:36:55.902Z";
json[0].instances[0].status = "RUNNING";
json[0].instances[0].uuid = "55a1050b-a7a1-4527-b332-90c784cf446d";
json[0].instances[0].vmId = 584;
json[0].instances[0]["activation-status"] = "RUNNING";
json[0].instances[0]["reboot.count"] = 0;
json[0].instances[0]["stopped.by"] = "";
json[0].is_publicep = true;
json[0].patterntype = "vsys";
json[0].role_error = false;
json[0].start_time = "2019-12-02T21:35:57.897Z";
json[0].status = "RUNNING";
json[0].topology = "https://127.0.0.1:9444/storehouse/user/deployments/d-8d324d84-668e-4e64-bce1-936940e3023f/topology.json";
json[0].version = "1.0";
json[0].virtual_system = {};
json[0].virtual_system.deploymentPriorityId = "1";
json[0].virtual_system.environmentProfile = "/resources/environmentProfiles/1";
json[0].virtual_system.environment_profile = "/resources/environmentProfiles/1";
json[0].virtual_system.environment_profile_name = "AXA Profile for ICO";
json[0].virtual_system.fsm_ip = "10.68.114.1";
json[0].virtual_system.id = "584";
json[0].virtual_system.is_local = true;
json[0].virtual_system.locations = [];
json[0].virtual_system.locations[0] = "PASPRELOADED";
json[0].virtual_system.platform = "PureSystems_ESX";
json[0].virtual_system.priority = "high";
```



### /admin/resources/storagereport - implemented in pasStorage.go
```
curl -k -H "Authorization: Basic amxpdTpWUDg3NTV6TjVzQT81bSQ="  -H "Accept: application/json" -H "X-IBM-PureSystem-API-Version: 2.0" https://10.68.114.1/admin/resources/storagereport
```

### /resources/ipUsageReport
```
curl -k -H "Authorization: Basic amxpdTpWUDg3NTV6TjVzQT81bSQ="  -H "Accept: application/json" -H "X-IBM-PureSystem-API-Version: 2.0" https://10.68.114.2/resources/ipUsageReport
```

### /admin/resources/cloudgroupreport
```
curl -k -H "Authorization: Basic amxpdTpWUDg3NTV6TjVzQT81bSQ="  -H "Accept: application/json" -H "X-IBM-PureSystem-API-Version: 2.0" https://10.68.114.2/admin/resources/cloudgroupreport
```

### /admin/resources/ipgroupreport

### /resources/environmentProfiles
```
curl -k -H "Authorization: Basic amxpdTpWUDg3NTV6TjVzQT81bSQ="  -H "Accept: application/json" -H "X-IBM-PureSystem-API-Version: 2.0" https://10.68.114.2/resources/environmentProfiles
```



### /resources/ipGroups
```
curl -k -H "Authorization: Basic amxpdTpWUDg3NTV6TjVzQT81bSQ="  -H "Accept: application/json" -H "X-IBM-PureSystem-API-Version: 2.0" https://10.68.114.2/resources/ipGroups
```

### /resources/ipGroups/58
```
curl -k -H "Authorization: Basic amxpdTpWUDg3NTV6TjVzQT81bSQ="  -H "Accept: application/json" -H "X-IBM-PureSystem-API-Version: 2.0" https://10.68.114.2/resources/ipGroups/58
```

### Example output
```
$ curl -k -H "Authorization: Basic amxpdTpWUDg3NTV6TjVzQT81bSQ="  -H "Accept: application/json" -H "X-IBM-PureSystem-API-Version: 2.0" https://10.68.114.2/resources/ipGroups/58 | ~/Downloads/gron.exe
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   624  100   624    0     0    205      0  0:00:03  0:00:03 --:--:--   206json = {};
json.alternategateway = null;
json.computernameprefix = null;
json.created = 1504257720381;
json.description = null;
json.domain = null;
json.domainsuffixes = null;
json.gateway = "10.79.193.254";
json.hostnameprefix = null;
json.id = 58;
json.name = "BOE01_NSM_DEVS_INT_IPG";
json.netmask = "255.255.254.0";
json.networks = [];
json.networks[0] = "/resources/networks/58";
json.primarydns = "10.77.250.85";
json.primarywins = null;
json.protocol = "static";
json.region = null;
json.secondarydns = "10.77.250.86";
json.secondarywins = null;
json.subnetaddress = "10.79.192.0";
json.updated = 1504257720381;
json.version = "IPv4";
json.workgroup = null;
```



```
curl -k -u jliu:e/4lmp\?1sEr5l2p -H "Accept: application/json" -H "X-IBM-PureSystem-API-Version: 2.0" https://10.68.114.1/resources/virtualApplications

$ curl -k -u jliu:e/4lmp\?1sEr5l2p -H "Accept: application/json" -H "X-IBM-PureSystem-API-Version: 2.0" https://10.68.114.1/resources/virtualApplications
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  3812  100  3812    0     0    920      0  0:00:04  0:00:04 --:--:--   920
[
   {
      "status": "RUNNING",
      "app_type": "application",
      "patterntype": "vsys",
      "desired": "RUNNING",
      "instances": [
         {
            "stopped.by": "",
            "reboot.count": 0,
            "location": {
               "cloud_group": {
                  "id": "1",
                  "name": "BNR02"
               },
               "placement": {
                  "cloud_group": {
                     "name": "BNR02"
                  },
                  "ip_groups": [
                     {
                        "address": "10.68.64.72",
                        "nic": "management",
                        "name": "BNR02_ATS_MGMT_ZU1_IPG",
                        "hostname": "DZQA8005.wwmgmt.intraxa",
                        "purpose": "data"
                     },
                     {
                        "address": "10.79.132.13",
                        "nic": "data_2",
                        "name": "BNR02_ZU1_DEVS_INT_IPG",
                        "hostname": "DZQA8005.ppprivmgmt.intraxa",
                        "purpose": "data"
                     }
                  ],
                  "location": "PASPRELOADED"
               },
               "name": "PASPRELOADED"
            },
            "status": "RUNNING",
            "activation_status": "RUNNING",
            "public_ip": "10.68.64.72",
            "runtimeid": "http://10.68.114.1:5006/deployment/resources/instances/55a1050b-a7a1-4527-b332-90c784cf446d",
            "health": "NORMAL",
            "memory": 4096,
            "master": true,
            "id": "LINUX_RH7.11575322557897",
            "previous_status": "RUNNING",
            "last_update": "2019-12-08T11:47:56.863Z",
            "private_ip": "10.68.64.72",
            "hypervisorHostname": "10.68.114.1",
            "public_hostname": "DZQA8005.wwmgmt.intraxa",
            "name": "LINUX_RH7.11575322557897",
            "activated": true,
            "previousStatus": "RUNNING",
            "activation-status": "RUNNING",
            "start_time": "2019-12-02T21:36:55.902Z",
            "vmId": 584,
            "cpucount": 1,
            "uuid": "55a1050b-a7a1-4527-b332-90c784cf446d",
            "displayname": "LINUX_RH7.11575322557897"
         }
      ],
      "health": "NORMAL",
      "role_error": false,
      "creator": "ZU1NA011549662490",
      "version": "1.0",
      "id": "d-8d324d84-668e-4e64-bce1-936940e3023f",
      "environment_profile_uuid": "ep-d9f87dfb-7439-48d4-9dfc-f3cbe313454c",
      "expirationTime": -1,
      "is_publicep": true,
      "create_time": "2019-12-02T21:35:09Z",
      "start_time": "2019-12-02T21:35:57.897Z",
      "app_id": "a-2310b6e7-3db9-4b57-9ee7-0ec880b714d3",
      "topology": "https://127.0.0.1:9444/storehouse/user/deployments/d-8d324d84-668e-4e64-bce1-936940e3023f/topology.json",
      "deployment": "https://127.0.0.1:9444/storehouse/user/deployments/d-8d324d84-668e-4e64-bce1-936940e3023f/deployment.json",
      "creator_name": "ZU1NA011549662490",
      "access_rights": {
         "ZU1NA011549662490": "F",
         "d-8d324d84-668e-4e64-bce1-936940e3023f": "R"
      },
      "virtual_system": {
         "id": "584",
         "platform": "PureSystems_ESX",
         "environment_profile_name": "AXA Profile for ICO",
         "deploymentPriorityId": "1",
         "environmentProfile": "/resources/environmentProfiles/1",
         "locations": [
            "PASPRELOADED"
         ],
         "fsm_ip": "10.68.114.1",
         "priority": "high",
         "environment_profile": "/resources/environmentProfiles/1",
         "is_local": true
      },
      "deployment_name": "ICO-PREP-F-DZQA8005",
      "appmodel": "https://127.0.0.1:9444/storehouse/user/deployments/d-8d324d84-668e-4e64-bce1-936940e3023f/appmodel.json"
   }
]

```


## References

### https://catonmat.net/cookbooks/curl

#### https://stackoverflow.com/questions/3044315/how-to-set-the-authorization-header-using-curl

#### https://stackoverflow.com/questions/50231945/how-to-get-session-id-from-curl-and-reuse-it-again-for-next-get-request/50232793
#### https://curl.haxx.se/docs/http-cookies.html










## Other curl examples

```
$ curl 'https://api6.ipify.org?format=json' ; echo
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    22  100    22    0     0     10      0  0:00:02  0:00:02 --:--:--    10{"ip":"141.191.36.11"}



$ curl http://ifconfig.me
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    13  100    13    0     0     14      0 --:--:-- --:--:-- --:--:--    14141.191.36.11
```


https://github.com/Risk3sixty-Labs/geoapi
https://news.ycombinator.com/item?id=21741991

```
$ curl https://geo.risk3sixty.com/me
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   173  100   173    0     0     26      0  0:00:06  0:00:06 --:--:--    48{"ip":"141.191.36.11","range":[2378114056,2378114063],"country":"US","region":"","eu":"0","timezone":"America/Chicago","city":"","ll":[37.751,-97.822],"metro":0,"area":1000}
```

```
$ curl https://geo.risk3sixty.com/me | ~/Downloads/gron.exe
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   173  100   173    0     0    212      0 --:--:-- --:--:-- --:--:--   212json = {};
json.area = 1000;
json.city = "";
json.country = "US";
json.eu = "0";
json.ip = "141.191.36.11";
json.ll = [];
json.ll[0] = 37.751;
json.ll[1] = -97.822;
json.metro = 0;
json.range = [];
json.range[0] = 2378114056;
json.range[1] = 2378114063;
json.region = "";
json.timezone = "America/Chicago";

```
```
$ curl https://geo.risk3sixty.com/me | ~/Downloads/jq-win64.exe -C
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   173  100   173    0     0    692      0 --:--:-- --:--:-- --:--:--   700
{
  "ip": "141.191.36.11",
  "range": [
    2378114056,
    2378114063
  ],
  "country": "US",
  "region": "",
  "eu": "0",
  "timezone": "America/Chicago",
  "city": "",
  "ll": [
    37.751,
    -97.822
  ],
  "metro": 0,
  "area": 1000
}

```


### Example from curl-to-go
```
$ curl canhazip.com
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    14  100    14    0     0     17      0 --:--:-- --:--:-- --:--:--    17141.191.20.11
```




# InBox


### 12/2/2019

What is OAuth really all about?   (Authorization not Authentication)
https://www.youtube.com/watch?v=t4-416mg6iU

What is JWT authorization relaly about
https://www.youtube.com/watch?v=soGRyl9ztjI

How OAuth flows work
https://www.youtube.com/watch?v=3pZ3Nh8tgTE



### 1/9/2020
#json
https://github.com/cube2222/jql
https://news.ycombinator.com/item?id=21981158

#hugo
https://flaviocopes.com/start-blog-with-hugo/
https://news.ycombinator.com/item?id=21978306




### 1/13/2020
## NOTE: the following book seems interesting - but the freecodecamp video seems much more useful.  I don't think that buying the book will help me as I don't actually need to build an API.  The free sample (1st chapter) is a pretty good summary of some of the principles and examples from the freecodecamp video.
#api
https://news.ycombinator.com/item?id=22026089

Good call and good approach. I'd recommend taking it a further step.
My co-author and I figured out that a positive feedback loop is key. In addition to our API Design Book at http://theapidesignbook.com/ my co-author and I have a weekly newsletter at http://bit.ly/apiWeekly then he followed the consulting route using those for lead generation while I did some classes for Lynda/LinkedIn Learning: https://www.linkedin.com/learning/instructors/keith-casey and in both cases, we refer back to the book and the newsletter. Then in my day job at Okta, I talk about the same topics and occasionally refer to our stuff among other things as "for more information."
Most of it is free, a few things are paid, but everything builds and complements each other.
No matter where anyone finds us, the threads lead to the other things. This allows people to understand the basics of APIs, learn some advanced topics, and generally stay up to date on the space as a whole.
https://leanpub.com/restful-api-design/read_sample


## NOTE: the following book is free but not sure if I can really get much out of it - unless I decide to try to code a game.
For what it's worth, I offer my entire book online for free, and I am very happy with both the print and eBook sales.
https://gameprogrammingpatterns.com/

1/21/2020
#api - not a particularly good API - not RESTful - interesting to explore
https://github.com/HackerNews/API
https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty
https://hacker-news.firebaseio.com/v0/item/22106536.json?print=pretty





### 1/13/2020
#web google cloud
https://news.ycombinator.com/item?id=22027459
https://alexolivier.me/posts/deploy-container-stateless-cheap-google-cloud-run-serverless



#nomad #kubernetes
https://endler.dev/2019/maybe-you-dont-need-kubernetes/
https://news.ycombinator.com/item?id=22034291





#sqlite and #go
Pretty good talk - the first 12 minutes is a great sqlite walk through - demonstration of full text search capabilities for sqlite
## NOTE: this seems to be more about SQLite than Go actually 
https://www.youtube.com/watch?v=RqubKSF3wig
```
sqlite3
.mode csv
.import data.csv tableName
.schema
select * from tableName limit 10;
select count(*) from tableName;

create virtual table playsearch using fts5(playsrowid, text);
insert into playsearch select rowid, text from plays;
select * from playsearch where text match "whether tis nobler";
```

https://www.sqlite.org/download.html


https://www.sqlitetutorial.net/download-install-sqlite/

```
sqlite> .help
.archive ...           Manage SQL archives: ".archive --help" for details
.auth ON|OFF           Show authorizer callbacks
.backup ?DB? FILE      Backup DB (default "main") to FILE
.bail on|off           Stop after hitting an error.  Default OFF
.binary on|off         Turn binary output on or off.  Default OFF
.cd DIRECTORY          Change the working directory to DIRECTORY
...
```

```
sqlite> .quit
 
c:\sqlite>
```

http://johnatten.com/2014/12/07/installing-and-using-sqlite-on-windows/




### 1/21/2020
#vscode #emmet #web #css
https://css-tricks.com/can-vs-code-emmet/
https://docs.emmet.io/cheat-sheet/

1/21/2020
#css  really cool css cascading tutorial (inline quiz and cool design)
https://wattenberger.com/blog/css-cascade
https://news.ycombinator.com/item?id=22107270



### 1/24/2020
#gcs  google cloud secret manager announced
https://cloud.google.com/blog/products/identity-security/introducing-google-clouds-secret-manager
https://news.ycombinator.com/item?id=22136699

#web This is why I use ad blockers and a pi-hole server
https://news.ycombinator.com/item?id=22124929



### 1/29/2020
#raspberrypi
https://snikt.net/blog/2020/01/29/building-a-simple-vpn-with-wireguard-with-a-raspberry-pi-as-server/
https://news.ycombinator.com/item?id=22183506

#javascript  good overview of fundamentals 
From OReilly email 1/15/2020
https://overreacted.io/what-is-javascript-made-of/


#redis 
From email 1/9/2020
https://redislabs.com/redis-enterprise/use-cases/
Free courses? 
https://university.redislabs.com



### 1/30/2020
#1password #otp #security 
https://youtu.be/8kafXiaCdRw?t=1716

NOTE: this demo setting a OTP field in 1Password for a Vault URL - I just tried it out for GANDI-2166 and it works too.
No more dependencies on Google Authenticator!  (although I don't need yet another dependency on 1Password)
NOTE: a bit later there's also a demo for 1Password capability to scan a QR code 

NOTE: KeePass (Windows app) plugin for TOTP 
https://keepass.info/plugins.html#keeotp
https://bitbucket.org/devinmartin/keeotp/wiki/Home
### it works!   just had to download and save to the plugins folder (tools -> plugins / click open folder button)

Related: Protect KeePass with a YubiKey https://keepass.info/help/kb/yubikey.html


NOTE: KeePassXC - multi-platform - v2.2 released in 2017 supports Yubikey and TOTP
https://keepassxc.org/download
https://news.ycombinator.com/item?id=14633576
NOTE: also recommended by the MIT course https://missing.csail.mit.edu/2020/security/



NOTE: MacPass - a KeePass compatiable port for Mac OS X - doesn't support TOTP
https://macpassapp.org 
Plugins for MacPass - as of 1/2020 kinda pitiful list of two plugins?
https://macpassapp.org/plugins




### 2/3/2020
most common openssl commands 
https://www.sslshopper.com/article-most-common-openssl-commands.html

General OpenSSL Commands
These commands allow you to generate CSRs, Certificates, Private Keys and do other miscellaneous tasks.

Generate a new private key and Certificate Signing Request
```
openssl req -out CSR.csr -new -newkey rsa:2048 -nodes -keyout privateKey.key
```

Generate a self-signed certificate (see How to Create and Install an Apache Self Signed Certificate for more info)
```
openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -keyout privateKey.key -out certificate.crt
```

Generate a certificate signing request (CSR) for an existing private key
```
openssl req -out CSR.csr -key privateKey.key -new
```
Generate a certificate signing request based on an existing certificate
```
openssl x509 -x509toreq -in certificate.crt -out CSR.csr -signkey privateKey.key
```

Remove a passphrase from a private key
```
openssl rsa -in privateKey.pem -out newPrivateKey.pem
```

Checking Using OpenSSL
If you need to check the information within a Certificate, CSR or Private Key, use these commands. You can also check CSRs and check certificates using our online tools.

Check a Certificate Signing Request (CSR)
```
openssl req -text -noout -verify -in CSR.csr
```

Check a private key
```
openssl rsa -in privateKey.key -check
```

Check a certificate
```
openssl x509 -in certificate.crt -text -noout
```

Check a PKCS#12 file (.pfx or .p12)
```
openssl pkcs12 -info -in keyStore.p12
```

Debugging Using OpenSSL
If you are receiving an error that the private doesn't match the certificate or that a certificate that you installed to a site is not trusted, try one of these commands. If you are trying to verify that an SSL certificate is installed correctly, be sure to check out the SSL Checker.

Check an MD5 hash of the public key to ensure that it matches with what is in a CSR or private key
```
openssl x509 -noout -modulus -in certificate.crt | openssl md5
openssl rsa -noout -modulus -in privateKey.key | openssl md5
openssl req -noout -modulus -in CSR.csr | openssl md5
```

Check an SSL connection. All the certificates (including Intermediates) should be displayed
```
openssl s_client -connect www.paypal.com:443
```

Converting Using OpenSSL
These commands allow you to convert certificates and keys to different formats to make them compatible with specific types of servers or software. For example, you can convert a normal PEM file that would work with Apache to a PFX (PKCS#12) file and use it with Tomcat or IIS. Use our SSL Converter to convert certificates without messing with OpenSSL.

Convert a DER file (.crt .cer .der) to PEM
```
openssl x509 -inform der -in certificate.cer -out certificate.pem
```

Convert a PEM file to DER
```
openssl x509 -outform der -in certificate.pem -out certificate.der
```

Convert a PKCS#12 file (.pfx .p12) containing a private key and certificates to PEM
```
openssl pkcs12 -in keyStore.pfx -out keyStore.pem -nodes
```
You can add -nocerts to only output the private key or add -nokeys to only output the certificates.

Convert a PEM certificate file and a private key to PKCS#12 (.pfx .p12)
```
openssl pkcs12 -export -out certificate.pfx -inkey privateKey.key -in certificate.crt -certfile CACert.crt
```





### 2/4/2020
#STEM #javascript  mini plain vanilla javascript projects
https://github.com/bradtraversy/vanillawebprojects
https://news.ycombinator.com/item?id=22231963


#linux #someday kernel sourcecode 
https://elixir.bootlin.com/linux/latest/source
#linux #someday BPF book
http://www.brendangregg.com/bpf-performance-tools-book.html




#web  weather.com alternative  (due to malicious code on the website)
https://weather.gov
#web  #cool really cool curl and Web browser weather forecast
http://wttr.in/paris?lang=fr&m
https://github.com/chubin/wttr.in
```
curl http://wttr.in/paris?lang=fr\&m

curl wttr.in/07901

curl http://wttr.in/shanghai?m
```



### 1/30/2020
#awesome  Nice collection of useful links 
https://github.com/jturgasen/my-links

