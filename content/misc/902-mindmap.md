+++
weight = 902
toc = true
date = "2020-02-01T05:03:55Z"
title = "mind map"
+++


# mind map

2/1/2020


```
jliu@JEFFREYs-MacBook-Pro ~/git/mindmap $ ~/Downloads/tree-1.8.0/tree -C
.
├── Books
│   ├── Ebooks
│   │   ├── redis-microservices-for-dummies.pdf
│   │   ├── web_development_with_go.pdf
│   │   └── wizardzines.com
│   │       ├── dangit-git-zine.pdf
│   │       ├── http-zine.pdf
│   │       └── sql-zine.pdf
│   ├── Fiction
│   └── NonFiction
│       ├── AmericanGirl
│       │   ├── _a-smart-girls-guide-boys-surviving-crushes-staying-true-to-yourself-and-other-stuff
│       │   ├── a-smart-girls-guide-babysitting
│       │   ├── a-smart-girls-guide-cooking
│       │   ├── a-smart-girls-guide-digital-a-smart-girls-guide-boys-surviving-crushes-staying-true-to-yourself-and-other-stuff
│       │   ├── a-smart-girls-guide-drama-rumors-and-secrets-staying-true-to-yourself-in-changing-times
│       │   ├── a-smart-girls-guide-friendship-troubles-dealing-with-fights-being-left-out-and-the-whole-popularity-thing
│       │   ├── a-smart-girls-guide-getting-it-together-how-to-organize-your-space-your-stuff-your-time-and-your-life
│       │   ├── a-smart-girls-guide-knowing-what-to-say-finding-the-words-to-fit-any0-situation
│       │   ├── a-smart-girls-guide-middle-school
│       │   ├── a-smart-girls-guide-money-how-to-make-it-save-it-and-spend-it
│       │   ├── a-smart-girls-guide-travel
│       │   ├── a-smart-girls-guide-worry
│       │   ├── friends-making-them-and-keeping-them
│       │   └── stand-up-for-yourself-and-your-friends-dealing-with-bullies-and-bosiness-and-finding-a-better-way
│       ├── STEM
│       ├── awesome-science-experiments-for-kids
│       ├── coding-for-kids-python
│       ├── help-your-kids-with-computer-science
│       ├── help-your-kids-with-english
│       ├── help-your-kids-with-geography
│       ├── help-your-kids-with-language-arts
│       ├── help-your-kids-with-math
│       ├── help-your-kids-with-science
│       ├── help-your-kids-with-study-skills
│       ├── python-for-kids
│       └── science-in-seconds-for-kids
├── Kits
│   ├── BooleanBox
│   └── GoogleAIY
└── Tech
    ├── Database
    │   ├── MS-SQL-Server
    │   ├── MongoDB
    │   ├── MySQL
    │   ├── Postgres
    │   ├── Redis
    │   ├── SQL
    │   └── SQLite
    ├── Excel
    │   └── You_Suck_at_Excel_with_Joel_Spolsky.xlsx
    ├── Infrastructure
    │   ├── Bash
    │   ├── Cloud-Providers
    │   │   ├── AWS
    │   │   ├── Azure
    │   │   ├── Digital-Ocean
    │   │   └── GCP
    │   ├── Configuration-Management
    │   │   ├── Ansible
    │   │   ├── Puppet
    │   │   └── Salt-Stack
    │   ├── Containers
    │   │   ├── Docker
    │   │   ├── Kubernetes
    │   │   ├── Nomad
    │   │   └── OpenShift
    │   ├── HashiCorp
    │   │   ├── Consul
    │   │   ├── Terraform
    │   │   └── Vault
    │   ├── Linux
    │   │   └── linux-file-system-basics
    │   ├── Mac
    │   ├── RDP
    │   │   └── mRemoteNG
    │   ├── SSH
    │   ├── TCPIP
    │   │   └── DNS
    │   └── Windows
    ├── Programming
    │   ├── Git
    │   ├── Golang
    │   ├── Javascript
    │   ├── Nodejs
    │   ├── Python
    │   ├── Visual-studio-code
    │   └── iOS
    │       └── Swift
    │           └── SwiftUI
    ├── STEM
    │   ├── Bitsbox
    │   ├── CTY
    │   └── Roblox-Scala
    └── Web
        ├── API
        │   ├── free-code-camp-api-video
        │   └── postman
        ├── CSS
        ├── Cool-curl-CLI
        ├── Firebase
        │   └── techlead-maze-javascript-firebase-cloud-firestore
        ├── HTML
        ├── Oauth
        ├── PKI
        │   ├── CA
        │   └── HTTPS
        └── SAML

73 directories, 35 files

```
