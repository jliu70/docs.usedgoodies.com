+++
weight = 50
toc = true
date = "2019-12-21T15:03:55Z"
title = "JQ for JSON"
+++

### jq tutorial
https://stedolan.github.io/jq/tutorial/


https://shapeshed.com/jq-json/
https://news.ycombinator.com/item?id=21858952


http://www.compciv.org/recipes/cli/jq-for-parsing-json/

https://thoughtbot.com/blog/jq-is-sed-for-json

https://blog.appoptics.com/jq-json/


```
$ curl https://geo.risk3sixty.com/me | ~/Downloads/jq-win64.exe -C
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   203  100   203    0     0    283      0 --:--:-- --:--:-- --:--:--   282
{
  "ip": "141.191.20.10",
  "range": [
    2378108928,
    2378110975
  ],
  "country": "MX",
  "region": "MEX",
  "eu": "0",
  "timezone": "America/Mexico_City",
  "city": "Ciudad Nezahualcoyotl",
  "ll": [
    19.3876,
    -98.9926
  ],
  "metro": 0,
  "area": 1000
}

```


4/8/2018
#json #next    grep type cli tool for JSON - better than just using “jq”
https://github.com/tomnomnom/gron/
https://news.ycombinator.com/item?id=16727665


Gron example
```
$ curl -k -u jliu:e/4lmp\?1sEr5l2p -H "Accept: application/json" -H "X-IBM-PureSystem-API-Version: 2.0" https://10.68.114.1/resources/virtualApplications | ~/Downloads/gron.exe
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  3812  100  3812    0     0   1832      0  0:00:02  0:00:02 --:--:--  1832json = [];
json[0] = {};
json[0].access_rights = {};
json[0].access_rights.ZU1NA011549662490 = "F";
json[0].access_rights["d-8d324d84-668e-4e64-bce1-936940e3023f"] = "R";
json[0].app_id = "a-2310b6e7-3db9-4b57-9ee7-0ec880b714d3";
json[0].app_type = "application";
json[0].appmodel = "https://127.0.0.1:9444/storehouse/user/deployments/d-8d324d84-668e-4e64-bce1-936940e3023f/appmodel.json";
json[0].create_time = "2019-12-02T21:35:09Z";
json[0].creator = "ZU1NA011549662490";
json[0].creator_name = "ZU1NA011549662490";
json[0].deployment = "https://127.0.0.1:9444/storehouse/user/deployments/d-8d324d84-668e-4e64-bce1-936940e3023f/deployment.json";
json[0].deployment_name = "ICO-PREP-F-DZQA8005";
json[0].desired = "RUNNING";
json[0].environment_profile_uuid = "ep-d9f87dfb-7439-48d4-9dfc-f3cbe313454c";
json[0].expirationTime = -1;
json[0].health = "NORMAL";
json[0].id = "d-8d324d84-668e-4e64-bce1-936940e3023f";
json[0].instances = [];
json[0].instances[0] = {};
json[0].instances[0].activated = true;
json[0].instances[0].activation_status = "RUNNING";
json[0].instances[0].cpucount = 1;
json[0].instances[0].displayname = "LINUX_RH7.11575322557897";
json[0].instances[0].health = "NORMAL";
json[0].instances[0].hypervisorHostname = "10.68.114.1";
json[0].instances[0].id = "LINUX_RH7.11575322557897";
json[0].instances[0].last_update = "2019-12-08T11:47:56.863Z";
json[0].instances[0].location = {};
json[0].instances[0].location.cloud_group = {};
json[0].instances[0].location.cloud_group.id = "1";
json[0].instances[0].location.cloud_group.name = "BNR02";
json[0].instances[0].location.name = "PASPRELOADED";
json[0].instances[0].location.placement = {};
json[0].instances[0].location.placement.cloud_group = {};
json[0].instances[0].location.placement.cloud_group.name = "BNR02";

json[0].instances[0].location.placement.ip_groups = [];
json[0].instances[0].location.placement.ip_groups[0] = {};
json[0].instances[0].location.placement.ip_groups[0].address = "10.68.64.72";
json[0].instances[0].location.placement.ip_groups[0].hostname = "DZQA8005.wwmgmt.intraxa";
json[0].instances[0].location.placement.ip_groups[0].name = "BNR02_ATS_MGMT_ZU1_IPG";
json[0].instances[0].location.placement.ip_groups[0].nic = "management";
json[0].instances[0].location.placement.ip_groups[0].purpose = "data";
json[0].instances[0].location.placement.ip_groups[1] = {};
json[0].instances[0].location.placement.ip_groups[1].address = "10.79.132.13";
json[0].instances[0].location.placement.ip_groups[1].hostname = "DZQA8005.ppprivmgmt.intraxa";
json[0].instances[0].location.placement.ip_groups[1].name = "BNR02_ZU1_DEVS_INT_IPG";
json[0].instances[0].location.placement.ip_groups[1].nic = "data_2";
json[0].instances[0].location.placement.ip_groups[1].purpose = "data";
json[0].instances[0].location.placement.location = "PASPRELOADED";
json[0].instances[0].master = true;
json[0].instances[0].memory = 4096;
json[0].instances[0].name = "LINUX_RH7.11575322557897";
json[0].instances[0].previousStatus = "RUNNING";
json[0].instances[0].previous_status = "RUNNING";
json[0].instances[0].private_ip = "10.68.64.72";
json[0].instances[0].public_hostname = "DZQA8005.wwmgmt.intraxa";
json[0].instances[0].public_ip = "10.68.64.72";
json[0].instances[0].runtimeid = "http://10.68.114.1:5006/deployment/resources/instances/55a1050b-a7a1-4527-b332-90c784cf446d";
json[0].instances[0].start_time = "2019-12-02T21:36:55.902Z";
json[0].instances[0].status = "RUNNING";
json[0].instances[0].uuid = "55a1050b-a7a1-4527-b332-90c784cf446d";
json[0].instances[0].vmId = 584;
json[0].instances[0]["activation-status"] = "RUNNING";
json[0].instances[0]["reboot.count"] = 0;
json[0].instances[0]["stopped.by"] = "";
json[0].is_publicep = true;
json[0].patterntype = "vsys";
json[0].role_error = false;
json[0].start_time = "2019-12-02T21:35:57.897Z";
json[0].status = "RUNNING";
json[0].topology = "https://127.0.0.1:9444/storehouse/user/deployments/d-8d324d84-668e-4e64-bce1-936940e3023f/topology.json";
json[0].version = "1.0";
json[0].virtual_system = {};
json[0].virtual_system.deploymentPriorityId = "1";
json[0].virtual_system.environmentProfile = "/resources/environmentProfiles/1";
json[0].virtual_system.environment_profile = "/resources/environmentProfiles/1";
json[0].virtual_system.environment_profile_name = "AXA Profile for ICO";
json[0].virtual_system.fsm_ip = "10.68.114.1";
json[0].virtual_system.id = "584";
json[0].virtual_system.is_local = true;
json[0].virtual_system.locations = [];
json[0].virtual_system.locations[0] = "PASPRELOADED";
json[0].virtual_system.platform = "PureSystems_ESX";
json[0].virtual_system.priority = "high";
```

