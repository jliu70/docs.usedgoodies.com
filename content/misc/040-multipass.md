+++
weight = 40
toc = true
date = "2019-11-18T05:03:55Z"
title = "Multipass"
+++


https://multipass.run/
https://news.ycombinator.com/item?id=21836528


https://github.com/CanonicalLtd/multipass


## What is Multipass?
Multipass is a lightweight VM manager for Linux, Windows and macOS. It's designed for developers who want a fresh Ubuntu environment with a single command. It uses KVM on Linux, Hyper-V on Windows and HyperKit on macOS to run the VM with minimal overhead. It can also use VirtualBox on Windows and macOS. Multipass will fetch images for you and keep them up to date.

Since it supports metadata for cloud-init, you can simulate a small cloud deployment on your laptop or workstation.

https://discourse.ubuntu.com/c/multipass/doc


### An alternative to Docker for Mac or Docker for Windows?

### As of 2019 I'd just consider running a k3s kubernetes cluster on Digital Ocean or some managed kubernetes service instead of Multipass or Docker for Windows/Mac