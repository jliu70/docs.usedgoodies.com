+++
weight = 902
toc = true
date = "2020-02-01T05:03:55Z"
title = "docs usedgoodies content layout"
+++


# docs.usedgoodies.com

```
jliu@JEFFREYs-MacBook-Pro ~/git/docs.usedgoodies.com/content (master) $ ~/Downloads/tree-1.8.0/tree -C
.
├── _index.en.md
├── about
│   ├── 001-why-this-site.md
│   ├── 100-initial-installation.md
│   ├── 110-configuration-details.md
│   ├── 120-git-clone-site.md
│   ├── 200-usage.md
│   ├── 210-markdown-examples.md
│   ├── 900-credits.md
│   └── _index.en.md
├── amazon-web-services
│   ├── 001-aws-introduction.md
│   ├── 010-aws-setup.md
│   ├── 011-aws-iam-rotate-access-keys.md
│   ├── 200-invalidate-cloudfront-cache.md
│   ├── _index.en.md
│   └── amazon-s3
│       ├── 110-amazon-s3-ia.md
│       ├── 120-amazon-s3-glacier.md
│       ├── 130-amazon-s3-static-site.md
│       └── index.md
├── ansible
│   ├── 001-introduction.md
│   ├── 900-ansible-resources.md
│   └── _index.en.md
├── consul
│   ├── 001-introduction.md
│   ├── 100-consul-POC-notes.md
│   ├── 800-consul-connect-demo.md
│   ├── 801-consul-basics-instruqt-interactive-lab.md
│   ├── 802-consul-connect-instruqt-interactive-lab.md
│   ├── 803-consul-service-discovery-instruqt-interactive-lab.md
│   ├── 804-consul-service-mesh-instruqt-interactive-lab.md
│   ├── 805-consul-service-mesh-consul-hybrid-instruqt-interactive-lab.md
│   ├── 806-consul-layer7-observability-instruqt-interactive-lab.md
│   ├── 807-consul-service-mesh-observability-instruqt-interactive-lab.md
│   ├── 900-consul-resources.md
│   └── _index.en.md
├── digitalOcean
│   ├── 001-first.md
│   ├── 100-second.md
│   └── _index.en.md
├── docker
│   ├── 001-docker-introduction.md
│   ├── 010-docker.md
│   ├── 020-docker.md
│   ├── 030-docker.md
│   ├── 200-frequently-used-commands.md
│   ├── 210-docker-clean-up-commands.md
│   ├── 220-docker-network-cleanup.md
│   ├── 230-docker-security-scanning.md
│   ├── 231-docker-security.md
│   ├── 400-hugo-in-docker-container.md
│   ├── 410-lego-in-docker-container.md
│   ├── 500-linux-gui-on-osx-using-docker.md
│   ├── 600-docker-live-debugging.md
│   ├── 700-docker-tutorials.md
│   ├── 800-docker-ctop.md
│   ├── 900-docker-resources.md
│   ├── _index.en.md
│   ├── docker-compose
│   │   ├── 300-docker-compose-version.md
│   │   ├── 301-docker-compose-links.md
│   │   ├── 310-docker-compose-frequently-used-commands.md
│   │   ├── 320-docker-compose-network.md
│   │   └── index.md
│   └── docker-installation
│       ├── 101-docker-ubuntu.md
│       ├── 110-docker-for-mac.md
│       ├── 120-docker-for-windows.md
│       ├── 130-docker-for-windows-7.md
│       └── index.md
├── docker-swarm
│   └── _index.en.md
├── git
│   ├── 001-git-introduction.md
│   ├── 010-git-installation.md
│   ├── 020-git-basic-config.md
│   ├── 021-git-basic-usage.md
│   ├── 022-git-additional-config.md
│   ├── 030-git-remote-repos.md
│   ├── 040-git-branches.md
│   ├── 050-git-commit-convention.md
│   ├── 060-git-code-review-flow.md
│   ├── 100-git-submodule.md
│   ├── 200-git-cli-cheat-sheet.md
│   ├── 300-github.md
│   ├── 400-gitlab.md
│   ├── 900-git-resources.md
│   └── _index.en.md
├── go
│   ├── 001-introduction.md
│   ├── 010-installation-mac.md
│   ├── 011-installation-windows.md
│   ├── 020-go-extensions-visual-studio-code-mac.md
│   ├── 021-go-extensions-visual-studio-code-windows.md
│   ├── 100-go-basics.md
│   ├── 900-go-resources.md
│   └── _index.en.md
├── kubernetes
│   ├── 001-first.md
│   ├── 100-second.md
│   ├── 101-third.md
│   ├── 900-kubernetes-resources.md
│   └── _index.en.md
├── linux
│   ├── 900-linux-resources.md
│   └── _index.en.md
├── mac
│   ├── 001-introduction.md
│   ├── 010-MacOS-security-and-privacy-guide.md
│   ├── 020-MacOS-custom-lock-screen-message.md
│   ├── 100-cli-tools.md
│   ├── 110-homebrew.md
│   ├── 120-smart-dashes.md
│   ├── 300-apple-notes.md
│   ├── 900-misc-tips.md
│   └── _index.en.md
├── misc
│   ├── 001-awesome-lists.md
│   ├── 010-atom-editor.md
│   ├── 011-atom-editor-cheat-sheet.md
│   ├── 020-visual-studio-code.md
│   ├── 021-visual-studio-code-tips.md
│   ├── 022-visual-studio-code-extensions.md
│   ├── 023-visual-studio-code-resources.md
│   ├── 030-unixlinux-command-cheat-sheet.md
│   ├── 040-multipass.md
│   ├── 050-jq-json.md
│   ├── 900-misc-resources.md
│   ├── 901-gtd.md
│   └── _index.en.md
├── node.js
│   ├── 001-nodejs-introduction.md
│   └── _index.en.md
├── nomad
│   ├── 001-introduction.md
│   ├── 900-nomad-resources.md
│   └── _index.en.md
├── prometheus
│   ├── 001-introduction.md
│   ├── 900-prometheus-resources.md
│   └── _index.en.md
├── puppet
│   ├── 001-introduction.md
│   ├── 010-first.md
│   └── _index.en.md
├── python
│   ├── 001-introduction.md
│   ├── 900-python-resources.md
│   └── _index.en.md
├── salt-stack
│   ├── 001-introduction.md
│   ├── 010-salt-stack-installation.md
│   ├── 011-salt-stack-master-setup.md
│   ├── 200-salt-cheat-sheet.md
│   └── _index.en.md
├── terraform
│   ├── 001-introduction.md
│   ├── 010-first.md
│   └── _index.en.md
├── vault
│   ├── 001-introduction.md
│   ├── 10-vault-getting-started-authentication.md
│   ├── 100-vault-POC-notes.md
│   ├── 800-vault-basics-instruqt-interactive-lab.md
│   ├── 900-vault-resources.md
│   └── _index.en.md
├── web
│   ├── 001-web-introduction.md
│   ├── 100-lets-encrypt.md
│   ├── 900-web-resources.md
│   ├── _index.en.md
│   └── static-web-sites
│       ├── 010-github-pages.md
│       ├── 020-gitlab-pages.md
│       └── _index_en.md
└── windows
    ├── 001-introduction.md
    ├── 800-windows-diff.md
    ├── 801-windows-dig.md
    ├── 802-windows10-multiple-desktops.md
    ├── 803-windows-open-explorer-from-cli.md
    ├── 900-windows-resources.md
    └── _index.en.md

27 directories, 158 files
```
