+++
weight = 900
toc = true
date = "2019-11-19T05:03:55Z"
title = "Misc Resources"
+++

# InBox

### 11/19/2019
https://news.ycombinator.com/item?id=21576936
https://www.c0ffee.net/blog/mail-server-guide/



### 1/8/2020
#vim interactive vim tutorial
https://www.openvim.com/
https://news.ycombinator.com/item?id=21990796


#vim how to exit vim 
https://github.com/hakluke/how-to-exit-vim/blob/master/README.md
https://news.ycombinator.com/item?id=21988968


#stem cornell note taking system
http://lsc.cornell.edu/study-skills/cornell-note-taking-system/
https://news.ycombinator.com/item?id=21989063



### 1/9/2020
#job - joining big tech in one's 40s
https://news.ycombinator.com/item?id=21981923


#personalFinance - how do you manage your personal budget?
https://news.ycombinator.com/item?id=22004515


#bash
https://www.linuxjournal.com/content/understanding-bash-elements-programming
https://news.ycombinator.com/item?id=22052890


### 1/21/2020
#knowledge 
https://www.nateliason.com/blog/roam
https://news.ycombinator.com/item?id=22104366

Some of the main features of Roam:  #tags and links
Dates are pages

Example of using VS Code - links 
[VS Code tips](./021-visual-studio-code-tips.md)

[docker compose](../docker/docker-compose/index.md)

#knowledge - Building a second brain
https://praxis.fortelabs.co/basboverview/


#knowledge - emacs org-mode
https://en.wikipedia.org/wiki/Org-mode
#knowledge - comparison of note taking software
https://en.wikipedia.org/wiki/Comparison_of_note-taking_software



#knowledge - procrastination
https://www.deprocrastination.co/blog/3-tricks-to-start-working-despite-not-feeling-like-it
https://news.ycombinator.com/item?id=22105229


#knowledge  1000 free audiobooks
http://www.openculture.com/freeaudiobooks
https://news.ycombinator.com/item?id=22104143


#education my story as a self-taught AI researcher
https://news.ycombinator.com/item?id=22101066
https://blog.floydhub.com/emils-story-as-a-self-taught-ai-researcher/

#education 
https://danluu.com/algorithms-interviews/
https://news.ycombinator.com/item?id=21961174


#freelance
https://tik.dev/dilemma
https://news.ycombinator.com/item?id=22048655


#python
https://github.com/alan-turing-institute/CleverCSV
https://news.ycombinator.com/item?id=22038317


#web #css darken mode make easy
https://github.com/ColinEspinas/darken
https://news.ycombinator.com/item?id=22070029



#go  distributed sql database in Go
https://github.com/tomarrell/lbadd
https://news.ycombinator.com/item?id=21893009


#education
https://slate.com/human-interest/2019/12/college-dreams-say-no-avoid-student-debt.html
https://news.ycombinator.com/item?id=21891974


#linux #network
https://www.redhat.com/sysadmin/networking-guides
https://news.ycombinator.com/item?id=21891816


#web cutting google out of your life
https://github.com/tycrek/degoogle


#stories
https://themoth.org


#stem #python
https://booleangirl.org/course/lights-camera-sensors-with-python/
https://booleangirltech.com/product/light-camera-sensors/


### 1/23/2020
#linux tree command
https://www.cyberciti.biz/faq/linux-show-directory-structure-command-line/
RELATED:  also see [Ascii Tree Generator Visual Studio Code Extension](./022-visual-studio-code-extensions.md)

RELATED: https://en.wikipedia.org/wiki/Box-drawing_character
```
└
┘
┤
├
┐	
┌
─
│
```

RELATED: 
#mac  Mac OS X version of the tree command
https://superuser.com/questions/359723/mac-os-x-equivalent-of-the-ubuntu-tree-command
http://mama.indstate.edu/users/ice/tree/
http://andre-als.blogspot.com/2012/02/how-to-install-command-tree-for-mac.html
#### Instructions on how to compile Mac OS X version of tree command
```
tree -C
```

#mac alternatives to tree command 
http://osxdaily.com/2016/09/09/view-folder-tree-terminal-mac-os-tree-equivalent/
```
find . -print | sed -e 's;[^/]*/;|____;g;s;____|; |;g'
```
http://www.xappsoftware.com/wordpress/2015/02/16/three-ways-to-use-the-tree-command-on-mac-os-x/
```
find . -print 2>/dev/null | awk '!/\.$/ { \
for (i=1; i<NF; i++) { \
printf("%4s", "|") \
} \
print "-- "$NF \
}' FS='/'
```



#job #education 
https://www.vice.com/en_us/article/wxenxx/mozilla-wants-young-people-to-consider-ethical-issues-before-taking-jobs-in-tech
https://news.ycombinator.com/item?id=22119723


#go go tooling
https://nullprogram.com/blog/2020/01/21/
https://news.ycombinator.com/item?id=22113827

#kubernetes
https://github.com/kubernetes/kops
https://news.ycombinator.com/item?id=22113938


#web - how I write back ends
https://github.com/fpereiro/backendlore
https://news.ycombinator.com/item?id=22106482


#STEM
https://sar3.ch/en/blog/kinder-programmieren-erfahrungen
https://news.ycombinator.com/item?id=22105447


#linux - really cool community tldr man pages
https://tldr.sh
https://news.ycombinator.com/item?id=22117237
  curl cht.sh/tr
   curl "http://cheat.sh/$1"

#fun
https://dustycloud.org/blog/terminal-phase-1.0/
https://news.ycombinator.com/item?id=22095092


#python #go
https://blog.heroku.com/see_python_see_python_go_go_python_go
https://news.ycombinator.com/item?id=22131325



### 1/26/2020
#sqlite  
https://www.sqlite.org/serverless.html
https://news.ycombinator.com/item?id=22151153

#sql joins - left, right and inner 
https://www.youtube.com/watch?v=sRwn1A8T6T4

#go #web
https://ieftimov.com/post/make-resilient-golang-net-http-servers-using-timeouts-deadlines-context-cancellation/
https://news.ycombinator.com/item?id=22150395

#ios
https://news.ycombinator.com/item?id=22152408




### 1/31/2020
https://mtlynch.io/solo-developer-year-2/
https://news.ycombinator.com/item?id=22201337


https://ihateregex.io/
https://news.ycombinator.com/item?id=22200584


#education  #chemistry
http://online.codedrome.com/periodictable/periodictable.htm
https://news.ycombinator.com/item?id=22191985
https://tips.darekkay.com/html/chemical-elements-en.html


Save this reference to 'doctoc' if not already 'filed'
https://github.com/thlorenz/doctoc



### 2/3/2020
#programming
https://blog.wesleyac.com/posts/engineering-beliefs
https://news.ycombinator.com/item?id=22222137


#linux
https://blog.alexellis.io/building-a-linux-desktop-for-cloud-native-development/
https://news.ycombinator.com/item?id=22222157


#ansible
https://www.digitalocean.com/community/tutorials/how-to-use-ansible-to-install-and-set-up-wordpress-with-lamp-on-ubuntu-18-04
https://www.digitalocean.com/community/cheatsheets/how-to-use-ansible-cheat-sheet-guide


#education
https://missing.csail.mit.edu
https://news.ycombinator.com/item?id=22226380




#swift
https://swift.org/blog/crypto/
https://news.ycombinator.com/item?id=22226532


#certificate   MS teams outage due to expired authentication cert
https://news.ycombinator.com/item?id=22226532




### 2/4/2020
Udemy classes  These seem pretty good. More advanced. But I just don’t have the time now. File under someday 
#mysql 
https://www.udemy.com/course/the-ultimate-mysql-bootcamp-go-from-sql-beginner-to-expert/
#go
https://www.udemy.com/course/golang-how-to-design-and-build-rest-microservices-in-go/
#python  
https://www.udemy.com/course/complete-python-developer-zero-to-mastery/
 



#ios #swift #swiftui
https://www.hackingwithswift.com/articles/212/whats-new-in-swift-5-2
https://news.ycombinator.com/item?id=22257889

#ios #swift #swiftui 
Free swiftUI video collection 
https://www.youtube.com/watch?v=uUMWEu2YJew&list=PLuoeXyslFTuZRi4q4VT6lZKxYbr7so1Mr
