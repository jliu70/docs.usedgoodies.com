+++
date = "2019-12-09T20:29:20Z"
title = "Python Resources"
weight = 900
+++


## Data Science 

Jupyter Notebooks in Visual Studio Code   12/8/2019
https://www.youtube.com/watch?v=rcBpQ9fwNTY


### Data Visualization

1/22/2020
#python #pandas
https://kanoki.org/2020/01/21/pandas-dataframe-filter-with-multiple-conditions/
https://news.ycombinator.com/item?id=22106575


# InBox

1/29/2020
#python #pandas
https://pandas.pydata.org/pandas-docs/stable/getting_started/index.html
https://news.ycombinator.com/item?id=22187121



2/3/2020
#python #jupyter A collection of 300+ Jupyter Python notebook examples for using Google Earth Engine with interactive mapping
https://github.com/giswqs/earthengine-py-notebooks
