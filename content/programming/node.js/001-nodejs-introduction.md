+++
toc = true
date = "2018-10-28T12:47:50Z"
title = "Introduction to Node.js"
weight = 1
+++

#### An overview of the general concepts of [Node.js][link-1]

{{% notice note %}}
WIP
{{% /notice %}}


Node.js came into existence when the original developers of JavaScript extended it from something you could only run in the browser to something you could run on your machine as a standalone application.

Now you can do much more with JavaScript than just making websites interactive.

JavaScript now has the capability to do things that other scripting languages like Python can do.

Both your browser JavaScript and Node.js run on the V8 JavaScript runtime engine. This engine takes your JavaScript code and converts it into a faster machine code. Machine code is low-level code which the computer can run without needing to first interpret it.



References:
[What exactly is Node.js][link-2]



[link-1]:https://nodejs.org/
[link-2]:https://medium.freecodecamp.org/what-exactly-is-node-js-ae36e97449f5

