+++
date = "2017-03-20T20:29:20Z"
title = "node.js"
weight = -1000
chapter = true
+++

# Node.js

Here’s a formal definition as given on the official Node.js [website][link-1]:
```
Node.js® is a JavaScript runtime built on Chrome’s V8 JavaScript engine.
Node.js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient.
Node.js’ package ecosystem, npm, is the largest ecosystem of open source libraries in the world.
```



Doctoc utility
```
npm install -g doctoc
npm update -g doctoc
npm ls -g doctoc
/usr/local/lib
└── doctoc@1.3.0 
```


[link-1]:https://node.js.org
