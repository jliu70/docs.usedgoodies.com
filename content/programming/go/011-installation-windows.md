+++
date = "2018-01-10T20:29:20Z"
title = "Go Installation Windows"
weight = 11
+++



## Download software

https://golang.org/dl/


Download the Go installer for your platform.


Optionally you can verify the sha256 checkum on the downloaded file.
```
$ openssl sha256 go1.9.2.windows-amd64.msi
SHA256(go1.9.2.windows-amd64.msi)= daeb761aa6fdb22dc3954fd911963b347c44aa5c6ba974b9c01be7cbbd6922ba

```

```
$ openssl sha256 go1.9.3.windows-amd64.msi
SHA256(go1.9.3.windows-amd64.msi)= 3d178a5f9dd10f24906cbc9d85d20489125fdb098967c967137f42df92509273
```
 

Install the Go package.  (NOTE: if you are upgrading, click "yes" when it asks if you would like to uninstall the old version first.)



## Verify Go Install
```
go version
```


## Setup your workspace

You really should setup GOPATH to point to your workspace prior to running any “go get” or installing Go extensions.
Todd tries to “go get” into workspace before talking about setting up related env var GOPATH. 
The other video focused on the mac with no windows support, so there are some defaults already set for GOPATH and GOROOT..  

NOTE: for the Mac, I was able to install the Go Extension for Visual Studio Code and the additional Go tools with the default GOPATH and GOROOT.  The GOROOT and GOPATH was NOT discussed at all in the “Go the Complete Developer’s Guide” video course.


### Add environment variables

Go has a unique approach of managing code where you have a single workspace for all your Go projects. For more information see the documentation.  https://golang.org/doc/code.html#Workspaces

```

export GOPATH=~/go
export PATH=$PATH:$GOPATH/bin
```



#### Setting Go Environment Variables on Windows 
Start -> search -> environment     

Choose “Edit the system environment variables” ->  click on “Environment Variables” button.

Note: GOPATH should be a user environment variable.   And also set a user env var PATH too. 

For Windows, setting the following user env variables  GOPATH and also PATH=%PATH%;%GOPATH%\bin
 

Make sure that GOROOT is set to where Go is installed (Default:  C:\Go\)

 
Make sure that GOPATH is set to your desired go projects directory

 
Make sure that PATH has the following entries:
C:\Go\bin;%GOPATH%\bin;C:\Program Files\Git\cmd

 
Complete the installation of the Go tools as outlined in the video course.


NOTE: Be patient -- it took around 15 minutes or so to complete.    And for some reason I had to run install twice for one of the tools.



### Create your workspace
```
mkdir -p $GOPATH/src $GOPATH/bin $GOPATH/pkg
```

And one last step (although I don't believe that it had any bearing on the Go Extensions install since I already had this set before attempting to install Go extensions) is to set the Windows Integrated Shell for Visual Studio Code.  Open user settings  (Ctrl-,)
"terminal.integrated.shell.windows": "C:\\Program Files\\Git\\bin\\bash.exe"


### Other helpful references:
https://github.com/abourget/getting-started-with-golang/blob/master/Getting_Started_for_Windows.md
http://www.wadewegner.com/2014/12/easy-go-programming-setup-for-windows/
 
