+++
date = "2019-11-18T20:29:20Z"
title = "Go Resources"
weight = 900
+++


1/8/2020
### Interesting

#### NOTE: useful for APIs which don't give you a Go example but does give you a curl example
#### NOTE: curl-to-go doesn't support cookies - good thing my code has it.

https://mholt.github.io/curl-to-go/

### The basic curl
```
curl canhazip.com
```

```
// Generated by curl-to-Go: https://mholt.github.io/curl-to-go
resp, err := http.Get("canhazip.com")
if err != nil {
	// handle err
}
defer resp.Body.Close()
```

### Adding a header 

```
curl -H "Accept: application/json" https://icanhazdadjoke.com/j/R7UfaahVfFd
```

```
// Generated by curl-to-Go: https://mholt.github.io/curl-to-go

req, err := http.NewRequest("GET", "https://icanhazdadjoke.com/j/R7UfaahVfFd", nil)
if err != nil {
	// handle err
	fmt.Println(err)
	os.Exit(1)
}
req.Header.Set("Accept", "application/json")

resp, err := http.DefaultClient.Do(req)
if err != nil {
	// handle err
}
defer resp.Body.Close()
```

### Adding basic Auth

```
curl -k -u jliu:e/4lmp\?1sEr5l2p -H "Accept: application/json" -H "X-IBM-PureSystem-API-Version: 2.0" https://10.68.114.1/resources/virtualApplications
```

```

// Generated by curl-to-Go: https://mholt.github.io/curl-to-go

// TODO: This is insecure; use only in dev environments.
tr := &http.Transport{
	TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
}
client := &http.Client{Transport: tr}

req, err := http.NewRequest("GET", "https://10.68.114.1/resources/virtualApplications", nil)
if err != nil {
	// handle err
}
req.SetBasicAuth("jliu", "e/4lmp?1sEr5l2p")
req.Header.Set("Accept", "application/json")
req.Header.Set("X-Ibm-Puresystem-Api-Version", "2.0")

resp, err := client.Do(req)
if err != nil {
	// handle err
}
defer resp.Body.Close()
```



# Need to document  json-to-go
https://github.com/mholt/json-to-go.git
#### converts json to Go Structs

NOTE: if you're using a struct to Unmarshal JSON the fields all have to be exported (begin with a Capital letter) otherwise the JSON Unmarshaler can't find it.
https://youtu.be/3llI65DQB_w?t=374




## NOTE: still struggling to understand and use GO interfaces - but at this point I'm wondering if this really matters - place it on the back burner 

1/7/2020
#go
golang example io.writer
 
https://yourbasic.org/golang/io-writer-interface-explained/
 
https://yourbasic.org/golang/interfaces-explained/
 
 
https://medium.com/@as27/a-simple-beginners-tutorial-to-io-writer-in-golang-2a13bfefea02
https://stackoverflow.com/questions/43793229/io-writer-in-go-beginner-trying-to-understand-them
 
 
https://medium.com/learning-the-go-programming-language/streaming-io-in-go-d93507931185
 
https://gobyexample.com/writing-files
 
https://medium.com/go-walkthrough/go-walkthrough-io-package-8ac5e95a9fbd
 
https://dev.to/dayvonjersen/how-to-use-io-reader-and-io-writer-ao0
 
https://www.grant.pizza/blog/the-beauty-of-io-writer/





https://npf.io/2014/05/intro-to-go-interfaces/

https://golang.org/doc/effective_go.html#interfaces_and_types

https://www.calhoun.io/how-do-interfaces-work-in-go/


https://www.youtube.com/watch?v=ak97oH0D6fI




Can't open the following links at work:
https://www.reddit.com/r/golang/comments/2r146a/why_would_you_create_a_named_interface/

https://www.reddit.com/r/golang/comments/2iskep/the_empty_interface_for_new_gophers/

https://www.reddit.com/r/golang/comments/8nz2mc/finding_all_types_which_implement_an_interface_at/

https://www.reddit.com/r/golang/comments/b5ks37/how_to_use_interfaces_with_structs_but_access_the/

https://www.reddit.com/r/golang/comments/51l83n/how_can_i_know_which_interface_a_struct_is/
https://docs.google.com/document/d/1_Y9xCEMj5S-7rv2ooHpZNH15JgRT5iM742gJkw5LtmQ/edit#

https://www.reddit.com/r/golang/comments/56lj2v/yet_another_confused_gopher_about_interfaces/



https://play.golang.org/p/TLRuw6OnItI
 
 
https://yourbasic.org/golang/io-writer-interface-explained/
https://yourbasic.org/golang/interfaces-explained/
 
https://medium.com/@as27/a-simple-beginners-tutorial-to-io-writer-in-golang-2a13bfefea02
https://medium.com/p/2a13bfefea02/responses/show
https://stackoverflow.com/questions/43793229/io-writer-in-go-beginner-trying-to-understand-them
 
https://medium.com/learning-the-go-programming-language/streaming-io-in-go-d93507931185
 
https://gobyexample.com/writing-files
 
https://medium.com/go-walkthrough/go-walkthrough-io-package-8ac5e95a9fbd
 
https://dev.to/dayvonjersen/how-to-use-io-reader-and-io-writer-ao0
 
https://www.grant.pizza/blog/the-beauty-of-io-writer/
 
and
 
Twelve Go best practices
https://www.youtube.com/watch?v=8D3Vmm1BGoY



1/8/2020
#go interfaces  go code use interface to abstract fmt.Println write to file
https://www.digitalocean.com/community/tutorials/how-to-use-interfaces-in-go

https://notes.shichao.io/gopl/ch7/

https://deployeveryday.com/2019/10/08/golang-auth-mock.html

https://www.geeksforgeeks.org/interfaces-in-golang/

https://medium.com/capital-one-tech/doing-well-by-doing-bad-writing-bad-code-with-go-part-1-2dbb96ce079a

https://medium.com/@gianbiondi/interfaces-in-go-59c3dc9c2d98

https://blog.alexellis.io/golang-writing-unit-tests/


1/9/2020
#go
https://www.justindfuller.com/2020/01/go-things-i-love-channels-and-goroutines/
https://news.ycombinator.com/item?id=21968939
https://www.justindfuller.com/2019/12/go-things-i-love-methods-on-any-type/
https://www.justindfuller.com/2019/10/keep-a-git-repository-of-all-your-practice-code/


1/14/2020
#go - example of converting code to use interfaces
https://blog.golang.org/json-rpc-tale-of-interfaces
https://github.com/golang/go/commit/dcff89057bc0e0d7cb14cf414f2df6f5fb1a41ec



12/6/2019
https://github.com/photonlines/Go-Web-Server
https://news.ycombinator.com/item?id=21723151
 
 
https://www.gopass.pw/
https://news.ycombinator.com/item?id=21636016
 
 
https://research.swtch.com/vgo-principles
https://news.ycombinator.com/item?id=21695658
 
 
 
https://github.com/segmentio/encoding
https://news.ycombinator.com/item?id=21706323
 


12/7/2019
 
https://github.com/MartinHeinz/go-project-blueprint
https://news.ycombinator.com/item?id=21725869
NOTE: some interesting side discussion on Docker for Mac slowness
 
GopherCon 2018:  How do you structure your go apps
https://www.youtube.com/watch?v=oL6JBUk6tj0
 
 
12/9/2019

https://utcc.utoronto.ca/~cks/space/blog/programming/GoSchedulerAndSyscalls
https://news.ycombinator.com/item?id=21736342
 
 
 
https://news.ycombinator.com/item?id=21733230
https://www.gocode.io/operation-go
 
 

## Tutorials
### Digital Ocean Tutorial Series
https://www.digitalocean.com/community/tutorials
https://www.digitalocean.com/community/tutorial_series/how-to-code-in-go



https://www.digitalocean.com/community/tutorials/how-to-write-switch-statements-in-go

https://www.digitalocean.com/community/tutorials/how-to-use-the-flag-package-in-go
 
https://www.digitalocean.com/community/tutorials/how-to-build-and-install-go-programs
 
https://www.digitalocean.com/community/tutorials/how-to-use-interfaces-in-go
 
https://www.digitalocean.com/community/tutorials/defining-methods-in-go
 
https://www.digitalocean.com/community/tutorials/how-to-use-struct-tags-in-go
 


# InBox

### 11/19/2019
https://blog.gnoack.org/post/go-goroutines-and-apis/
https://news.ycombinator.com/item?id=21567929


https://benjamincongdon.me/blog/2019/11/11/The-Value-in-Gos-Simplicity/
https://news.ycombinator.com/item?id=21545425


#go #freecodecamp Learn Go Programming - Golang Tutorial for Beginners 6/20/2019
https://www.youtube.com/watch?v=YS4e4q9oBaU
