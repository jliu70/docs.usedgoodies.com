+++
date = "2018-01-10T20:29:20Z"
title = "Go Basics"
weight = 100
+++



## go commands
  - `go build` - when run inside a folder that has an executable, builds an executable and places it in your folder.  Will have different functionality when applied against packages
  - `go clean` -  removes the executable in the project folder
  - `go install` - for an executable, builds the executable, names the executable the folder holding the code,  and places it in workspace/bin $GOPATH/bin

  - `go get` - downloads packages along with their dependencies, then installs the named packages, like 'go install'
    
### Update a package 
  - Example: `go get -u github.com/clair-scanner`
  - `go help get`
	The -u flag instructs get to use the network to update the named packages
	and their dependencies. By default, get uses the network to check out
	missing packages but does not use it to look for updates to existing packages.

## Go built-in documentation
   - Example:  `godoc fmt`


## Format 

Go has a built-in tool that automatically formats Go source code.

#### To format a single file run:
```
$ gofmt -w yourcode.go
```

#### You can also format an entire package (Note that the command is different from formatting a single file):
```

$ gofmt path/to/your/package
```

#### You can also format recursively all folders from current folder
```

$ go fmt ./...
```


lecture 34
   Recommends a youtube video on utf-8   (just search for “utf8” on youtube)   
   https://www.youtube.com/watch?v=MijmeoH9LT4

lecture 35
   Recommends “go get github.com/GoesToEleven/GolangTraining”
     about 74MB  

