+++
date = "2018-01-10T20:29:20Z"
title = "Go Introduction"
weight = 1
+++

## Where to Find More Information

The best source for more information about Go is the official website, https://golang.org 

### Documentation 

- Go documentation, https://golang.org/doc/
- Go Programming Language Specification  https://golang.org/ref/spec
- Go Standard library online https://golang.org/pkg


### Go Playground
https://play.golang.org

- Online playground that makes it convenient to run simple Go code
- Check one's understanding of syntax, semantics, or library packages. 
- Persisitent URLs are great for sharing snippets of Go code with others.

The primary shortcoming of the Playground and the Tour is that they allow only standard libraries to be imported, and many library features -- networking for example -- are restricuted for practical or security reasons.  

### Interactive Tutorial
https://tour.golang.org/welcome/1

Built atop the Playground, the Go Tour is a sequence of short interactive lessons.


### Go by Example
https://gobyexample.com

Go by Example is a hands-on introduction to Go using annotated example programs. 
NOTE: on the upper right, click on the Gopher icon to view within the Go Playground.

