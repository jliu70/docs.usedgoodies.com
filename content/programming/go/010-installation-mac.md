+++
date = "2018-01-10T20:29:20Z"
title = "Go Installation Mac"
weight = 10
+++

## Download software
https://golang.org/dl/

Download the Go installer for your platform.


Optionally you can verify the sha256 checkum on the downloaded file.
```
$ openssl sha256 ~/Downloads/go1.13.4.darwin-amd64.pkg
SHA256(C:/Users/jeffrey.liu/Downloads/go1.13.4.darwin-amd64.pkg)= 9f0721551a24a1eb43d2005cd58bd7b17574e50384b8da8896b0754259790752

```

If you are upgrading from an older version of Go you must first remove the existing version.

To remove an existing Go installation from your system delete the go directory. This is usually /usr/local/go under Linux, macOS, and FreeBSD or c:\Go under Windows.

If you installed Go with the macOS package then you should remove the /etc/paths.d/go file. 


##### NOTE: No longer a fan of Homebrew 
via Homebrew
```
brew install go
```


### Verify
```
go version
```


## Setup your workspace

You really should setup GOPATH to point to your workspace prior to running any “go get” or installing Go extensions.
Todd tries to “go get” into workspace before talking about setting up related env var GOPATH. 
The other video focused on the mac with no windows support, so there are some defaults already set for GOPATH and GOROOT..  

NOTE: for the Mac, I was able to install the Go Extension for Visual Studio Code and the additional Go tools with the default GOPATH and GOROOT.  The GOROOT and GOPATH was NOT discussed at all in the “Go the Complete Developer’s Guide” video course.


On the Mac the default GOPATH is ~/go.
```
GOPATH="/Users/jliu/go"
```



### Add environment variables

Go has a unique approach of managing code where you have a single workspace for all your Go projects. For more information see the documentation.  https://golang.org/doc/code.html#Workspaces

I appended the following to my `.bash_profile` 
```
export GOPATH=~/go
export PATH=$PATH:$GOPATH/bin
```

### Create your workspace
```
mkdir -p $GOPATH/src $GOPATH/bin $GOPATH/pkg
```


#### Go environment 


```
$  go help gopath

jliu@JEFFREYs-MacBook-Pro ~ $ go help gopath
The Go path is used to resolve import statements.
It is implemented by and documented in the go/build package.

The GOPATH environment variable lists places to look for Go code.
On Unix, the value is a colon-separated string.
On Windows, the value is a semicolon-separated string.
On Plan 9, the value is a list.

If the environment variable is unset, GOPATH defaults
to a subdirectory named "go" in the user's home directory
($HOME/go on Unix, %USERPROFILE%\go on Windows),
unless that directory holds a Go distribution.
Run "go env GOPATH" to see the current GOPATH.

See https://golang.org/wiki/SettingGOPATH to set a custom GOPATH.
```
 
#### Go environment 

```
jliu@JEFFREYs-MacBook-Pro ~ $ go help environment
The go command, and the tools it invokes, examine a few different
environment variables. For many of these, you can see the default
value of on your system by running 'go env NAME', where NAME is the
name of the variable.

General-purpose environment variables:

	GCCGO
		The gccgo command to run for 'go build -compiler=gccgo'.
	GOARCH
		The architecture, or processor, for which to compile code.
		Examples are amd64, 386, arm, ppc64.
	GOBIN
		The directory where 'go install' will install a command.
	GOOS
		The operating system for which to compile code.
		Examples are linux, darwin, windows, netbsd.
	GOPATH
		For more details see: 'go help gopath'.
	GORACE
		Options for the race detector.
		See https://golang.org/doc/articles/race_detector.html.
	GOROOT
		The root of the go tree.
```




### References
http://sourabhbajaj.com/mac-setup/Go/README.html

