+++
date = "2018-01-10T20:29:20Z"
title = "Go Extensions Installation for Visual Studio Code on Mac"
weight = 20
+++


Visual Studio Code Editor download link   https://code.visualstudio.com

Out of the box Visual Studio Code does not offer native support for Go.
We can install an extension which offers great Go support. 

This extension uses a set of Go tools to provide the various rich features. These tools are installed in your GOPATH by default. If you wish to have these tools in a separate location, provide the desired location in the setting go.toolsGopath. 
https://github.com/Microsoft/vscode-go/wiki/GOPATH-in-the-VS-Code-Go-extension


Read more about this and the tools at Go tools that the Go extension depends on
https://github.com/Microsoft/vscode-go/wiki/Go-tools-that-the-Go-extension-depends-on


#### Release Notes
https://github.com/Microsoft/vscode-go/wiki/Release-Notes


## Install Go Extensions for Visual Studio Code

Install and open Visual Studio Code. Press Ctrl+Shift+X or Cmd+Shift+X to open the Extensions pane.

Find and install the Go extension. You can also install the extension from the Marketplace.
Under View -> Extensions 
Search for "Go"
The first result that pops up should have around 2 Million Downloads.


Open any .go file in VS Code. The extension is now activated.


You will see Analysis Tools Missing in the bottom right, clicking this will offer to install all of the dependent Go tools. You can also run the command Go: Install/Update tools to install/update the same.



GOPATH or GOROOT, although these seem to be set to default values on the Mac.


For Mac, setting the following user env variables  GOPATH and also PATH=%PATH%;%GOPATH%\bin
Confirmed that it works.
 
Make sure that GOROOT is set to where Go is installed (Default:  C:\Go\)
 
Make sure that GOPATH is set to your desired go projects directory
 
Make sure that PATH has the following entries:
C:\Go\bin;%GOPATH%\bin;C:\Program Files\Git\cmd
 
Complete the installation of the Go tools as outlined in the video course.
NOTE: Be patient -- it took around 15 minutes or so to complete.    And for some reason I had to run install twice for one of the tools.

