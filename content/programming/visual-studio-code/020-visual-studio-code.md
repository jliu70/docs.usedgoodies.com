+++
weight = 20
toc = true
date = "2017-12-28T05:03:55Z"
title = "Visual Studio Code Editor"
+++

# Visual Studio Code

Download link   https://code.visualstudio.com


Open source, cross platform

I admit initially I wasn't completely sold on it, but after seeing it used in countless tutorial videos, I gave it a try, and I have to say, I'm impressed.

- Pretty clean interface
- Integrated terminal(s)
- Markdown live preview

  ![Visual Studio Code Screenshot](/images/misc/Visual_Studio_Code_Screenshot.png?&classes=border,shadow)


