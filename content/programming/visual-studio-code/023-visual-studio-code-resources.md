+++
weight = 23
toc = true
date = "2020-01-21T05:03:55Z"
title = "Visual Studio Code Resources"
+++


# Visual Studio Code Resources

Download link   https://code.visualstudio.com

// TODO - 1/21/2020 move these tips to the [tips page](./021-visual-studio-code-tips.md)
## Tips
https://github.com/microsoft/vscode-tips-and-tricks
Command Palette  ctrl+shift+p
    Reference keybindings
Status Bar  Cmd+shift+m
    cycle through errors shift+8
Interactive Playground
Toggle sidebar   ctrl+b
    switch to sidebar w/o using the mouse  ctrl+0  (ctrl+1 to get back to editor)
Navigate to a file  ctrl+e  or  ctrl+shift+e
Search   ctrl+shift+f


https://jsmanifest.com/21-vscode-shortcuts-to-code-faster-and-funner/

#vscode code navigation
https://code.visualstudio.com/docs/editor/editingevolved

#vscode tips and tricks
https://code.visualstudio.com/docs/getstarted/tips-and-tricks


## Awesome list
#awesome
[visual studio code awesome list](https://viatsko.github.io/awesome-vscode/)



### Using Visual Studio Code diff 
https://www.meziantou.net/comparing-files-using-visual-studio-code.htm


### What I learned writing my own VS code extension
https://css-tricks.com/what-i-learned-by-building-my-own-vs-code-extension/



### Keyboard shortcuts
https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf
https://code.visualstudio.com/shortcuts/keyboard-shortcuts-macos.pdf


### Multiple windows color settings
https://dev.to/iamarek/working-on-multiple-windows-in-vs-code-style-title-bars-to-increase-your-productivity-2oii
