+++
weight = 22
toc = true
date = "2019-12-18T05:03:55Z"
title = "Visual Studio Code Extensions"
+++

# Visual Studio Code

Download link   https://code.visualstudio.com

## Extensions

Rainbow CSV 
Go
Python
Powershell



// TODO - evaluate and install these extensions 

ASCII Tree generator 1.2.1  - can generate from text or from the sidebar explorer 
https://github.com/aprilandjan/ascii-tree-generator

#api 
REST Client
https://marketplace.visualstudio.com/items?itemName=humao.rest-client

#knowledge - `TODO tree`extension is probably not needed - how is this any different from using ctrl+shift+f and typing in TODO (followed by a ctrl+1)
TODO tree (for code)
https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree

