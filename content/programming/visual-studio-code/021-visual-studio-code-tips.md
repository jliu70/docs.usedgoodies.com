+++
weight = 21
toc = true
date = "2017-12-28T05:03:55Z"
title = "Visual Studio Code Editor Tips"
+++



## Tips

### Default Keyboard Shortcuts

####  Can override via  File > Preferences > Keyboard Shortcuts


### Toggle comment for a line 
```
Mac: command-/ 

Windows: ctrl-/
```

### Duplicate a line 
```
Copy lines down: shift-alt-down 
Copy lines up: shift-alt-up 

```

### Move lines
```
Move lines down: alt-down 
Move lines up: alt-up 

```

### switch between editor tabs
```
Mac: command-alt-left arrow  or command-alt-right arrow

Windows:  ctrl-pageUp or ctrl-pageDown

```

### switch directly to specific editor group
```
Mac:  CTRL+1, CTRL+2, CTRL+3 etc..to switch between groups.
  CMD+1, CMD+2, and CMD+3 switch between and create tab groups

Windows: Mac:  alt+1, alt+2, alt+3 etc..to switch between tabs.
  CTRL+1, CTRL+2, and CTRL+3 switch between and create tab groups

```

### Switch workspaces  (aka project folder tree)
```
ctrl-r
```


### Toggle integrated shell
```
ctrl-`

the following also works on Windows:  ctrl-j

the following also works on Mac:  ⌘-j  
  NOTE: on Mac ctrl-j  deletes the new line
```

### Markdown preview
```
Mac:
  command-k v

Windows:
  ctrl-k v
```

### Open tab in new window
```
Mac:
  command-k o

Windows:
  ctrl-k o
```

### Toggle side bar
```
Mac:  command-b

Windows:  ctrl-b
```

### Multiple Cursors (aka multiple input)
```
VS Code supports multiple cursors for fast simultaneous edits. 

You can add secondary cursors (rendered thinner) with Alt+Click. 
  Each cursor operates independently based on the context it sits in. 
  A common way to add more cursors is with ⌥⌘↓ or ⌥⌘↑ that insert cursors below or above.  (opt+cmd+up or opt+cmd+down)

Tip: You can also add more cursors with ⇧⌘L, which will add a selection at each occurrence of the current selected text.
```

### Go to Definition 
```
“F12” to “Go to Definition”  of function source code.   (Go -> Go To Definition  “F12”)


Mac:  CMD-click
  CMD-alt-Click opens in split editor view.

Windows: ctrl-click 
  ctrl-alt click opens in split editor view.

User preferences can change it:
// The modifier to be used to add multiple cursors with the mouse. `ctrlCmd` maps to `Control` on Windows and Linux and to `Command` on OSX. The Go To Definition and Open Link mouse gestures will adapt such that they do not conflict with the multicursor modifier.
  "editor.multiCursorModifier": "alt",
```


### Multi-root workspaces (aka multple folder trees in Explorer sidebar)
```
Note: Multi-root workspace mode is still a relatively new feature and some extensions may not have adopted the new APIs. Many extensions such as themes and snippets don't require any changes. If some of your extensions do not handle multiple folders, you may need to limit their use to a single folder.


Add Folder to Workspace...
The File > Add Folder to Workspace... command brings up an Open Folder dialog to select the new folder.

```

### Open another workspace in a new window
```
ctrl-r  ctrl-r  
NOTE: you would hit the second ctlr-r on the line above the workspace you wish to open in a new window
```



## Settings

### Keyboard shortcut to bring up user settings
```
Mac:  cmd-,

Windows:  ctrl-,
```

### Disable data usage collection and reporting to MS
Go to Settings:
```
 "telemetry.enableTelemetry": false
```

### Enable word wrap
Go to Settings:
```
"editor.wordWrap": "on"
```

### Windows integrated shell
visual studio code on Windows (set to git bash)
```
"terminal.integrated.shell.windows": "C:\\Program Files\\Git\\bin\\bash.exe"
```



#### References
https://code.visualstudio.com/docs/getstarted/tips-and-tricks
https://code.visualstudio.com/docs/editor/codebasics
https://code.visualstudio.com/docs/editor/multi-root-workspaces
https://code.visualstudio.com/docs/supporting/faq#_how-to-disable-telemetry-reporting
https://code.visualstudio.com/docs/languages/markdown
http://thisdavej.com/build-an-amazing-markdown-editor-using-visual-studio-code-and-pandoc
https://stackoverflow.com/questions/30203752/how-do-i-duplicate-a-line-or-selection-within-visual-studio-code


## general intro
https://code.visualstudio.com/docs?start=true
