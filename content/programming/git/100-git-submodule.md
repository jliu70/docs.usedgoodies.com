+++
weight = 100
toc = true
date = "2017-03-17T07:27:21Z"
title = "100 git submodule"
+++


### 3/17/2017  - General info on git submodule
http://stackoverflow.com/questions/7124483/difference-between-subprojects-and-submodules-in-git  
https://git-scm.com/docs/git-submodule

### 3/17/2017  - hugo theme as a git submodule
https://github.com/hbpasti/heather-hugo  
http://choomnuan.com/blog/2015/07/18/how-to-setup-hugos-theme-using-git-submodule/  
https://chrisjean.com/git-submodules-adding-using-removing-and-updating/

http://stackoverflow.com/questions/14720034/no-submodule-mapping-found-in-gitmodules-for-path
```
$ git rm --cached themes/hugo-theme-learn

$ git submodule add https://github.com/matcornic/hugo-theme-learn.git themes/hugo-theme-learn
Adding existing repo at 'themes/hugo-theme-learn' to the index

$ git submodule status
 fcca8a21f1000bdb0de5db80d396ea30dc15f53e themes/hugo-theme-learn (heads/master)

$ cat .git/config
[core]
        repositoryformatversion = 0
        filemode = false
        bare = false
        logallrefupdates = true
        symlinks = false
        ignorecase = true
[remote "origin"]
        url = git@gitlab.com:jliu70/docs.usedgoodies.com.git
        fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
        remote = origin
        merge = refs/heads/master
[submodule "themes/hugo-theme-learn"]
        url = https://github.com/matcornic/hugo-theme-learn.git



$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        new file:   .gitmodules


$ git add -A

$ git commit -m 'add git submodule for hugo-theme-learn'
[master fa496b2] add git submodule for hugo-theme-learn
 1 file changed, 3 insertions(+)
 create mode 100644 .gitmodules

```

```
On the macbook Pro

$ cat .git/config
[core]
	repositoryformatversion = 0
	filemode = true
	bare = false
	logallrefupdates = true
	ignorecase = true
	precomposeunicode = true
[remote "origin"]
	url = git@gitlab.com:jliu70/docs.usedgoodies.com.git
	fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
	remote = origin
	merge = refs/heads/master


$ cat .gitmodules
[submodule "themes/hugo-theme-learn"]
	path = themes/hugo-theme-learn
	url = https://github.com/matcornic/hugo-theme-learn.git
```

#### Submodules
```
# Import .gitmodules
  git submodule init

# Clone missing submodules, and checkout commits
  git submodule update --init --recursive

# Update remote URLs in .gitmodules
# (Use when you changed remotes in submodules)
  git submodule sync

# display the contents of the .gitmodules file to see what’s in there
git show :.gitmodules

```


### Read More
https://conferences.oreilly.com/oscon/oscon-tx/public/schedule/detail/56595
https://github.com/brentlaster/conf/raw/master/oscon2017/pg-labs-2017.pdf
https://docs.gitlab.com/ce/ci/git_submodules.html

