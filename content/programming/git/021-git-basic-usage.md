+++
date = "2017-03-20T19:48:07Z"
title = "21 git basic usage"
weight = 21
toc = true
+++


{{% notice info %}}
Here are a series of commands outlining basic usage of git.
{{% /notice %}}


## Initialize a repo
```
git init
```

## Add a file to the staging area
```
git add $filename

git add -A   # add all files
```

## Checking the status of files.  Current branch.
### Four main states of a file
#### Untracked, Unmodified, Modified, Staged
```
git status
```

## Commit staged changes to the repo
```
git commit [ -m "message here" ]
```

## View the log of the repo
```
git log
```


# Additional resources
## Have 15 minutes? - try out an interactive tutorial on git
https://try.github.io/levels/1/challenges/1

### Also see
`git help everyday`
