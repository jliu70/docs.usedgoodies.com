+++
date = "2017-03-24T06:51:54Z"
title = "10 git installation"
weight = 10
toc = true
+++


## Linux 
For GNU/Linux distributions Git should be available in the standard
system repository. For example in Debian/Ubuntu please type in
the terminal:
```
$ sudo apt-get install git
```

If you want or need to install Git from source, you can get it from
https://git-scm.com/downloads.



## Windows
https://git-scm.com/download/win


## Mac OS X
https://git-scm.com/download/mac

