+++
title = "1 git introduction"
weight = 1
toc = true
date = "2017-03-15T21:07:26Z"
+++

## Background
Git was originally created by Linus in 2005 to replace commercial software ‘bitkeeper’ to maintain the Linux kernel source code.

Although git has its origins in the Open source community, git is being used within close source projects also.

#### Some companies that use git:
![Chapter page](/images/git/companies-that-use-git.png?width=50%&classes=noborder)


## What is Git?
- Git is a distributed version control system

- Git can be used in many different ways
  - **You decide how to use Git**

- Can just be used locally (on your own PC)

- Can be used between teams

- Many web services/interfaces
  - GitHub, Gitlab, BitBucket, etc



### Distributed vs Central

### Traditional Version Control - Centralized repository
![Chapter page](/images/git/centralized-version-control.png?width=50%&classes=border,shadow)


### slide 12 from powerpoint  - insert picture?
So what does it really mean to have a distributed version control system?
Everyone on your team has a complete history of the code locally.  Just about everything in Git can be done offline.
It is not just that Git works well when it is offline -- Git was designed from the beginning to have no real dependencies on a specific server.  If your server is offline for some reason your team can still work.
Given Git’s nature you can even share changes in a peer-to–peer nature. This does not happen often but Git does enable this ability.

### slide 13
But no one really wants to keep track of changes in the peer-to-peer way.
This is where you add in a remote server.
There really is nothing special about the remote server. You can have more than one. But it enables a nice integration location, just like you are used to in centralized version control.


### slide 14
Many companies and open source projects adopt a model similar to the one shown here where developers can pull from the “blessed repository”, but can only submit changes for review and acceptance by a select few who can actually merge the changes into the “blessed repository.”   The submitted changes are often referred to as “Pull Requests” (GitHub) or “Merge Requests” (Gitlab).




### Originally from the powerpoint (slide 139)
Git can be both amazingly simple and mind-blowingly complex.

You can start with the basics and work yourself into more complex graph manipulation over time.

There’s no need to grok all of it before you can use it.

### Tips:
- Remember: Just commit, don’t try any fancy things
- Get status if you’re lost
- Go back to master
- Throw away things / commit things – it doesn’t matter, you can always make a new commit.   “commits are free”


## Git Guides

### Git Help Commands
Git comes with a handful of guides ready for you to explore. Run `git help -g` to see what's available:
```
The common Git guides are:

   attributes   Defining attributes per path
   everyday     Everyday Git With 20 Commands Or So
   glossary     A Git glossary
   ignore       Specifies intentionally untracked files to ignore
   modules      Defining submodule properties
   revisions    Specifying revisions and ranges for Git
   tutorial     A tutorial introduction to Git (for version 1.5.1 or newer)
   workflows    An overview of recommended workflows with Git
```
Jump to a Git tutorial with `git help tutorial`, go through the glossary with `git help glossary` or learn about the most common commands with `git help everyday`.


### Git Book
An excellent Git course can be found in the great Pro Git book by Scott Chacon and Ben Straub. The book is available online for free
at https://git-scm.com/book


# More


Originally from `https://portal.paas.intraxa/confluence/pages/viewpage.action?spaceKey=IaaS&title=GitHub`

Documentation
For users that have no or limited knowledge of GitHub, GitHub foresees clear documentation. You can find them on the following pages:
�         Set Up Git https://help.github.com/articles/set-up-git
�         Creating a new repository https://help.github.com/articles/creating-a-new-repository
�         Fork a repository https://help.github.com/articles/fork-a-repo
�         Be Social https://help.github.com/articles/be-social
GitHub Help
�         github:help https://help.github.com/
