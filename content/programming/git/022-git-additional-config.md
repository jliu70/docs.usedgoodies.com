+++
date = "2017-04-09T19:48:07Z"
title = "22 git additional config"
weight = 22
toc = true
+++

Here we explore some additional git configuration options which you may find useful.

## Alternative to built-in git diff: icdiff

[icdiff][link-1] is interesting in that it attempts to replicate colorful, GitHub-style, split diffs right in your console. A little easier to read than the normal chunk-based diff style.

Instead of trying to be a diff replacement for all circumstances, the goal of [icdiff][link-1] is to be a tool you can reach for to get a better picture of what changed when it's not immediately obvious from diff.

To see what it looks like, try:
`git difftool --extcmd icdiff`

To install this as a tool you can use with git, copy `icdiff` onto your path and run:
`git icdiff`

After you update your git config with parameters listed below, you can use it just like normal: `git difftool master branch`

Built in `git diff` and `git diff --cached` output example:
![Chapter page](/images/git/git-diff-diff-cached-example.png?&classes=border,shadow)



You can update your .gitconfig file with the following:
```
[alias]
  showtool = "!f() { git difftool $1^ $1; }; f"
  added = difftool --cached
[diff]
  tool = icdiff
[difftool]
  prompt = false
[difftool "icdiff"]
  cmd = /usr/bin/icdiff --line-numbers $LOCAL $REMOTE
[pager]
  difftool = true
```
Reference:  [github-style-diff-in-terminal-with-icdiff][link-2]


### icdiff usage examples
```
git difftool

git added
```
![Chapter page](/images/git/git-difftool-added-example.png?&classes=border,shadow)



```
git showtool $COMMITHASH
```

Example: `git showtool ad3532f`
![Chapter page](/images/git/git-showtool-example.png?&classes=border,shadow)


{{% notice warning %}}
Keep `git diff` in your back pocket - icdiff doesn’t seem to handle comparisons against /dev/null.  For example, there is no output from `git difftool --cached` after you’ve staged a new file.  You'll need to use `git diff --cached` in that case.
{{% /notice %}}




## Stash

Add this to your .gitconfig file:
```
[alias]
  stash-all = stash save --include-untracked
```
[Stash][link-3] is extremely useful when someone randomly asks you to check out another branch, but you’re right in the middle of something. The above command ensures that when you stash, you catch the new files you haven’t caught with a git add yet.

Additional reference:  https://stackoverflow.com/questions/835501/how-do-you-stash-an-untracked-file


## Git 2.13 - conditional git configs
https://github.com/blog/2360-git-2-13-has-been-released


Git 2.13 introduces conditional configuration includes. For now, the only supported condition is matching the filesystem path of the repository, but that's exactly what we need in this case. You can configure two conditional includes in your home directory's ~/.gitconfig file:

```
[includeIf "gitdir:~/work/"]
  path = .gitconfig-work
[includeIf "gitdir:~/play/"]
  path = .gitconfig-play
```

Now you can put whatever options you want into those files:

```
$ cat ~/.gitconfig-work
[user]
name = Serious Q. Programmer
email = serious.programmer@business.example.com

$ cat ~/.gitconfig-play
[user]
name = Random J. Hacker
email = rmsfan1979@example.com
The appropriate config options will be applied automatically whenever you're in a repository that's inside your work or play directories.
```


## Prettier and more concise Git log 

My preference for looking at git logs, configurable aliases in your global .gitconfig
```
[alias]
        lol = log --pretty=oneline --abbrev-commit --graph --decorate
        ls = log --pretty=format:\"%C(yellow)%h %C(green)%ad%C(yellow)%d %C(reset)%s%C(red) [%cn] %C(green) %ar \" --graph --decorate --date=short
```


## Show your commits for the last 24 hours

Cool alias to list out all your commits from the past day -- configurable alias in your global .gitconfig
```
[alias]
        oneday = log --since '1 day ago' --oneline  --pretty=format:\"%C(yellow)%h %C(red)%ad%C(yellow)%d %C(reset)%s%C(green) [%cn]\"
```


## Shorter Git Status Alias

My preference for looking at git status in a more compact, less verbose format -- configurable alias in your global .gitconfig
```
[alias]
        st = status --short --branch

```


### References

[Better Git Configuration][link-4]

[5 useful tricks you didn't know for git](https://densitylabs.io/blog/5-useful-tricks-you-didn't-know-for-git)
>  1/24/2020 - Hacker News conversation regarding the above: https://news.ycombinator.com/item?id=22139014


[link-1]: https://www.jefftk.com/icdiff
[link-2]: http://blog.owen.cymru/github-style-diff-in-terminal-with-icdiff/
[link-3]: https://git-scm.com/docs/git-stash
[link-4]: https://blog.scottnonnenberg.com/better-git-configuration/
[link-5]: https://news.ycombinator.com/item?id=14045787
