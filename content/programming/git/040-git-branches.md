+++
date = "2017-03-24T05:33:43Z"
title = "40 git branches"
weight = 40
toc = true
+++

## git branch

### git branch 

```
 git branch [-a]
List all local branches in repository. With -a: show all branches (with remote).
$ git branch [name]
Create new branch, referencing the current HEAD.
$ git checkout [-b] [name]
Switch working directory to the specified branch. With -b: Git will create the
specified branch if it does not exist.
$ git merge [from name]
Join specified [from name] branch into your current branch (the one you are
on currenlty).
$ git branch -d [name]
Remove selected branch, if it is already merged into any other. -D instead of
-d forces deletion.


# List of remote branches
$ git branch -r

# Track all remote branches
$ git branch -r | grep -v '\->' | while read remote; do git branch --track "${remote#origin/}" "$remote"; done



# rename your local branch
$ git branch -m newName

# rename if you are on a different branch
$ git branch -m oldName newName

# delete oldName remote branch and push newName local branch to remote
$ git push origin :oldName newName
$ git push origin -u newName
## NOTE: if the old branch was being referenced anywhere, the references won’t be updated since you’re technically deleting an old branch and making a new one, not just changing the name of the same branch!
```

