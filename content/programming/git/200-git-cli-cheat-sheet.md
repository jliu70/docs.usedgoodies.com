+++
title = "200 git cli cheat sheet"
weight = 200
toc = true
date = "2017-03-15T21:07:40Z"
+++

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [git branches](#git-branches)
    - [Create a new branch](#create-a-new-branch)
    - [Push branch to remote repo](#push-branch-to-remote-repo)
    - [List branches](#list-branches)
    - [Get a remote branch](#get-a-remote-branch)
    - [Track all  remote branches](#track-all-remote-branches)
    - [Delete local remote-tracking branches (lol)](#delete-local-remote-tracking-branches-lol)
    - [List merged branches](#list-merged-branches)
    - [Switch to branch $branchname](#switch-to-branch-branchname)
    - [Merge branch](#merge-branch)
    - [Delete local branch $branchname](#delete-local-branch-branchname)
    - [Delete remote branch $branchname](#delete-remote-branch-branchname)
    - [rename your local branch](#rename-your-local-branch)
    - [rename if you are on a different branch](#rename-if-you-are-on-a-different-branch)
    - [delete oldName remote branch and push newName local branch to remote](#delete-oldname-remote-branch-and-push-newname-local-branch-to-remote)
- [Reverting changes](#reverting-changes)
    - [Un-stage changes - remove from git index](#un-stage-changes-remove-from-git-index)
        - [typically after you already ran `git add $fileName`](#typically-after-you-already-ran-git-add-filename)
    - [Undo changes - restore contents of $fileName back to last commit](#undo-changes-restore-contents-of-filename-back-to-last-commit)
    - [Rewriting the most recent commit message - only if it's local and not yet pushed to remote](#rewriting-the-most-recent-commit-message-only-if-its-local-and-not-yet-pushed-to-remote)
- [Move/Rename file](#moverename-file)
- [Remove file](#remove-file)
- [git diff](#git-diff)
    - [git diff - staged files (git index)](#git-diff-staged-files-git-index)
    - [git diff - single file](#git-diff-single-file)
    - [git diff - single file from the master branch to an arbitrary older version in Git](#git-diff-single-file-from-the-master-branch-to-an-arbitrary-older-version-in-git)
    - [git diff - just filenames and stats](#git-diff-just-filenames-and-stats)
    - [git diff $firstBranch...$secondBranch](#git-diff-firstbranchsecondbranch)
- [Get current commit id (sha1)](#get-current-commit-id-sha1)
- [git commit](#git-commit)
    - [Redo commits](#redo-commits)
        - [Erase mistakes and craft replacement history](#erase-mistakes-and-craft-replacement-history)
- [git log options](#git-log-options)
    - [show single commit info](#show-single-commit-info)
        - [git log for single commit (with diff)](#git-log-for-single-commit-with-diff)
        - [Example: git log for single commit (with diff)](#example-git-log-for-single-commit-with-diff)
        - [git log for single commit (without diff)](#git-log-for-single-commit-without-diff)
        - [Example: git log for single commit (without diff)](#example-git-log-for-single-commit-without-diff)
        - [git log for single commit via regex](#git-log-for-single-commit-via-regex)
    - [git log --follow $fileName](#git-log-follow-filename)
    - [git shortlog](#git-shortlog)
    - [Searching](#searching)
    - [log format strings](#log-format-strings)
    - [git revision ranges](#git-revision-ranges)
    - [git basic log filters](#git-basic-log-filters)
    - [git log search](#git-log-search)
    - [git log limiting](#git-log-limiting)
    - [git log simplification](#git-log-simplification)
    - [git log ordering](#git-log-ordering)
    - [git log formatting](#git-log-formatting)
    - [git standup (commits for last day)](#git-standup-commits-for-last-day)
        - [https://github.com/kamranahmedse/git-standup](#httpsgithubcomkamranahmedsegit-standup)
        - [https://coderwall.com/p/f4shwg/git-for-daily-standup](#httpscoderwallcompf4shwggit-for-daily-standup)
    - [git commits from last Friday)](#git-commits-from-last-friday)
- [git References](#git-references)
    - [git refs](#git-refs)
    - [git searching back](#git-searching-back)
    - [git search other](#git-search-other)
    - [git ranges](#git-ranges)
        - [git ranges illustration](#git-ranges-illustration)
        - [examples](#examples)
- [git tag](#git-tag)
- [git submodules](#git-submodules)
- [git grep](#git-grep)
- [git stash - Shelve and restore incomplete changes](#git-stash-shelve-and-restore-incomplete-changes)
- [git Bisect](#git-bisect)
    - [git bisect manually](#git-bisect-manually)
- [git Blame](#git-blame)
- [Existing Git repository](#existing-git-repository)
- [References](#references)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## git branches

### Create a new branch
```
git branch $branchname
  or
git checkout -b $branchname     # With -b: Git will create the specified branch if it does not exist
```

### Push branch to remote repo
```
git push -u origin $branchname

Example:
  git push -u origin master       # set the upstream remote for branch 'master'
```

### List branches
```
git branch
git branch -r       # With -r: show remote branches
git branch -a       # With -a: show all branches (with remote)
```

### Get a remote branch
```
git fetch origin
git checkout --track origin/$branchname
```

### Track all  remote branches
```
git branch -r | grep -v '\->' | while read remote; do git branch --track "${remote#origin/}" "$remote"; done
```

### Delete local remote-tracking branches (lol)
```
git remote prune origin
```

### List merged branches
```
git branch -a --merged
```

### Switch to branch $branchname
```
git checkout $branchname
```

### Merge branch
```
git merge $branchname  # Join specified $branchname branch into your current branch (the one you are on currently).
```
```
Example:
  git checkout master   # switch to branch 'master'
  git merge devel       # merge branch 'devel' into 'master'
```

### Delete local branch $branchname
```
git branch -d $branchname  # Remove selected branch, if it is already merged into any other. -D instead of -d forces deletion.
```

### Delete remote branch $branchname
```
git push origin :$branchname
```


### rename your local branch
```
git branch -m newName
```

### rename if you are on a different branch
```
git branch -m oldName newName
```

### delete oldName remote branch and push newName local branch to remote
```
$ git push origin :oldName newName
$ git push origin -u newName
## NOTE: if the old branch was being referenced anywhere, the references won’t be updated since you’re technically deleting an old branch and making a new one, not just changing the name of the same branch!
```



## Reverting changes
### Un-stage changes - remove from git index
#### typically after you already ran `git add $fileName`
```
git reset $fileName     # Unstages the file, but preserves its contents
```

### Undo changes - restore contents of $fileName back to last commit
```
git checkout -- $fileName
```

### Rewriting the most recent commit message - only if it's local and not yet pushed to remote
```
git commit --amend
```
For older or multiple commit messages
Reference: https://help.github.com/articles/changing-a-commit-message/



## Move/Rename file
```
git mv $currentName $newName
```

## Remove file
```
git rm $fileName      # Deletes the file from the working directory and stages the deletion

git rm --cached $fileName   # Removes the file from version control but preserves the file locally
```



## git diff

### git diff - staged files (git index)
```
git diff --staged   # shows file differences between staged and the last committed file version
  (or synonym --cached)
git diff --cached   # shows file differences between staged and the last committed file version

```

### git diff - single file
```
git diff -- content/about/100-initial-installation.md
```

### git diff - single file from the master branch to an arbitrary older version in Git
```
git diff master~9:content/git/100-git-cli-cheat-sheet.md content/git/100-git-cli-cheat-sheet.md
```
Reference: http://stackoverflow.com/questions/5586383/how-to-diff-one-file-to-an-arbitrary-version-in-git


### git diff - just filenames and stats
```
git diff --stat
app/a.txt    | 2 +-
app/b.txt    | 8 ++----
2 files changed, 10 insertions(+), 84 deletions(-)

```
### git diff $firstBranch...$secondBranch
```
git diff $firstBranch...$secondBranch       # Show content differences between two branches
```


## Get current commit id (sha1)
```
git show-ref --head --abbrev
3970f6a HEAD
3970f6a refs/heads/master
3970f6a refs/remotes/origin/master
```
## git commit

### Redo commits
#### Erase mistakes and craft replacement history
```
git reset [$commit]           # Undoes all commits after $commit, preserving changes locally

git reset [--hard] [$targetReference]   # Switch current branch to the target reference, and leaves a difference as an uncommited changes. When --hard is used, all changes are discarded.
```

## git log options
```
--oneline
  e11e9f9 Commit message here

--decorate
  shows "(origin/master)"

--graph
  shows graph lines

--date=relative
  "2 hours ago"
```

### show single commit info
#### git log for single commit (with diff)
Reference: http://stackoverflow.com/questions/4082126/git-log-of-a-single-revision
```
git show c
(where c is the commit id)
```

#### Example: git log for single commit (with diff)
```

jliu@JEFFREYs-MacBook-Pro ~/git/docs.usedgoodies.com (master) $ git show 54359ac
commit 54359ac72e526e5ffe770655c26897d274c612a8
Author: Jeff Liu <jliu@hanwave.net>
Date:   Thu Mar 16 18:12:16 2017 -0400

    Test of git commit

    Here is the body of the commit message.

    We are adding a hashtag in s3.sh to do an example commit
    message.  Let's see if it works.

diff --git a/s3.sh b/s3.sh
index ac4e292..e8f36e6 100755
--- a/s3.sh
+++ b/s3.sh
@@ -1,4 +1,4 @@
-
+#
 s3cmd sync --acl-public --exclude '.DS_Store' --delete-removed --recursive /Users/jliu/git/docs.usedgoodies.com/public/ s3://docs.usedgoodies.com/


```

#### git log for single commit (without diff)
```
git log -1 c
(where c is the commit id)
```

#### Example: git log for single commit (without diff)
```
git log -1 54359ac
commit 54359ac72e526e5ffe770655c26897d274c612a8
Author: Jeff Liu <jliu@hanwave.net>
Date:   Thu Mar 16 18:12:16 2017 -0400

   Test of git commit

   Here is the body of the commit message.

   We are adding a hashtag in s3.sh to do an example commit
   message.  Let's see if it works.

```

#### git log for single commit via regex
```
git show ":/Test of"     # via regex

```


### git log --follow $fileName
```
git log --follow $fileName      # Lists version history for a file, including renames
```


### git shortlog
```
git shortlog

git shortlog HEAD~20..        # last 20 commits

```

### Searching
```
git log --grep="fixes things"  # search in commit messages
git log -S"window.alert"       # search in code
git log -G"foo.*"              # search in code (regex)
```

### log format strings
```
git log --pretty="format: ..."

%H  - commit hash
%h  - commit hash, abbrew
%T  - tree hash
%T  - tree hash, abbrev
%P  - parent hash
%p  - parent hash, abbrew
%an - author
%aN - author, respecting mailmap
%ae - author email
%aE - author email, respending mailmap
%aD - date (rfc2882)
%ar - date (relative)
%at - date (unix timestamp)
%ai - date (iso8601)
%cn - committer name
%cN - committer name
%ce - committer email
%cE - committer email
%cd - committer date
%cD - committer date
%cr
%ct
%ci
%cd - ref names
%e  - econding
%s  - commit subject
%f  - commit subject, filename style
%b  - commit body
```
### git revision ranges
```
git log master             # branch
git log origin/master      # branch, remote
git log v1.0.0             # tag

git log master develop

git log v2.0..master       # reachable from *master* but not *v2.0*
git log v2.0...master      # reachable from *master* and *v2.0*, but not both
```

### git basic log filters
```
git log
  -n, --max-count=2
      --skip=2

      --since="1 week ago"
      --until="yesterday"

      --author="Rico"
      --committer="Rico"

```
### git log search
```
git log
      --grep="Merge pull request"   # in commit messages
      -S"console.log"               # in code
      -G"foo.*"                     # in code (regex)

      --invert-grep
      --all-match                   # AND in multi --grep



git log --grep="fixes things"  # search in commit messages
git log -S"window.alert"       # search in code
git log -G"foo.*"              # search in code (regex)

```
### git log limiting
```
git log
      --merges
      --no-merges

      --first-parent          # no stuff from merged branches

      --branches="feature/*"
      --tags="v*"
      --remotes="origin"

```
### git log simplification
```
git log -- app/file.rb            # only file
      --simplify-by-decoration    # tags and branches

```

### git log ordering
```
--date-order
--author-date-order
--topo-order              # "smart" ordering
--reverse
```

### git log formatting
```
--pretty="..."       # see man git-log / PRETTY FORMATS
--abbrev-commit
--oneline
--graph


--oneline
  e11e9f9 Commit message here

--decorate
  shows "(origin/master)"

--graph
  shows graph lines

--date=relative
  "2 hours ago"

```


### git standup (commits for last day)
#### https://github.com/kamranahmedse/git-standup
#### https://coderwall.com/p/f4shwg/git-for-daily-standup
```
git log --reverse --branches --since=$(if [[ "Mon" == "$(date +%a)" ]]; then echo "last friday"; else echo "yesterday"; fi) --author=$(git config --get user.email) --format=format:'%C(cyan) %ad %C(yellow)%h %Creset %s %Cgreen%d' --date=local
```
### git commits from last Friday)
```
git log --reverse --branches --since="last friday" --author=$(git config --get user.email) --format=format:'%C(cyan) %ad %C(yellow)%h %Creset %s %Cgreen%d' --date=local
```


## git References
```
`commit` # an object
`HEAD`	 # a reference - a place where your working directory is now
`branch` # a reference to a commit; can have a tracked upstream
    origin/master	aka, refs/remotes/origin/master
    heads/master	aka, refs/heads/master
`tag`    # a reference (standard) or an object (annotated)  # human readable label to a specific commit object
```

### git refs
```
HEAD^       # 1 commit before head
HEAD^^      # 2 commits before head
HEAD~5      # 5 commits before head
```


### git searching back
```
master@{yesterday}	also 1 day ago, etc
master@{2}	2nd prior value
master@{push}	where master would push to
master^	parent commit
master^2	2nd parent, eg, what it merged
master~5	5 parents back
master^0	this commit; disambiguates from tags
v0.99.8^{tag}	can be commit, tag, tree, object
v0.99.8^{}	defaults to {tag}
:/fix bug	searches commit messages
```
### git search other
```
HEAD:README	…
0:README	(0 to 3) …
```

### git ranges
```
master	reachable parents from master
^master	exclude reachable parents from master
master..fix	reachable from fix but not master
master...fix	reachable from fix and master, but not both
HEAD^@	parents of HEAD
HEAD^!	HEAD, then excluding parents’s ancestors
HEAD^{:/fix}	search previous HEADs matching criteria
```

#### git ranges illustration
```
A -+- E - F - G   master
   '- B - C - D   fix

master..fix   = BCD
master...fix  = BCD and EFG
```

#### examples
```
git log master...develop    # inspect differences in branches
git rebase -i HEAD~3        # rebase last 3 commits
git reset --hard HEAD@{2}   # undo last operation that changed HEAD
git show ":/fix bug"        # search commit with regex
git checkout v2^{}          # checkout the `v2` tag (not `v2` branch)
```

## git tag
```
$ git tag                           # List all tags.
$ git tag [name] [commit sha]       # Create a tag reference named name for current commit. Add commit sha to tag a specific commit instead of current one.
$ git tag -a [name] [commit sha]    # Create a tag object named name for current commit.
$ git tag -d [name]                 # Remove a tag from a local repository.
```



## git submodules

```
# Import .gitmodules
  git submodule init

# Clone missing submodules, and checkout commits
  git submodule update --init --recursive

# Update remote URLs in .gitmodules
# (Use when you changed remotes in submodules)
  git submodule sync

# display the contents of the .gitmodules file to see what’s in there
git show :.gitmodules

```

## git grep
```
git grep <regexp> $(git rev-list --all)
```
http://stackoverflow.com/questions/2928584/how-to-grep-search-committed-code-in-the-git-history


## git stash - Shelve and restore incomplete changes
```
git stash           # temporarily stores all modified tracked files
git stash list      # Lists all stashed changesets
git stash pop       # Restored the most recently stashed files
git stash drop      # Discards the most recently stashed changeset
```
### git stash include untracked files
```
git stash save --include-untracked
```
[Stash][link-1] is extremely useful when someone randomly asks you to check out another branch, but you’re right in the middle of something. The above command ensures that when you stash, you catch the new files you haven’t caught with a git add yet.
Additional Reference: https://stackoverflow.com/questions/835501/how-do-you-stash-an-untracked-file


## git Bisect

```
git bisect start HEAD HEAD~6
git bisect run npm test
git checkout refs/bisect/bad   # this is where it screwed up
git bisect reset

```

### git bisect manually
```
git bisect start
git bisect good   # current version is good

git checkout HEAD~8
npm test          # see if it's good
git bisect bad    # current version is bad

git bisect reset  # abort
```

## git Blame

Check which developer was responsible for changing a particular piece of code:
```
# See who last changed lines 5 through 10 of the buttons’ CSS:
$ git blame -L5,10 _components.buttons.scss
```


## Existing Git repository
```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:jliu70/axatech-zim.git
git push -u origin --all
git push -u origin --tags
```



## References
  https://about.gitlab.com/2016/12/08/git-tips-and-tricks/
  https://gitlab.com/gitlab-com/marketing/raw/master/design/print/git-cheatsheet/print-pdf/git-cheatsheet.pdf
  http://ricostacruz.com/cheatsheets/git.html


  https://csswizardry.com/2017/05/little-things-i-like-to-do-with-git/
  https://news.ycombinator.com/item?id=14409269

[link-1]: https://git-scm.com/docs/git-stash


