+++
title = "400 gitlab"
weight = 400
toc = true
date = "2017-03-15T21:07:46Z"
+++

# Gitlab


## 12/12/2017
## Gitlab system check
https://docs.gitlab.com/omnibus/settings/smtp.html
 
https://forum.gitlab.com/t/setting-up-and-testing-email/3320
### Gitlab system check
```bash
gitlab-rake gitlab:check 
``` 
 
 
 
https://docs.gitlab.com/ce/administration/troubleshooting/debug.html
 
### Run a rails console
```bash
sudo gitlab-rails console production
```

 

### set delievery method

```bash
irb(main):001:0> ActionMailer::Base.delivery_method
=> :smtp
```
 

### Check mail settings

```bash
irb(main):002:0> ActionMailer::Base.smtp_settings
```

### send a test message via the console
```bash
irb(main):003:0> Notify.test_email('jeffrey.liu@axa-tech.com', 'gitlab message ccca2009', 'This is a gitlab message').deliver_now
irb(main):003:0> Notify.test_email(‘jliu@hanwave.net', 'gitlab message ccca2009', 'This is a gitlab message').deliver_now
```
NOTE: both emails worked
