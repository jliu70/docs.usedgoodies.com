+++
title = "20 git basic config"
weight = 20
toc = true
date = "2017-03-17T07:26:26Z"
+++


## Set your username and email
```
git config --global user.name "Johnny Appleseed"
```
```
git config --global user.email "johnny@example.com"
```

## Enable some colorization of Git output
```
git config --global color.ui auto
```

## Set global git ignore
```
git config --global core.excludesfile=~/.gitignore
```
```
$ cat .gitignore
logs/*
!logs/.gitkeep
tmp/
*.swp

Thanks to this file Git will ignore all files in logs directory (excluding
the .gitkeep file), whole tmp directory and all files '*.swp'.

Described file ignoring will work for the directory (and children directories)
where .gitignore file is placed.  (In other words, you can place at the project
top-level directory or any other child directory you require.)
```


## Edit your global git configuration
```
git config --global -e
```

## Edit your local git repo configuration
```
cd /path/to/git/repo
git config -e
```

## List your git configuration
```
cd /path/to/git/repo
git config -l
```

## List (only) your global git configuration
```
git config --global -l
```


## Optional: Customizing bash prompt on Mac to display git branch 
```
jliu@JEFFREYs-MBP ~ $ cat .bash_profile
# Default PS1
#$ grep PS1 /etc/bash*
#/etc/bashrc:if [ -z "$PS1" ]; then
#/etc/bashrc:PS1='\h:\W \u\$ '
#PS1='\h:\W \u\$ '
#
# Adding git branch to PS1
# https://gist.github.com/trey/2722934
# https://coderwall.com/p/fasnya/add-git-branch-name-to-bash-prompt
parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
export PS1="\u@\h \[\033[32m\]\w\[\033[33m\]\$(parse_git_branch)\[\033[00m\] $ "
```


## References
### 12/10/2016
Add git global config excludesfile  (core.excludesfile=~/.gitignore)  info to blog.   
#### Maybe add it to powerpoint too.  
  https://about.gitlab.com/2016/12/08/git-tips-and-tricks/  
