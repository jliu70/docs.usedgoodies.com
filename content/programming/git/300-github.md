+++
title = "300 github"
weight = 300
toc = true
date = "2017-03-15T21:07:40Z"
+++

github


### Sync your fork to upstream repo
https://help.github.com/articles/syncing-a-fork/

#### URLs
```
github.com/:userrepo/blame/:branch/:path
github.com/:userrepo/commit/:commit
```


Orginally from `https://portal.paas.intraxa/confluence/pages/viewpage.action?spaceKey=IaaS&title=GitHub`

What is Github (snazzy video)
https://www.youtube.com/watch?v=w3jLJU7DT5E

Documentation
For users that have no or limited knowledge of GitHub, GitHub foresees clear documentation. You can find them on the following pages:
·         Set Up Git https://help.github.com/articles/set-up-git
·         Creating a new repository https://help.github.com/articles/creating-a-new-repository
·         Fork a repository https://help.github.com/articles/fork-a-repo
·         Be Social https://help.github.com/articles/be-social
GitHub Help
·         github:help https://help.github.com/
