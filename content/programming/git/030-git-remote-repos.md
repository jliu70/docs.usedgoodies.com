+++
toc = true
date = "2017-03-17T07:27:17Z"
title = "30 git remote repositories"
weight = 30
+++



## git clone
```
git clone $projectUrl
```

Git clone can be run via one of two types of transports:  HTTPS or SSH.

To easily grab the repo string on Github
`picture 1`
`picture 2`

To easily grab the repo string on Gitlab.
`picture 1`
`picture 2`

### git clone via ssh

To git clone via ssh you will need to generate a new ssh key, or upload your existing key to Github/Gitlab.


#### Verify your ssh key
```
ssh git@github.com
```
#### Customize via ~/.ssh/config
Optionally you can customize your ssh configuration to use a specific ssh key for Github or Gitlab.




### git clone via https

An alternative to SSH, you may clone the git repo over HTTPS.  

#### https with 2FA
If you have Two-factor authentication enabled for your account, you will need to generate a Personal Access Token.



## List your remote repositories (if any)
```
git remote -v
```

## Push your changes to remote repository
```
git push $remoteRepo $branchname
```
```
git push -u origin master    # set the upstream remote for branch 'master'
```

## Pull updates from remote upstream repository to your local repo
```
git pull
```
Pull = Fetch + Merge
Fetch – downloads branches and/or tags (collectively, “refs”) and objects needed to complete their histories.   
Pull essentially does a fetch and then runs the merge in one step.  

The pull command is combination of a fetch from the remote server and a merge of the changes.

You can do these steps separately, but if you are not working on the branch you are pulling down, pull is just a nice way to get up to date.

When no remote is specified, by default the origin remote will be used, unless there’s an upstream branch configured for the current branch.
