+++
toc = true
date = "2017-03-02T12:47:50Z"
title = "first content"
weight = 1
+++

Digital Ocean 
### Use cases for Digital Ocean

12/13/2019
Digital Ocean Upgrade droplet. Use an external storage volume to store home directory?  Then in the future to change droplets would be simpler?
https://www.digitalocean.com/docs/volumes/
Volumes cost $0.10 per GiB per month and range from 1 GiB to 16 TiB (16,384 GiB). Charges accrue hourly for as long as the volume exists.
Volumes are available in NYC1, NYC3, SFO2, FRA1, SGP1, TOR1, BLR1, LON1, and AMS3.
(Unfortunately, "jack" is in NYC2)
Backups for volumes:  https://www.digitalocean.com/docs/images/snapshots/how-to/snapshot-volumes/
Snapshots are charged at $0.05/GiB per month. Pricing is based on the size of the snapshot, not the size of the filesystem being saved.


