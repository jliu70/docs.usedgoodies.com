+++
title = "- Amazon S3 Glacier"
weight = 120
toc = true
date = "2017-03-13T20:03:26Z"
+++

Originally from `Amazon S3 glacier`

====== AmazonGlacier ======
Created Thursday 03 January 2013


Here’s a cool video podcast which I subscribe to that happens to give a basic overview to Amazon Glacier in their latest episode:
http://theartofphotography.tv/episodes/episode-118-photo-storage-with-amazon-glacier-and-s3/

Glacier  http://aws.amazon.com/glacier/
"Cold Storage"  - Need to store but not need to access very often
Cost about 1cent per gigabyte
You can retrieve up to 5% of your average monthly storage (pro-rated daily) for free each month. If you choose to retrieve more than this amount of data in a month, you are charged a retrieval fee starting at $0.01 per gigabyte.  In addition, there is a pro-rated charge of $0.03 per gigabyte for items deleted prior to 90 days (Penalty if you have to retrieve within 90 days).  
3 to 5 hour wait time for access

http://thegreyblog.blogspot.com/2012/12/amazon-s3-and-glacier-cheap-solution.html
