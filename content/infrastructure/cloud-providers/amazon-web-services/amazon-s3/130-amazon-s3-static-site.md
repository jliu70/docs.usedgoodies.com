+++
title = "- Amazon S3 Static Site"
weight = 130
toc = true
date = "2017-03-04T22:48:28Z"
+++


3/13/2017  

Seems like there's a new version of the AWS console.  Documentation is in "preview" mode.
This is documentation for the preview release of the new Amazon S3 console. To use the new Amazon S3 console, choose Opt In in the following box, which appears on the Amazon S3 console home page.  
http://docs.aws.amazon.com/AmazonS3/latest/user-guide/what-is-s3.html


http://docs.aws.amazon.com/AmazonS3/latest/user-guide/static-website-hosting.html
Directions are straight forward - however, it seems as if the info skips over slightly.
Bucket "Permissions"  - "Bucket Policy"   NOTE: you need to change the Resource name below.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::docs.usedgoodies.com/*"
            ]
        }
    ]
}

```

NOTE: turn on logging to target bucket "logs.docs.usedgoodies.com"  
NOTE: looks like you can only have one bucket use a target logging bucket.  (Was not able to point to logs.usedgoodies.com bucket)

```
target prefix: root/
```


NOTE: seems like the documentation for adding DNS record to Route53 is a bit inadequate
Found this: http://docs.aws.amazon.com/Route53/latest/DeveloperGuide/RoutingToS3Bucket.html

You need to add the Resource Record as "A" record - then set as an "alias" "Yes" - choose the s3 bucket.


Changes generally propagate to all Amazon Route 53 servers in a couple of minutes. In rare circumstances, propagation can take up to 30 minutes. When propagation is done, you'll be able to route traffic to your S3 bucket by using the name of the alias resource record set that you created in this procedure.


You can also "test record set"

Also you can check here for info on negative TTL and related stuff
http://serverfault.com/questions/620406/aws-route-53-alias-record-change-takes-too-long


## NOTE: seems like I'm having problems with the Route53 DNS record - it's not propagating and does not resolve.  Will need to wait I guess.  If it still doesn't work later (or tomorrow) then I'd think something more seriously wrong.
Perhaps Best to just skip to cloudfront then?


### List of S3 endpoints
For a list of Amazon S3 endpoints, see Amazon S3 Regions and Endpoints in the Amazon Web Services General Reference.  
http://docs.aws.amazon.com/general/latest/gr/rande.html#s3_region

### Old version of the documentation
http://docs.aws.amazon.com/AmazonS3/latest/dev/website-hosting-custom-domain-walkthrough.html
http://docs.aws.amazon.com/AmazonS3/latest/dev/HowDoIWebsiteConfiguration.html


https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/resource-record-sets-values-alias.html#rrsets-values-alias-alias-target


### blah


Amazon S3 Static site

Probably should reference from web section static site to here....

Probably also include a reference to web section on static sites....

http://docs.aws.amazon.com/AmazonS3/latest/dev/website-hosting-custom-domain-walkthrough.html

http://docs.aws.amazon.com/AmazonS3/latest/user-guide/static-website-hosting.html


### Amazon S3 Static site

Did I ever document it on the blog?  Does not appear to be the case....


Originally `Only-AmazonS3`


NOTE: 3/1/2011 Very positive feedback from parents in Hawaii, relatives in China.  They all say speed to view photos is much improved.

NOTE: 3/10/2011 - Awaiting feedback from relatives in China for new Tokyo Amazon datacenter.    3/16/11 Sherry's Uncle reports that Tokyo is faster.


For more information about Amazon S3, see Home:Tech:Web:AmazonS3intro


Amazon S3 now allows you to directly host your static webpages and "enable website functionality" - You can now host an entire website on Amazon S3.  The only limitation is that Amazon S3 can only be used for hosting HTML websites and not dynamic sites like WordPress blogs.  No database backend means that for photo albums you will need to use a javascript gallery instead of a full-blown gallery (e.g., visual lightbox - link below).



Resources:

New AWS feature: Run your website from Amazon S3
http://www.allthingsdistributed.com/2011/02/website_amazon_s3.html
Free at Last - A Fully Self-Sustained Blog Running in Amazon S3
http://www.allthingsdistributed.com/2011/02/weblog_in_amazon_s3.html
Next step: replace my Movable Type installation with Jekyll so blog generation becomes simpler as well.

Tutorial: How to Host Your Website on Amazon S3
http://www.labnol.org/internet/web-hosting-with-amazon-s3/18742/

Amazon S3 Docs: Setting Up a Website
http://docs.amazonwebservices.com/AmazonS3/latest/dev/index.html?WebsiteHosting.html

Amazon S3 Docs: website hosting custom domain walkthrough  
http://docs.aws.amazon.com/AmazonS3/latest/dev/website-hosting-custom-domain-walkthrough.html

Setting up DNS entries to an Amazon bucket
http://serverfault.com/questions/169044/alias-multiple-dns-entries-to-one-amazon-s3-bucket


The following table lists Amazon S3 regions and corresponding website endpoints.
Region	Website endpoint
US Standard	s3-website-us-east-1.amazonaws.com
US West (Northern California) Region
s3-website-us-west-1.amazonaws.com
EU (Ireland) Region
s3-website-eu-west-1.amazonaws.com
Asia Pacific (Singapore) Region
s3-website-ap-southeast-1.amazonaws.com
Asia Pacific (Tokyo) Region
s3-website-ap-northeast-1.amazonaws.com


Beginning Nov 1, 2010, Amazon S3 is free for one year - then standard rates apply.
Limitations: 5GB of Amazon S3 storage, 20000 Get Requests, 2000 Put Requests
30GB of data transfer
http://aws.amazon.com/free/

http://aws.amazon.com/s3/#pricing
Storage:  First 1TB $0.140 per GB  or  Reduced Redundancy Storage First 1TB $0.093 per GB.


Tools to interface with Amazon S3
Only-AmazonS3:AmazonS3tools



NOTE: create multiple buckets "storytellersphotos.com", "www.storytellersphotos.com"
For the bucket "storytellersphotos.com" create a simple index.html with the redirect and a simple notfound.html file.  For example, storytellersphotos.com bucket with this index.html redirect to www.storytellers.com bucket.

```
 <html><head>
  <meta http-equiv="Refresh" content="0; url=http://www.storytellersphotos.com/" />
</head><body>
  <p>Please follow <a href="http://www.storytellersphotos.com/">link</a>!</p>
</body></html>
```

Create buckets for photos2web.com and www.photos2web.com similar to above.

[*] Next: transfer everything from "storytellersphotos.com" bucket to "www.photos2web.com"  - be sure to create "www.photos2web.com" bucket in "Northern California" Region
[ ] Delete "storytellersphotos.com" bucket contents - add a redirect to "www.photos2web.com"




Comparison of different lightboxes
http://planetozh.com/projects/lightbox-clones/

Topup lightbox supports video files natively, but no automation like visuallightbox
http://gettopup.com/documentation




Sent: Monday, May 17, 2010 11:53 AM
To: Robert Gil
Cc: Zheng Xu; Faneendra Pisupati; Val Zhupan; Philip Pactor

http://visuallightbox.com/

Overview
Visual LightBox JS is a free wizard program that helps you easily generate web photo galleries with a nice Lightbox-style overlay effect, in a few clicks without writing a single line of code.
Just drag&drop your photos into Visual LightBox window, press "Publish" and your own web gallery with beautiful LightBox 2 effects will open in the browser instantly!
No javascript,css,html coding, no image editing, just a click to get your gallery ready.


http://visuallightbox.com/#FAQ
Q:How do I embed the lightbox image gallery into an existing page?

For videos from files/directory, from URL Web-hosted Videos (YouTube, Vimeo, etc):
http://videolightbox.com/
NOTE: videolightbox generates files which match the directory structure and also will co-exist with visuallightbox.   
IMPORTANT: the version of jQuery tools they deploy (1.0.5) is outdated and breaks the image lightbox after you add video.  I was able to download 1.2.5 and copy/replace jquery.tools.min.js which worked. ( cp ~/jquery.tools.min.js engine/js/)
Merging output from visuallightbox and videolightbox works.  The previous thumbnails and videos are never removed, and it was just a matter of merging the index.html files with the additional relevant entries.  Also, their index.html file has DOS carriage returns.  This Mac terminal command helps do the same thing as dos2unix:  cat index.html | tr -d '\r' > index.html.visual
[ ] 4/11/2011 - New version of visuallightbox with updated jQuery core.  Test to see if they have updated jQuery tools.  Support for multiple galleries on a web page.  Confirm integration with Video Lightbox.
[ ] 7/12/2011 - Consider using LR Web Engine to generate HTML web galleries.  Not as "nifty" as lightbox, but maybe it's easier to integrate in the workflow?  See Home:PhotoNotes:Lightroom





I think a good "directory" structure to follow is:
webroot/photos/YYYY/MM/DD
webroot/photos/specialDirectory
webroot/blog



    Workflow for combining photos/videos
1. Generate photos via visuallightbox first (be sure to specify correct directory location under Publish tab)
2. Run "cat index.html | tr -d '\r' > index.html.visual"  and remove index.html
3a. Generate videolightbox to same directory (be sure to specify "Play button" overlay under "Customize thumbnails" tab and correct directory location under Publish tab)
3b. Also run "cat index.html | tr -d '\r' > index.html.video"
4. Copy and paste videolightbox HEAD and BODY fields from index.html to index.html.visual.  
5. cp ~/jquery.tools.min.js engine/js/
6. Update photos/index.html with a link to the new album
7a. Use "S3Hub" to upload the photos/index.html and new album to the website
7b. Alternatively use "s3cmd" to sync - see Home:Tech:Web:Only-AmazonS3:AmazonS3tools:s3cmd
8. Use "s3cmd" to sync buckets to ap.photos2web.com (NOTE: copying data across S3 regions still triggers data transfer charges)
 3/1/2011 - I sent an email to them asking them to upgrade jQuery tools to 1.2.5 and if it's possible to combine into one tool.  Customer support replied that they will forward request to developers, but I have no idea if that means anything.   See above 4/11/2011 note.





Try CSS menumaker (free): http://cssmenumaker.com
For inspiration, try to model after neilvn.com or bcphoto.biz

Maybe just better to create a manual index.html page.  (Hey, remember the temp/webpages/index.html from my original site? :) )
I can write a script which can scan the directory tree and generate index.html files for each level.

The following is another  non-free product which can generate a fancy menu:
http://apycom.us/jquery-menu/
And they also offer non-free:
http://css3menu.com/index.html#new
