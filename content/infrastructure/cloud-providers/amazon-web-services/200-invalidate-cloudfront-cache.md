+++
toc = true
date = "2017-03-04T22:45:48Z"
title = "invalidate cloudfront cache"
weight = 200
+++

#### NOTE: at this point I'm leaning towards just keep it a "basic" S3 static site since I'm still making numerous updates, and it takes too long to invalidate cache.  


Originally from `2016-02-05`



#### Last edited: 1/8/2016 - Invalidating Cloudfront cache
http://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/Invalidation.html

http://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/cache-hit-ratio.html

http://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/cache-statistics.html


[link-1]: https://pages.github.com
