+++
title = "aws iam rotate access keys"
weight = 11
toc = true
date = "2017-03-06T19:50:06Z"
+++

from zim-jliu-projects wiki "web" page.  (Also copied to Apple Notes)
[ ] 10/2/2015  AWS IAM - best practices - look into updating S3cmd scripts to use new role account:
http://docs.aws.amazon.com/IAM/latest/UserGuide/best-practices.html


Originally `2016-09-23`


[Blog post rotate access keys for AWS IAM users][link-1]


Running aws in a docker container.  Based off [this docker image][link-6]

```
aws configure

$ docker run --rm -ti aws:latest
6e56e11d9559:/tmp# aws configure
AWS Access Key ID [None]:

```

Also mentioned here:  https://lostechies.com/gabrielschenker/2016/09/21/easing-the-use-of-the-aws-cli/
But they have an imperfect setup - image is too large.
However in the user comments section there are quite a few discussions on correct security approach to secrets.

Which got me to do a google search `using awscli with vault -glacier hashi`
https://www.vaultproject.io/docs/secrets/aws/
https://gist.github.com/mlimotte/6422690a9d914c954068acca29df594a
https://www.promptworks.com/blog/handling-environment-secrets-in-docker-on-the-aws-container-service
https://medium.com/@alfonso_cabrera/terraform-security-with-aws-vault-901b64c72003#.b5we8pkym
https://github.com/fugue/credstash/blob/master/README.md
  https://blogs.aws.amazon.com/security/post/Tx3D6U6WSFGOK2H/A-New-and-Standardized-Way-to-Manage-Credentials-in-the-AWS-SDKs





[link-1]: https://blogs.aws.amazon.com/security/post/Tx15CIT22V4J8RP/How-to-rotate-access-keys-for-IAM-users
[link-2]: http://blogs.aws.amazon.com/security/post/Tx1GZCHQC7LR3UT/New-in-IAM-Quickly-Identify-When-an-Access-Key-Was-Last-Used
[link-3]: http://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html?icmpid=docs_iam_console#Using_RotateAccessKey
[link-4]: https://damnhandy.com/2015/09/10/how-putting-credentials-in-git-can-cost-you-at-least-6500-in-just-a-few-hours/
[link-5]: http://docs.aws.amazon.com/cli/latest/userguide/getting-help.html
[link-6]: https://github.com/cgswong/docker-aws
