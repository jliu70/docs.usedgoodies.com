+++
date = "2017-03-03T19:42:01Z"
title = "Docker Swarm"
weight = -1500
chapter = true
icon = "<b>2. </b>"

+++

### Overview

# Docker Swarm

As of 2019 Kubernetes has won the container orchestration war.

November 2019
Docker Enterprise sold to company Mirantis
https://thenewstack.io/mirantis-acquires-docker-enterprise/

Docker goes back to its roots
https://mailchi.mp/thenewstack/docker-goes-back-to-its-roots?e=5b4dfb0fc4
