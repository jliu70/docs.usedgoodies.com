+++
toc = true
date = "2017-03-02T12:47:50Z"
title = "second content"
weight = 100
+++


# k3s
k3s takes a unique approach of stripping-down Kubernetes to its bare essentials and makes it perfect for running in edge locations. Given that less resources are needed, the spend may be lower, and a LoadBalancer (10-15 USD) is not required because of the way k3s exposes traffic through HostPorts.
https://github.com/rancher/k3s
https://k3s.io/
https://rancher.com/blog/2019/2019-02-26-introducing-k3s-the-lightweight-kubernetes-distribution-built-for-the-edge/


K3s was released to general availability on November 19, 2019. IoT and edge systems are pushing the limits of where containers and Kubernetes can run. 
https://rancher.com/blog/2019/why-k3s-is-the-future-of-k8s-at-the-edge


## k3sup
https://blog.alexellis.io/create-a-3-node-k3s-cluster-with-k3sup-digitalocean/
https://github.com/alexellis/k3sup


## Doctl
https://www.digitalocean.com/community/tutorials/how-to-use-doctl-the-official-digitalocean-command-line-client


https://github.com/digitalocean/doctl

https://github.com/digitalocean/doctl/blob/master/README.md
NOTE: this README walks you through some basics like Authenticating with Digital Ocean
```
doctl auth init
```



https://sumit.murari.me/doctl-command-snippets.html
 
 
NOTE: Was not able to run doctl auth init from git bash – I had to open the command prompt.
 
Thereafter I was able to use the git bash to run doctl commands


https://www.digitalocean.com/community/questions/what-is-the-exact-doctl-command-to-create-a-droplet-of-given-specification



```
$ doctl compute ssh-key list
ID         Name    FingerPrint
8620180    jliu    ef:44:79:93:81:84:67:29:7e:a4:29:64:08:aa:eb:f5
 
 
$ doctl compute image list-distribution
ID          Name                 Type        Distribution     Slug                    Public    Min Disk
31354013    6.9 x32              snapshot    CentOS           centos-6-x32            true      20
33948356    28 x64               snapshot    Fedora           fedora-28-x64           true      20
34114584    28 x64 Atomic        snapshot    Fedora Atomic    fedora-28-x64-atomic    true      20
34902021    6.9 x64              snapshot    CentOS           centos-6-x64            true      20
43507983    v1.4.3               snapshot    RancherOS        rancheros-1.4           true      20
45203822    29 x64               snapshot    Fedora           fedora-29-x64           true      20
47384041    30 x64               snapshot    Fedora           fedora-30-x64           true      20
49532121    12.0 x64 zfs         snapshot    FreeBSD          freebsd-12-x64-zfs      true      20
49532140    12.0 x64 ufs         snapshot    FreeBSD          freebsd-12-x64          true      20
49532500    11.3 x64 zfs         snapshot    FreeBSD          freebsd-11-x64-zfs      true      20
49532502    11.3 x64 ufs         snapshot    FreeBSD          freebsd-11-x64-ufs      true      20
50903182    7.6 x64              snapshot    CentOS           centos-7-x64            true      20
51286794    v1.5.4               snapshot    RancherOS        rancheros               true      20
53621447    9.7 x64              snapshot    Debian           debian-9-x64            true      20
53624336    10.0 x64             snapshot    Debian           debian-10-x64           true      20
53871280    19.10 x64            snapshot    Ubuntu           ubuntu-19-10-x64        true      20
53884440    19.04 x64            snapshot    Ubuntu           ubuntu-19-04-x64        true      20
53893572    18.04.3 (LTS) x64    snapshot    Ubuntu           ubuntu-18-04-x64        true      20
54203610    16.04.6 (LTS) x32    snapshot    Ubuntu           ubuntu-16-04-x32        true      20
54639737    2247.6.0 (stable)    snapshot    CoreOS           coreos-stable           true      20
54640535    2317.0.1 (alpha)     snapshot    CoreOS           coreos-alpha            true      20
54640536    2303.1.1 (beta)      snapshot    CoreOS           coreos-beta             true      20
55022766    16.04.6 (LTS) x64    snapshot    Ubuntu           ubuntu-16-04-x64        true      20
 
 
$ doctl compute region list
Slug    Name               Available
nyc1    New York 1         true
nyc2    New York 2         true
sgp1    Singapore 1        true
lon1    London 1           true
nyc3    New York 3         true
ams3    Amsterdam 3        true
fra1    Frankfurt 1        true
tor1    Toronto 1          true
sfo2    San Francisco 2    true
blr1    Bangalore 1        true
 
 
```
