+++
toc = true
date = "2017-03-02T12:47:50Z"
title = "Kubernetes Resources"
weight = 900
+++

11/24/2019
https://www.oreilly.com/ideas/kubernetes-a-simple-overview

https://www.oreilly.com/radar/kubernetes-for-the-impatient/


Consul on Kubernetes
https://www.consul.io/docs/platform/k8s/run.html
https://github.com/kelseyhightower/consul-on-kubernetes
https://testdriven.io/blog/running-vault-and-consul-on-kubernetes/



11/22/2019
https://medium.com/better-programming/using-a-k3s-kubernetes-cluster-for-your-gitlab-project-b0b035c291a9

https://rancher.com/docs/rancher/v2.x/en/cluster-provisioning/hosted-kubernetes-clusters/eks/

https://rancher.com/blog/2019/era-of-multi-cluster-multi-cloud-kubernetes-has-arrived/



https://news.ycombinator.com/item?id=18080390
The operational complexity today is too high. Kubernetes is a big business system, it caters to enterprise. If you aren't an enterprise I would caution you against k8s.

For sure running k8s is difficult, I don't think anybody would disagree with me there, but of course you can buy GKE. I have to point out that GKE is the only decent k8s implementation, EKS and AKS are not real contenders. Digital Ocean will launch their service, I've seen it and it's good and I hope they do well. But in general you can't really expect the Kubernetes landscape to change much from what it is today, which is basically GKE good, everything else meh. Why? Kubernetes is not really a proven business model for cloud providers yet. It costs the cloud provider more to offer Kubernetes and it not significantly drawing more users in (and consuming more). The obvious example of this is AWS lack luster support and commitment to EKS. If there was a big business opportunity Amazon would be doing more. I have no concrete proof but ECS and Fargate stand to make much more money for Amazon (just a guess). So in general the argument that running on GKE isn't bad I don't think holds up because GKE is the only decent option. DO has yet to launch, but they can't really standup to GKE because you have to look at things holistically. Just running the cluster is like 10% of the whole solution. You need to then figure out how to do authentication, authorization, load balancing (built in service LB and ingress are not sufficient), monitoring, logging, and disaster recovery. Google has a product in each of these areas, but they have 100x more resources than DO. Basically nobody can (or is willing to) put the money into K8s like Google does and Google has yet to prove that their investment is worth it.
That's not to say I don't think Kubernetes will survive. I do think it will survive and it will be around for awhile. Because it's a good fit for enterprise right now. Not a good cloud provider business, not good for small dev shops, but good for enterprise.

Containers have a lot of value. My rule of thumb is basically use containers (docker) and as little as possible of anything else in the container ecosystem. So basically put your app in a container try to not change too much from however you currently do things. There is no shame in running --net=host or just doing "-v /mnt/someebsvolume:/data." I personally like Hashicorp because their tools are simple and grow with you. Besides Hashicorp, I don't have great recommendations for tools. In my opinion the current container ecosystem is a disaster for users. Big business ideas took over and killed innovation. Simple lightweight modular systems all got annihilated by Kubernetes and now it's pretty much a monoculture.

k3s is not intended to be a competitor, but let's say I wanted to build it out to compete with systems. Swarm excels in UX and cluster management. It is easy to run and use. But it is incomplete in features and functionality. So k3s addresses the cluster management issues and has tons of features. It makes k8s easy to run, but it still has Kubernetes UX which is quite difficult. So in the end I think it is a wash between k3s and Swarm. You either get hard to use (k3s) or incomplete (Swarm). I have a different project Rio (https://github.com/rancher/rio) that attempts to address UX (and it's built on k3s). So Rio could possibly be a system that is easy to use, powerful, and simple to run. I think Rio could be something that is better than both raw k8s and Swarm for regular users, but it's a very early.


Rio in Beta
https://rancher.com/blog/2019/introducing-rio/
Rio is a MicroPaaS that can be layered on any standard Kubernetes cluster. Consisting of a few Kubernetes custom resources and a CLI to enhance the user experience, users can easily deploy services to Kubernetes and automatically get continuous delivery, DNS, HTTPS, routing, monitoring, autoscaling, canary deployments, git-triggered builds, and much more. All it takes to get going is an existing Kubernetes cluster and the rio CLI.




https://dzone.com/articles/lightweight-kubernetes-k3s-installation-and-spring



11/19/2019
https://www.redhat.com/en/blog/red-hat-introduces-open-source-project-quay-container-registry

https://www.projectquay.io/

https://access.redhat.com/documentation/en-us/red_hat_quay/3/html/deploy_red_hat_quay_-_basic/preparing_for_red_hat_quay_basic

https://access.redhat.com/solutions/3533201

https://github.com/quay/quay

https://www.redhat.com/en/technologies/cloud-computing/quay

https://www.redhat.com/en/resources/quay-datasheet



12/5/2019
Kubernetes and OpenShift - What's the difference?  8/1/2019
https://www.youtube.com/watch?v=cTPFwXsM2po


12/6/2019
A visual guide on troubleshooting k8s deployments
https://learnk8s.io/troubleshooting-deployments
https://news.ycombinator.com/item?id=21711748



11/15/2019
CNCF Cloud Native Interactive Landscape
https://landscape.cncf.io/



