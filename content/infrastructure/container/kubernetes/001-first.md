+++
toc = true
date = "2017-03-02T12:47:50Z"
title = "Kubernetes Introduction"
weight = 1
+++

# Kubernetes 

## Architecture 

Some notes from VMware KubeAcademy 

https://kube.academy/lessons/kubernetes-architecture

### Control plane (similar to vCenter)

- API Server
- Scheduler
- Controller Manager

### Nodes   (similar to ESXi Hosts)
- Where your workload runs

### Data  (etcd)




## Other Learning Resources

## Videos

Getting started with Kubernetes and its ecosystem
https://blogs.vmware.com/cloudnative/2019/10/15/getting-started-kubernetes/

VMware KubeAcademy 
https://kube.academy/
https://kube.academy/lessons/kubernetes-architecture


## Web articles

### Great Digital Ocean curriculum
https://www.digitalocean.com/community/curriculums/kubernetes-for-full-stack-developers


### Other Digital Ocean tutorials
https://www.digitalocean.com/resources/kubernetes/

https://www.digitalocean.com/community/tutorials/an-introduction-to-kubernetes
https://www.digitalocean.com/community/tutorials/an-introduction-to-the-kubernetes-dns-service

https://www.digitalocean.com/community/tutorials/an-introduction-to-helm-the-package-manager-for-kubernetes

https://www.digitalocean.com/community/tutorials/how-to-inspect-kubernetes-networking

https://www.digitalocean.com/community/tutorials/modernizing-applications-for-kubernetes

https://www.digitalocean.com/community/tutorials/how-to-back-up-and-restore-a-kubernetes-cluster-on-digitalocean-using-heptio-ark


### Digital Ocean Managed Kubernetes
https://blog.digitalocean.com/digitalocean-releases-k8s-as-a-service/




### Prometheus talk
https://kccna18.sched.com/event/GrXX/adopting-prometheus-the-hard-way-tim-simmons-digitalocean
https://static.sched.com/hosted_files/kccna18/b4/Adopting%20Prometheus%20the%20Hard%20Way.pdf

