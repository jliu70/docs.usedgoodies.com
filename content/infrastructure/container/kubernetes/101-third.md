+++
toc = true
date = "2017-03-02T12:47:50Z"
title = "third content"
weight = 101
+++



11/15/2019
#kubernetes - really good looking free courses from Digital Ocean community
https://www.digitalocean.com/community/curriculums/kubernetes-for-full-stack-developers#3.Containers

#kubernetes - #next     give the following a try
https://blog.alexellis.io/create-a-3-node-k3s-cluster-with-k3sup-digitalocean/
NOTE: just remember that Droplets are still charged when powered off.  You’ll need to destroy them to stop getting charged for them.  For a test k3s environment, not a big deal to destroy them I guess.



Challenge - AXA network blocks ssh - can redirect to 443
Confirmed that doctl and k3sup can both specify custom SSH ports (e.g., 443)

Install k3sup. 
curl -sLSf https://get.k3sup.dev | sh


•	First thing to check is if doctl works at work at all. (It should if it’s just making HTTPS API calls.  But DigitalOcean might be blocked.)
If doctl works then we should consider creating a new Digital Ocean image.    

•	Creating a Digital Ocean image with SSH listening on port 443
This GitHub issue references how we can create a snapshot of a Droplet (powered off after we change ssh to 443)
https://github.com/docker/machine/issues/3702
provision a fresh droplet yourself so that sshd listens on port 234, make a snapshot and use your custom image id in --digitalocean-image
https://www.digitalocean.com/docs/images/

doctl auth init
Enter the API key which was created (and I saved to 1Password)


Create Tag (note: if you specify a tag for droplet creation, it must already exist)
doctl compute tag create template
doctl compute tag create k3s


doctl compute tag list
Name        Droplet Count
k3s         0
template    0




•	Here’s the command that we should use to create a base VM to prep 
doctl compute droplet create jliu-ubuntu-18-04-x64 --size s-1vcpu-1gb  --image ubuntu-18-04-x64 --region nyc3 --ssh-keys 8620180 --enable-private-networking --tag-name template

NOTE: ssh key did not get installed  - and email with temporary password was sent



•	Get the droplet ID
./doctl compute droplet list --format "ID,Name,PublicIPv4"
ID        Name    Public IPv4
432146    jack    192.241.169.43


•	jliu@JEFFREYs-MacBook-Pro ~/test $ ./doctl compute droplet list
ID           Name                     Public IPv4       Private IPv4     Public IPv6    Memory    VCPUs    Disk    Region    Image                       Status    Tags        Features                     Volumes
432146       jack                     192.241.169.43    10.128.48.214                   512       1        20      nyc2      Ubuntu Ubuntu 12.04 x64     active                virtio,private_networking    
167367927    jliu-ubuntu-18-04-x64    138.197.102.77    10.132.106.66                   1024      1        25      nyc3      Ubuntu 18.04.3 (LTS) x64    off       template    private_networking           

jliu@JEFFREYs-MacBook-Pro ~/test $ ./doctl compute droplet list --format "ID,Name,PublicIPv4"
ID           Name                     Public IPv4
432146       jack                     192.241.169.43
167367927    jliu-ubuntu-18-04-x64    138.197.102.77





Login and reconfigure the droplet sshd to listen to port 443

Shutdown the droplet

Then Create a snapshot
Use the snapshot as an image - wonder if the snapshot is still valid if we delete the source droplet - worth testing.
./doctl compute droplet-action snapshot 167367927 --snapshot-name ubuntu-18-04-x64-443


$ ./doctl compute snapshot list
ID          Name                    Created at              Regions    Resource ID    Resource Type    Min Disk Size    Size        Tags
55051610    ubuntu-18-04-x64-443    2019-11-16T19:53:18Z    [nyc3]     167367927      droplet          25               1.03 GiB    


NOTE: a slight delay before the image is listed
jliu@JEFFREYs-MacBook-Pro ~/test $ ./doctl compute image list-user 
ID          Name                    Type        Distribution    Slug    Public    Min Disk
55051610    ubuntu-18-04-x64-443    snapshot    Ubuntu                  false     25




•	See if you can create a new droplet from the user image:

./doctl compute droplet create k3smaster1 --size s-1vcpu-1gb  --image 55051610 --region nyc3 --ssh-keys 8620180  --enable-private-networking --tag-name k3s
             
•	List droplets
jliu@JEFFREYs-MacBook-Pro ~/test $ ./doctl compute droplet list
ID           Name          Public IPv4        Private IPv4     Public IPv6    Memory    VCPUs    Disk    Region    Image                          Status    Tags    Features                     Volumes
432146       jack          192.241.169.43     10.128.48.214                   512       1        20      nyc2      Ubuntu Ubuntu 12.04 x64        active            virtio,private_networking    
167404246    k3smaster1    134.209.175.108    10.132.118.17                   1024      1        25      nyc3      Ubuntu ubuntu-18-04-x64-443    active    k3s     private_networking           


NOTE:  SSH is now correctly set — looks like it was the “—“ which was being autocorrected by Notes to a single dash for ssh-keys (and possibly other parameters) which threw doctl into a mess.  IT WORKS NOW.



NOTE: if you delete the snapshot, the image is gone too.

jliu@JEFFREYs-MacBook-Pro ~/test $ ./doctl compute snapshot delete 55051415
Warning: Are you sure you want to delete snapshot(s) (y/N) ? y
jliu@JEFFREYs-MacBook-Pro ~/test $ ./doctl compute image list-user 
ID    Name    Type    Distribution    Slug    Public    Min Disk


NOTE: if you delete the original VM, it seems that the snapshot and image still lives


 $ ./doctl compute snapshot list

ID          Name                    Created at              Regions    Resource ID    Resource Type    Min Disk Size    Size        Tags
55051610    ubuntu-18-04-x64-443    2019-11-16T19:53:18Z    [nyc3]     167367927      droplet          25               1.03 GiB    
jliu@JEFFREYs-MacBook-Pro ~/test $ 
jliu@JEFFREYs-MacBook-Pro ~/test $ ./doctl compute image list-user 
ID          Name                    Type        Distribution    Slug    Public    Min Disk
55051610    ubuntu-18-04-x64-443    snapshot    Ubuntu                  false     25



•	Deploy k3s  - ISSUE:  seems to prevent SSH afterwards  


Finally k3s install works on port 443
$ ./k3sup-darwin install --ip 167.172.238.70 --ssh-port 443 
Public IP: 167.172.238.70
ssh -i /Users/jliu/.ssh/id_rsa root@167.172.238.70
ssh: curl -sLS https://get.k3s.io | INSTALL_K3S_EXEC='server --tls-san 167.172.238.70 ' INSTALL_K3S_VERSION='v0.9.1' sh -

[INFO]  Using v0.9.1 as release
[INFO]  Downloading hash https://github.com/rancher/k3s/releases/download/v0.9.1/sha256sum-amd64.txt
[INFO]  Downloading binary https://github.com/rancher/k3s/releases/download/v0.9.1/k3s
[INFO]  Verifying binary download
Created symlink /etc/systemd/system/multi-user.target.wants/k3s.service → /etc/systemd/system/k3s.service.
[INFO]  Installing k3s to /usr/local/bin/k3s
[INFO]  Creating /usr/local/bin/kubectl symlink to k3s
[INFO]  Creating /usr/local/bin/crictl symlink to k3s
[INFO]  Creating /usr/local/bin/ctr symlink to k3s
[INFO]  Creating killall script /usr/local/bin/k3s-killall.sh
[INFO]  Creating uninstall script /usr/local/bin/k3s-uninstall.sh
[INFO]  env: Creating environment file /etc/systemd/system/k3s.service.env
[INFO]  systemd: Creating service file /etc/systemd/system/k3s.service
[INFO]  systemd: Enabling k3s unit
[INFO]  systemd: Starting k3s
 Created symlink /etc/systemd/system/multi-user.target.wants/k3s.service → /etc/systemd/system/k3s.service.

ssh: sudo cat /etc/rancher/k3s/k3s.yaml

apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUJXRENCL3FBREFnRUNBZ0VBTUFvR0NDcUdTTTQ5QkFNQ01DTXhJVEFmQmdOVkJBTU1HR3N6Y3kxelpYSjIKWlhJdFkyRkFNVFUzTXprek5EVTRNREFlRncweE9URXhNVFl5TURBek1EQmFGdzB5T1RFeE1UTXlNREF6TURCYQpNQ014SVRBZkJnTlZCQU1NR0dzemN5MXpaWEoyWlhJdFkyRkFNVFUzTXprek5EVTRNREJaTUJNR0J5cUdTTTQ5CkFnRUdDQ3FHU000OUF3RUhBMElBQkZCUGtZNkZyeS9PSkZTUHNySEpMbzJnWWd1MG0ydS9FSGFlcnB3K0tYOXYKeWoyZm5qUmZXK2RYRVViTEZnK2FXbjhWQStQcmRtd1NBUEZoeFZpRHkvR2pJekFoTUE0R0ExVWREd0VCL3dRRQpBd0lDcERBUEJnTlZIUk1CQWY4RUJUQURBUUgvTUFvR0NDcUdTTTQ5QkFNQ0Ewa0FNRVlDSVFEV3VBOUVVeWRPClBycmp0NWhTdjJYTk5WSDFYTmNHZFkyTlJRVCsrTStLRlFJaEFOMVNrYXA5bE4yTktsb1VRSG5BL3lHL3Rjb2cKeXNaOU1zZm9YYy9wOHJYeAotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg==
    server: https://127.0.0.1:6443
  name: default
contexts:
- context:
    cluster: default
    user: default
  name: default
current-context: default
kind: Config
preferences: {}
users:
- name: default
  user:
    password: 86e2cabe8b228d724947941bd74baf86
    username: admin
Result: apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUJXRENCL3FBREFnRUNBZ0VBTUFvR0NDcUdTTTQ5QkFNQ01DTXhJVEFmQmdOVkJBTU1HR3N6Y3kxelpYSjIKWlhJdFkyRkFNVFUzTXprek5EVTRNREFlRncweE9URXhNVFl5TURBek1EQmFGdzB5T1RFeE1UTXlNREF6TURCYQpNQ014SVRBZkJnTlZCQU1NR0dzemN5MXpaWEoyWlhJdFkyRkFNVFUzTXprek5EVTRNREJaTUJNR0J5cUdTTTQ5CkFnRUdDQ3FHU000OUF3RUhBMElBQkZCUGtZNkZyeS9PSkZTUHNySEpMbzJnWWd1MG0ydS9FSGFlcnB3K0tYOXYKeWoyZm5qUmZXK2RYRVViTEZnK2FXbjhWQStQcmRtd1NBUEZoeFZpRHkvR2pJekFoTUE0R0ExVWREd0VCL3dRRQpBd0lDcERBUEJnTlZIUk1CQWY4RUJUQURBUUgvTUFvR0NDcUdTTTQ5QkFNQ0Ewa0FNRVlDSVFEV3VBOUVVeWRPClBycmp0NWhTdjJYTk5WSDFYTmNHZFkyTlJRVCsrTStLRlFJaEFOMVNrYXA5bE4yTktsb1VRSG5BL3lHL3Rjb2cKeXNaOU1zZm9YYy9wOHJYeAotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg==
    server: https://127.0.0.1:6443
  name: default
contexts:
- context:
    cluster: default
    user: default
  name: default
current-context: default
kind: Config
preferences: {}
users:
- name: default
  user:
    password: 86e2cabe8b228d724947941bd74baf86
    username: admin
 
Saving file to: /Users/jliu/test/kubeconfig



•	Edit the IP address to LB and port to 443 if you implement the Load balancer
export KUBECONFIG=`pwd`/kubeconfig

•	NOTE: kubectl won’t work from office laptop because of port 6443 outbound to Internet will be blocked.
jliu@JEFFREYs-MacBook-Pro ~/test $ netstat -an |grep 443 |grep 134.209
tcp4       0      0  192.168.1.174.64927    134.209.175.108.6443   ESTABLISHED
tcp4       0      0  192.168.1.174.64926    134.209.175.108.6443   ESTABLISHED

NOTE: at work can use Digital Ocean load balancer to redirect 443 to 6443
https://www.digitalocean.com/docs/networking/load-balancers/


jliu@JEFFREYs-MacBook-Pro ~/doctl-k3sup $ ./doctl compute load-balancer create --forwarding-rules entry_protocol:tcp,entry_port:443,target_protocol:tcp,target_port:6443 --health-check protocol:tcp,port:6443 --name kubectl --region nyc3 --droplet-ids 167408104
ID                                      IP    Name       Status    Created At              Algorithm      Region    Tag    Droplet IDs    SSL      Sticky Sessions                                Health Check                                                                                                                   Forwarding Rules
5b4c1a12-1d34-4aaa-9a40-72a4042f5a35          kubectl    new       2019-11-17T04:01:34Z    round_robin    nyc3             167408104      false    type:none,cookie_name:,cookie_ttl_seconds:0    protocol:tcp,port:6443,path:,check_interval_seconds:10,response_timeout_seconds:5,healthy_threshold:5,unhealthy_threshold:3    entry_protocol:tcp,entry_port:443,target_protocol:tcp,target_port:6443,certificate_id:,tls_passthrough:false


$  ./doctl compute load-balancer list
ID                                      IP                 Name       Status    Created At              Algorithm      Region    Tag    Droplet IDs    SSL      Sticky Sessions                                Health Check                                                                                                                   Forwarding Rules
5b4c1a12-1d34-4aaa-9a40-72a4042f5a35    174.138.117.199    kubectl    active    2019-11-17T04:01:34Z    round_robin    nyc3             167408104      false    type:none,cookie_name:,cookie_ttl_seconds:0    protocol:tcp,port:6443,path:,check_interval_seconds:10,response_timeout_seconds:5,healthy_threshold:5,unhealthy_threshold:3    entry_protocol:tcp,entry_port:443,target_protocol:tcp,target_port:6443,certificate_id:,tls_passthrough:false



$ ./kubectl get node -o wide
Unable to connect to the server: x509: certificate is valid for 167.172.225.57, 127.0.0.1, 10.43.0.1, not 174.138.117.199

•	Include “--insecure-skip-tls-verify” option for Kubectl to allow it to work to LB
jliu@JEFFREYs-MacBook-Pro ~/doctl-k3sup $ ./kubectl --insecure-skip-tls-verify get node -o wide
NAME         STATUS   ROLES    AGE     VERSION         INTERNAL-IP      EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
k3smaster1   Ready    master   3m15s   v1.15.4-k3s.1   167.172.225.57   <none>        Ubuntu 18.04.3 LTS   4.15.0-66-generic   containerd://1.2.8-k3s.1





NOTE: k3s is not supported on MacOS or Windows.    

https://github.com/rancher/k3s/issues/88

Other options: 
Seems like you will need to run with a lightweight Linux VM - one option is Multipass
https://multipass.run/
https://news.ycombinator.com/item?id=21836528
https://github.com/CanonicalLtd/multipass
https://medium.com/@zhimin.wen/running-k3s-with-multipass-on-mac-fbd559966f7c

Other options discussed here. (Including K3d - run k3s in Docker)
https://blog.zeerorg.site/post/k3d-kubernetes-dev-env
https://rancher.com/docs/k3s/latest/en/advanced/


•	This one works
What about just installing kubectl on Mac?
https://rancher.com/blog/2019/how-to-manage-kubernetes-with-kubectl/
https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-macos

https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-windows


————

jliu@JEFFREYs-MacBook-Pro ~/test $ ./kubectl get node -o wide
NAME         STATUS   ROLES    AGE   VERSION         INTERNAL-IP       EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
k3smaster1   Ready    master   71s   v1.15.4-k3s.1   134.209.175.108   <none>        Ubuntu 18.04.3 LTS   4.15.0-66-generic   containerd://1.2.8-k3s.1



jliu@JEFFREYs-MacBook-Pro ~/test $ ./kubectl run -t -i curl --image alexellis/curl:0.1.1 -- /bin/sh
kubectl run --generator=deployment/apps.v1 is DEPRECATED and will be removed in a future version. Use kubectl run --generator=run-pod/v1 or kubectl create instead.

jliu@JEFFREYs-MacBook-Pro ~/test $ ./kubectl get pods
NAME                    READY   STATUS    RESTARTS   AGE
curl-5664f756cb-rrwc7   1/1     Running   0          117s


jliu@JEFFREYs-MacBook-Pro ~/test $ ./kubectl exec -t -i curl-5664f756cb-rrwc7 /bin/sh
~ $ curl 'https://api6.ipify.org?format=json' ; echo
{"ip":"167.172.233.245"}

jliu@JEFFREYs-MacBook-Pro ~/test $ ./kubectl delete deploy/curl
deployment.extensions "curl" deleted

jliu@JEFFREYs-MacBook-Pro ~/test $ ./kubectl get pods
NAME                    READY   STATUS        RESTARTS   AGE
curl-5664f756cb-rrwc7   1/1     Terminating   0          4m24s

jliu@JEFFREYs-MacBook-Pro ~/test $ ./kubectl get pods
No resources found in default namespace.





jliu@JEFFREYs-MacBook-Pro ~/doctl-k3sup $ ./kubectl --insecure-skip-tls-verify run -t -i curl --image alexellis/curl:0.1.1 -- /bin/sh
kubectl run --generator=deployment/apps.v1 is DEPRECATED and will be removed in a future version. Use kubectl run --generator=run-pod/v1 or kubectl create instead.


If you don't see a command prompt, try pressing enter.

~ $ 
~ $ curl 'https://api6.ipify.org?format=json' ; echo
{"ip":"167.172.225.57"}


jliu@JEFFREYs-MacBook-Pro ~/doctl-k3sup $ ./kubectl --insecure-skip-tls-verify delete deploy/curl
deployment.extensions "curl" deleted



[jliu@JEFFREYs-MacBook-Pro ~/doctl-k3sup $ ./kubectl --insecure-skip-tls-verify create -f privileged-pod.yml 
pod/privileged-pod created

jliu@JEFFREYs-MacBook-Pro ~/doctl-k3sup $ ./kubectl --insecure-skip-tls-verify exec -ti privileged-pod sh
/ # chroot /host
root@k3smaster1:/# 


jliu@JEFFREYs-MacBook-Pro ~/doctl-k3sup $ ./kubectl --insecure-skip-tls-verify delete pod privileged-pod
pod "privileged-pod" deleted
jliu@JEFFREYs-MacBook-Pro ~/doctl-k3sup $ ./kubectl --insecure-skip-tls-verify get pods
No resources found in default namespace.



jliu@JEFFREYs-MacBook-Pro ~/doctl-k3sup $ ./doctl compute droplet list
ID           Name          Public IPv4       Private IPv4     Public IPv6    Memory    VCPUs    Disk    Region    Image                          Status    Tags    Features                     Volumes
432146       jack          192.241.169.43    10.128.48.214                   512       1        20      nyc2      Ubuntu Ubuntu 12.04 x64        active            virtio,private_networking    
167408104    k3smaster1    167.172.225.57    10.132.145.69                   1024      1        25      nyc3      Ubuntu ubuntu-18-04-x64-443    active    k3s     private_networking           
jliu@JEFFREYs-MacBook-Pro ~/doctl-k3sup $ ./doctl compute droplet delete 167408104
Warning: Are you sure you want to delete 1 droplet(s) (y/N) ? y
jliu@JEFFREYs-MacBook-Pro ~/doctl-k3sup $ ./doctl compute droplet list
ID        Name    Public IPv4       Private IPv4     Public IPv6    Memory    VCPUs    Disk    Region    Image                      Status    Tags    Features                     Volumes
432146    jack    192.241.169.43    10.128.48.214                   512       1        20      nyc2      Ubuntu Ubuntu 12.04 x64    active            virtio,private_networking    
jliu@JEFFREYs-MacBook-Pro ~/doctl-k3sup $ 



[jliu@JEFFREYs-MacBook-Pro ~/doctl-k3sup $  ./doctl compute load-balancer list
ID                                      IP                 Name       Status    Created At              Algorithm      Region    Tag    Droplet IDs    SSL      Sticky Sessions                                Health Check                                                                                                                   Forwarding Rules
5b4c1a12-1d34-4aaa-9a40-72a4042f5a35    174.138.117.199    kubectl    active    2019-11-17T04:01:34Z    round_robin    nyc3                            false    type:none,cookie_name:,cookie_ttl_seconds:0    protocol:tcp,port:6443,path:,check_interval_seconds:10,response_timeout_seconds:5,healthy_threshold:5,unhealthy_threshold:3    entry_protocol:tcp,entry_port:443,target_protocol:tcp,target_port:6443,certificate_id:,tls_passthrough:false


jliu@JEFFREYs-MacBook-Pro ~/doctl-k3sup $  ./doctl compute load-balancer delete 5b4c1a12-1d34-4aaa-9a40-72a4042f5a35 
Warning: Are you sure you want to delete this load balancer (y/N) ? y


jliu@JEFFREYs-MacBook-Pro ~/doctl-k3sup $  ./doctl compute load-balancer list
ID    IP    Name    Status    Created At    Algorithm    Region    Tag    Droplet IDs    SSL    Sticky Sessions    Health Check    Forwarding Rules






However, SSH still fails:
jliu@JEFFREYs-MacBook-Pro ~/test $ ssh -p 443 root@167.172.233.245
ssh_exchange_identification: Connection closed by remote host

However, it seems like this is by design.   This post seems to give you a “privileged” container which is in essence the same as the host itself.
https://gardener.cloud/050-tutorials/content/howto/ssh-into-node/

jliu@JEFFREYs-MacBook-Pro ~/test $ ./kubectl create -f privileged-pod.yml 
^C
jliu@JEFFREYs-MacBook-Pro ~/test $ ./kubectl get pods
NAME             READY   STATUS    RESTARTS   AGE
privileged-pod   1/1     Running   0          30s



jliu@JEFFREYs-MacBook-Pro ~/test $ ./kubectl exec -ti privileged-pod sh



 # chroot /host
root@8620180:/# shutdown -h now


jliu@JEFFREYs-MacBook-Pro ~/test $ ./doctl compute droplet list
ID           Name       Public IPv4        Private IPv4     Public IPv6    Memory    VCPUs    Disk    Region    Image                          Status    Tags    Features                     Volumes
432146       jack       192.241.169.43     10.128.48.214                   512       1        20      nyc2      Ubuntu Ubuntu 12.04 x64        active            virtio,private_networking    
167400554    8620180    167.172.233.245                                    1024      1        25      nyc3      Ubuntu ubuntu-18-04-x64-443    off       k3s             



jliu@JEFFREYs-MacBook-Pro ~/test $ ./kubectl delete pod privileged-pod
pod "privileged-pod" deleted


jliu@JEFFREYs-MacBook-Pro ~/test $ ./doctl compute droplet delete 167400554
Warning: Are you sure you want to delete 1 droplet(s) (y/N) ? y
jliu@JEFFREYs-MacBook-Pro ~/test $ ./doctl compute droplet list
ID        Name    Public IPv4       Private IPv4     Public IPv6    Memory    VCPUs    Disk    Region    Image                      Status    Tags    Features                     Volumes
432146    jack    192.241.169.43    10.128.48.214                   512       1        20      nyc2      Ubuntu Ubuntu 12.04 x64    active            virtio,private_networking    




—————





11/15/2019
#kubernetes
https://www.digitalocean.com/docs/kubernetes/

#kubernetes - VMware free resources / courses - seems okay
https://blogs.vmware.com/cloudnative/2019/08/27/introducing-kubernetes-academy-free-cloud-native-education-platform/
https://kube.academy
https://blogs.vmware.com/cloudnative/2017/10/25/kubernetes-introduction-vmware-users/




11/8/2019
#kubernetes
https://news.ycombinator.com/item?id=21423612
https://github.com/ReSearchITEng/kubeadm-playbook/

#kubernetes #docker
https://thehftguy.com/2019/10/22/the-demise-of-docker-and-the-rise-of-kubernetes/
https://news.ycombinator.com/item?id=21321601

#docker
https://jdlm.info/articles/2019/09/06/lessons-building-node-app-docker.html
https://news.ycombinator.com/item?id=21209216



10/28/2019
#kubernetes
https://about.gitlab.com/blog/2019/10/24/kubernetes-101/


10/21/2019
#go free intro to go lesson 
https://www.codecademy.com/learn/learn-go


9/30/2019
#iPhone iOS 13
https://www.cnn.com/2019/09/19/tech/ios-13-download/index.html
https://gizmodo.com/19-things-you-can-do-in-ios-13-that-you-couldnt-before-1838244175


#python
https://www.zdnet.com/article/microsoft-we-want-you-to-learn-python-programming-language-for-free/
https://www.youtube.com/playlist?list=PLlrxD0HtieHhS8VzuMCfQD4uJ9yne1mE6
https://github.com/microsoft/c9-python-getting-started


#swift
https://developer.apple.com/tutorials/swiftui/
https://news.ycombinator.com/item?id=20320820
http://www.alwaysrightinstitute.com/swiftwebui/


7/29/2019
o	Capital One security breach on S3 bucket through leaked IAM credentials. https://news.ycombinator.com/item?id=20560342
Some links to interesting projects which can help automate security of Cloud infrastructure
https://github.com/cloud-custodian/cloud-custodian


7/28/2019
#job   no agesim in tech
https://noageismintech.com
https://news.ycombinator.com/item?id=20252097



5/20/2019
o	Git  - git ransom   (recommendation is to not use Personal Access Tokens - thanks Axa, for not allowing ssh)
https://about.gitlab.com/2019/05/14/git-ransom-campaign-incident-report-atlassian-bitbucket-github-gitlab/
Understand the risks associated with the use of personal access tokens. Personal access tokens, used via Git or the API, circumvent multi-factor authentication. Tokens have may have read/write access to repositories depending on scope and should be treated like passwords. If you enter your token into the clone URL when cloning or adding a remote, Git writes it to your .git/config file in plain text, which may carry a security risk if the .git/config file is publicly exposed. When working with the API, use tokens as environment variables instead of hardcoding them into your programs.
Do not expose .git directories and .git/config files containing credentials or tokens in public repositories or on web servers. Information on securing .git/config files on popular web servers is available here.


