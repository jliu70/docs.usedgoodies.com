+++
title = "docker tutorials"
weight = 700
toc = true
date = "2017-03-06T19:50:31Z"
+++


Featured at DockerCon 2017 from members of its Docker Captains program (a group of individuals who are contribute much to the Docker community), this cool hack was Play with Docker by Marcos Nils and Jonathan Leibiusky. Their app is a Docker playground which can run in your browser. Its architecture is a Swarm of Swarms, running in Docker in Docker instances. This app is completely [open source][link-1], so developers can run it in their own infrastructure.

NOTE: seems like the latest commits have removed the local run option, but you can easily go back to commit e9c73b2 to do so.  `git checkout e9c73b2`


Online (and up to date) tutorial: 
http://training.play-with-docker.com




[link-1]: https://github.com/play-with-docker/play-with-docker.github.io
