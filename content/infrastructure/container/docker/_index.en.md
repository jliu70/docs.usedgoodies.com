+++
title = "Docker"
weight = -1550
chapter = true
icon = "<b>1. </b>"
date = "2017-03-02T14:04:17Z"
+++

### Overview

# Docker

[Docker][link-1] is an open-source project that automates the deployment of applications inside software containers.

Quote of features from Docker web pages:

> Docker containers wrap up a piece of software in a complete filesystem that contains everything it needs to run: code, runtime, system tools, system libraries – anything you can install on a server. This guarantees that it will always run the same, regardless of the environment it is running in.


[link-1]:https://docker.com
