+++
toc = true
date = "2017-03-06T20:08:06Z"
title = "Docker Architecture"
weight = 10
+++

# Docker Internals

## Skip this if you're impatient

## Docker Engine
### A client-server application

#### Major components
- A server which is a type of long-running program called a daemon process
- A REST API which specifies interfaces that programs can use to talk to the daemon and instruct it what to do
- A command line interface (CLI) client

![Docker Engine](/images/docker/Docker_Engine.png?&classes=border,shadow)


#### Recall that the Docker platform has two distinct components

- Docker Engine - creating and running containers
- Docker Hub - a cloud service for distributing container images

Client commands call the REST API on the Docker Host which instruct the Docker Engine (daemon) to start/stop create/destroy containers.  
The Registry is where Docker Images can be pushed to/pulled from.  
Docker Hub is a public cloud registry provided by Docker.  There are also private registry implementations available.

![Docker Architecture](/images/docker/Docker_Architecture.png?&classes=border,shadow)

Reference: https://docs.docker.com/engine/understanding-docker/

Client/Server architecture
Unlike most software, same binary runs Docker client & server

Docker daemon
Creating, running, and monitoring containers
Building images

Docker client
Used to talk to the Docker daemon via HTTP

Docker Registries store and distribute images

The main component of a Docker-based workflow is an image, which contains everything needed to run an application. Images are often created automatically as part of continuous integration so they are updated whenever code changes. When images are built to be shared between developers and machines, they need to be stored somewhere, and that's where a container registry comes in.
The registry is the place to store and tag images for later use. Developers may want to maintain their own registry for private, company images, or for throw-away images used only in testing.


## Docker Underlying Technologies

CGroups
Responsible for managing resources used by a container
E.g., CPU and memory usage
Also responsible for freezing and unfreezing containers, as used in the docker pause functionality

Namespaces
Responsible for isolating containers
Ensure container’s filesystem, hostname, users, networking, and processes are separated from the rest of the system

Union File System (UFS)
Allow multiple file systems to be overlaid, appearing as a single filesystem
Docker Images are made up of multiple layers.
Each layer is a read-only filesystem
A layer is created for each instruction in a Dockerfile and sits on top of previous layers
Container –  when running a Container the Docker Engine takes the image and adds a read-write filesystem on top

Images, Containers, and the Union File System
In order to understand the relationship between images and containers, we need to explain a key piece of technology that enables Docker—the UFS (sometimes simply called a union mount).
Union file systems allow multiple file systems to be overlaid, appearing to the user as a single filesytem. Folders may contain files from multiple filesystems, but if two files have the exact same path, the last mounted file will hide any previous files. Docker supports several different UFS implentations, including AUFS, Overlay, devicemapper, BTRFS, and ZFS. Which implementation is used is system dependent and can be checked by running docker info where it is listed under “Storage Driver.” It is possible to change the filesystem, but this is only recommended if you know what you are doing and are aware of the advantages and disadvantages.
Docker images are made up of multiple layers. Each of these layers is a read-only fileystem.  A layer is created for each instruction in a Dockerfile and sits on top of the previous layers. When an image is turned into a container (from a docker run or docker create command), the Docker engine takes the image and adds a read-write filesystem on top (as well as initializing various settings such as the IP address, name, ID, and resource limits).
Because unnecessary layers bloat images (and the AUFS filesystem has a hard limit of 127 layers), you will notice that many Dockerfiles try to minimize the number of layers by specifying several UNIX commands in a single RUN instruction.
A container can be in one of several states: created, restarting, running, paused, or exited.  A “created” container is one that has been initialized with the docker create command but hasn’t been started yet. The exited status is commonly referred to as “stopped” and indicates there are no running processes inside the container (this is also true of a “created” container, but an exited container will have already been
started at least once). A container exits when its main processes exits. An exited container can be restarted with the docker start command. A stopped container is not the same as an image. A stopped container will retain changes to its settings, metadata, and filesystem, including runtime configuration such as IP address that are not stored in images. The restarting state is rarely seen in practice and occurs when the Docker engine attempts to restart a failed container.


### CGroups
CGroups(Control Groups) allocate resources and apply limits to the resources a process can take between containers
Memory
CPU
Disk I/O

Ensure that each container gets its fair share of memory, CPU, disk I/O(resources),

Guarantee that a single container does not over consume resources

Also responsible for freezing and unfreezing containers, as used in the Docker pause functionality

### Namespaces

Kernel namespaces provide basic isolation

Guarantee that each container cannot see or affect other containers

Example
multiple processes with same PID in different containers
NOTE: typically the primary process in a container is PID 1

There are six types of namespaces available
pid (processes)
net (network interfaces, routing…)
ipc (System V IPC)
mnt (mount points, filesystems)
uts (hostname)
user (UIDs)

Reference:  https://www.plesk.com/2016/09/18/docker-containers-explained/


### Copy on write
Create a new container instantly
Instead of copying its whole filesystem

Storage keeps track of what has changed

Many options available
AUFS, overlay (file level)
Device mapper thinp (block level)
BTRFS, ZFS (filesystem level)

Considerably reduces footprint and “boot” times

For more: Deep dive into Docker storage drivers
https://jpetazzo.github.io/assets/2015-03-03-not-so-deep-dive-into-docker-storage-drivers.html#1



Want to go deeper into Cgroups and Namespaces?
View this great talk: https://www.youtube.com/watch?v=sK5i-N34im8


## Docker – Surrounding Technologies

Docker Engine and Docker Hub do not in and of themselves constitute a complete solution for working with containers
Most users will find the require supporting services and software such as cluster management, service discovery tools, and advanced networking capabilities.
Docker plans to build a complete solution that includes all these features but allows users to easily swap out default components for third party ones.

Docker Swarm
Docker’s clustering solution.  Swarm can group together several Docker hosts, allowing the user to treat them as a unified resource

Docker Compose
Tool for building and running applications composed of multiple Docker containers.  
Primarily used in development and testing rather than production.


Docker Machine
Installs and configures Docker Hosts on local or remote resources

Kitematic
A Mac OS and Windows GUI for running and managing Docker containers

Docker Trusted Registry
On-premise solution for storing and managing Docker images
Effectively a local version of the Docker Hub
Features include metrics, RBAC, logs, all managed through an administrative console
The only non-open source product from Docker, Inc.


Networking
Creating networks of containers that span hosts is a nontrivial problem
Several solutions
Weave  http://weave.works/net
Project Calico http://www.projectcalico.org
Docker Overlay  https://docs.docker.com/engine/userguide/networking/dockernetworks/

Service Discovery
When container comes up, it needs some way of finding other services
Consul  https://consul.io
Registrator https://github.com/gliderlabs/registrator
Etcd https://github.com/coreos/etcd

Orchestration and Cluster Management
Kubernetes
Marathon
Mesos
Docker Swarm




Docker provides:
• the daemon that runs a container (engine)
• format for building containers (images)
• a way to ship packaged containers (registries)

## Slide 38 - Docker Ecosystem Overview diagram - this may be outdated - check for updated version


https://www.digitalocean.com/community/tutorial_series/the-docker-ecosystem

https://www.digitalocean.com/community/tutorials/the-docker-ecosystem-an-overview-of-containerization

https://www.digitalocean.com/community/tutorials/the-docker-ecosystem-an-introduction-to-common-components


#### Docker EE
https://www.docker.com/enterprise-edition

#### Docker CE
https://www.docker.com/community-edition

#### parent
https://www.docker.com/get-docker


#### Docker Milestones
https://www.docker.com/company


#### Cool Docker Documentation
https://docs.docker.com/engine/getstarted/last_page/
