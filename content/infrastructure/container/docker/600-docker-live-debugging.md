+++
title = "docker live debugging"
weight = 600
toc = true
date = "2017-03-06T19:50:31Z"
+++

Originally `2016-06-17`

Docker Live debugging


This [blog post][link-1] talks about Docker for Mac and how to do live debugging of a Node.js app via Visual Studio IDE.

Related: https://blogs.msdn.microsoft.com/monub/2016/09/20/145/
Docker Blog Series Part 1- Building ASP.NET Core Application using Docker For Windows




https://medium.com/@Jesse_White/docker-kata-001-1aae05545e3d#.wc861jvue
https://medium.com/@Jesse_White/docker-kata-002-a20f49249dca#.zg8x9oz6x

https://medium.com/@Jesse_White/wish-you-were-here-aws-summit-nyc-2016-9d51c225decc#.by0qtl2d8

https://sematext.com/blog/2016/09/19/docker-swarm-mode-full-cluster-monitoring-logging-with-1-command/

https://blog.mi.hdm-stuttgart.de/index.php/2016/09/13/exploring-docker-security-part-3-docker-content-trust/



[link-1]: https://medium.com/@Jesse_White/docker-kata-003-130c81bc3c39#.2p6j84hst
