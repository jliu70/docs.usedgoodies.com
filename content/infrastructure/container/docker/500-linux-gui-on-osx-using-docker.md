+++
date = "2017-03-04T22:57:08Z"
title = "Linux GUI on OS X using Docker"
weight = 500
toc = true
+++

Originally `2016-06-13`


## Info on running linux X11 GUI apps on MacOS.

[Docker for Mac][link-3] doesn't work with sharing the X11 socket.  So we need to use socat, which is a command line utility that establishes two bidirectional byte streams and transfers data between them.  We use this to allow the X11 traffic to occur between the linux docker host VM, and MacOS.




## Installation of Prerequitsites

```
Install home brew
Install XQartz
brew install socat
```

## Setting up the container
```
JEFFREYs-MBP:zim-docker jliu$ cat Dockerfile
FROM ubuntu:14.04

RUN apt-get update && apt-get install -y xterm && apt-get install -y firefox && apt-get install -y zim && rm -rf /var/cache/apt/

# Replace 1000 with your user / group id
RUN export uid=501 gid=20 && \
    mkdir -p /home/developer && \
    echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:${uid}:" >> /etc/group && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    chmod 0440 /etc/sudoers.d/developer && \
    chown ${uid}:${gid} -R /home/developer

USER developer
ENV HOME /home/developer
#CMD /usr/bin/firefox -no-remote
#CMD /usr/bin/zim
```


## Execution

```
JEFFREYs-MBP:~ jliu$ socat TCP-LISTEN:6000,reuseaddr,fork,range=192.168.1.239/32 UNIX-CLIENT:\"$DISPLAY\"

JEFFREYs-MBP:~ jliu$ env |grep DISPLAY
DISPLAY=/private/tmp/com.apple.launchd.R53BebLmH3/org.macosforge.xquartz:0



JEFFREYs-MBP:zim-docker jliu$ docker run --rm -ti -e DISPLAY=192.168.1.239:0 zim sh

```

### NOTE: next step is to add volume for zim notebook directory

```
docker run --rm -ti -e DISPLAY=192.168.1.239:0 -v /Users/jliu/test zim sh


```

### NOTE: zim does not handle updates on the host filesystem well.  You need to reload zim for changes to be seen.  

### NOTE: hugo does not have this problem and is able to detect changes as it normally would. (sporadically works and sometimes it doesn't work)


## Oct 9, 2016 - Docker network issue
Ran into an issue in which the container could no longer X display to the host.

Narrowed down the issue to the fact that the container could no longer connect to the Host on port 6000.

This was due to overlapping network ranges from Docker networks with the private [RFC1928][link-2] network my laptop was on.

### The Solution
### See the post 2016-06-19-docker-network-cleanup.md



## References

8/1/2016 - a new tutorial http://blog.alexellis.io/linux-desktop-on-mac/

Original post which worked prior to Docker for Mac Beta:  http://fabiorehm.com/blog/2014/09/11/running-gui-apps-with-docker/


From May 21, 2016 - Explanation of why Docker for Mac Beta doesn't work with sharing X11 socket https://forums.docker.com/t/socket-pipes-in-mounted-volumes-not-working/12861/2
There's no time line for this to be implemented - so the recommendation is to use socat for now.

You need socat, which is a command line based utility that establishes two bidirectional byte streams and transfers data between them, and XQuartz - Apples version of the X server.
[Reference on socat][link-1]  
NOTE: the reference to tweak the security settings for XQuartz is not needed.  

https://support.apple.com/en-us/HT201341

https://www.xquartz.org



https://forums.docker.com/t/solved-cant-run-gui-applications-under-security-constraints/13397/4
This contains a snippet which can grab your primary host interface IP address and then export the DISPLAY variable to the docker container


[link-1]: http://kartoza.com/how-to-run-a-linux-gui-application-on-osx-using-docker/
[link-2]: https://en.wikipedia.org/wiki/Private_network
[link-3]: https://docs.docker.com/docker-for-mac/
