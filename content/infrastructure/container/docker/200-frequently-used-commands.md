+++
toc = true
date = "2017-03-02T12:47:50Z"
title = "Frequently Used Commands"
weight = 200
+++

Originally from `2016-06-11`


# Next: Refer to my powerpoint deck for Docker

# Next:  compare docker-compose to Ansible Container - not sure what advantages Ansible may have over docker-compose?


[Docker][link-1]
# Docker Frequently Used Commands

## This is a list of frequently used docker commands

### Docker Containers

```
docker run --rm -ti -d -e DISPLAY=x.x.x.x:0 -v /path/to/mount:/mount/point/in/container -p hostPort:containerPort imageID

docker run --rm -ti -p 1313:1313 -v $(pwd):/srv/hugo hugo sh


docker ps -a

To drop out of interactive session on container: <ctrl-p><ctrl-q>
https://docs.docker.com/engine/quickstart/#running-an-interactive-shell
NOTE: this does not seem to work with Docker Beta for Mac on "docker run", but does work for "docker exec"

docker stop containerID
docker start  containerID


NOTE: may need to remove 'attach' from this list

docker attach containerID

http://stackoverflow.com/questions/30960686/difference-between-docker-attach-and-docker-exec
https://forums.docker.com/t/exec-vs-attach-commands/4210


docker exec -ti containerID /bin/bash

docker logs containerID

$ docker help logs

Usage:	docker logs [OPTIONS] CONTAINER

Fetch the logs of a container

  -f, --follow        Follow log output
  --help              Print usage
  --since             Show logs since timestamp
  -t, --timestamps    Show timestamps
  --tail=all          Number of lines to show from the end of the logs


docker rm containerID

```

### Docker images
```

docker images
docker rmi imageID

docker build -t containerName .  
(assumes that valid `Dockerfile` exists in the current directory)

```

### Docker Registry
```
docker login

docker pull

docker push


```




[link-1]:https://docker.com
[link-2]: https://docs.docker.com/engine/quickstart/#running-an-interactive-shell
