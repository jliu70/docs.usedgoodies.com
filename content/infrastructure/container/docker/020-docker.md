+++
title = "Docker Version Numbering"
weight = 20
toc = true
date = "2017-03-13T18:45:14Z"
+++


## New Version Numbering format

Docker Engine (the free one we all know and love) is now Docker CE (Community Edition). Notice the drop of the word Engine. I suspect we can just call this Docker now. It includes the cli client and backend daemon/service and api (just like it did before). You can get it the same ways as before.

Docker Data Center is now Docker EE (Enterprise Edition) and adds additional paid products and support on top of Docker in three pricing tiers.

These changes don't affect Docker Compose or Docker Machine.

### Docker Version convention changes on 3/1/2017

Old Version: `1.13`

Now uses `17.03`


Docker's version is now `YY.MM` based, using the month of its expected release, and the first one will be 17.03.0, which is "the first point release" of the 17.03 release. If any bug/security fixes are needed to that release, the first update will be 17.03.1 etc.
We now have two release tracks (called variants) "Edge" and "Stable".

Edge is released monthly and supported for a month.

Stable is released quarterly and support for 4 months.

You can extend Stable's support and backported fixes through a Docker EE subscription.


#### See emails from 3/2/2017 and 3/3/2017-03

#### See email from 3/8/2017 too


![Docker Release Lifecycle](https://i0.wp.com/blog.docker.com/wp-content/uploads/lifecycle.png?w=2280&ssl=1)


![Docker EE CE](/images/docker/docker-EE-CE.png?&classes=border,shadow)


## NOTE: the following has an image of features and pricing info.
https://thenewstack.io/docker-launches-enterprise-edition-refines-market-strategy/



#### References
[Brett Fisher - Docker Captain TL;DR blog post][link-2]


[link-2]:http://www.bretfisher.com/docker-version-name-change-highlights/
