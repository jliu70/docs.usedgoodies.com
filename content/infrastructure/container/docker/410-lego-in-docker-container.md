+++
date = "2017-03-12T15:31:33Z"
title = "Lego in a Docker container"
weight = 410
toc = true
+++

Originally `2016-06-15`



## 9/2/2016 - the following is a good recap of what I did to build my own lego docker container


```
legodocker_lego           latest              c2ced16f9435        3 hours ago         23.28 MB
hugo                      0.3                 3d9d3309d834        6 weeks ago         66.69 MB
zim                       0.2                 7f1d52076dbb        5 weeks ago         506.8 MB
```


## although for 'zim' I used ubuntu and installed a bunch of X stuff as well

```
JEFFREYs-MacBook-Pro:ael jliu$ docker exec -ti zimdocker_zim_1 cal -3
    August 2016          September 2016         October 2016      
Su Mo Tu We Th Fr Sa  Su Mo Tu We Th Fr Sa  Su Mo Tu We Th Fr Sa  
    1  2  3  4  5  6               1  2  3                     1  
 7  8  9 10 11 12 13   4  5  6  7  8  9 10   2  3  4  5  6  7  8  
14 15 16 17 18 19 20  11 12 13 14 15 16 17   9 10 11 12 13 14 15  
21 22 23 24 25 26 27  18 19 20 21 22 23 24  16 17 18 19 20 21 22  
28 29 30 31           25 26 27 28 29 30     23 24 25 26 27 28 29  
                                            30 31                 
```

######
## 9/2/2016 dockviz to help backfill the deprecated docker tree
```
JEFFREYs-MacBook-Pro:~ jliu$ docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock nate/dockviz images -t
```



https://nathanleclaire.com/blog/2016/08/11/curl-with-http2-support---a-minimal-alpine-based-docker-image/
written 11 Aug 2016



The general outline of the build is like so:

We install some packages, intended to stay around, for the libraries we need for SSL (HTTPS) and HTTP2 support
We install packages needed to compile cURL
We download and extract the cURL source (latest stable version at time of writing)
We configure, compile, and install curl
We clean up dependencies that we needed to perform the build, but don’t want in the finished image
We set the default CMD to curl
Why Alpine?

Alpine Linux is a minimal Linux distribution with an emphasis on security and speed. Package installs are fast using apk, the image contains only the bare minimum to do basic UNIX-ey things by default, and it is tiny relative to other Docker base images people use. Disk space, and especially network usage, really does matter, so the lighter we can make our image, the better.

Comparison of uncompressed size of common base images (using :latest at time of writing):

alpine - 4.8 MB
ubuntu - 124.8 MB
debian - 125.1 MB
centos - 196 MB



Conclusion

Alpine Linux is great
Building your own tools from scratch is scary but exciting
Docker is extremely useful for tinkering with building tools from source
You can haz teh cURL with HTTP2 support


```
jliu@JEFFREYs-MacBook-Pro ~/.lego/certificates $ for i in $(ls *.crt)
> do
> echo $i
> openssl x509 -text -noout -in $i | egrep "Not After"
> done
blog.storypath.com.crt
            Not After : Nov  1 06:47:00 2016 GMT
gitlab-dr.lightcloudit.com.crt
            Not After : Nov 16 13:39:00 2016 GMT
gitlab-lfs.ael.li.crt
            Not After : Dec  8 13:41:00 2016 GMT
gitlab.ael.li.crt
            Not After : Dec  8 13:25:00 2016 GMT
gitlab.lightcloudit.com.crt
            Not After : Nov  1 06:12:00 2016 GMT
rancher.ael.li.crt
            Not After : Dec  8 14:02:00 2016 GMT
rancher.lightcloudit.com.chained.crt
            Not After : Mar 17 16:40:46 2021 GMT
rancher.lightcloudit.com.crt
            Not After : Dec 11 08:22:00 2016 GMT
registry-dr.gitlab.lightcloudit.com.crt
            Not After : Nov 16 13:41:00 2016 GMT
registry.atna.lightcloudit.com.crt
            Not After : Dec 15 15:47:00 2016 GMT
registry.gitlab-lfs.ael.li.crt
            Not After : Dec  8 13:44:00 2016 GMT
registry.gitlab.ael.li.crt
            Not After : Dec  8 13:18:00 2016 GMT
registry.gitlab.lightcloudit.com.crt
            Not After : Nov  1 06:09:00 2016 GMT
test.lightcloudit.com.crt
            Not After : Nov  1 05:34:00 2016 GMT
test2.lightcloudit.com.crt
            Not After : Nov  1 05:46:00 2016 GMT
test4.lightcloudit.com.crt
            Not After : Nov  1 06:16:00 2016 GMT
usedgoodies.com.crt
            Not After : Nov  1 14:30:00 2016 GMT
jliu@JEFFREYs-MacBook-Pro ~/.lego/certificates $
```

```
cd ael/docker/docker-lego
docker-compose up -d
docker exec -ti dockerlego_lego_1 sh

# env

# export AWS_ACCESS_KEY=secret
# export AWS_SECRET_ACCESS_KEY="superSecret"


# lego --path="/.lego" --email="jliu@hanwave.net" --domains="registry.gitlab.lightcloudit.com"   --dns="route53" renew

# cd /.lego/certificates
# ls -lrt *.crt
# ls -lrt registry.gitlab.lightcloudit.com.*

# exit

cd ~/.lego/certificates
tar cvfBp certificates.tar $(find . -name '*.crt' -mtime -1 -ls | awk '{print $NF}')
tar tvf certificates.tar
scp certificates.tar 192.241.169.43:.


```



[Lego][link-1]

[link-1]: https://github.com/xenolf/lego
