+++
toc = true
date = "2017-03-04T22:23:46Z"
title = "Docker Clean Up Commands"
weight = 210
+++

Originally from `2016-06-03`



## Docker 1.13 released
```
liu@JEFFREYs-MBP ~ $ docker system df
TYPE                TOTAL               ACTIVE              SIZE                RECLAIMABLE
Images              30                  7                   4.009 GB            2.841 GB (70%)
Containers          8                   0                   358.1 kB            358.1 kB (100%)
Local Volumes       46                  1                   743.1 MB            743.1 MB (100%)
jliu@JEFFREYs-MBP ~ $ docker volume prune
WARNING! This will remove all volumes not used by at least one container.
Are you sure you want to continue? [y/N] y
Deleted Volumes:
927ceea713d8586c0e6d98f31fb86d063afdbd83b9542031d9ab2ea4dd7a1d6f
9df3b3473717fe7b713e0fecc98627672978716c2d374050b6c6937882c96475
ad5c9fcd5b25e82029b68e80ae5272609094fc194f70d449439517274f46306a
c1601fcb0c975b45169f9dba90311c2acae9e3f70c8036542a3fa4e61748e237
c7ed9828ad2a497c239f07bc95168b63817ed5fec68b734b33e0030a0038e556
89d393f52631dd139379c70ed849391e84f3c6ff7868b06f93bbb50dfc1df806
4cd47f8bd3ea814d3a112e91eeebd115d91c867a0ca5a13c6cb8365a531d466c
63f9e32159dd176342da4fca6440436bb85d9ca74a16ca67b8253edd453b1c4f
b1c2d89d014fd16d2c81baa3d783364f41e05e5c6494dd61b51f75c833870462
status
20549893d75eb108985e922d7684864f911f79d32185d05fbc25848e81a89e04
ae8dff659d9fd3643659bc6c25308066f127493c8cd2274fa242b770ed74d228
d7493558802b3f61b69df21bd70bcc015d192aa367421b6cd8339a970db39a4f
e6334672367d2e2d56c25369185b54066f18c2c3cf29d810898f70ca50487b1a
49756e2cbab3a14e59581f1301167e62feba98604a596034463b4ab786ef2267
93caeae3efa4903d41310722b2486f2be8ee664ee5dd9c02ff14c5582f20ba20
4f1701e5e29e88415161ba1d96a8084de5bf5b6844f7c57c08f65f1f87cf5059
67d69e298e62d3524797b4b3fd911f2cc4999209031e3a03f02fc4c3b7193e26
7fcf790a37d7ff44adeb4e63b83a3f573071b3e12dc6380d387cba7411f07a2d
94cb9f8dfca6d078d91e1469a651b9262551f4101b392425bd170c185d6ef775
f7d91c58388ceb2250d61db7947a94676b4d76d209fe8802ea9e7d6bce2e265b
0771bbde380f060add465457bfbc05272a966862f9b6340d574f267ff1a0f569
193712ae19eae404b26dfb4de6cc240b07781994cb9c1a0b95f451dd92c58843
256e4defc7b5055df6c95b94c00c9f1714a8f47b3761977cad27d39a13af3b2d
328e431487630b1bbfc517df18780005f38008cb7d2c4725944fa54c607c2650
75fd202bfc784f430cfbd20b9c2c33720187c7743cc47f6126c2ad4c7b45583c
a8b1f39498068d5834b1da354a12171fc1fade15ac438001e07cab32de2b8805
aa5c938d097d3c6fa7a637949d60075a8c977b07bdcbb05a6f196b096f56be3c
b7c311963fd4f14111736d9d3ad193448010ca7346960e6741f6aedc40b6af38
0069a474e7014670f7b3d0b4c7b6fe35afd9cdfb6b7af467fe4269c67aa7ce13
16720f7faf3f63588c1ff503a291f32704156132aaa7c6719e6d4d19f4c91563
f2a4766b690fe604620338f18e11a974b8572c3f2478412e58205fbbe2c6a66c
f3c0902271fb965345dc7b40aad3137a4a9b3cf254a6255468d80e0f0a9a9e34
8775b91bd712cd1a52c4ed0bfafde00d681a79776715915d297c5c31f5575e1a
8938426cbb2df465fd51b391139af3026960d57e9c45e47e7c4bd55520c0c417
d8c5d3369139980614473e54ebdd321f8357d458b50b352109d08fab0f56da79
ddd283b1e026ba267fb7d1c818e81f408480f32e8304d82c4f5cf1400f03ca77
45951ef01684dd4c98b0fcbc30d61c01c71fbb7a08d7f56ce380321d92a80031
5ec00d16fc64567451c17f03f9bdd50b2526793416129521ed8f21003d724077
88225b155a836756af21711fd947ba794f5eb9448595973540a4afc4b8bd0b30
d9b1c234da4df9045324f94729cbb65f08a96cf4db82e931fb7a13c77325091b
dc379c042453ce5b5c3f2d485520e06992486c91238af14de9a4448fda698e72
eaee1f197f4b631bd295fab15e29330cdd4289b7b66ffa2bfa477991862f8102
1147cacb485162522c5155889b07801fa20a8d24a47c1c240c4d7a31f9d87f55
86f617ef627d67318b13bef866b563498475535ad9901a10bf1ac8c0a442842a

Total reclaimed space: 743.1 MB
jliu@JEFFREYs-MBP ~ $ docker system df
TYPE                TOTAL               ACTIVE              SIZE                RECLAIMABLE
Images              30                  7                   4.009 GB            2.841 GB (70%)
Containers          8                   0                   358.1 kB            358.1 kB (100%)
Local Volumes       1                   1                   0 B                 0 B
jliu@JEFFREYs-MBP ~ $

```


```
jliu@JEFFREYs-MBP ~/git/usedgoodies.com/content/posts (master) $ docker network ls
NETWORK ID          NAME                 DRIVER              SCOPE
5b8b3ae57c4c        bridge               bridge              local
a2249edd1536        dockeraws_net        bridge              local
942a7875e19c        dockerlego_net       bridge              local
3a13cbbd3279        dockerzim_net        bridge              local
a85059744ab5        host                 host                local
4537bc677fd8        none                 null                local
060988cb6d4d        todomvcjquery_net    bridge              local
6a7e3171e8a3        usedgoodiescom_net   bridge              local
jliu@JEFFREYs-MBP ~/git/usedgoodies.com/content/posts (master) $ docker network prune
WARNING! This will remove all networks not used by at least one container.
Are you sure you want to continue? [y/N] y
Deleted Networks:
todomvcjquery_net
dockerzim_net
usedgoodiescom_net
dockerlego_net
dockeraws_net

jliu@JEFFREYs-MBP ~/git/usedgoodies.com/content/posts (master) $ docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
5b8b3ae57c4c        bridge              bridge              local
a85059744ab5        host                host                local
4537bc677fd8        none                null                local

```


[link-1]:https://docker.com
[link-2]:https://docs.docker.com
[link-3]: http://container.training
