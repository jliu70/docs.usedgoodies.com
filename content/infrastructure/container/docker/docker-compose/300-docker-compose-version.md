+++
title = "  - Docker compose versions"
weight = 300
toc = true
date = "2018-04-30T18:14:09Z"
+++


4/30/2018

Docker Compose currently has 3 major API versions (v1, v2, v3).

Certain features are enabled / disabled depending on which one you use.

You have probably seen the version: '2' or version: '3' property at the top of a docker-compose.yml file right?

Should You Ever Use v1?

If you don’t see a version property at all, that means you’re using v1 and nowadays that would be considered legacy and isn’t recommended. For starters, it doesn’t support named volumes, networking or custom build arguments. v1 will be deprecated at some point.

You can read all of v1’s limitations at Docker’s documentation.
https://docs.docker.com/compose/compose-file/compose-versioning/#version-1


What about v2 or v3?

A hard and fast rule would be to use the latest version, but you aren’t really hurting yourself if you use an older version which is compatible with your docker-compose.yml file.

Some of the major differences between v2 and v3 is that v3 does not support using the volumes_from and extends properties. A whole bunch of the CPU and memory properties have also been moved to a deploy property in v3 instead.

It’s also worth mentioning if you happen to be using Docker Swarm, you’ll need to use v3+.


A full list of what works with v2 and v3 can be found here.
https://docs.docker.com/compose/compose-file/compose-versioning/#version-2x-to-3x


https://docs.docker.com/compose/compose-file/compose-versioning/#version-3

https://docs.docker.com/compose/compose-file/compose-versioning/#version-23



What about the docker-compose Binary Version?

You should always update to the latest version whenever possible because it will likely contain bug fixes. You can view the compatibility matrix on Docker’s site to see if your API version is compatible with a specific binary version (they are separate things).
