+++
toc = true
date = "2017-03-04T21:24:29Z"
title = "  - Docker compose network"
weight = 320
+++

Originally `2016-06-19`


## Oct 9, 2016 - Docker network issue
Ran into an issue in which the container could no longer X display to the host.

Narrowed down the issue to the fact that the container could no longer connect to the Host on port 6000.

This was due to overlapping network ranges from Docker networks with the private [RFC1928][link-1] network my laptop was on.

### The Solution
Clean up your local Docker network definitions to address the possiblity that these [RFC1918][link-1] ranges might overlap/conflict with the network you're currently on.

First and foremost, Docker does not clean up network allocations.  When you start a new `docker-compose` project, Docker will automatically allocate a new docker network.  After you run `docker-compose rm -f` the containers are removed, but the network is not touched.  This is similar to how Docker images need to be manually removed independently from Docker containers.

It seems that some of the networks were allocated by Docker on the [RFC1918][link-1] 172.16.x.x/12 space with /16 blocks, and when the 172.16.x.x/12 ranges were used up (which is fairly easy when allocating with /16 blocks) Docker allocated a new 192.168.2.x/20 network block.  This network overlapped with my home network (192.168.1.x/24) - the result: the container could not reach my Host.


### List current Docker networks
```
docker network ls
docker network rm network1 network2
```

10/15/2016
NOTE: the ipcalc shows the issue is related to overlapping network range - see below.  Mystery solved.



```
 $ docker network create test --subnet 192.168.2.0/24
25012fbb11d832895681d00225cd7381a0299101c08550c25b4e251973748ba0
jliu@JEFFREYs-MBP ~/git/usedgoodies.com/content/posts (develop) $ docker network inspect test
[
    {
        "Name": "test",
        "Id": "25012fbb11d832895681d00225cd7381a0299101c08550c25b4e251973748ba0",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "192.168.2.0/24"
                }
            ]
        },
        "Internal": false,
        "Containers": {},
        "Options": {},
        "Labels": {}
    }
]

$ docker run --name zim -h zim -ti --network test -e DISPLAY=192.168.1.239:0 -v /Users/jliu/git:/home/developer/git zim:0.2 sh
```

```
$ docker network create test --subnet 192.168.2.0/20
771deb694b1184c6b2ddf4c06e457a05a742f00a514230686562bc10fc0080b2
jliu@JEFFREYs-MBP ~/git/usedgoodies.com/content/posts (develop) $ docker network inspect test
[
    {
        "Name": "test",
        "Id": "771deb694b1184c6b2ddf4c06e457a05a742f00a514230686562bc10fc0080b2",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "192.168.2.0/20"
                }
            ]
        },
        "Internal": false,
        "Containers": {},
        "Options": {},
        "Labels": {}
    }
]


jliu@JEFFREYs-MBP ~/ael/docker $ docker exec -ti dockerzim_zim_1 ipcalc 192.168.2.0/20
Address:   192.168.2.0          11000000.10101000.0000 0010.00000000
Netmask:   255.255.240.0 = 20   11111111.11111111.1111 0000.00000000
Wildcard:  0.0.15.255           00000000.00000000.0000 1111.11111111
=>
Network:   192.168.0.0/20       11000000.10101000.0000 0000.00000000
HostMin:   192.168.0.1          11000000.10101000.0000 0000.00000001
HostMax:   192.168.15.254       11000000.10101000.0000 1111.11111110
Broadcast: 192.168.15.255       11000000.10101000.0000 1111.11111111
Hosts/Net: 4094                  Class C, Private Internet

```

### Listing out the docker networks
```
jliu@JEFFREYs-MacBook-Pro ~ $ docker network inspect $(docker network ls -q) | | jq -r '.[].Name , .[].IPAM.Config'

or

for i in $(docker network ls -q); do docker network inspect $i | jq -r '.[].Name, .[].IPAM.Config'; done


```

### Configuring a network within docker-compose.yml
NOTE: look at compose documentation on info with creating a custom network https://docs.docker.com/compose/networking/#specifying-custom-networks
https://docs.docker.com/compose/compose-file/#network-configuration-reference
http://stackoverflow.com/questions/35708873/how-do-you-define-a-network-in-a-version-2-docker-compose-definition-file
https://www.linux.com/learn/docker-volumes-and-networks-compose


So to avoid this from recurring, you can provide a network definition within your `docker-compose.yml` file with a more limited network scope or one that does not conflict with the network you're currently on.
```
version: '2'

services:
  app:
    image: busybox
    command: ifconfig
    networks:
      app_net:
        ipv4_address: 172.18.18.10

networks:
  app_net:
    driver: bridge
    driver_opts:
      com.docker.network.enable_ipv6: "false"
    ipam:
      driver: default
      config:
      - subnet: 172.18.18.0/24
        gateway: 172.18.18.1

```


### Sanity check across all your docker-compose.yml files

```
find . -name docker-compose.yml -exec egrep "ipv4_address|subnet" {} \; -print
```
or 
```
find . -name docker-compose.yml -print -exec egrep "ipv4_address|subnet" {} \; 
```

NOTE:  with Docker 1.13 release, there is now `prune` commands which can streamline this process.
### See Docker clean up commands


[link-1]: https://en.wikipedia.org/wiki/Private_network
[link-2]: https://docs.docker.com/docker-for-mac/
