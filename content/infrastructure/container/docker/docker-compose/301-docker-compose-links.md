+++
title = "  - Docker compose links"
weight = 301
toc = true
date = "2018-01-24T18:14:09Z"
+++


1/18/2018
## docker  what do links do in docker compose?
https://nickjanetakis.com/blog/docker-tip-37-what-do-links-do-in-docker-compose
```
TL;DNR    Links are deprecated (V1 docker compose yaml).   Instead use “depends_on” and “networks”.   Can continue to use ‘expose’ and ‘ports’
```