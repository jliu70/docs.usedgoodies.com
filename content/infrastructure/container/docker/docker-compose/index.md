+++
toc = true
date = "2017-03-02T12:48:02Z"
title = "Docker compose"
weight = 300
+++

Originally `2016-06-10`



[Docker Compose documentation][link-1]

## Introduction

Docker Compose (originally Fig) is written in Python.  

Docker-Compose provides a template for running one or more containers

Most of the command line options for ”docker run” can be specified in the docker-compose.yml file
Simplifies and documents what is required to run the docker containers

Docker-compose allows to have a single document for all environments via injection of Environment variables

Developers can start/stop/rebuild containers without any deep knowledge of Docker

Configuration is in one YAML file: ‘docker-compose.yml’
YAML is the cleanest data format

Docker-Compose has traditionally been focused on development and testing workflows on a single host.


Using Compose is basically a three-step process

- Define your app’s environment with a “Dockerfile” so it can be reproduced anywhere
- Define the services that make up your app in “docker-compose.yml” so they can be run together in an isolated environment
- Run “docker-compose up” and Docker Compose will start and run your entire app


## Installation

Docker for Mac and Docker for Windows includes both Engine and Compose, so Mac and Windows users do not need to install Docker Compose separately.

For Linux users, follow the directions here:  https://docs.docker.com/compose/install/





## Upgrade containers

Reference: http://staxmanade.com/2016/09/how-to-update-a-single-running-docker-compose-container/
To bring up all the service containers with a simple `docker-compose up` starts everything. However, what if you want to replace an existing container without tearing down the entire suite of containers?
do it with a single command
```
 docker-compose up -d --no-deps --build <service_name>
```

https://realpython.com/blog/python/dockerizing-flask-with-compose-and-machine-from-localhost-to-the-cloud/

# NEXT: perhaps add to my powerpoint deck?


# NOTE: libcompose also referenced in ael.txt
docker-compose (originally Fig) is Python, rancher-compose uses libcompose in Go, which rancher contributed to Docker.
lib-compose is currently maintained by Docker and added Compose V2 support in July.  Lib-compose is used by rancher-compose, openshift, amazon-ecs-cli, compose2kube and other projects.
https://forums.rancher.com/t/why-not-support-a-docker-compose-file-with-a-configured-mysql-image-to-run-rancher-server/1783/5
https://github.com/docker/libcompose




MISC
http://developers.redhat.com/blog/2016/03/09/more-about-docker-images-size/



### Docker environment variable file
```
Finally implemented 11/13/2016  for docker-aws
Env file for AWS secret key
(Until u can setup hashi vault)

done with 'env_file' https://docs.docker.com/compose/compose-file/#env-file

decided against updating used goodies with awscli.  probably not needed - just run separate containers?
```




```
jliu@JEFFREYs-MacBook-Pro ~ $ docker network inspect $(docker network ls -q) | | jq -r '.[].Name , .[].IPAM.Config'

or

for i in $(docker network ls -q); do docker network inspect $i | jq -r '.[].Name, .[].IPAM.Config'; done


```


https://docs.docker.com/compose/compose-file/

```
version: '2'

services:
  app:
    image: busybox
    command: ifconfig
    networks:
      app_net:
        ipv4_address: 172.18.18.10

networks:
  app_net:
    driver: bridge
    driver_opts:
      com.docker.network.enable_ipv6: "false"
    ipam:
      driver: default
      config:
      - subnet: 172.18.18.0/24
        gateway: 172.18.18.1

```

Originally from `ael/docker-compose.txt`

7/31/2016
# Dockercon2016 - Docker for Developers - Part 1
# https://www.youtube.com/watch?v=SK0sqfVn7ls&spfreload=1
# https://github.com/dgageot/dockercon16

# https://docs.docker.com/compose/overview/

# https://medium.com/@giorgioto/docker-compose-yml-from-v1-to-v2-3c0f8bb7a48e#.spqoyzs17
  Early february 2016 Docker officially announced the availability of the new version (V2) of the docker-compose.yml file format, with support for the latest features in docker engine: volumes and networks.


JEFFREYs-MacBook-Pro:www jliu$ cat docker-compose.yml
version: "2"

services:
    www:
      image: nginx
      ports:
        - "8080:80"
      volumes:
        - "./htdocs:/usr/share/nginx/html"
JEFFREYs-MacBook-Pro:www jliu$ docker-compose up -d
Creating www_www_1
JEFFREYs-MacBook-Pro:www jliu$ docker-compose ps
  Name            Command          State               Ports
------------------------------------------------------------------------
www_www_1   nginx -g daemon off;   Up      443/tcp, 0.0.0.0:8080->80/tcp
JEFFREYs-MacBook-Pro:www jliu$ docker-compose stop www
Stopping www_www_1 ... done
JEFFREYs-MacBook-Pro:www jliu$ docker-compose rm -f www
Going to remove www_www_1
Removing www_www_1 ... done
JEFFREYs-MacBook-Pro:www jliu$ docker-compose ps
Name   Command   State   Ports
------------------------------
JEFFREYs-MacBook-Pro:www jliu$ docker-compose up -d www
Creating www_www_1


NOTE: scale only works if you are not presenting ports
docker-compose scale www=2


docker-compose exec www env

docker-compose logs -f


docker-compose stop


JEFFREYs-MacBook-Pro:~ jliu$ cd git/orchestration-workshop/www
JEFFREYs-MacBook-Pro:www jliu$ docker-compose up -d
Creating www_www_1
JEFFREYs-MacBook-Pro:www jliu$ docker-compose stop
Stopping www_www_1 ... done
JEFFREYs-MacBook-Pro:www jliu$ docker-compose rm
Going to remove www_www_1
Are you sure? [yN] y
Removing www_www_1 ... done
JEFFREYs-MacBook-Pro:www jliu$



8/2/2016
How do I set the hostname in a docker-compose.yml http://stackoverflow.com/questions/29924843/how-do-i-set-hostname-in-docker-compose

Using docker-compose with CI - how to deal with exit codes and daemonized linked containers?  http://stackoverflow.com/questions/29568352/using-docker-compose-with-ci-how-to-deal-with-exit-codes-and-daemonized-linked

8/10/2016
Get ip address of container
option "a")

  docker inspect --format '{{ .NetworkSettings.IPAddress }}' c64b45c9b8ff
172.17.0.2


  JEFFREYs-MacBook-Pro:ael-expenses jliu$ docker ps
  CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
  ee90737b6380        zim:0.2             "/bin/bash"         4 days ago          Up 2 days                               zimdocker_zim_1
  193960cfd488        ubuntu:14.04        "bash"              9 days ago          Up 7 days                               rancher_cli
  JEFFREYs-MacBook-Pro:ael-expenses jliu$ docker inspect --format '{{ .NetworkSettings.IPAddress }}' ee907
  172.17.0.2
  JEFFREYs-MacBook-Pro:ael-expenses jliu$ docker inspect --format '{{ .NetworkSettings.IPAddress }}' 1939
  172.17.0.3



option "b")

  docker exec -it c64b45c9b8ff ifconfig eth0 | head -n 2
  eth0      Link encap:Ethernet  HWaddr 02:42:AC:11:00:02
            inet addr:172.17.0.2  Bcast:0.0.0.0  Mask:255.255.0.0



8/30/2016 https://dzone.com/articles/microservices-an-example-with-docker-go-and-mongod




[link-1]:https://docs.docker.com/compose/
[link-2]:https://docs.docker.com
[link-3]: http://container.training
