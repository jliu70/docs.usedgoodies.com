+++
title = "  - Docker compose frequently used commands"
weight = 310
toc = true
date = "2017-03-06T20:14:09Z"
+++


## Docker Compose frequently used commands


### Docker Compose
```
docker-compose up

docker-compose ps

docker-compose stop

docker-compose start

docker-compose rm -f

```
