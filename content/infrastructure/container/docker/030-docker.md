+++
title = "30 Docker"
weight = 30
toc = true
date = "2017-03-11T19:18:34Z"
+++

30 Docker


### add to docs.usedgoodies - Docker section
NOTE: these may need to be updated after the new command structure for Docker CLI in 1.13  
http://ricostacruz.com/cheatsheets/docker.html
http://ricostacruz.com/cheatsheets/docker-compose.html



### List Docker hub remote tags from CLI
Gist online: https://gist.github.com/jliu70/6fcaf3383df9434e4cb11031be5360ef

```
#!/bin/bash

if [ $# -lt 1 ];
then
    echo "Usage:"
    echo "    $0  (image1) [image2] [image3]"
    exit 1
fi

for i in "$@"
do
    chkofficial=$( echo $i | grep \/ )
    if [ $chkofficial ];
    then
        echo "### $i tags ###"
        curl -s -S "https://registry.hub.docker.com/v2/repositories/$i/tags/" | jq '."results"[]["name"]' | sed -e 's/"//g' | sort
    else
        echo "### $i tags ###"
        curl -s -S "https://registry.hub.docker.com/v2/repositories/library/$i/tags/" | jq '."results"[]["name"]' | sed -e 's/"//g' | sort
    fi
done
```
