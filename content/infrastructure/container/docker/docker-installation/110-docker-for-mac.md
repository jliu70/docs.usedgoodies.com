+++
date = "2017-03-05T14:48:44Z"
title = "- Docker for Mac"
weight = 110
toc = true
+++

Originally `ael/docker-local-development.txt`



The goal with Docker for Mac and Windows has been to let developers use Docker on the machines on which they are developing, no matter what the operating system. Before this Docker was essentially a Linux application, and if you wanted to use it to develop on Mac and Windows, you had to use the Docker Toolbox.  
The new version contains a Docker engine running in an Alpine Linux distribution on top of an xhyve Virtual Machine on Mac OS X or on a Hyper-V VM on Windows, and that VM is managed by the Docker application.  
Docker for Mac is a Mac application and Docker for Windows is a Windows application. Both include a native user interface, and come bundled with the Docker toolset of the Docker command line, Compose, and Notary command line.  
The new version is also designed to provide an easier flow of development work. You get volume mounting for your code and data, and access to running containers on the localhost network. There's also In-container debugging with supported IDEs. Docker for Mac and Windows include a DNS server for containers, and are integrated with the Mac OS X and Windows networking system.  
Docker for Mac can be used at the same time as Docker Toolbox on the same machine, allowing developers to continue using Toolbox as they evaluate Docker for Mac. On Windows you need to stop Toolbox before using Docker for Windows.  
While the new versions are being made generally available, the developers have come up with a plan to keep the new versions moving forward rapidly. To achieve this, Docker for Mac and Windows will be available from two channels – stable and beta. New features and bug fixes will go out first in auto-updates to users in the beta channel, then when well-tested will be rolled out to the stable channel on a less frequent basis, in sync with major and minor releases of the Docker engine.


Inspiration:  Running Docker and Rancher Locally on Dev Machines - May 2016 Online Meetup
http://rancher.com/running-rancher-on-a-laptop/
http://info.rancher.com/may-2016-online-meetup
https://www.youtube.com/watch?v=sS3gO5h88M4
His voting app demo:  https://github.com/chrisurwin/may2016-demo
 (basically identical to Docker Voting App Example https://github.com/docker/example-voting-app - just adds the rancher-compose.yml for Rancher to add load balancer and scales the voting app)

#### NOTE: he likes to use Rancher as it gives visualzation of the environment, as well as load balancing which can help if you're working on multiple projects.   You don't need the Jenkins stuff there at all - just there to show what's possible.
#### Also refer to the beginning of the video to list out his reasons
#### Managing containers is a pain, rancher solves that, so give it to everyone
#### rancher lets everyone easily see memory, io, network traffic - early diagnosis
#### more easily test multi-stack systems - don't leave that to Prod, solve the pain early
#### internal registry trick "localhost:5000/"  - all dev-in-a-box docker compose files reference localhost so you don't need to run a full registry locally - devs can just push certified builds to the real registry
#### Same deployment and management tools as PTL and Prod

8/10/2016
Introduction to xhyve: http://www.pagetable.com/?p=831

8/10/2016
NOTE: the following information is probably deprecated due to Docker for Mac
https://blog.andyet.com/2016/01/25/easy-docker-on-osx/
https://allysonjulian.com/posts/setting-up-docker-with-xhyve/
Reference:  Alternative to docker-machine-driver-xhyve, dlite which integrates xhyve with docker without going through the hassle of dealing with docker-machine and fiddling with environment variable shenanigans.
https://github.com/nlf/dlite
NOTE: The above seem to be about running Docker on Mac, which no longer applies.  However, the reason I may still need to use docker-machine-driver-xhyve is to spin up additional VMs for use as Rancher Hosts.



Reference:
Rancher on docker-for-mac with xhyve (created May 2016)
https://gist.github.com/vincent99/656b799a384d64ca8c5362543d4986d5


NOTE: to find your Docker for Mac alpine host IP  (not necessarily 192.168.64.1)
https://forums.docker.com/t/how-to-make-changes-to-xhyve-host/11820
# screen ~/Library/Containers/com.docker.docker/Data/com.docker.driver.amd64-linux/tty
The username is root with no password.
This information is available from https://beta.docker.com/docs/mac/troubleshoot/43 under Troubleshooting > Workarounds.

9/25/2016 - to exit screen session:  ctrl-a, k
# https://medium.com/@Jesse_White/docker-kata-004-f5b20607c9ff


# Docker for Mac disk space consumption
# https://medium.com/@Jesse_White/docker-kata-004-f5b20607c9ff
# JEFFREYs-MacBook-Pro:aws-docker jliu$ du -sh ~/Library/Containers/com.docker.docker/Data/com.docker.driver.amd64-linux/*qcow2
 20G	/Users/jliu/Library/Containers/com.docker.docker/Data/com.docker.driver.amd64-linux/Docker.qcow2


NOTE: on my machine it's 192.168.65.2 /29



NOTE: Alternative way to get to the Docker for Mac Host root shell via privileged container (from the dockercon2016 session Docker for Mac and Windows: The Insider's Guide - Black Belt Track  https://www.youtube.com/watch?v=7da-B3rY9V4 minute 20:00)
JEFFREYs-MacBook-Pro:ael jliu$ docker run -it --privileged --pid=host debian nsenter -t 1 -m -u -n -i sh



NOTE: From https://github.com/docker/for-mac/issues/17
I ran hwclock -s directly on the Moby VM* (rather than from within a privileged container) and that immediately cleared up the time drift.
* via screen ~/Library/Containers/com.docker.docker/Data/com.docker.driver.amd64-linux/tty



# Troubleshooting
syslog -k Sender Docker



docker-machine-drive-xhye installation alternative to homebrew:
https://github.com/zchee/docker-machine-driver-xhyve/releases
$  curl -L https://github.com/zchee/docker-machine-driver-xhyve/releases/download/v0.2.3/docker-machine-driver-xhyve > /usr/local/bin/docker-machine-driver-xhyve
$ chmod +x /usr/local/bin/docker-machine-driver-xhyve
$ sudo chown root:wheel /usr/local/bin/docker-machine-driver-xhyve
$ sudo chmod u+s /usr/local/bin/docker-machine-driver-xhyve


Install docker-macine-driver-xhyve
  brew install docker-machine-driver-xhyve
  sudo chown root:wheel $(brew --prefix)/opt/docker-machine-driver-xhyve/bin/docker-machine-driver-xhyve
  sudo chmod u+s $(brew --prefix)/opt/docker-machine-driver-xhyve/bin/docker-machine-driver-xhyve


Installing Rancher
JEFFREYs-MacBook-Pro:git jliu$ docker run --restart=always -p 8080:8080 --name rancher -h rancher -d rancher/server:stable

###
8/10/2016
NOTE: BEWARE!!!  You might not be able to release the VMs IP Addresses!!
https://github.com/zchee/docker-machine-driver-xhyve/issues/8
https://github.com/zchee/docker-machine-driver-xhyve/

From the project page README.md:

  Known issue

  Experimental shared folders

  docker-machine-driver-xhyve can create a NFS share automatically for you, but this feature is highly experimental. To use it specify --xhyve-experimental-nfs-share when creating your machine.

  Does not clean up the vmnet when remove a VM

  Current state, docker-machine-driver-xhyve does not clean up the vmnet configuration.

  Running xhyve vm (e.g. IP: 192.168.64.1)
          |
  `docker-machine rm`, ACPI signal(poweroff) in over ssh
          |
  vm is poweroff, `goxhyve` also killed with to poweroff
          |
  Re-create xhyve vm.
  New vm's ip, It will probably be assigned to 192.168.64.2. If create another new vm, assigned to 192.168.64.3
  but 192.168.64.1 are not using anyone.

  vmnet.framework seems to have decided to IP based on /var/db/dhcpd_leases and /Library/Preferences/SystemConfiguration/com.apple.vmnet.plist
  So, To remove it manually, or Don’t even bother.
  Maybe vmnet.framework shared net address range 192.168.64.1 ~ 192.168.64.255. You can make 255 vm.

  I will fix after I understand the vmnet.framework

###

###

8/10/2016
NOTE: https://github.com/zchee/docker-machine-driver-xhyve/issues/18#issuecomment-176291029  When I first spun up a docker-machine, I did not use 4 cores and one of my test processes took 2 minutes and 21 seconds. After I switched to 4 cores, it only takes 20 seconds, which is about the same as what vmware fusion was doing.

docker-machine create --driver xhyve --xhyve-cpu-count=4 --xhyve-memory-size='512' --xhyve-disk-size=80000 --xhyve-boot-cmd='loglevel=3 user=docker console=ttyS0 console=tty0 noembed nomodeset norestore waitusb=10 base host=host1' --xhyve-experimental-nfs-share host1


https://github.com/zchee/docker-machine-driver-xhyve/issues/18#issuecomment-205763379
Everything is working fine. For NFS support I used https://github.com/adlogix/docker-machine-nfs.

###

Create some hosts (set the host name in both places at the end)

docker-machine create --driver=xhyve --xhyve-memory-size='512' --xhyve-boot-cmd='loglevel=3 user=docker console=ttyS0 console=tty0 noembed nomodeset norestore waitusb=10 base host=host1' host1
docker-machine create --driver=xhyve --xhyve-memory-size='512' --xhyve-boot-cmd='loglevel=3 user=docker console=ttyS0 console=tty0 noembed nomodeset norestore waitusb=10 base host=host2' host2
docker-machine create --driver=xhyve --xhyve-memory-size='512' --xhyve-boot-cmd='loglevel=3 user=docker console=ttyS0 console=tty0 noembed nomodeset norestore waitusb=10 base host=host3' host3


Go to http://localhost:8080 once server is up, Add Host

Set host-registration URL to 192.168.64.1:8080   (NOTE: for me it's 192.168.65.2:8080)

Fill in the version and URL from the command the UI shows, but run this to register each host instead:

docker-machine ssh host1 sudo docker run -e CATTLE_AGENT_IP=$(ifconfig eth0 | grep "inet " | awk -F'[: ]+' '{ print $4 }') -d --privileged -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/docker/rancher:/var/lib/rancher rancher/agent:##VERSION## ##URL##

If you have error on getting the host ip address using eth0 please ssh into the host then run ifconfig to get the eth0 interface then exit from the ssh and run the above command using use the real ip of the host instead of this: $(ifconfig eth0 | grep "inet " | awk -F'[: ]+' '{ print $4 }')


Rinse, lather, repeat for host2...N

https://docs.docker.com/machine/get-started/



JEFFREYs-MacBook-Pro:ael jliu$ docker-machine ls
NAME   ACTIVE   DRIVER   STATE   URL   SWARM   DOCKER   ERRORS
JEFFREYs-MacBook-Pro:ael jliu$


# 8/23/2016 - misc http://blog.alexellis.io/follow-all-captains/


Originally `2016-06-16`


[Docker for Mac][link-3] is a great product, but here's a list of some gotchas and where possible the workarounds to them.

## 8/11/2016 - Docker for Mac Time drift issue

Noticed today (8/11/2016) that the zim docker container was stuck on 8/8/2016.

To fix:  restart Docker for Mac from the Moby system tray or see below....


Apprantly there are two threads in the Docker Forums:
https://forums.docker.com/t/syncing-clock-with-host/10432/10
https://forums.docker.com/t/time-in-container-is-out-of-sync/16566/7

Seems like it's also an issue on Docker for Windows.


Github issue:  https://github.com/docker/for-mac/issues/17
### As of 5/11/2017 this is finally fixed (above issue 17)

Time drift in Moby VM and containers caused by system sleep #17


### Work Around
```
+* If system does not have access to an NTP server then after a hibernate the time seen by Docker for Mac may be considerably out of sync with the host. Furthermore the time may slowly drift out of sync during use. The time can be manually reset after hibernation by running:
 +
 +        docker run --rm --privileged alpine hwclock -s
 +
 +  Or alternatively to resolve both issues you may like to add the local clock as a low priority (high stratum) fallback NTP time source for the host by editing the host's `/etc/ntp-restrict.conf` to add:
 +
 +        server 127.127.1.1              # LCL, local clock
 +        fudge  127.127.1.1 stratum 12   # increase stratum
 +
 +  And then restarting the NTP service with:
 +
 +        sudo launchctl unload /System/Library/LaunchDaemons/org.ntp.ntpd.plist
 +        sudo launchctl load /System/Library/LaunchDaemons/org.ntp.ntpd.plist

```

### Alternative Work Around
```

docker run -it --rm --privileged --pid=host debian nsenter -t 1 -m -u -n -i date -u $(date -u +%m%d%H%M%Y)

Make it as alias command and run it when time drift.
```



## 9/28/2016 Docker for Mac - disk consumption of 64GB maximum

This [blog post][link-1] talks about Docker for Mac and how its disk consumption continuously grows over time.

This [Github issue][link-2] tracks the disk consumption issue.

The .qcow2 is exposed to the VM as a block device with a maximum size of 64GiB. As new files are created in the filesystem by containers, new sectors are written to the block device. These new sectors are appended to the .qcow2 file causing it to grow in size, until it eventually becomes fully allocated. It stops growing when it hits this maximum size.

Unfortunately when files are deleted from the filesystem, the block device doesn't find out, because the connection protocol we currently use virtio-blk doesn't support a "TRIM" or "DISCARD" operation. The block device keeps filling up with junk. A similar problem manifests on real hardware in SSDs -- they start slowing down as they fill up, unless TRIM is enabled in the OS and drivers.

We're hoping to fix this in several stages: (note this is still at the planning / design stage, but I hope it gives you an idea)

we'll switch to a connection protocol which supports TRIM, and implement free-block tracking in a metadata file next to the qcow2. We'll create a compaction tool which can be run offline to shrink the disk (a bit like the qemu-img convert but without the dd if=/dev/zero and it should be fast because it will already know where the empty space is)

we'll automate running of the compaction tool over VM reboots, assuming it's quick enough

we'll switch to an online compactor (which is a bit like a GC in a programming language)

We're also looking at making the maximum size of the .qcow2 configurable. Perhaps 64GiB is too large for some environments and a smaller cap would help?

Let me know what you think! Thanks for your patience.


### Check the current size of your qcow2 file
```
$ du -sh ~/Library/Containers/com.docker.docker/Data/com.docker.driver.amd64-linux/Docker.qcow2
```

## The Solution
### The Nuclear Option
You can simply reset your Docker for Mac installation to stock and your qcow file will regenerate. This `WILL DELETE all of your images and containers` however, so proceed with caution. `Repeat WARNING!  This will DELETE ALL of your images and containers.`

### The Hard Way
Follow the steps in this [blog post][link-1] which involves installing the qemu utility via home brew to re-compress our qcow2 disk.



## Oct 9, 2016 Docker-Compose and Docker network limitations
[Docker-compose and docker network limitations][xref-1] - see this other blog post on how to clean up your local docker network definitions and deal with the possiblity that these [RFC1918][link-4] ranges might overlap/conflict with the network you're on.


[link-1]: https://medium.com/@Jesse_White/docker-kata-004-f5b20607c9ff
[link-2]: https://github.com/docker/for-mac/issues/371
[link-3]: https://docs.docker.com/engine/installation/mac/
[link-4]: https://en.wikipedia.org/wiki/Private_network
[xref-1]: {{<relref "500-linux-gui-on-osx-using-docker.md">}}
