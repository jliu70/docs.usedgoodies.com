+++
date = "2017-03-06T19:50:16Z"
title = "- Docker for windows 7"
weight = 130
toc = true
+++

Originally slide `43` on powerpoint

The new Docker for Windows is only for Windows 10
Docker on Windows 7
https://docs.docker.com/toolbox/toolbox_install_windows


### Diagram on slide 43

In an Windows installation, the docker daemon is running inside a Linux virtual machine. You use the Windows Docker client to talk to the Docker host VM. Your Docker containers run inside this host.
Since Windows 10 first introduced namespaces, older versions of Windows (namely 7 and 8) will need to install Docker Toolbox. https://docs.docker.com/toolbox/toolbox_install_windows/


Originally `2016-06-18`


## Docker for Windows 7

Docker for Windows is only supported for Windows 10 and above (due to required kernel updates).


Because the Docker Engine daemon uses Linux-specific kernel features, you can’t run Docker Engine natively in Windows. Instead, you must use the Docker Machine command, docker-machine, to create and attach to a small Linux VM on your machine. This VM hosts Docker Engine for you on your Windows system.



When openinng a git-for-windows bash shell, you can set the required environment variables via:

```
$ docker-machine env default


$ eval $(docker-machine env default)

```


As Docker Toolbox runs in a virtual machine, it uses docker-machine to comunicate and configure it. If you want to turn the virtual machine off, run:
```

docker-machine stop default

```


### Differences between Git-for-Windows Bash and 'Mintty'

NOTE: this is not an issue if you use the quickstart terminal shortcut


https://github.com/docker/toolbox/issues/136


Trying to run a shell into a running container (docker exec -it containerid bash) using DockerToolbox/docker-machine on Windows 7 gives this error:

```
"cannot enable tty mode on non tty input"
```


As a workaround, prefix the docker command with winpty, so:
```
winpty docker exec -it containerid bash

```

or a more creative solution
```
alias docker="winpty docker"
```
the alias trick results in problems when calling docker from inside scripts (like build scripts) and when redirecting input and output.



http://willi.am/blog/2016/08/08/docker-for-windows-interactive-sessions-in-mintty-git-bash/


https://github.com/docker/toolbox/issues/323   Use mintty for windows "Docker Quickstart Terminal" shortcut

This should be fixed in the Docker QuickStart Terminal - but not in other instances of Git Bash. In that case I think appending winpty is the best option.

```
/c/Program\ Files/Git/bin/bash.exe

vs

/c/Program\ Files/Git/usr/bin/mintty.exe

```


### Docker Toolbox for Windows 7 only supports volume mounts from c:/Users directory
11/11/2016
https://github.com/docker/compose/issues/2548#issuecomment-232390294
NOTE: You can not mount a volume outside of c:/Users and even then you need to be mounting something in your home directory due to permission issues.    As a result I had to temporarily move usedgoodies.com to /c/Users/jeffrey.liu in order for the docker container to be able to copy to AWS S3.





[link-1]: https://docker.github.io/toolbox/toolbox_install_windows/
