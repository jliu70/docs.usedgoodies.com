+++
toc = true
date = "2017-03-13T18:51:09Z"
title = "Docker Installation"
weight = 100
+++

{{% notice warning %}}
Docker is constantly evolving and the information listed here can quickly be out of date.
Be sure to check the official Docker documentation.
{{% /notice %}}

## Different Installation Options
From the viewpoint of the user, Docker has many installation options.  
- Docker on your own laptop/desktop  - Most developers will want to run Docker on their own machine.
- Docker on a server (aka Docker Host).  



### Linux server
#### CentOS/RHEL  
yum install docker-engine

#### Ubuntu
```
$ cat ubuntu-16.04-docker.sh
# https://docs.docker.com/engine/installation/linux/ubuntulinux/
apt-get update
apt-get -y install apt-transport-https ca-certificates
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" >> /etc/apt/sources.list.d/docker.list
# 14.04  echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" >> /etc/apt/sources.list.d/docker.list
apt-get update
apt-get purge lxc-docker
apt-get -y install linux-image-extra-$(uname -r)
# apt-cache policy docker-engine
apt-get -y install docker-engine
# 14.04  root@5458a3e77137:/# apt-get -y install docker-engine=1.10.3-0~trusty
docker version
docker pull rancher/server:v1.1.2
```


### Windows server

### Your laptop


https://docs.docker.com/docker-for-mac/
https://docs.docker.com/docker-for-windows/

```
Native application (faster and more reliable)
Less dependencies (no need for virtualbox or similar)
Host-native virtualization
Windows:  Hyper-V
Mac:           xhyve
Better networking/filesystem mounting
Docker CLI, Compose and Notary


Mac OS X – network port forwarding to localhost
docker run –d –p 8080:80 nginx
http://localhost:8080
Windows 10 – network port forwarding to docker.local
docker run –d –p 8080:80 nginx
http://docker.local:8080

```



https://blog.docker.com/2016/05/docker-unikernels-open-source/

### Diagram slide 42

[link-1]: https://docs.docker.com
