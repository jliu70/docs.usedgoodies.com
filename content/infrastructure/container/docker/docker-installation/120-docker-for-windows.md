+++
weight = 120
toc = true
date = "2017-03-13T18:49:53Z"
title = "- Docker for windows"
+++

Docker for Windows

Windows 10


https://blog.docker.com/2017/11/docker-for-windows-17-11/
```
Because there’s only one Docker daemon, and because that daemon now runs on Windows, it will soon be possible to run Windows and Linux Docker containers side-by-side, in the same networking namespace. This will unlock a lot of exciting development and production scenarios for Docker users on Windows.
```



More info
https://stefanscherer.github.io/run-linux-and-windows-containers-on-windows-10/
