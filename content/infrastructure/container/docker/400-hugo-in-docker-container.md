+++
toc = true
date = "2017-03-04T22:54:57Z"
title = "Hugo in a Docker container"
weight = 400
+++

Originally `2016-06-12`



[Docker][link-1]

Run Hugo in a Docker container.

```
JEFFREYs-MBP:usedgoodies.com jliu$ cat Dockerfile
# http://zhezhang.co/2015/10/hugo-docker-and-daocloud/
# docker build -t hugo .
# docker images
# cd /path/to/hugo; docker run --rm -ti -p 1313:1313 hugo
FROM alpine:3.2
MAINTAINER Zhe Zhang (https://github.com/zhe)

RUN apk add --update wget && rm -rf /var/cache/apk/*

ENV HUGO_VERSION 0.15
ENV HUGO_BINARY hugo_${HUGO_VERSION}_linux_amd64

WORKDIR /tmp

RUN wget --no-check-certificate https://github.com/spf13/hugo/releases/download/v${HUGO_VERSION}/${HUGO_BINARY}.tar.gz \
    && tar -xzf ${HUGO_BINARY}.tar.gz \
    && mv /tmp/${HUGO_BINARY}/${HUGO_BINARY} /usr/local/bin/hugo \
    && rm -rf /tmp/* \
    && adduser -D hugo

RUN mkdir -p /srv/hugo
#COPY . /srv/hugo

WORKDIR /srv/hugo
EXPOSE 1313
USER hugo
#CMD ["hugo", "--buildDrafts", "server", "--bind=0.0.0.0", "--baseUrl=http://usedgoodies.com/", "--port=80", "--appendPort=false"]
#CMD ["hugo", "--buildDrafts", "server", "--bind=0.0.0.0", "--port=1313"]
# docker run --rm -ti -p 1313:1313 -v $(pwd):/srv/hugo hugo sh

```

docker-compose.yml
```
version: '2'

services:
  usedgoodies:
    image: hugo:v0.4
    #build: .
    ports:
      - 1313:1313
    volumes:
      - /Users/jliu/git/usedgoodies.com:/srv/hugo:ro
    networks:
      net:
        ipv4_address: 172.18.22.10

networks:
  net:
    driver: bridge
    driver_opts:
      com.docker.network.enable_ipv6: "false"
    ipam:
      driver: default
      config:
      - subnet: 172.18.22.0/24
        gateway: 172.18.22.1

```


[link-1]:https://docker.com
[link-2]: https://docs.docker.com/engine/quickstart/#running-an-interactive-shell
