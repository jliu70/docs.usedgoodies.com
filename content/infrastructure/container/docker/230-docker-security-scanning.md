+++
toc = true
date = "2017-11-11T14:23:46Z"
title = "Docker Security Scanning"
weight = 230
+++

## Docker Security Scanning

Scanning for Security vulnerabilities within Docker containers

Docker Hub provides scanning features for private repos with their first paid tier (as of 2017 $7/month)
[link-2]
[link-3]

Quay.io is another commercial service.



## Free open source for your own use

Clair [link-4]

When you work with containers (Docker) you are not only packaging your application but also part of the OS. Therefore it is crucial to know what kind of libraries might be vulnerable in you container. One way to find this information is to use and look at the Docker Hub or Quay.io security scan. The problem whit these scans is that they are only showing you the information but are not part of your CI/CD that actually blocks your container when it contains vulnerabilities.

What you want is:

Build and test your application
Build the container
Test the container for vulnerabilities
Check the vulnerabilities against allowed ones, if everything is allowed pass, otherwise fail
This straight forward process is not that easy to achieve when using the services like Docker Hub or Quay.io. This is because they work asynchronously which makes it harder to do straight forward CI/CD pipeline.

Clair to the rescue

CoreOS has created an awesome container scan tool called "clair". Clair is also used by Quay.io. What clair does not have is a simple tool that scans your image and compares the vulnerabilities against a whitelist to see if they are approved or not.

This is where clair-scanner comes in to place. The clair-scanner does the following:

* Scans an image against Clair server
* Compares the vulnerabilities against a whitelist
* Tells you if there are vulnerabilities that are not in the whitelist and fails
* If everything is fine it completes correctly



11/4/2017  Steps to build clair-scanner (tool to do local Docker image security scanning)
Google: clair docker for mac
http://blog.xebia.com/docker-containers-vulnerability-scan-clair/
https://github.com/arminc/clair-scanner
https://github.com/arminc/clair-local-scan
Deprecated:  https://github.com/coreos/analyze-local-images
https://github.com/coreos/clair
Pre-requisites
cli tools
homebrew
Go
```
brew install go
brew install dep
```
Install and Build
```
cd GOPATH/src/github.com
git clone https://github.com/arminc/clair-scanner.git
make ensure && make build
```
```
clair-scanner -w example-alpine.yaml --ip YOUR_LOCAL_IP alpine:3.5
clair-scanner --ip YOUR_LOCAL_IP alpine:3.5
```

```
jliu@JEFFREYs-MacBook-Pro ~/go/src/github.com/clair-scanner (master) $ ./clair-scanner --ip 192.168.1.239 alpine:3.4
2017/11/04 16:04:40 [INFO] ▶ Start clair-scanner
2017/11/04 16:04:41 [INFO] ▶ Server listening on port 9279
2017/11/04 16:04:41 [INFO] ▶ Analyzing 2d80c41a63ff9495fe6a1a47ec10f141418371198eda767d44cd7f937b2ce375
2017/11/04 16:04:41 [INFO] ▶ Unapproved vulnerabilities [[CVE-2016-9842 CVE-2016-9843 CVE-2016-9841 CVE-2016-9840 CVE-2017-15650]]
jliu@JEFFREYs-MacBook-Pro ~/go/src/github.com/clair-scanner (master) $ ./clair-scanner --ip 192.168.1.239 jliu70/hugo
2017/11/04 16:04:45 [INFO] ▶ Start clair-scanner
2017/11/04 16:04:48 [INFO] ▶ Server listening on port 9279
2017/11/04 16:04:48 [INFO] ▶ Analyzing 1cf95bb54177909eb5e000d2849cb49b1743af4cb41ebb0ab942ba34c78585f1
2017/11/04 16:04:48 [INFO] ▶ Analyzing 50406b57432eec2c7c9a0ff0caa806997d17d3838bb23bbcc18ea5833c3e6dce
2017/11/04 16:04:48 [INFO] ▶ Unapproved vulnerabilities [[CVE-2016-9842 CVE-2016-9843 CVE-2016-9841 CVE-2016-9840 CVE-2017-15650]]


jliu@JEFFREYs-MacBook-Pro ~/go/src/github.com/clair-scanner (master) $ ./clair-scanner --ip 192.168.1.239 alpine:3.5
2017/11/04 16:07:26 [INFO] ▶ Start clair-scanner
2017/11/04 16:07:26 [INFO] ▶ Server listening on port 9279
2017/11/04 16:07:26 [INFO] ▶ Analyzing d93ea7a61b57bdeeb3b7c4a1b184d1d406c8d193dd3bab928cd51389113cf6b6
2017/11/04 16:07:27 [INFO] ▶ Image [alpine:3.5] not vulnerable
jliu@JEFFREYs-MacBook-Pro ~/go/src/github.com/clair-scanner (master) $ ./clair-scanner --ip 192.168.1.239 jliu70/hugo
2017/11/04 16:07:41 [INFO] ▶ Start clair-scanner
2017/11/04 16:07:45 [INFO] ▶ Server listening on port 9279
2017/11/04 16:07:45 [INFO] ▶ Analyzing 28d0899b6e8db80661a50ec2c4ee18433ee70987cf781d0fa790db8c5c3a3b4c
2017/11/04 16:07:45 [INFO] ▶ Analyzing eba164aad67d56ed294ca2df51f98239680e603a2b217abdafd7058cb32f0962
2017/11/04 16:07:49 [INFO] ▶ Image [jliu70/hugo] not vulnerable
```




##docker container scanning
https://thenewstack.io/draft-vulnerability-scanners/


OpenSCAP for RHEL 7 Docker containers
https://www.open-scap.org/resources/documentation/security-compliance-of-rhel7-docker-containers/


[link-1]:https://docker.com
[link-2]:https://docs.docker.com/docker-cloud/builds/image-scan/
[link-3]:https://blog.docker.com/2016/05/docker-security-scanning/
[link-4]:https://github.com/coreos/clair
[link-5]:http://blog.xebia.com/docker-containers-vulnerability-scan-clair/
[link-6]:https://github.com/arminc/clair-scanner

