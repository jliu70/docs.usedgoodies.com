+++
toc = true
date = "2017-03-02T12:47:50Z"
title = "Introduction to Docker Concepts"
weight = 1
+++

#### An overview of the general concepts of containers and [Docker.][link-1]

{{% notice note %}}
We won't cover Docker installation, Docker commands, or technical details in-depth here.  Those will be covered in subsequent articles.
{{% /notice %}}


## Containers

- Containers have become a key area of discussion in the cloud computing space
    - Central to hybrid cloud strategies as it enables freedom of choice of on premise / private and public environments

- A container consists of an entire runtime environment (an application, its dependencies, libraries and other binaries) bundled into one package

- Organizations may be reluctant to adopt containers as they are instead familiar with virtualization

- Containers are much more lightweight and use fewer resources than virtual machines


#### Container Adoption - Eight surprising facts about Docker Adoption
> https://www.datadoghq.com/docker-adoption/
> Latest update - June 2016

- Real Docker Adoption is up 30% in one year
- Docker now runs on 10% of the hosts Datadog monitors
- Larger companies are leading adoption
- Two-thirds of companies that try Docker adopt it
- Adopters 5x their container count within 9 months
- Most widely used images are still Registry, NGINX, and Redis
- Docker hosts often run five containers at a time
- VMs live 6x longer than containers


#### Three Facts about container Adoption
> https://www.oreilly.com/ideas/3-facts-about-container-adoption-you-dont-know

- Containers are a key enabler as organizations adopt continuous delivery as part of the digital transformations

- Increased container adoption raises new concerns about managing containers at scale

- Users prefer platforms over orchestration tools for container management


## Docker and Containers

#### Containers are an old concept
- Unix chroot (1982)
- FreeBSD jail (1998)
- Solaris zones (2001)
- Google develops Linux Cgroups (2005)
- Linux Containers (LXC) (2008)
- Docker (2013)


#### Docker took the existing complex, hard-to-implement Linux container technology and wrapped and extended it.
- Portable images
- User-friendly interface

The Docker Engine provides a fast and convenient interface for running containers.  Before this, running a container using a technology such as LXC required significant specialist knowledge and manual work. The Docker Hub provides an enormous number of public container images for download, allowing users to quickly get started and avoid duplicating work already done by others.

#### Docker platform has two distinct components
- Docker Engine - creating and running containers
- Docker Hub - a cloud service for distributing container images


## Docker is not the same as traditional virtualization

- While there are similarities, at its core Docker is about delivering applications quickly and with the highest level of flexibility

- Containers are an encapsulation of an application with its dependencies.

- Containers can be deployed on a wide variety of platforms or infrastructures, managed either locally or in the cloud.

- Containers are fundamentally changing the way we develop, distribute, and run software.
  - Developers can build software locally, knowing that it will run identically regardless of host environment—be it a rack in the Datacenter, a user’s laptop, or a cluster in the cloud.
  - Operations engineers can concentrate on networking, resources, and uptime and spend less time configuring environments and battling system dependencies.

Containers are popular with developers of all persuasions because they make it easier to deploy software, move it from location to location, and execute updates to it. Docker capitalizes on a handful of standards in the marketplace to allow applications to become more moveable between servers.
Here’s another take on the differences: https://blog.docker.com/2016/03/containers-are-not-vms/


Though containers and VMs seem similar at first, there are some important differences, which are easiest to explain using diagrams.
The purpose of a VM is to fully emulate a foreign environment.  The purpose of a container is to make applications portable and self-contained.
Both VMs and containers can be used to isolate applications from other applications running on the same host. VMs have an added degree of isolation from the hypervisor and are a trusted and battle-hardened technology.   Containers are comparatively new, and many organizations are hesitant to completely trust the isolation features of containers before they have a proven track record.
A container provides a segregated section of a host resources managed within the host kernel, without the overhead of providing simulated hardware.  This means a process running in a container sees a world that appears to be a separate system, but in reality is seeing a filtered view of the based on kernel namespaces and capabilities.  Separate users, file systems, networking, process tables can all be provided to a container.  Or, a container can interact with the host resource, say mapping a directory from the host to provide as web content to nginx running in the container.
Ok, that’s fine from how the host system provides the resources to the container, what about the application running inside the container?  This will differ slightly based on the container format, but we put the application and all of it’s dependencies inside the container.  That’s our code, libraries, binaries, and any other operating services down the chain.  Since we’re running an OS and kernel on our host, we can run slimmed down versions of the OS inside the container.
Hopefully you can already see a good use for containers: application deployment issues.  If your application is carrying it’s operating environment with it, all it needs to run is a container engine that supports the format.  And it should be fairly simple to publish in several formats.  No more worries about conflicting libraries, incompatible patches, or the dreaded “works for me” syndrome.  If it works in development, it will work in production.


![Docker-VM-comparison](/images/docker/docker-vm-comparison.png?&classes=border,shadow)

![Docker-Physical-Host](/images/docker/Docker_Host-Physical-Server.png?&width=50%&classes=border,shadow)



#### Additional common models

![Docker-Hypervisor](/images/docker/docker-hypervisor.png?&width=50%&classes=border,shadow)

![docker-hypervisor-vm](/images/docker/docker-hypervisor-vm.png?&width=50%&classes=border,shadow)

## Docker speed
- A container can be spun up in a fraction of a second
  - Just running another process
- VMs can boot up quickly, but even the fastest boot up time will be around 10 seconds
- A Docker container is a writeable copy of an image


![Docker-Image](/images/docker/docker-image.png?&width=50%&classes=border,shadow)

Containers are very lightweight compared to VMs.

Containers are started from images.

Images can be shipped and run anywhere.



- Concept of “cache” on Docker host – initial image download – then almost instant startup thereafter.  
- When done properly Docker images are measured in Megabytes (MB)
  - unlike VMs which are typically measured in Gigabytes (GB)


![Docker-Image-Size-Chart](/images/docker/docker-image-size-chart.png?&width=50%&classes=border,shadow)

Comparison on Docker Image sizes
https://www.brianchristner.io/docker-image-base-os-size-comparison/
http://stackoverflow.com/questions/29696656/finding-the-layers-and-layer-sizes-for-each-docker-image
Top 10 Docker Images
https://www.ctl.io/developers/blog/post/docker-hub-top-10/
Refactoring your own Docker Images
https://blog.replicated.com/2016/02/05/refactoring-a-dockerfile-for-image-size


### Docker - Immutable server
Same Docker image deployed everywhere
Locally, Dev, QA, Prod – all with the same image
You can do this with other tools, but not as easily or fast

Deploying a consistent production environment is easier said than done. Even if you use tools like chef and puppet, there are always OS updates and other things that change between hosts and environments. 
There is considerable scope for configuration drift, and unexpected changes to running servers.

Immutable Server pattern was not practical with VMs
Dealing with several Gigabytes images, moving those big images around, perhaps just to change some fields in the app, is very laborious. 

Think of Docker/Containers as similar to “Cloning VMs as a service”

Treat your servers as Cattle not Pets.
Reference:  http://stackoverflow.com/questions/16047306/how-is-docker-different-from-a-normal-virtual-machine/25938347#25938347


## Docker - flexibility
- Infrastructure agnostic
- Easy to deploy on other docker hosts

Developers love that they run the same environment on their laptops that can easily be deployed to Dev, QA, Staging, Prod
No more “but it worked on my machine”

Example:  running an ARM-based Docker container for IoT applications directly on Microsoft Windows
http://blog.hypriot.com/post/close-encounters-of-the-third-kind/
“Docker for Windows” uses Window native hypervisor Hyper-V
The Docker Engine runs in a tiny Alpine Linux VM
ARM-based Docker containers can be easily enabled with QEMU emulator
Docker works great on Windows 10


![Docker-Flexibility](/images/docker/docker-flexibility.png?&width=50%&classes=border,shadow)

## Not all rainbows and unicorns
![Rainbow-Unicorn](/images/docker/rainbow-and-unicorn.png?&width=50%&classes=border,shadow)

### Two main limitations of Docker

- Persistent Storage (aka “state”)
- Cross-Host container communications

https://www.joyent.com/blog/persistent-storage-patterns



## Container or VM?
- When does an application go in a VM and when does it go in a container?
  - **It Depends**

![Docker-Container-or-VM](/images/docker/docker-container-or-vm.png?&classes=border,shadow)


1) Portability - where companies want some benefits that Docker offers, and they move monolithic apps from VMs to containers with no intention of ever rewriting them.
“Typically these customers are interested in the portability aspect that Docker containers offer out of the box. Imagine if your CIO came to you and said “Those 1,000 VMs we got running in the data center, I want those workloads up in the cloud by the end of next week.” That’s a daunting task even for the most hardcore VM ninja. There just isn’t good portability from the data center to the cloud, especially if you want to change vendors. Imagine you have vSphere in the datacenter and the cloud is Azure — VM converters be what they may.”
“However, with Docker containers, this becomes a pretty pedestrian effort. Docker containers are inherently portable and can run in a VM or in the cloud unmodified, the containers are portable from VM to VM to bare metal without a lot of heavy lifting to facilitate the transition.”

Think of Docker/Container portability as similar to  “Cloning VMs as a service”

2) Rather than wait until an app is rewritten to a microservice-based architecture, some companies will “lift and shift” an existing application from a VM into a Docker container.  
Over time, functions will be split out into discrete microservices across loosely coupled containers


3) Greenfield – new application designed around a microservices-based architecture, then it is obvious containers are the way to go.

## Containerize Legacy Applications

When to Containerize Legacy Applications — And When Not To
http://thenewstack.io/containerize-legacy-applications-not/



Incremental revolution

1. Start Small
  - A single app, DevOps project or migrate one legacy app
2. Get some quick wins
3. Build muscle memory, tooling and more champions
4. Expand and migrate infrastructure and apps over time



![Why-Containerize-Applications](/images/docker/why-containerize-applications.png?&classes=border,shadow)

![2016-state-of-application-survey](/images/docker/2016-state-of-application-survey.png?&classes=border,shadow)




# break - here is current edited location - following is original content which should be split off




Originally from `2016-06-01`

### Docker inspect can expose environment variables
#### Security concern with using environment variables for secrets

```

root@rancher:~# docker inspect mysql | jq .[0].Config.Env
[
  "MYSQL_ROOT_PASSWORD=techitup2015",
  "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
  "GOSU_VERSION=1.7",
  "MYSQL_MAJOR=5.7",
  "MYSQL_VERSION=5.7.13-1debian8"
]

```

Other resources

More information can be found within the [official Docker documentation][link-2]


[Container.training A Great Tutorial][link-3]

https://dinosaurscode.xyz/tutorials/2016/07/02/docker-tutorial-for-beginners/

https://www.blindside.io/courses/a-practical-guide-to-docker

Hands on Docker - some labs
https://github.com/alexellis/HandsOnDocker/blob/master/Labs.md


https://dzone.com/articles/an-introduction-to-docker-for-mac

http://developers.redhat.com/blog/2016/03/09/more-about-docker-images-size/





[link-1]:https://docker.com
[link-2]:https://docs.docker.com
[link-3]: http://container.training
