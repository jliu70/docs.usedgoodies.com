+++
weight = 10
toc = true
date = "2020-02-04T05:03:55Z"
title = "Puppet Guides"
+++


Puppet guides



https://learn.puppet.com

https://learn.puppet.com/user/learning/enrollments



NOTE: Bolt is NOT currently in use at AXA
https://puppet.com/docs/bolt/latest/bolt_installing.html
https://puppet.com/docs/bolt/latest/bolt_installing.html#install-bolt-with-chocolatey
https://puppet.com/docs/bolt/latest/bolt_installing.html#install-bolt-on-rhel-sles-or-fedora



### 2/6/2020  
From Puppet email
New Puppet training videos
https://puppet.com/learning-training/kits/puppet-language-basics/
Puppet Language Basics - This beginner-level course will cover the fundamentals of the Puppet language, and should be a first step for anyone new to Puppet Enterprise or Open Source Puppet. You don’t need any previous Puppet knowledge or software in order to follow along.

https://puppet.com/learning-training/kits/intro-to-bolt/
Intro to Bolt - This beginner-level course will introduce you to Bolt, an easy-to-use and intuitive tool for solving problems and automating system administration tasks.


Orchestrate resource provisioning with Terraform and Bolt
Terraform is a powerful tool for provisioning cloud resources, but managing those resources and integrating them with your infrastructure requires a different solution. Watch this 30-minute webinar to learn how Bolt and Terraform can be used together to orchestrate the creation of new resources, ensure their proper configuration, and destroy obsolete resources.
https://puppet.com/resources/webinar/orchestrate-resource-provisioning-terraform-bolt/
