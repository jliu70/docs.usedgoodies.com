+++
weight = 1
toc = true
date = "2017-03-17T05:03:55Z"
title = "Puppet Introduction"
+++

Puppet


The good news is that Puppet is simpler than you’d think. People who use Puppet come from all types of backgrounds, including Linux and Windows, and once you’ve brushed up on a few fundamental tools, you’ll be better prepared to get started. 

Our guide, The Tools for Learning Puppet, provides step-by-step tutorials for tools like the command line (CLI), the popular (and free) Vim text editor, and Git. Once you’ve gone through the guide, you’ll feel a lot more comfortable with these tools, and be able to take the next steps in your Puppet journey. (Bonus: It’s fun.)



[Download Tools for Learning Puppet PDF](/pdfs/puppet/puppet-ebook-tools-for-learning-puppet.pdf)



