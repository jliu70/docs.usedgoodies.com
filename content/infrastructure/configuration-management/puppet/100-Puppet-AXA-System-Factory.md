+++
weight = 100
toc = true
date = "2020-02-06T05:03:55Z"
title = "Puppet AXA System Factory"
+++

### 2/6/2020
James ARMSTRONG email
The last training we did had some new slides, they are too big to mail but can be found here…
https://confluence.axa.com/confluence/pages/viewpageattachments.action?pageId=95120316&metadataLink=true
[Puppet AXA Training Slide Deck](/static/pdfs/puppet/AXA1_Puppet_student_slide_deck_191119.pdf)

Here you go Jeff, The password  is …. 2338
[Puppet Fundamentals](/static/pdfs/puppet/Fundamentals-virtual-w-v5.0.0.pdf)

[Puppet Fundamentals Exercises](/static/pdfs/puppet/Fundamentals-virtual-w-v5.0.0-exercises.pdf)

[Puppet Fundamentals Solutions](/static/pdfs/puppet/Fundamentals-virtual-w-v5.0.0-solutions.pdf)



```
[root@cm3a200c ~]# netstat -anltp | grep 777
tcp        0      0 10.68.73.69:7777        0.0.0.0:*               LISTEN      60873/perl
tcp        0      0 10.68.73.69:7777        10.68.82.249:59669      TIME_WAIT   -
tcp        0      0 10.68.73.69:7777        10.68.82.250:35033      TIME_WAIT   -
tcp        0      0 10.68.73.69:7777        10.68.82.249:32836      TIME_WAIT   -
tcp        0      0 10.68.73.69:7777        10.68.82.250:27323      TIME_WAIT   -
tcp        0      0 10.68.73.69:7777        10.68.82.249:43290      TIME_WAIT   -
tcp        0      0 10.68.73.69:7777        10.68.82.249:45569      TIME_WAIT   -
tcp        0      0 10.68.73.69:7777        10.68.82.250:34206      TIME_WAIT   -
[root@cm3a200c ~]# ps -ef |grep perl
root      40378  40280  0 17:03 pts/1    00:00:00 grep --color=auto perl
pe-pupp+  60873      1  0 09:32 ?        00:00:00 /usr/bin/perl /opt/puppetlabs/axa/bin/register_intermediate.pl
[root@cm3a200c ~]# more /opt/puppetlabs/axa/bin/register_intermediate.pl
#!/usr/bin/perl

# Intended to run on the puppet compilers. Will listen on port 7777 and expect commands overgiven
# from the clients and the loadbalancer (alive check).
# The ip addresses of the requesting clients are assumed to be inserted as a x-forward-for into the html header.
# This address will be checked against dns to prevent a machine sends requests for another machines.

use IO::Socket::INET;

$mastermasterip="10.68.242.211";

$logfile="/var/log/puppetlabs/register_intermediate/register_intermediate.log";

$myip = `ifconfig eth1 |grep inet |awk '{ print \$2 }'`; chomp $myip;

sub log($) {
   open (OF,">>$logfile");
   my $date=`date`;
   chomp $date;
   print OF "$date $_[0]";
   close (OF);
}

sub callmaster($) {
   my $socket = new IO::Socket::INET (
     PeerHost => $mastermasterip,
     PeerPort => '7777',
     Proto => 'tcp',
   ) ;
   die "cannot connect to the server $!\n" unless $socket;
   my $size = $socket -> send ($_[0]);
   shutdown($socket, 1);
   my $response = "";
   $socket -> recv($response, 1024);
   $socket -> close();
   return $response;
}


# auto-flush on socket
$| = 1;

# creating a listening socket
my $socket = new IO::Socket::INET (
     LocalHost => $myip,
     LocalPort => '7777',
     Proto => 'tcp',
     Listen => 5,
     Reuse => 1

);

die "cannot create socket $!\n" unless $socket; &log("server waiting for client connection on port 7777\n");

while(1) {
   # waiting for a new client connection
   my $client_socket = $socket->accept();
   # get information about a newly connected client
   my $client_address = $client_socket->peerhost();
   my $client_port = $client_socket->peerport();
#&log("connection from $client_address:$client_port\n");  log later now, only on add/delete

   # read up to 1024 characters from the connected client
   my $data = "";

   $client_socket->recv($data, 1024);
#&log("received data: $data\n");  loga later now, only on add/delete
   ($firstline,@rest)=split (/\n/,$data);
   ($req,$hostcommand,@dummy)=split(/\s/,$firstline);
   ($empty,$host,$command)=split(/\//,$hostcommand);
   if (($command eq "add") || ($command eq "delete") || ($command eq "status")) {
     if ($command eq "status") {
       $resp="alive";
     } else {
       &log("connection from $client_address:$client_port\n");
       &log("received data: $data\n");
       my $clientip="";
       foreach $entry (@rest) {
         #print $entry."\n";
         if ($entry =~ "X-Forwarded-For") {
           ($dummy, $addresses)=split(/:/,$entry);
           ($clientip,@proxyips)=split(/\,/,$addresses);
           $clientip =~ s/\r//g;
           $clientip =~ s/\s//g;
         }
       }
       if ($clientip ne "") {
         foreach $proxy (@proxyips) {
           $proxy=~s/\s//g;
         }
         if ((scalar @proxyips) != 0 ) {
           # faked X-forward-for possible
           $resp="IP does not match";
           $proxyipslist="";
           foreach $proxy (@proxyips) {
             $proxyipslist=$proxyipslist." ".$proxy;
           }
           &log("X-Forward-For seems to be faked, proxyipslist=$proxyipslist \n");
         } else {
           $match=(1==0);
           $hosts_short_dns_list="";
           $dns_name=`host $clientip|awk '{ print \$5 }'`;
           @dnsentries=split(/\n/,$dns_name);
           foreach $dnsentry (@dnsentries) {
             if (!$match) {
               chomp $dnsentry;
               chop $dnsentry;
               #print "<$dnsentry>\n";
[root@cm3a200c ~]# netstat -anltp | more
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 127.0.0.1:54923         0.0.0.0:*               LISTEN      24600/klzagent
tcp        0      0 0.0.0.0:42923           0.0.0.0:*               LISTEN      24600/klzagent
tcp        0      0 0.0.0.0:8140            0.0.0.0:*               LISTEN      43587/java
tcp        0      0 0.0.0.0:3661            0.0.0.0:*               LISTEN      24600/klzagent
tcp        0      0 0.0.0.0:8142            0.0.0.0:*               LISTEN      43587/java
tcp        0      0 0.0.0.0:9999            0.0.0.0:*               LISTEN      27484/java
tcp        0      0 0.0.0.0:7887            0.0.0.0:*               LISTEN      10410/ubrokerd
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      34960/sshd
tcp        0      0 0.0.0.0:1270            0.0.0.0:*               LISTEN      9933/omiserver
tcp        0      0 0.0.0.0:8887            0.0.0.0:*               LISTEN      20395/zso
tcp        0      0 0.0.0.0:8888            0.0.0.0:*               LISTEN      20395/zso
tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN      10826/master
tcp        0      0 0.0.0.0:6014            0.0.0.0:*               LISTEN      24600/klzagent
tcp        0      0 0.0.0.0:37599           0.0.0.0:*               LISTEN      24600/klzagent
tcp        0      0 0.0.0.0:20000           0.0.0.0:*               LISTEN      27484/java
tcp        0      0 0.0.0.0:1920            0.0.0.0:*               LISTEN      24600/klzagent
tcp        0      0 10.68.73.69:7777        0.0.0.0:*               LISTEN      60873/perl
tcp        0      0 10.68.73.69:7777        10.68.82.250:53868      TIME_WAIT   -
tcp        0      0 10.68.73.69:8142        10.68.82.16:52521       ESTABLISHED 43587/java
tcp        0      0 10.68.73.69:50300       10.68.242.211:8140      TIME_WAIT   -
tcp        0      0 10.68.73.69:7777        10.68.82.250:14177      TIME_WAIT   -
tcp        0      0 10.68.73.69:7777        10.68.82.249:36088      TIME_WAIT   -
tcp        0      0 10.68.73.69:33140       10.67.176.33:4505       ESTABLISHED 10427/python
tcp        0      0 10.68.73.69:50314       10.68.242.211:8140      TIME_WAIT   -
tcp        0      0 10.68.73.69:7777        10.68.82.249:14531      TIME_WAIT   -
tcp        0      0 10.68.73.69:50318       10.68.242.211:8140      ESTABLISHED 43587/java
tcp        0     28 10.68.73.69:22          10.78.219.30:57705      ESTABLISHED 40220/sshd: coreima
tcp        0      0 10.68.73.69:7777        10.68.82.249:34522      TIME_WAIT   -
tcp        0      0 10.68.73.69:7777        10.68.82.250:32126      TIME_WAIT   -
tcp        0      0 10.68.73.69:50320       10.68.242.211:8140      ESTABLISHED 43587/java
tcp        0      0 127.0.0.1:51208         127.0.0.1:1920          ESTABLISHED 27484/java
tcp        0      0 10.68.73.69:37650       10.68.242.211:8142      ESTABLISHED 44509/pxp-agent
tcp        0      0 10.68.73.69:50288       10.68.242.211:8143      ESTABLISHED 43587/java
tcp        0      0 10.68.73.69:8142        10.68.82.16:58692       ESTABLISHED 43587/java
[root@cm3a200c ~]# ps -ef |grep java
root      20400  20395  0 Jan24 ?        00:00:58 /opt/IBM/ibm-java-x86_64-70/jre/bin/java -Xshareclasses:name=Zero_%u -Xgcpolicy:gencon -Dderby.stream.error.file=/opt/IBM/maestro/maestro.deployment.ui/logs/derby.log -Djava.io.tmpdir=/opt/IBM/maestro/maestro.deployment.ui/tmp -Dnetworkaddress.cache.negative.ttl=600 -Dfile.encoding=UTF8 -Xms32M -Xmx128M -Dnetworkaddress.cache.negative.ttl=600 -Dfile.encoding=UTF8 -Djava.awt.headless=true -classpath /opt/IBM/maestro/maestro.deployment.ui/private/expanded/zero/zero.kernel-1.1.1.6.171116/lib/zero.kernel.jar:/opt/IBM/maestro/maestro.deployment.ui/private/expanded/zero/zero.kernel-1.1.1.6.171116/lib/antlr-2.7.7.jar -Djava.library.path=/opt/IBM/maestro/maestro.deployment.ui/private/expanded/zero/zero.management.native.process-1.1.1.6.171116/lib/x86_64/linux:/opt/IBM/maestro/maestro.deployment.ui/private/expanded/zero/zero.management.spi-1.1.1.6.171116/lib/x86_64/linux:/opt/IBM/maestro/maestro.deployment.ui/private/expanded/zero/zero.management.zso-1.1.1.6.171116/lib/x86_64/linux:/opt/IBM/maestro/maestro.deployment.ui/private/expanded/zero/zero.management.monitor-1.1.1.6.171116/lib/x86_64/linux -Dzero.force.log.rotation=true -Dzero.firstrun=true -Dzero.recycle.supported=true zero.core.Main -apphome=/opt/IBM/maestro/maestro.deployment.ui run
root      27484  27417  0 Jan24 ?        01:24:09 /opt/IBM/ibm-java-x86_64-70/bin/java -Xms32M -Xmx1024M -javaagent:lib/bootstrap-agent.jar -jar lib/com.ibm.alpine_1.0.jar --cold LINUX_RH7.11571867499196
root      40721  40280  0 17:05 pts/1    00:00:00 grep --color=auto java
pe-pupp+  43587      1  2 Jan24 ?        06:21:36 /opt/puppetlabs/server/bin/java -Xmx4096m -Xms4096m -Djava.io.tmpdir=/opt/puppetlabs/server/apps/puppetserver/tmp -XX:ReservedCodeCacheSize=512m -XX:+PrintGCDetails -XX:+PrintGCDateStamps -Xloggc:/var/log/puppetlabs/puppetserver/puppetserver_gc.log -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=16 -XX:GCLogFileSize=64m -XX:OnOutOfMemoryError=kill -9 %p -cp /opt/puppetlabs/server/apps/puppetserver/puppet-server-release.jar:/opt/puppetlabs/server/apps/puppetserver/jruby-9k.jar:/opt/puppetlabs/puppet/lib/ruby/vendor_ruby/facter.jar:/opt/puppetlabs/share/java/bcprov-jdk15on.jar:/opt/puppetlabs/share/java/bcpkix-jdk15on.jar:/opt/puppetlabs/server/data/puppetserver/jars/* clojure.main -m puppetlabs.trapperkeeper.main --config /etc/puppetlabs/puppetserver/conf.d --bootstrap-config /etc/puppetlabs/puppetserver/bootstrap.cfg --restart-file /opt/puppetlabs/server/data/puppetserver/restartcounter
[root@cm3a200c ~]# which puppet
/usr/local/bin/puppet
[root@cm3a200c ~]# puppet agent -t
Info: Using configured environment 'production'
Info: Retrieving pluginfacts
Info: Retrieving plugin
Info: Retrieving locales
Info: Loading facts
Info: Caching catalog for cm3a200c.wwmgmt.intraxa
Info: Applying configuration version '1580995862'
Notice: role base
Notice: /Stage[main]/Role::Base/Notify[role base]/message: defined 'message' as 'role base'
Notice: 2 - NODE NAME not present into csv file.
Notice: /Stage[main]/Axa_patch_management::Management_window_fact/Notify[axa_patch_management::management_window_fact error code]/message: defined 'message' as '2 - NODE NAME not present into csv file.'
Notice: testing patch management
Notice: /Stage[main]/Profile::Base::Axa_patch_management/Notify[testing patch management]/message: defined 'message' as 'testing patch management'
Notice: Using local facts to setup patching cron job
Notice: /Stage[main]/Axa_patch_management::Patch_schedule::Linux/Notify[Using local facts to setup patching cron job]/message: defined 'message' as 'Using local facts to setup patching cron job'
Notice: entering sssd config
Notice: /Stage[main]/Profile::Base::Linux::Sssd/Notify[entering sssd config]/message: defined 'message' as 'entering sssd config'
Notice: No appropriate hiera data for managing qualys found.
Notice: /Stage[main]/Axa_qualys/Notify[No appropriate hiera data for managing qualys found.]/message: defined 'message' as 'No appropriate hiera data for managing qualys found.'
Notice: region is NA
Notice: /Stage[main]/Profile::Base::Linux::Updatedb/Notify[region is NA]/message: defined 'message' as 'region is NA'
Notice: vmtools
Notice: /Stage[main]/Profile::Base::Linux::Vmwaretools/Notify[vmtools]/message: defined 'message' as 'vmtools'
Notice: Applied catalog in 16.61 seconds
[root@cm3a200c ~]# Connection to cm3a200c.wwmgmt.intraxa closed by remote host.
Connection to cm3a200c.wwmgmt.intraxa closed.
```