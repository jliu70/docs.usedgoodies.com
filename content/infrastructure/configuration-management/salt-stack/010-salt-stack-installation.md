+++
weight = 10
toc = true
date = "2017-03-17T05:03:55Z"
title = "10 installation"
+++

Installation

Linux: Any Linux OS with python 2.6 and above (excluding python 3)
RHEL 5.x, RHEL 6.x, RHEL 7.x
RHEL 4.x is not supported because it runs python 2.4.x

Windows
Windows 2003 Server, Windows 2008R2, Windows 2012R2
NOTE: There is no plans for the Salt Master to run on Windows
Only the Salt Minion is supported on Windows


For RHEL/CentOS
Easily install via Salt Stack repo
https://docs.saltstack.com/en/latest/topics/installation

Windows
Install via EXE package
https://docs.saltstack.com/en/latest/topics/installation/windows.html

To complete the installation of Salt Minion software, you will need to configure the minion to point to the Salt Master
By default minions refer to the DNS short name “salt”
On Windows, this is entered in a dialog box when you run the install package (or provide as a parameter on the CLI silent install method.)
```
/S /master=salt.equitable.com /minion=hostname
```
On Linux, after installing the salt-minion rpm, you can update /etc/salt/minion and restart the salt-minion service








Items needed for Saltstack Minion install
(added these to Saltstack install script package v1.0.0.1 4/27/2017)
```
cat /etc/resolv.conf
search ppprivmgmt.intraxa equitable.com
nameserver 10.77.249.21
nameserver 10.77.249.22
```

Upon first start up, a Minion will send the Master it’s public key. 
Keys are used for security.  When a minion initializes it generates an asymmetric cryptographic key that must be approved by the master before the master will send commands to that minion.
The following screen shot shows you the general process.  
You first list all the minion keys which are in one of the following categories  accepted/denied/unaccepted/rejected.   
And then accept the key for minion “dsc01”.

Slide 12 picture



