+++
weight = 11
toc = true
date = "2017-03-17T05:03:55Z"
title = "11 salt stack master setup"
+++

Salt Master configuration

Salt Master to enable file share
```
/etc/salt/master
file_roots:
base:

- /srv/salt/base
```




BB3b Salt Master – salt.equitable.com

Auto accept keys from minions with *.intraxa names 

```
[root@CCC3D001 ~]# cat /etc/salt/master.d/reactor.conf
reactor:
	- 'salt/auth':
		- /srv/reactor/auth-pending.sls 
	- 'salt/minion/*/start':
		- /srv/reactor/auth-complete.sls 
```


