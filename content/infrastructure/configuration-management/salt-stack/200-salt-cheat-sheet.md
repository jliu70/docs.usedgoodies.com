+++
weight = 200
toc = true
date = "2017-03-17T05:03:55Z"
title = "200 salt cheat sheet"
+++

Minion Basic Connectivity Test
salt -G 'os:Windows' test.ping
salt -G 'os:RedHat' test.ping

Minion Version
```
salt ‘*’ test.version
```
```
[root@CCC3D001 ~]# salt-run manage.versions
Master:

2016.11.3
Up to date:
----------
DCDA0000.ppprivmgmt.intraxa:
2016.11.3
DCDA0001.PPPRIVMGMT.intraxa:
2016.11.3
DCDA0002.PPPRIVMGMT.intraxa:
2016.11.3
DCDA0004.ppprivmgmt.intraxa:
2016.11.3
DCDA0006.ppprivmgmt.intraxa:
2016.11.3
DCDA0008.ppprivmgmt.intraxa:
2016.11.3
```


Available grains can be listed by using the 'grains.ls' module:
```
salt '*' grains.ls
```
Grains data can be listed by using the 'grains.items' module:
```
salt '*' grains.items
```
Individual grain data can be listed by using the 'grains.item' module:
```
salt '*' grains.item ipv4
salt \* grains.item os
salt \* grains.item num_cpus
salt \* grains.item osfullname
salt \* grains.item osrelease
salt '*' grains.item osfinger
```



Additional Reference: http://stackoverflow.com/questions/8372537/converting-olson-tzid-to-windows-time-zone
https://docs.saltstack.com/en/latest/ref/modules/all/salt.modules.win_timezone.html
```
[root@CCC3D001 salt]# salt -G 'os:Windows' grains.item timezone
DCDA0001.PPPRIVMGMT.intraxa:

----------
timezone:
(UTC-05:00) Eastern Time (US & Canada)
DCDA0005.PPPRIVMGMT.intraxa:
----------
timezone:
(UTC-05:00) Eastern Time (US & Canada)
DCDA0002.PPPRIVMGMT.intraxa:
----------
timezone:
(UTC) Coordinated Universal Time
```

List installed Software
```
salt '*' pkg.list_pkgs
salt -G 'os:Windows' pkg.list_pkgs
```


Ad-hoc commands
```
[root@CCC3D001 chrony]# salt -G os:RedHat cmd.run 'chronyc sources'
DCDA0000.ppprivmgmt.intraxa:

210 Number of sources = 6
MS Name/IP address Stratum Poll Reach LastRx Last sample
===============================================================================
^+ PPNAATSDCPT01.equitable.c 7 6 37 24 -7479us[ -364us] +/- 397ms
^* PPNAATSDCPT02.equitable.c 7 6 37 23 +7461us[ +15ms] +/- 400ms
```


Copy file to Minions
https://docs.saltstack.com/en/latest/ref/modules/all/salt.modules.cp.html
```
[root@CCC3D001 base]# salt -G host:dcda0004 cp.get_file salt://software/uxauth-128-1.0.1912.x86_64.rpm /tmp/uxauth-128-1.0.1912.x86_64.rpm
DCDA0004.ppprivmgmt.intraxa:

/tmp/uxauth-128-1.0.1912.x86_64.rpm
```

http://stackoverflow.com/questions/27687886/copy-a-file-from-salt-master-to-minions
```
/var/tmp/rpm:
file.recurse:

- source: salt://software/
- target: /var/tmp
- makedirs: True
```

Salt sate apply to all
```
salt \* state.apply
```

Salt state apply specific state to target Minions
```
salt DCDA0008.ppprivmgmt.intraxa state.apply unab.config
```


On target minon
```
salt-call state.sls unab -l debug
```


Checking Salt Minions status
Reference: https://serverfault.com/questions/529049/how-do-i-list-all-connected-salt-stack-minions
"salt-run -d" Gives a full list of all the modules you can run with salt-run, well worth a read

```
[root@CCC3D001 ~]# salt-run manage.up
- DCDA0008.ppprivmgmt.intraxa
- DCDA0002.PPPRIVMGMT.intraxa
- DCDA0001.PPPRIVMGMT.intraxa
- DCDA0006.ppprivmgmt.intraxa
- DCDA0000.ppprivmgmt.intraxa
- DCDA0004.ppprivmgmt.intraxa

[root@CCC3D001 ~]# salt-run manage.status
down:

- DCDA0005.PPPRIVMGMT.intraxa
up:
- DCDA0008.ppprivmgmt.intraxa
- DCDA0002.PPPRIVMGMT.intraxa
- DCDA0001.PPPRIVMGMT.intraxa
- DCDA0006.ppprivmgmt.intraxa
- DCDA0000.ppprivmgmt.intraxa
- DCDA0004.ppprivmgmt.intraxa
[root@CCC3D001 ~]# salt-run manage.down
- DCDA0005.PPPRIVMGMT.intraxa
[root@CCC3D001 ~]#
[root@CCC3D001 ~]# salt-run manage.list_not_state show_ipv4="True"
- DCDA0005.PPPRIVMGMT.intraxa
[root@CCC3D001 ~]# salt-run manage.alived show_ipv4="True"
DCDA0000.ppprivmgmt.intraxa:

10.79.204.10
DCDA0001.PPPRIVMGMT.intraxa:
10.79.204.11
DCDA0002.PPPRIVMGMT.intraxa:
10.79.204.13
DCDA0004.ppprivmgmt.intraxa:
10.79.204.15
DCDA0006.ppprivmgmt.intraxa:
10.79.204.17
DCDA0008.ppprivmgmt.intraxa:
10.79.204.19
[root@CCC3D001 ~]# salt-run manage.alived
- DCDA0000.ppprivmgmt.intraxa
- DCDA0001.PPPRIVMGMT.intraxa
- DCDA0002.PPPRIVMGMT.intraxa
- DCDA0004.ppprivmgmt.intraxa
- DCDA0006.ppprivmgmt.intraxa
- DCDA0008.ppprivmgmt.intraxa

[root@CCC3D001 ~]# salt-run manage.up show_ipv4="True"
The following keyword arguments are not valid: show_ipv4=True


[root@CCC3D001 master]# salt-run manage.versions
Master:

2016.11.3
Up to date:
----------
DCDA0000.ppprivmgmt.intraxa:
2016.11.3
DCDA0001.PPPRIVMGMT.intraxa:
2016.11.3
DCDA0002.PPPRIVMGMT.intraxa:
2016.11.3
DCDA0004.ppprivmgmt.intraxa:
2016.11.3
DCDA0006.ppprivmgmt.intraxa:
2016.11.3
DCDA0008.ppprivmgmt.intraxa:
2016.11.3
DCDA000A.ppprivmgmt.intraxa:
2016.11.3
DCDA000B.ppprivmgmt.intraxa:
2016.11.3
DCDA1000.ppprivmgmt.intraxa:
2016.11.3
```


Salt Cleanup - method to delete keys
https://github.com/saltstack/salt/issues/13141
```
# SALT_CRON_IDENTIFIER:Clean old minion keys
*/5 * * * * salt-run manage.down removekeys=True
```

Experimental: Add Thorium (Complex Reactor) states to auto clean out old keys  https://github.com/saltstack/salt/issues/34358
https://docs.saltstack.com/en/latest/topics/thorium/index.html
```
[root@CCC3D001 master]# salt-run manage.down removekeys=True
- DCDA0005.PPPRIVMGMT.intraxa
```

Troubleshooting
https://docs.saltstack.com/en/latest/topics/troubleshooting/
On the minion: salt-call -l debug state.apply
```
salt-run jobs.lookup_jid 20170506195613526738
```

https://docs.saltstack.com/en/latest/topics/jobs/
```
salt-run jobs.active
```

https://docs.saltstack.com/en/latest/ref/modules/all/salt.modules.saltutil.html
```
salt '*' saltutil.kill_all_jobs
salt '*' saltutil.is_running state.highstate
```

Minion calling specific salt state
```
salt-call state.sls unab -l debug
```
