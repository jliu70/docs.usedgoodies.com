+++
toc = true
date = "2019-11-24T12:47:50Z"
title = "Prometheus Resources"
weight = 900
+++



11/24/2019


Prometheus Deep Dive
https://www.youtube.com/watch?v=Me-kZi4xkEs


## Tutorial



11/15/2019
CNCF Cloud Native Interactive Landscape
https://landscape.cncf.io/






12/1/2017
Prometheus
FYI – did a quick and dirty test and Prometheus seems to work well. 
  NOTE: I did not test AlertManager (module which sends emails or other formats for alerts) – maybe next week if I have some free time.
Pro:  Extremely simple to get up and running – just one binary on the server, and one node_collector binary on hosts.
Con: No automatic discovery – still needs to have configs updated – but can be scripted easily.
 
 
Prometheus is an open source monitoring system developed by SoundCloud.
 
https://prometheus.io/blog/2017/11/08/announcing-prometheus-2-0/
 
Like Graphite, Prometheus stores all its data in a time series database.
 
It's the new "fad" and seems to be better suited for monitoring services as opposed to individual hosts and therefore more applicable to containerized environments on Kubernetes.
https://grafana.com/blog/2016/01/12/evolving-from-machines-to-services/
 
 
 
Comparison to other monitoring tools
https://prometheus.io/docs/introduction/comparison/
https://stackoverflow.com/questions/35305170/whats-the-difference-between-prometheus-and-zabbix
https://medium.com/@amit.bezalel/the-docker-age-monitoring-showdown-bda595b4b599
 
 
Intro
https://prometheus.io/docs/introduction/first_steps/
https://prometheus.io/docs/introduction/faq/
 
 
 
Binaries
https://prometheus.io/download/
https://github.com/prometheus/prometheus/releases
 
 
CentOS 7
https://www.digitalocean.com/community/tutorials/how-to-use-prometheus-to-monitor-your-centos-7-server
https://fritshoogland.wordpress.com/2017/07/31/installation-overview-of-node_exporter-prometheus-and-grafana/
https://www.linuxnix.com/setting-up-prometheus-monitoring-tool-on-centos-7/
 
 
Monitoring Linux hosts via Node_exporter
https://github.com/prometheus/node_exporter
Didn’t see it in EPEL  but it’s supposed to be there?  https://copr.fedorainfracloud.org/coprs/ibotty/prometheus-exporters/
 
 
Monitoring Windows hosts via WMI exporter
https://github.com/martinlindhe/wmi_exporter
 
 
Sending alerts via AlertManager
https://github.com/prometheus/alertmanager
https://github.com/martinlindhe/wmi_exporter/releases
https://gist.github.com/hemdagem/d74241b72167202d7715a80985cef611
 
 


