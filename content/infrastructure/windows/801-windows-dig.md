+++
date = "2019-12-10T13:46:39Z"
toc = true
weight = 801
title = "Windows dig"
+++



12/8/2019

Dig for Windows
https://nil.uniza.sk/how-install-dig-dns-tool-windows-10/
https://www.smythsys.es/11248/como-instalar-dig-en-windows/


```

Microsoft Windows [Version 10.0.18362.30]
(c) 2019 Microsoft Corporation. All rights reserved.

C:\Users\jeffrey.liu>Downloads\BIND9.14.8.x64\dig.exe -p 8600 usldnygo9ylc2z2.node.dc1.consul @127.0.0.1

; <<>> DiG 9.14.8 <<>> -p 8600 usldnygo9ylc2z2.node.dc1.consul @127.0.0.1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 39423
;; flags: qr aa rd; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 2
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;usldnygo9ylc2z2.node.dc1.consul. IN    A

;; ANSWER SECTION:
usldnygo9ylc2z2.node.dc1.consul. 0 IN   A       127.0.0.1

;; ADDITIONAL SECTION:
usldnygo9ylc2z2.node.dc1.consul. 0 IN   TXT     "consul-network-segment="

;; Query time: 0 msec
;; SERVER: 127.0.0.1#8600(127.0.0.1)
;; WHEN: Sun Dec 08 15:22:42 Eastern Standard Time 2019
;; MSG SIZE  rcvd: 112


C:\Users\jeffrey.liu>


```



```
jeffrey.liu@USLDNYGO9YLC2Z2 MINGW64 ~  
$ ~/Downloads/BIND9.14.8.x64/dig -p 8600 usldnygo9ylc2z2.node.dc1.consul @127.0.0.1

; <<>> DiG 9.14.8 <<>> -p 8600 usldnygo9ylc2z2.node.dc1.consul @127.0.0.1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 30891
;; flags: qr aa rd; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 2
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;usldnygo9ylc2z2.node.dc1.consul. IN    A

;; ANSWER SECTION:
usldnygo9ylc2z2.node.dc1.consul. 0 IN   A       127.0.0.1

;; ADDITIONAL SECTION:
usldnygo9ylc2z2.node.dc1.consul. 0 IN   TXT     "consul-network-segment="

;; Query time: 3 msec
;; SERVER: 127.0.0.1#8600(127.0.0.1)
;; WHEN: Sun Dec 08 15:13:03 Eastern Standard Time 2019
;; MSG SIZE  rcvd: 112

```


A quick primer on dig
https://mrkaran.dev/posts/dig-overview/
https://news.ycombinator.com/item?id=21721205


Keep it short
`dig +short` keeps the information to a bare minimum and only displays the ANSWER

```
dig +short www.google.com

```

Nameserver details
If you want to find the nameserver

```
dig axa.com ns +short
```

Use a specific DNS server
```
dig www.ax.com @1.1.1.1
```

PTR lookup
```
dig -x 10.79.192.22 +short
```

Multiple queries
```
$ cat digfile
mrkaran.dev
joinmastodon.org
zoho.com
```
To list down all MX records for the domains in a file, you can use something like:
```
$ dig -f digfile +noall mx +answer
mrkaran.dev.		242	IN	MX	10 mx.zoho.in.
mrkaran.dev.		242	IN	MX	20 mx2.zoho.in.
mrkaran.dev.		242	IN	MX	50 mx3.zoho.in.
joinmastodon.org.	21599	IN	MX	10 in1-smtp.messagingengine.com.
joinmastodon.org.	21599	IN	MX	20 in2-smtp.messagingengine.com.
zoho.com.		299	IN	MX	10 smtpin.zoho.com.
zoho.com.		299	IN	MX	20 smtpin2.zoho.com.
zoho.com.		299	IN	MX	50 smtpin3.zoho.com.
```

Search list
option +search which can be used to find all domains matching the search path defined in /etc/resolv.conf 

```
dig redis +search +short
10.100.32.73
```



Looking up SRV records for KMS
```

[root@ccfa2049 ~]# dig @10.77.250.21 -t SRV _vlmcs._tcp.prprivmgmt.intraxa

; <<>> DiG 9.9.4-SuSE-9.9.4-73.el7_6 <<>> @10.77.250.21 -t SRV _vlmcs._tcp.prprivmgmt.intraxa
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 8635
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 3

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4000
;; QUESTION SECTION:
;_vlmcs._tcp.prprivmgmt.intraxa.        IN      SRV

;; ANSWER SECTION:
_vlmcs._tcp.prprivmgmt.intraxa. 3600 IN SRV     0 0 1688 cs2a000f.prprivmgmt.intraxa.
_vlmcs._tcp.prprivmgmt.intraxa. 3600 IN SRV     0 0 1688 cs2a000e.prprivmgmt.intraxa.

;; ADDITIONAL SECTION:
cs2a000f.prprivmgmt.intraxa. 3600 IN    A       10.109.209.194
cs2a000e.prprivmgmt.intraxa. 3600 IN    A       10.109.209.193

;; Query time: 355 msec
;; SERVER: 10.77.250.21#53(10.77.250.21)
;; WHEN: Wed Dec 18 14:58:12 CST 2019
;; MSG SIZE  rcvd: 185



```
