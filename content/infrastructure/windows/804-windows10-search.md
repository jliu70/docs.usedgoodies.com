+++
date = "2020-02-05T13:46:39Z"
toc = true
weight = 802
title = "Windows 10 Search"
+++




## Windows 10 Tip: Desktop search - disable Bing search integration


Hi Jeff 

You can open the Taskbar and close the Cortana  and End task   and then you should be able to do the search functionality , It worked for me. Thanks


Thanks & Regards,
Sreenivasa reddy
AXA-Group Operations Americas
3 W 35th Street, 11th Floor, New York, NY 10001 USA .
Mobile : +13478431764
Email : sreenivasa.sangu@axa.com 


Life is like riding a bicycle. To keep your balance, you must keep moving -  Albert Einstein


Internal
From: LIU Jeffrey <jeffrey.liu@axa.com> 
Sent: Wednesday, February 5, 2020 1:10 PM
To: PATEL Saumil <saumil.patel@axa.com>; SANGU Sreenivasa <sreenivasa.sangu@axa.com>; DEWITT Kenneth <kenneth.dewitt.external@axa.com>
Subject: WIndows 10 search 

FYI

In case other people have complained that the search functionality in Windows 10 is broken.

https://www.windowscentral.com/windows-search-down-many-showing-blank-box-instead-search-results

You can disable Bing integration in the local Windows 10 desktop search.
This seems to have worked, although it requires a reboot for it to take effect.

Thanks,
Jeff


