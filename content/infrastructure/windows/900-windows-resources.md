+++
date = "2019-12-09T13:46:39Z"
toc = true
weight = 900
title = "  Windows Resources"
+++

Windows 


## jq
https://stedolan.github.io/jq/download/

```
jeffrey.liu@USLDNYGO9YLC2Z2 MINGW64 ~

$ curl https://geo.risk3sixty.com/me | ~/Downloads/jq-win64.exe -C
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   173  100   173    0     0    285      0 --:--:-- --:--:-- --:--:--   285
{
  "ip": "141.191.36.10",
  "range": [
    2378114056,
    2378114063
  ],
  "country": "US",
  "region": "",
  "eu": "0",
  "timezone": "America/Chicago",
  "city": "",
  "ll": [
    37.751,
    -97.822
  ],
  "metro": 0,
  "area": 1000
}
```



### 11/11/2019
git bash for windows
https://superuser.com/questions/1053633/what-is-git-bash-for-windows-anyway


five-best-rdp-connecion-managers.txt
https://activedirectorypro.com/rdp-connection-manager/

mRemoteNG  GPLv2
https://mremoteng.org/download



### 2/3/2020
GPO Group Policy Object results command
```

PS C:\Users\virtuser> gpresult /r
 
Microsoft (R) Windows (R) Operating System Group Policy Result tool v2.0
© 2016 Microsoft Corporation. All rights reserved.
 
Created on 11/11/2019 at 10:27:45 AM
 
 
RSOP data for CM3A0013\virtuser on CM3A0013 : Logging Mode
-----------------------------------------------------------
 
OS Configuration:            Member Server
OS Version:                  10.0.14393
Site Name:                   NA01
Roaming Profile:             N/A
Local Profile:               C:\Users\virtuser
Connected over a slow link?: No
 
 
COMPUTER SETTINGS
------------------
 
    Last time Group Policy was applied: 11/11/2019 at 8:41:56 AM
    Group Policy was applied from:      CCCZ0504.WWMGMT.intraxa
    Group Policy slow link threshold:   500 kbps
    Domain Name:                        WWMGMT
    Domain Type:                        Windows 2008 or later
 
    Applied Group Policy Objects
    -----------------------------
        NA_SRV_OPS_ALL
        NA_SRV_ALL_SIEM
        WW_SRV_ALL_ArcSight_C
        WW_SRV_S16_MTSB_BaseRules
        Default Domain Policy
        WW_DOM_PrivilegeAccountsRestrictions_C
        WW_DOM_WindowsTimeMember_C
        WW_DOM_ComputerParameters_C
        Local Group Policy
 
    The following GPOs were not applied because they were filtered out
    -------------------------------------------------------------------
        WW_SRV_S12_MTSB_BaseRules
            Filtering:  Denied (WMI Filter)
 
        WW_SRV_S08_MTSB_BaseRules
            Filtering:  Denied (WMI Filter)
 
    The computer is a part of the following security groups
    -------------------------------------------------------
        BUILTIN\Administrators
        Everyone
        Message Capture Users
        Performance Log Users
        BUILTIN\Users
        NT AUTHORITY\NETWORK
        NT AUTHORITY\Authenticated Users
        This Organization
        CM3A0013$
        Domain Computers
        Authentication authority asserted identity
        System Mandatory Level
 
 
USER SETTINGS
--------------
 
    Last time Group Policy was applied: 10/31/2019 at 5:39:56 PM
    Group Policy was applied from:      N/A
    Group Policy slow link threshold:   500 kbps
    Domain Name:                        CM3A0013
    Domain Type:                        Windows 2008 or later
 
    Applied Group Policy Objects
    -----------------------------
        Local Group Policy
 
    The user is a part of the following security groups
    ---------------------------------------------------
        None
        Everyone
        Local account and member of Administrators group
        BUILTIN\Users
        BUILTIN\Administrators
        Remote Desktop Users
        REMOTE INTERACTIVE LOGON
        NT AUTHORITY\INTERACTIVE
        NT AUTHORITY\Authenticated Users
        This Organization
        Local account
        LOCAL
        NTLM Authentication
        High Mandatory Level
PS C:\Users\virtuser>
 
```




### 2/5/2020
https://www.windowscentral.com/windows-search-down-many-showing-blank-box-instead-search-results

#### Remove Bing from your searches
In light of the recent Windows 10 Search issues where nothing is displayed in the bar or window, many have found a fix by disabling Bing integration. This does require working with RegEdit, so doing a backup of both your PC and its registry are recommended.

Once you've performed any necessary backups, run through these steps to get Search working again. Multiple people here at Windows Central have performed the same steps with success.

Hit the Windows Key + R shortcut on your keyboard.
Type regedit and hit Enter on your keyboard.
Double-click HKEY_CURRENT_USER.
Double-click SOFTWARE.
Double-click Microsoft.
Double-click Windows.
Double-click CurrentVersion.
Double-click Search.
Right-click the right pane of the RegEdit window to bring up the menu.
Click New.
Click D-WORD (32-bit) Value.
Type BingSearchEnabled and hit Enter on your keyboard.
Double-click the BingSearchEnabled entry you just created.
Type 0 in the Value data field. (It should already be 0 but make sure).
Click OK.
Double-click CortanaConsent.
Type 0 in the data field. (It should also already be 0 but make sure).
Click OK.
Restart your PC.
This should bring Windows 10 (desktop/local) Search back online.