+++
date = "2019-12-23T13:46:39Z"
toc = true
weight = 802
title = "Windows 10 Multiple Desktops"
+++




## Windows 10 Tip: Use Multiple Desktops


### New Desktop - Keyboard shortcut: WINKEY + CTRL + D.
### Swith Desktop - Keyboard shortcut: WINKEY + CTRL + LEFT ARROW or WINKEY + CTRL + RIGHT ARROW.
### Close a desktop: WINKEY + CTRL + F4 (will close the current desktop)
### Switch between App Windows - Keyboard shortcut: ALT + TAB.

### Open Task View (similar to Mac OS X Expose) - Keyboard shortcut:  WINKEY + TAB

https://www.itprotoday.com/windows-server/windows-10-tip-use-multiple-desktops


https://support.microsoft.com/en-us/help/4028538/windows-10-multiple-desktops

https://www.howtogeek.com/198122/32-new-keyboard-shortcuts-in-the-windows-10-technical-preview/

