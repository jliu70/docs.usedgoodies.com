+++
toc = true
date = "2019-12-03T10:47:50Z"
title = "Consul Service Discover Instruqt interactive lab"
weight = 803
+++

12/3/2019

https://instruqt.com/hashicorp/tracks/service-discovery-with-consul

Tired of manually updating IP addresses in config files? Is your network CMDB or spreadsheet woefully out of date? Learn how Consul can automatically keep your service catalog up to date and current.

Join us on an adventure of service discovery. In this track you'll learn how to connect a web application with its database using Consul. Topics covered include service registration, health checks, service discovery, automated config management, and seamless DNS integration.


### Gathering info

Consul has a complete and up-to-date map of all the hosts in your network.

Gone are the days of cumbersome, error-prone spreadsheets and Configuration Management Database (CMDB) systems.

The current IP address of each host is easy to find in the Consul UI.

Visit the Nodes tab of the Consul UI to see the IP addresses of all your machines.



The website is DOWN! Your application server is not working correctly. You've determined that the app can't connect to the database.

Click on the Nodes tab in the Consul UI to find the database server's IP address.

Edit the wp-config.php file in the App Config tab. On line 32 you'll find the configuration for the database server. Update the IP address in the file and save it with CTRL-S.

Verify that the application is loading in the Website tab. You may need to hit the refresh button for the site to load.


#### Configure health check

Now that you've got the website back up and running, it's time to put a health check on that database service.

Application health checks are easy to build and can check a wide array of conditions. Once you have this rich data it is easy to build automation around it.

In this challenge you'll configure a service health check on the database server, so that you'll always know where the database service is, and whether it is healthy.

https://youtu.be/CIv65T172mU


In this challenge you'll create a service check that will register the database service in Consul's catalog. You'll need to copy a service definition file into the Consul config directory to activate the service.

Click on the database server tab and use the cat command to have a look inside the service definition file:

```
# cat /database_service.json
{ "service":
  { "name": "mysql",
    "tags": ["database","production"],
    "port": 3306,
    "check": {
      "id": "mysql",
      "name": "MySQL TCP on port 3306",
      "tcp": "localhost:3306",
      "interval": "10s",
      "timeout": "1s"
    }
  }
}
#
```

The config file contains a service check named mysql with a health check that monitors port 3306. If the database ever goes down Consul can immediately mark it as unhealthy. Consul can automatically route traffic to healthy nodes.

Next copy the file into the Consul config directory:

```
cp /database_service.json /consul/config/database_service.json
```

Then go ahead and reload the Consul service:

```
consul reload
```

Look at the services tab in the Consul UI. You should now see a service name and health check for your database.

To find the health check in the UI click on Services and then mysql.

NOTE: It may take a moment for the health check to show up as healthy.



### Add a Service check

You can use Consul to monitor all kinds of services. In this challenge you'll add a service check to your application.

Just like in the previous challenge, you'll need to copy a service definition file and reload Consul.

Click on the application server tab and use the cat command to have a look inside the service definition file:
```
root@app:~# cat /app_service.json
{ "service":
  { "name": "http",
    "tags": ["application","production"],
    "port": 80,
    "check": {
      "id": "http",
      "name": "Web traffic on port 80",
      "tcp": "localhost:80",
      "interval": "10s",
      "timeout": "1s"
    }
  }
}
root@app:~#
```

The config file contains a service check named http with a health check that monitors port 80. If the app ever goes down Consul can immediately mark it as unhealthy. Consul can automatically route traffic to healthy nodes.

Next copy the file into the Consul config directory:
```
cp /app_service.json /consul/config/app_service.json
```

Then go ahead and reload the Consul service:
```
consul reload
```
Look at the services tab in the Consul UI. You should now see a service name and health check for your application.

To find the health check in the UI click on Services and then http.

NOTE: It may take a moment for the health check to show up as healthy.


### Consul Template
The database and application are both registered in the Consul catalog. The health status and IP addresses of each service are always up to date, and can be queried at any time.

In the next challenge you'll automate the configuration of your wp-config.php file using Consul Template.

Consul Template is a small agent that can manage files and populate them with data from the Consul catalog.

In this challenge you'll use consul-template to ensure that the application config always has the correct IP address. Run the following command on the App Server terminal tab to query the Consul service catalog for your database server:
```
root@app:~# dig @localhost -p 8600 mysql.service.consul

; <<>> DiG 9.11.5-P4-5.1-Debian <<>> @localhost -p 8600 mysql.service.consul
; (2 servers found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 16556
;; flags: qr aa rd; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 2
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;mysql.service.consul.          IN      A

;; ANSWER SECTION:
mysql.service.consul.   0       IN      A       10.96.26.209

;; ADDITIONAL SECTION:
mysql.service.consul.   0       IN      TXT     "consul-network-segment="

;; Query time: 2 msec
;; SERVER: 127.0.0.1#8600(127.0.0.1)
;; WHEN: Tue Dec 03 16:00:42 UTC 2019
;; MSG SIZE  rcvd: 101

root@app:~#
```
In the output you'll see a line called ANSWER SECTION. Right below it is the current IP address of your database server. Consul template can automatically insert this IP address into your application config file.

Click on the Consul Template tab and look inside the file. Consul template can update your wp-config.php file whenever the database IP address changes. Notice the template configuration for DB_HOST on line 32 of the file. This part is automatically replaced with the address of your healthy database server.

Run the following commands on in the App Server terminal to activate Consul Template.
```
cd /var/www/html
consul-template  -log-level debug -template "wp-config.php.tpl:wp-config.php"
```

Reload the `wp-config.php` file in your App Config tab to see the updated IP address.

Open the Website tab and verify that the web app is loading correctly.

Consul Template will continuously watch for changes and update your configuration files automatically.

The Template file does not have the hard-coded value as the original file does:

/var/www/html/wp-config.php.tpl
```
<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress');

/** MySQL database username */
define( 'DB_USER', 'root');

/** MySQL database password */
define( 'DB_PASSWORD', 'HashiCorp123');

/** MySQL hostname */
define( 'DB_HOST', '{{ range service "mysql" }}{{ .Address }}{{ end }}');
...
...
...
```



/var/www/html/wp-config.php
```
<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress');

/** MySQL database username */
define( 'DB_USER', 'root');

/** MySQL database password */
define( 'DB_PASSWORD', 'HashiCorp123');

/** MySQL hostname */
define( 'DB_HOST', '10.96.26.209');
...
...
...
```




### Seamless Service Discover with Consul DNS

The Consul catalog can seamlessly integrate with your current application infrastructure without changing any of your code. Imagine giving all your applications and services easy access to configuration data, health checks, and network info.

This is the magic of Consul DNS integration. With this configuration any application that speaks DNS can take advantage of the powerful Consul service catalog. In the next challenge we'll use dnsmasq to route all local queries for *.consul addresses to the Consul service. Our application will now be able to use a simple Consul-managed DNS name for connecting to the database.

There are several different ways to integrate Consul into your system DNS.
Check out the HashiCorp Learn guide to learn more about DNS forwarding: https://learn.hashicorp.com/consul/security-networking/forwarding

In this exercise you'll configure Consul to act as a local DNS server, providing seamless access to your service catalog via simple DNS hostnames. The Linux dnsmasq service is running on your application server.

Run the following command to see the dnsmasq configuration:
```
root@app:~# cat /etc/dnsmasq.d/consul
user=root
server=/consul/127.0.0.1#8600
server=10.94.0.10
root@app:~#
```

The server config lines say Direct all traffic for .consul queries to Consul on local port 8600, everything else to our normal DNS server.

Now look at the /etc/resolv.conf file. This is the file that controls where DNS queries are routed.
```
root@app:~# cat /etc/resolv.conf
nameserver 127.0.0.1
search 4o8qws1ildpr-7621d93834a3233b6d866721455cced0.svc.cluster.local svc.cluster.local cluster.local c.instruqt-prod.internal google.internal
options ndots:5
root@app:~#
```

We are now routing all DNS queries through our local system, but only *.consul queries are handled by Consul. All other DNS traffic is passed upstream to the corporate DNS server. Try it yourself with the dig command:

```
root@app:~# dig mysql.service.consul

; <<>> DiG 9.11.5-P4-5.1-Debian <<>> mysql.service.consul
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 57129
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 2

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;mysql.service.consul.          IN      A

;; ANSWER SECTION:
mysql.service.consul.   0       IN      A       10.96.26.209

;; ADDITIONAL SECTION:
mysql.service.consul.   0       IN      TXT     "consul-network-segment="

;; Query time: 3 msec
;; SERVER: 127.0.0.1#53(127.0.0.1)
;; WHEN: Tue Dec 03 16:06:59 UTC 2019
;; MSG SIZE  rcvd: 101

root@app:~#
```


Edit line 32 of the wp-config.php file again, but this time instead of using an IP address use the Consul DNS name for your database server node:
```
mysql.service.consul
```

Visit the Website tab and verify that the app has connected to the database using Consul DNS.

```
<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress');

/** MySQL database username */
define( 'DB_USER', 'root');

/** MySQL database password */
define( 'DB_PASSWORD', 'HashiCorp123');

/** MySQL hostname */
define( 'DB_HOST', 'mysql.service.consul');

```



For example, dig `mysql.service.consul` to the upstream DNS server will fail:
```
root@app:~# dig @10.94.0.10 www.google.com

; <<>> DiG 9.11.5-P4-5.1-Debian <<>> @10.94.0.10 www.google.com
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 28995
;; flags: qr rd ra; QUERY: 1, ANSWER: 6, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;www.google.com.                        IN      A

;; ANSWER SECTION:
www.google.com.         55      IN      A       64.233.166.105
www.google.com.         55      IN      A       64.233.166.106
www.google.com.         55      IN      A       64.233.166.147
www.google.com.         55      IN      A       64.233.166.103
www.google.com.         55      IN      A       64.233.166.104
www.google.com.         55      IN      A       64.233.166.99

;; Query time: 3 msec
;; SERVER: 10.94.0.10#53(10.94.0.10)
;; WHEN: Mon Dec 30 14:35:56 UTC 2019
;; MSG SIZE  rcvd: 139

```


External DNS queries to the upstream DNS server:
```
root@app:~# dig @10.94.0.10 www.google.com

; <<>> DiG 9.11.5-P4-5.1-Debian <<>> @10.94.0.10 www.google.com
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 28995
;; flags: qr rd ra; QUERY: 1, ANSWER: 6, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;www.google.com.                        IN      A

;; ANSWER SECTION:
www.google.com.         55      IN      A       64.233.166.105
www.google.com.         55      IN      A       64.233.166.106
www.google.com.         55      IN      A       64.233.166.147
www.google.com.         55      IN      A       64.233.166.103
www.google.com.         55      IN      A       64.233.166.104
www.google.com.         55      IN      A       64.233.166.99

;; Query time: 3 msec
;; SERVER: 10.94.0.10#53(10.94.0.10)
;; WHEN: Mon Dec 30 14:35:56 UTC 2019
;; MSG SIZE  rcvd: 139
```

