+++
toc = true
date = "2019-11-24T12:47:50Z"
title = "Consul Resources"
weight = 900
+++



https://www.consulproject.io

https://www.consul.io/intro/index.html

Consul vs Other software
https://www.consul.io/intro/vs/index.html



11/26/2019
https://www.hashicorp.com/blog/protecting-consul-from-rce-risk-in-specific-configurations/


11/24/2019

https://medium.com/velotio-perspectives/a-practical-guide-to-hashicorp-consul-part-1-5ee778a7fcf4

https://medium.com/velotio-perspectives/a-practical-guide-to-hashicorp-consul-part-2-3c0ebc0351e8

https://www.consul.io/intro/vs/nagios-sensu.html


11/25/2019

https://medium.com/hashicorp-engineering/introduction-to-hashicorp-consul-connect-with-kubernetes-d7393f798e9d

https://testdriven.io/blog/running-vault-and-consul-on-kubernetes/

https://www.infoq.com/news/2018/11/consul-kubernetes/

https://www.consul.io/docs/connect/index.html#getting-started-with-connect



12/1/2019

https://www.consul.io/docs/connect/mesh_gateway.html


12/2/2019
https://www.hashicorp.com/blog/roadmap-preview-what-s-next-for-consul-service-mesh/

https://www.hashicorp.com/blog/category/consul/

https://thenewstack.io/implementing-service-discovery-of-microservices-with-consul/
https://thenewstack.io/implement-a-service-mesh-with-consul-connect/



Envoy
https://www.getenvoy.io/
Learn more: https://www.getenvoy.io/install/
RHEL: https://www.getenvoy.io/install/envoy/rhel/


https://github.com/envoyproxy/envoy

https://blog.envoyproxy.io/introduction-to-modern-network-load-balancing-and-proxying-a57f6ff80236

https://jvns.ca/blog/2018/10/27/envoy-basics/

https://blog.getambassador.io/envoy-vs-nginx-vs-haproxy-why-the-open-source-ambassador-api-gateway-chose-envoy-23826aed79ef

https://medium.com/@copyconstruct/envoy-953c340c2dca

https://www.youtube.com/watch?v=55yi4MMVBi4

https://www.youtube.com/watch?v=Pus2ytdEfrQ



## Tutorial
https://learn.hashicorp.com/consul

https://learn.hashicorp.com/consul/getting-started/next-steps


## Video 

What is a Service Mesh
https://www.hashicorp.com/resources/what-is-a-service-mesh


Introduction to Consul
https://www.youtube.com/watch?v=mxeMdl0KvBI&t=

Introduction to Consul Connect
https://www.youtube.com/watch?v=8T8t4-hQY74


Consul with Kubernetes
https://www.youtube.com/watch?v=K93ZaUzwEWk


What is the Crawl, Walk, Run Journey of Adopting Consul
https://www.youtube.com/watch?v=Qbo8Oc-pJwc


Connecting Services with Consul: Connect Deep-dive on Usage and Internals
https://www.youtube.com/watch?v=KZIu33sbwQQ




11/28/2019

Comparing Service Mesh Architectures
https://www.youtube.com/watch?v=cWfACBIp0a8
NOTE: seems that ISTIO is the open source service mesh which is emerging as the leader for Kubernetes service meshes.  
https://platform9.com/blog/kubernetes-service-mesh-a-comparison-of-istio-linkerd-and-consul/
https://www.consul.io/intro/vs/istio.html

 
How does Consul work with Kubernetes and other workloads?
https://www.youtube.com/watch?v=K93ZaUzwEWk&t=248s
 
 
Running Consul on Kubernetes and Beyond
https://www.youtube.com/watch?v=nZqAAjHI0c4&t=958s
 
 
5 Minute Guide to Consul
https://www.youtube.com/watch?v=ur81Vi6N0yI
 
 
How to get a service mesh into prod without getting fired
https://www.youtube.com/watch?v=XA1aGpYzpYg
 
Should you use Kubernetes and docker in your next project? (11/26/2019)
https://www.youtube.com/watch?v=u8dW8DrcSmo
 
 
Painless password rotation with Hashicorp Vault
https://www.youtube.com/watch?v=-74bFgUzOWo&t=1107s
 


12/1/2019
Getting started with Consul
https://www.youtube.com/watch?v=5iAVi2hctSI


HashiCorp Consul Demos
https://www.youtube.com/watch?v=k-GQCuFHrw0


12/2/2019
Interesting Consul demo and Q&A (5/23/2019)
https://youtu.be/QnvU4vCgAlE?t=2824

https://www.cncf.io/webinars/securing-cloud-native-communication/  (same speakers - 7/25/2019 - much more focus on Kubernetes and Ambassador)

https://instruqt.com/hashicorp/tracks/sock-shop-tutorial
Deploy Consul on Kubernetes with Consul Connect service mesh and Ambassador


https://instruqt.com/hashicorp/tracks/consul-basics
The track is a crash course in Consul operations. You will accomplish the following:
Set Up a 3 Node Consul Cluster
Use the Consul UI
Use the Consul CLI
Use the Consul API
Add an App Node to Your Cluster
Test Consul's High Availability
Add Basic Security with Consul's ACLs
Once you've learned these fundamentals you can proceed to intermediate and advanced topics like Service Discovery and Consul Connect.


https://instruqt.com/hashicorp/tracks/consul-connect
In this hands-on lab, you'll start two services and connect them over a TLS encrypted proxy with Consul Connect. Integrating applications with Consul Connect is as easy as talking to a port on localhost.


https://instruqt.com/hashicorp/tracks/service-discovery-with-consul
Tired of manually updating IP addresses in config files? Is your network CMDB or spreadsheet woefully out of date? Learn how Consul can automatically keep your service catalog up to date and current.
Join us on an adventure of service discovery. In this track you'll learn how to connect a web application with its database using Consul. Topics covered include service registration, health checks, service discovery, automated config management, and seamless DNS integration.


https://instruqt.com/hashicorp/tracks/service-mesh-with-consul
Evolve from service discovery to service mesh. No more mapping IP addresses and ports across load balancers and firewalls. Consul Connect ensures secure connections between services wherever they run.
This track will build on what you learned in the service discovery track and take our application from service discovery to service mesh. We'll dive into mesh fundamentals and use an Envoy proxy to connect our application to its database.


https://instruqt.com/hashicorp/tracks/consul-sandbox
The environment contains a cluster with an HTTP server, a client, and a Consul server. Each service also has a co-located Consul client.
There are tabs exposing a terminal on each of the nodes, an editor for the server node, and the UI for Consul.


https://instruqt.com/hashicorp/tracks/consul-l7-observability
L7 Observability into your Service Mesh with Consul Connect
As a number of loosely coupled services interact with each other as part of an application, observability helps debug and isolate communication failures. One of the key benefits of adopting a Service Mesh is that the fleet of sidecar proxies provide a uniform and consistent view of metrics, tracing and logging of all the services, irrespective of different programming languages and frameworks used by different teams. Consul forms the control plane layer of the Service Mesh, which simplifies the configuration of sidecar proxies for secure traffic communication and telemetry collection. Consul is built to support a variety of proxies as sidecars. Envoy’s lightweight footprint and observability support make it a preferred sidecar in production deployments.


https://instruqt.com/hashicorp/tracks/app-migration
Migrating applications from virtual machines to Kubernetes with Consul
Bridging the gap between brownfield and greenfield applications with a service mesh


https://instruqt.com/hashicorp/tracks/service-mesh
600 minutes
Service Mesh Workshop
Deploying and Observing Secure, and Highly Available Applications with a Service Mesh
Modern application architectures are embracing public clouds, microservices, and container schedulers like Kubernetes and Nomad. These bring complex service-to-service communication patterns, increased scale, dynamic IP addresses, and ephemeral infrastructure. A service mesh is an infrastructure layer that provides observability, reliability, and security for these modern deployment architectures.
In this workshop, you’ll learn how service meshes provide observability, reliability, and security. Through hands-on exercises, you’ll learn how to implement a service mesh in Kubernetes to deploy a reliable and highly available application.
Participants should have some experience deploying microservices and/or containerized applications. Experience with Kubernetes helpful but not required.


https://instruqt.com/hashicorp/tracks/service-mesh-with-consul-hybrid
Service Mesh with Consul Hybrid
Connect disparate platforms across datacenters.
In this track you leverage Mesh Gateways to connect applications and services between K8s and VMs in remote datacenters.



https://instruqt.com/hashicorp/tracks/consul-k8s-l7-observability
Layer 7 Observability with Kubernetes, Prometheus, Grafana
Collect and visualize layer 7 metrics from services in your Kubernetes cluster using Consul Connect, Prometheus, and Grafana.
A service mesh is made up of proxies deployed locally alongside each service instance, which control network traffic between their local instance and other services on the network. These proxies "see" all the traffic that runs through them, and in addition to securing that traffic, they can also collect data about it. Starting with version 1.5, Consul Connect is able to configure Envoy proxies to collect layer 7 metrics including HTTP status codes, request latency, along with many others, and export those to monitoring tools like Prometheus.

In this guide, you will deploy a basic metrics collection and visualization pipeline on a Kubernetes cluster using the official Helm charts for Consul, Prometheus, and Grafana. This pipeline will collect and display metrics from a demo application.

Tip: While this guide shows you how to deploy a metrics pipeline on Kubernetes, all the technologies the guide uses are platform agnostic; Kubernetes is not necessary to collect and visualize layer 7 metrics with Consul Connect.

Learning Objectives:

Configure Consul Connect with metrics using Helm
Install Prometheus and Grafana using Helm
Install and start the demo application
Collect metrics




Target 2016 talk on their implementation of Consul  - interesting use of dnsmasq mentioned
https://youtu.be/_w4O1RB1ZFo?t=1857

https://www.linux.com/tutorials/dns-and-dhcp-dnsmasq/
https://www.linux.com/tutorials/advanced-dnsmasq-tips-and-tricks/
https://www.linux.com/tutorials/dnsmasq-easy-lan-name-services/




12/5/2019
Demystifying Service Mesh  10/15/2019  (decent high-level overview)
https://www.youtube.com/watch?v=bEFILWrRJJ4

Consul Service networking made easy  10/21/2019
https://www.youtube.com/watch?v=Z4wkTDdjWBY

Consul Service Mesh Deep Dive  10/15/2019
https://www.youtube.com/watch?v=Aq1uTozNajI




12/14/2019
https://www.youtube.com/watch?v=GeU-7sgukRU

Not sure if this is still active?  Worth a look or just use Envjoy?

https://github.com/criteo/haproxy-consul-connect

https://www.hashicorp.com/resources/integrating-consul-connect-with-haproxy




## PDFs
[The Little Book of Consul PDF](/pdfs/consul/The_Little_Book_of_Consul.pdf)



1/28/2020
Consul Enterprise 1.7 - namespaces
https://youtu.be/Ff6kLvKkJBE?t=985
```
consul members
consul catalog services
consul services register -name api -address 1.1.1.1 -port 80
dig @localhost -p 8600 api.service.consul +short


consul services register -name web -address 1.1.1.2 -port 32768 -id 1.1.1.2:32768
consul services register -name web -address 1.1.1.2 -port 32769 -id 1.1.1.2:32769
dig @localhost -p 8600 web.service.consul SRV


consul services register -name team1-web -adress 1.1.1.1 -port 80
consul services register -name team2-web -adress 2.2.2.2 -port 80

```




### TODO Consul: Multi-Cloud and Multi-Platform Service Mesh
1/3/2020  https://medium.com/hashicorp-engineering/hashicorp-consul-multi-cloud-and-multi-platform-service-mesh-372a82264e8e
### TODO IP addresses are Cattle
12/15/2019 https://medium.com/hashicorp-engineering/ip-addresses-are-cattle-76b1c3d4a4db

### TODO Solutions Engineering Hangout: Securing Multi-Region Applications with Consul Connect 
10/29/2019 https://www.youtube.com/watch?v=u33pSaXjxDU





11/15/2019
CNCF Cloud Native Interactive Landscape
https://landscape.cncf.io/

12/2/2019
https://www.cncf.io/webinars/


