+++
toc = true
date = "2019-12-03T14:47:50Z"
title = "Consul Connect Instruqt Interactive Lab"
weight = 802
+++


## Tutorial
https://instruqt.com/hashicorp/tracks/consul-connect

In this hands-on lab, you'll start two services and connect them over a TLS encrypted proxy with Consul Connect. Integrating applications with Consul Connect is as easy as talking to a port on localhost.

The frontend will be a demo dashboard web application that displays a number. The backend will be a counting service that serves a JSON feed with a constantly incrementing number.


The frontend uses websockets to update its user interface every few seconds with fresh data from the backend counting service.

You'll spend most of your time executing commands against our demo applications (dashboard and counting), but Consul works to help services discover each other and connect through encrypted proxies.

We've configured Consul for you and started a single agent, so it's ready to go. Let's get started!


### Examining the counting service


We're already running Consul for you, with the Consul web UI running on port 8500.

To start, you'll see a red X next to the counting and dashboard services since neither are running (so both are unhealthy).

On the next slide we will explain what Consul Connect is and how it works.
https://www.youtube.com/watch?v=8T8t4-hQY74


Take a look at the contents of the counting service configuration.

The counting service configuration is stored in a file located at /etc/consul.d/counting.json.

You can do this run the following command in the Terminal tab:
```
root@consul:~# cat /etc/consul.d/counting.json
{
  "service": {
    "name": "counting",
    "port": 9003,
    "connect": {
      "proxy": {
      }
    },
    "check": {
      "id": "counting-check",
      "http": "http://localhost:9003/health",
      "method": "GET",
      "interval": "1s",
      "timeout": "1s"
    }
  }
}
root@consul:~#

```
The counting.json service configuration file contains three important settings:

Consul will look for a service running on port 9003. It will advertise that as the counting service. On a properly configured node, this can be reached as counting.service.consul through DNS.

A blank proxy is defined. This enables proxy communication through Consul Connect but doesn't define any connections right away.

A health check examines the local /health endpoint every second to determine whether the service is healthy and can be exposed to other services.




### Start the counting service

Now let's start some services.

In the following challenges we will then let them communicate via Consul Connect.

Start the counting service, specifying PORT as an environment variable.

You can do this by running the following command in the Terminal tab:
```
PORT=9003 counting-service
```
You can view the output of the counting service in the Counting tab at the top of the screen. It's a simple JSON API that returns a number.

If you refresh the Consul Web UI in the Consul UI tab, you'll notice that the counting service now shows as healthy.


### Start the dashboard service

In this challenge you'll start the frontend dashboard-service and it will communicate to the counting service through an encrypted proxy.

Now start the dashboard service, specifying PORT as an environment variable as in the previous challenge.

Consul is configured to look for the dashboard-service on port 9002. The configuration for this service is located at /etc/consul.d/dashboard.json.

You can do this by running the following command in the Terminal tab:

PORT=9002 dashboard-service

When started correctly, you can view the demo dashboard application in the Dashboard tab. And the health check will return alive in the Consul UI tab.


```
/etc/consul.d
  counting.json
  dashboard.json
  server.hcl
```

```
dashboard.json
{
  "service": {
    "name": "dashboard",
    "port": 9002,
    "connect": {
      "proxy": {
        "config": {
          "upstreams": [
            {
              "destination_name": "counting",
              "local_bind_port": 9001
            }
          ]
        }
      }
    },
    "check": {
      "id": "dashboard-check",
      "http": "http://localhost:9002/health",
      "method": "GET",
      "interval": "1s",
      "timeout": "1s"
    }
  }
}
```

### Deny Service connections

Let's secure these services by defining intentions in Consul.

Intentions are the way that we describe which services should be able to connect to other services.

Create an intention that denies all communication between all services.

This is achieved be creating an intention from * (All Services) to * (All Services) with a value of deny.

You can optionally add a description such as "Deny all communication by default".

Then save the newly created intention.

Since existing proxies will not be terminated when a deny rule is created, we must restart the dashboard-service.

Restart it by typing `killall dashboard-service` in the Terminal tab. We will then restart it for you.

If you look at the Dashboard and you will see that it cannot reach the backend counting-service because all connections are denied.




### Allow dashboard connection

For the final step in this tutorial, let's enable communication between the dashboard-service and the counting-service.

This intention will allow communication from the source dashboard service to the destination counting service.


Add an intention that allows the dashboard service to connect to the counting service.

You can do this by selecting dashboard in the first pulldown and counting in the second pulldown. Then selecting allow below. Optionally adding a description.

Then save the new intention.

Finally, view the Dashboard tab which will automatically discover the new connection (no refresh is needed for this websockets-driven application). It should not take more than a few seconds.

You will not need to restart any services since intentions which allow connectivity will take effect dynamically.



### My notes:  CLI interface for intentions

```
root@consul:~# consul intention --help
Usage: consul intention <subcommand> [options] [args]

  This command has subcommands for interacting with intentions. Intentions
  are permissions describing which services are allowed to communicate via
  Connect. Here are some simple examples, and more detailed examples are
  available in the subcommands or the documentation.

  Create an intention to allow "web" to talk to "db":

      $ consul intention create web db

  Test whether a "web" is allowed to connect to "db":

      $ consul intention check web db

  Find all intentions for communicating to the "db" service:

      $ consul intention match db

  For more examples, ask for subcommand help or view the documentation.

Subcommands:
    check     Check whether a connection between two services is allowed.
    create    Create intentions for service connections.
    delete    Delete an intention.
    get       Show information about an intention.
    match     Show intentions that match a source or destination.
root@consul:~#

```


Alternatively you can run via cli
```
root@consul:~# consul intention create -allow dashboard counting
Created: dashboard => counting (allow)

root@consul:~# consul intention check dashboard counting
Allowed

root@consul:~# consul intention match dashboard
* => * (deny)

root@consul:~# consul intention match counting
dashboard => counting (allow)
* => * (deny)

root@consul:~# consul intention delete dashboard counting
Intention deleted.

root@consul:~# consul intention match counting
* => * (deny)
root@consul:~#

```



```
server.hcl
data_dir= "/tmp/consul"

bootstrap = true

connect {
  enabled = true
  proxy {
   allow_managed_api_registration = true
   allow_managed_root = true
  }
}

server = true
ui = true

```



