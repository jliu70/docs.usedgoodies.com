+++
toc = true
date = "2020-01-01T12:47:50Z"
title = "Consul Service Mesh Observability Instruqt Interactive Lab"
weight = 807
+++


# Layer 7 Observability into your Service Mesh with Consul Connect


## Tutorial
https://instruqt.com/hashicorp/tracks/consul-l7-observability

One of the key benefits of adopting a Service Mesh is that the fleet of sidecar proxies provide a uniform and consistent view of metrics, tracing and logging of all the services, irrespective of different programming languages and frameworks used by different teams.

As a number of loosely coupled services interact with each other as part of an application, observability helps debug and isolate communication failures. One of the key benefits of adopting a Service Mesh is that the fleet of sidecar proxies provide a uniform and consistent view of metrics, tracing and logging of all the services, irrespective of different programming languages and frameworks used by different teams. Consul forms the control plane layer of the Service Mesh, which simplifies the configuration of sidecar proxies for secure traffic communication and telemetry collection. Consul is built to support a variety of proxies as sidecars. Envoy’s lightweight footprint and observability support make it a preferred sidecar in production deployments.


### Inspecting the cluster

This environment will be populated with a 3 node Consul cluster and 5 other nodes that will be running a three-tier application and Consul agents.

In this track you will be configuring a Service Mesh managed by Consul Connect to send L7 metrics to Prometheus.

You will then create dashboards in Grafana to view these metrics.

When applications are broken down into separate (micro)services, visibility into these services can be hard to achieve.

A single transaction can flow through a number of independently deployed services being developed and deployed by different teams.

A Service Mesh alleviates this problem by providing a networking infrastructure that leverages "sidecar" proxies for microservice deployments.

To learn more about Consul Connect and it's internals, you can watch the video:
Introduction to Consul Connect
https://www.youtube.com/watch?v=8T8t4-hQY74


In one of the terminals, check if the cluster has been formed correctly by running `consul members`.

There should be 8 nodes with status alive:
1. consul-1
2. consul-2
3. consul-3
4. api
5. cache
6. facebox
7. ingress
8. website

If for some reason one of the nodes did not come up, check if supervisord has started all services correctly by running `supervisorctl status` on the node of the missing service.

```
root@consul-1:~# consul members
Node      Address            Status  Type    Build  Protocol  DC   Segment
consul-1  10.96.25.36:8301   alive   server  1.5.0  2         dc1  <all>
consul-2  10.96.35.123:8301  alive   server  1.5.0  2         dc1  <all>
consul-3  10.96.25.37:8301   alive   server  1.5.0  2         dc1  <all>
api       10.96.30.47:8301   alive   client  1.5.0  2         dc1  <default>
cache     10.96.24.250:8301  alive   client  1.5.0  2         dc1  <default>
facebox   10.96.26.186:8301  alive   client  1.5.0  2         dc1  <default>
ingress   10.96.29.181:8301  alive   client  1.5.0  2         dc1  <default>
website   10.96.28.223:8301  alive   client  1.5.0  2         dc1  <default>
root@consul-1:~#
```

```
root@consul-1:~# supervisorctl status
consul                           RUNNING   pid 76, uptime 0:01:34
statsd                           RUNNING   pid 56, uptime 0:01:52
root@consul-1:~#
```


### Emjoifying

We will be using Emojify to show how to configure the Service Mesh Observability features of Consul Connect.

Emojify consists of 5 micro-services that are communicating via a Service Mesh.

To inspect the Service mesh, take a look at the Consul UI where you can see the registered services with accompanying sidecar proxies, and the defined upstreams for each proxy.

Check out the Consul UI to check if the sidecar proxy for each of the Emojify services is up and running.

As soon as all the services and the sidecar proxies are healthy, the Emojify application will work.

You can enable automatic refreshing of the services by going to the Settings page and enabling Blocking Queries.

Then try out the Emojify application by emojifying the following picture.

https://emojify.today/pictures/1.jpg






# NOTE: this lab isn't working and can't be completed

NOTE: stuck on module 2 - can't proceed to module 3

### Getting Insights
Get L3/L4 observability for our services

### L7 observability
Add L7 observability

### Explore
Explore


