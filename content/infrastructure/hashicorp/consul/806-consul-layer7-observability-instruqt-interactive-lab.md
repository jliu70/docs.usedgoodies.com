+++
toc = true
date = "2019-12-31T12:47:50Z"
title = "Consul Layer 7 Observability Instruqt Interactive Lab"
weight = 806
+++


# Layer 7 Observability with Kubernetes, Prometheus, Grafana


## Tutorial
https://instruqt.com/hashicorp/tracks/consul-k8s-l7-observability


Collect and visualize layer 7 metrics from services in your Kubernetes cluster using Consul Connect, Prometheus, and Grafana.

A service mesh is made up of proxies deployed locally alongside each service instance, which control network traffic between their local instance and other services on the network. These proxies "see" all the traffic that runs through them, and in addition to securing that traffic, they can also collect data about it. Starting with version 1.5, Consul Connect is able to configure Envoy proxies to collect layer 7 metrics including HTTP status codes, request latency, along with many others, and export those to monitoring tools like Prometheus.

In this guide, you will deploy a basic metrics collection and visualization pipeline on a Kubernetes cluster using the official Helm charts for Consul, Prometheus, and Grafana. This pipeline will collect and display metrics from a demo application.

Tip: While this guide shows you how to deploy a metrics pipeline on Kubernetes, all the technologies the guide uses are platform agnostic; Kubernetes is not necessary to collect and visualize layer 7 metrics with Consul Connect.

Learning Objectives:
```
Configure Consul Connect with metrics using Helm
Install Prometheus and Grafana using Helm
Install and start the demo application
Collect metrics
```


### Deploying Consul Connect Using Helm

Follow the guide at https://learn.hashicorp.com/consul/getting-started-k8s/l7-observability-k8s and try it out yourself in this hands-on lab.


NOTE: many of these files are in this Github repo: https://github.com/eveld/consul-k8s-l7-obs-guide.git 


Open the file in your working directory called `consul-values.yaml`. This file will configure the Consul Helm chart to:

- specify a name for your Consul datacenter
- enable the Consul web UI
- enable secure communication between pods with Connect
- configure the Consul settings necessary for layer 7 metrics collection
- specify that this Consul cluster should run one server
- enable metrics collection on servers and agents so that you can monitor the Consul cluster itself

You can override many of the values in Consul's values file using annotations on specific services. For example, later in the guide you will override the centralized configuration of defaultProtocol.


/root/guide/consul-values.yml
```
# Available parameters and their default values for the Consul chart.

global:
  datacenter: dc1

server:
  replicas: 1
  bootstrapExpect: 1
  disruptionBudget:
    enabled: true
    maxUnavailable: 0

client:
  enabled: true
  grpc: true

ui:
  enabled: true

connectInject:
  enabled: true
  default: false
  centralConfig:
    enabled: true
    defaultProtocol: "http"
    proxyDefaults: |
      {
      "envoy_dogstatsd_url": "udp://127.0.0.1:9125"
      }
```


Now install Consul in your Kubernetes cluster and give Kubernetes a name for your Consul installation. The output will be a list of all the Kubernetes resources created (abbreviated in the code snippet).

```
helm install -f consul-values.yaml --name l7-guide ./consul-helm
```


```
root@kubernetes:~/guide# helm install -f consul-values.yaml --name l7-guide ./consul-helm
NAME:   l7-guide

LAST DEPLOYED: Tue Dec 31 21:14:51 2019
NAMESPACE: default
STATUS: DEPLOYED

RESOURCES:
==> v1/ClusterRole
NAME                                      AGE
l7-guide-consul-client                    1s
l7-guide-consul-connect-injector-webhook  1s
l7-guide-consul-server                    1s

==> v1/ClusterRoleBinding
NAME                                                         AGE
l7-guide-consul-client                                       1s
l7-guide-consul-connect-injector-webhook-admin-role-binding  1s
l7-guide-consul-server                                       1s

==> v1/ConfigMap
NAME                           DATA  AGE
l7-guide-consul-client-config  2     1s
l7-guide-consul-server-config  3     1s

==> v1/DaemonSet
NAME             DESIRED  CURRENT  READY  UP-TO-DATE  AVAILABLE  NODE SELECTOR  AGE
l7-guide-consul  0        0        0      0           0          <none>         1s

==> v1/Deployment
NAME                                                 READY  UP-TO-DATE  AVAILABLE  AGE
l7-guide-consul-connect-injector-webhook-deployment  0/1    0           0          1s

==> v1/Service
NAME                                  TYPE       CLUSTER-IP     EXTERNAL-IP  PORT(S)                                                   AGE
l7-guide-consul-connect-injector-svc  ClusterIP  10.43.150.51   <none>       443/TCP                                                   1s
l7-guide-consul-dns                   ClusterIP  10.43.125.225  <none>       53/TCP,53/UDP                                                   1s
l7-guide-consul-server                ClusterIP  None           <none>       8500/TCP,8301/TCP,8301/UDP,8302/TCP,8302/UDP,8300/TCP,8600/TCP,8600/UDP  1s
l7-guide-consul-ui                    ClusterIP  10.43.153.82   <none>       80/TCP                                                   1s

==> v1/ServiceAccount
NAME                                                  SECRETS  AGE
l7-guide-consul-client                                1        1s
l7-guide-consul-connect-injector-webhook-svc-account  1        1s
l7-guide-consul-server                                1        1s

==> v1/StatefulSet
NAME                    READY  AGE
l7-guide-consul-server  0/1    1s

==> v1beta1/MutatingWebhookConfiguration
NAME                                  AGE
l7-guide-consul-connect-injector-cfg  1s

==> v1beta1/PodDisruptionBudget
NAME                    MIN AVAILABLE  MAX UNAVAILABLE  ALLOWED DISRUPTIONS  AGE
l7-guide-consul-server  N/A            0                0                    1s


NOTES:

Thank you for installing HashiCorp Consul!

Now that you have deployed Consul, you should look over the docs on using
Consul with Kubernetes available here:

https://www.consul.io/docs/platform/k8s/index.html


Your release is named l7-guide. To learn more about the release, try:

  $ helm status l7-guide
  $ helm get l7-guide

root@kubernetes:~/guide#
```


Check that Consul is running in your Kubernetes cluster via the Kubernetes dashboard or CLI.



### Deploying the Metrics Pipeline

You'll follow a similar process as you did with Consul to install Prometheus via Helm. First, open the file named prometheus-values.yaml that configures the Prometheus Helm chart.

The file specifies how often Prometheus should scrape for metrics, and which endpoints it should scrape from. By default Prometheus scrapes all the endpoints that Kubernetes knows about, even if those endpoints don't expose Prometheus metrics. To prevent Prometheus from scraping these endpoints unnecessarily the values file includes some relabel configurations.

Install the official Prometheus Helm chart using the values in prometheus-values.yaml.

```
helm install -f prometheus-values.yaml --name prometheus stable/prometheus
```


/root/guide/prometheus-values.yaml
```
server:
  global:
    scrape_interval: 10s
extraScrapeConfigs: |
  - job_name: 'sample-job'
    scrape_interval: 1s
    kubernetes_sd_configs:
      - role: pod
    relabel_configs:
      - action: keep
        source_labels: [__meta_kubernetes_pod_container_name]
        regex: "prometheus-statsd"
      - source_labels: [__meta_kubernetes_pod_label_app]
        target_label: job
```


Once Prometheus has come up, you should be able to see your new services on the Kubernetes dashboard and in the Consul UI. This might take a short while.


```
root@kubernetes:~/guide# helm install -f prometheus-values.yaml --name prometheus stable/prometheus
NAME:   prometheus
LAST DEPLOYED: Tue Dec 31 21:21:52 2019
NAMESPACE: default
STATUS: DEPLOYED

RESOURCES:
==> v1/ConfigMap
NAME                     DATA  AGE
prometheus-alertmanager  1     1s
prometheus-server        3     1s

==> v1/PersistentVolumeClaim
NAME                     STATUS   VOLUME      CAPACITY  ACCESS MODES  STORAGECLASS  AGE
prometheus-alertmanager  Pending  local-path  1s
prometheus-server        Pending  local-path  1s

==> v1/Service
NAME                           TYPE       CLUSTER-IP     EXTERNAL-IP  PORT(S)   AGE
prometheus-alertmanager        ClusterIP  10.43.186.124  <none>       80/TCP    0s
prometheus-kube-state-metrics  ClusterIP  None           <none>       80/TCP    0s
prometheus-node-exporter       ClusterIP  None           <none>       9100/TCP  0s
prometheus-pushgateway         ClusterIP  10.43.69.205   <none>       9091/TCP  0s
prometheus-server              ClusterIP  10.43.71.48    <none>       80/TCP    0s

==> v1/ServiceAccount
NAME                           SECRETS  AGE
prometheus-alertmanager        1        1s
prometheus-kube-state-metrics  1        1s
prometheus-node-exporter       1        1s
prometheus-pushgateway         1        1s
prometheus-server              1        1s

==> v1beta1/ClusterRole
NAME                           AGE
prometheus-kube-state-metrics  1s
prometheus-server              1s

==> v1beta1/ClusterRoleBinding
NAME                           AGE
prometheus-kube-state-metrics  1s
prometheus-server              1s

==> v1beta1/DaemonSet
NAME                      DESIRED  CURRENT  READY  UP-TO-DATE  AVAILABLE  NODE SELECTOR  AGE
prometheus-node-exporter  0        0        0      0           0          <none>         0s

==> v1beta1/Deployment
NAME                           READY  UP-TO-DATE  AVAILABLE  AGE
prometheus-alertmanager        0/1    0           0          0s
prometheus-kube-state-metrics  0/1    0           0          0s
prometheus-pushgateway         0/1    0           0          0s
prometheus-server              0/1    0           0          0s


NOTES:
The Prometheus server can be accessed via port 80 on the following DNS name from within your cluster:
prometheus-server.default.svc.cluster.local


Get the Prometheus server URL by running these commands in the same shell:
  export POD_NAME=$(kubectl get pods --namespace default -l "app=prometheus,component=server" -o jsonpath="{.items[0].metadata.name}")
  kubectl --namespace default port-forward $POD_NAME 9090


The Prometheus alertmanager can be accessed via port 80 on the following DNS name from within yourcluster:
prometheus-alertmanager.default.svc.cluster.local


Get the Alertmanager URL by running these commands in the same shell:
  export POD_NAME=$(kubectl get pods --namespace default -l "app=prometheus,component=alertmanager" -o jsonpath="{.items[0].metadata.name}")
  kubectl --namespace default port-forward $POD_NAME 9093


The Prometheus PushGateway can be accessed via port 9091 on the following DNS name from within your cluster:
prometheus-pushgateway.default.svc.cluster.local


Get the PushGateway URL by running these commands in the same shell:
  export POD_NAME=$(kubectl get pods --namespace default -l "app=prometheus,component=pushgateway"-o jsonpath="{.items[0].metadata.name}")
  kubectl --namespace default port-forward $POD_NAME 9091

For more information on running Prometheus, visit:
https://prometheus.io/

root@kubernetes:~/guide#
```


### Deploy Grafana with Helm

Installing Grafana will follow a similar process. Open and look through the file named grafana-values.yaml. It configures Grafana to use Prometheus as its datasource.

Use the official Helm chart to install Grafana with your values file.

```
helm install -f grafana-values.yaml --name grafana stable/grafana
```


```
root@kubernetes:~/guide# helm install -f grafana-values.yaml --name grafana stable/grafana
NAME:   grafana
LAST DEPLOYED: Tue Dec 31 21:26:05 2019
NAMESPACE: default
STATUS: DEPLOYED

RESOURCES:
==> v1/ClusterRole
NAME                 AGE
grafana-clusterrole  1s

==> v1/ClusterRoleBinding
NAME                        AGE
grafana-clusterrolebinding  1s

==> v1/ConfigMap
NAME          DATA  AGE
grafana       2     1s
grafana-test  1     1s

==> v1/PersistentVolumeClaim
NAME     STATUS   VOLUME      CAPACITY  ACCESS MODES  STORAGECLASS  AGE
grafana  Pending  local-path  1s

==> v1/Role
NAME          AGE
grafana-test  1s

==> v1/RoleBinding
NAME          AGE
grafana-test  1s

==> v1/Secret
NAME     TYPE    DATA  AGE
grafana  Opaque  3     1s

==> v1/Service
NAME     TYPE       CLUSTER-IP     EXTERNAL-IP  PORT(S)  AGE
grafana  ClusterIP  10.43.226.254  <none>       80/TCP   1s

==> v1/ServiceAccount
NAME          SECRETS  AGE
grafana       1        1s
grafana-test  1        1s

==> v1beta1/PodSecurityPolicy
NAME          PRIV   CAPS      SELINUX   RUNASUSER  FSGROUP   SUPGROUP  READONLYROOTFS  VOLUMES
grafana       false  RunAsAny  RunAsAny  RunAsAny   RunAsAny  false     configMap,emptyDir,projected,secret,downwardAPI,persistentVolumeClaim
grafana-test  false  RunAsAny  RunAsAny  RunAsAny   RunAsAny  false     configMap,downwardAPI,emptyDir,projected,secret

==> v1beta1/Role
NAME     AGE
grafana  1s

==> v1beta1/RoleBinding
NAME     AGE
grafana  1s

==> v1beta2/Deployment
NAME     READY  UP-TO-DATE  AVAILABLE  AGE
grafana  0/1    0           0          1s


NOTES:
1. Get your 'admin' user password by running:

   kubectl get secret --namespace default grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo

2. The Grafana server can be accessed via port 80 on the following DNS name from within your cluster:

   grafana.default.svc.cluster.local

   Get the Grafana URL to visit by running these commands in the same shell:

     export POD_NAME=$(kubectl get pods --namespace default -l "app=grafana,release=grafana" -o jsonpath="{.items[0].metadata.name}")
     kubectl --namespace default port-forward $POD_NAME 3000

3. Login with the password from step 1 and the username: admin

root@kubernetes:~/guide#
```



At the bottom of your terminal output are shell-specific instructions to access your Grafana UI and log in, displayed as a numbered list.

Get your "admin" password by running:

```
kubectl get secret --namespace default grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
```


```
root@kubernetes:~/guide# kubectl get secret --namespace default grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
lJdOhYEzqZ89tBfj5h0NtRbNrPvbE4dB6r0PcK4v
root@kubernetes:~/guide#
```


Once you have logged into the Grafana UI, hover over the dashboards icon (four squares in the left hand menu) and then click the "manage" option. This will take you to a page that gives you some choices about how to upload Grafana dashboards. Click the black `Import` button on the right hand side of the screen.

Open the `Dashboard.json` tab and copy the contents of `overview-dashboard.json` into the json window of the Grafana UI. Click through the rest of the options, and you will end up with a blank dashboard, waiting for data to display.



### Deploy a Demo Application on Kubernetes

Now that your monitoring pipeline is set up, deploy a demo application that will generate data. We will be using Emojify, an application that recognizes faces in an image and pastes emojis over them. The application consists of a few different services and automatically generates traffic and HTTP error codes.

All the files defining Emojify are in the app directory. Open app/cache.yml and take a look at the file. Most of services that make up Emojify communicate over HTTP, but the cache service uses gRPC. In the annotations section of the file you'll see where consul.hashicorp.com/connect-service-protocol specifies gRPC, overriding the defaultProtocol of HTTP that we centrally configured in Consul's value file.

At the bottom of each file defining part of the Emojify app, notice the block defining a prometheus-statsd pod. These pods translate the metrics that Envoy exposes to a format that Prometheus can scrape. They won't be necessary anymore once Consul Connect becomes compatible with Envoy 1.10. Apply the configuration to deploy Emojify into your cluster.

```
kubectl apply -f app
```

```
root@kubernetes:~/guide# ls app
api.yml  cache.yml  facebox.yml  ingress.yml  website.yml
```


```
root@kubernetes:~/guide# kubectl apply -f app
deployment.apps/emojify-api created
deployment.apps/emojify-cache created
deployment.apps/emojify-facebox created
service/emojify-ingress created
configmap/emojify-ingress-configmap created
deployment.apps/emojify-ingress created
configmap/emojify-website-configmap created
deployment.apps/emojify-website created
root@kubernetes:~/guide#
```


Emojify will take a little while to deploy. Once it's running you can check that it's healthy by taking a look at your Kubernetes dashboard or Consul UI. Next, visit the Emojify UI.

Test the application by emojifying a picture. You can do this by pasting the following URL into the URL bar and clicking the submit button. (We provide a demo URL because Emojify can be picky about processing some image URLs if they don't link directly to the actual picture.)

```
https://emojify.today/pictures/1.jpg
```



### Generating Simulated Traffic

Now that you know the application is working, start generating automatic load so that you will have some interesting metrics to look at.

```
kubectl apply -f traffic.yaml
```


```
root@kubernetes:~/guide# kubectl apply -f traffic.yaml
deployment.apps/emojify-traffic created
root@kubernetes:~/guide#
```


/root/guide/traffice.yaml
```
root@kubernetes:~/guide# cat traffic.yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: emojify-traffic
  labels:
    app: emojify-traffic
spec:
  replicas: 1
  selector:
    matchLabels:
      app: emojify-traffic
  template:
    metadata:
      labels:
        app: emojify-traffic
      annotations:
        "consul.hashicorp.com/connect-inject": "false"
    spec:
      containers:
      - name: load-test
        image: nicholasjackson/emojify-traffic:v0.2.1
        env:
        - name: BASE_URI
          value: http://emojify-ingress
        - name: DURATION
          value: "1m"
        - name: SHOW_PROGRESS
          value: "false"
        - name: USERS
          value: "5"
```

Envoy exposes a huge number of metrics, but you will probably only want to monitor or alert on a subset of them. Which metrics are important to monitor will depend on your application. For this getting-started guide we have preconfigured an Emojify-specific Grafana dashboard with a couple of basic metrics, but you should systematically consider what others you will need to collect as you move from testing into production.

Now that you have metrics flowing through your pipeline, navigate back to your Grafana dashboard. The top row of the dashboard displays general metrics about the Emojify application as a whole, including request and error rates. Although changes in these metrics can reflect application health issues once you understand their baseline levels, they don't provide enough information to diagnose specific issues.

The following rows of the dashboard report on some of the specific services that make up the emojify application: the website, API, and cache services. The website and API services show request count and response time, while the cache reports on request count and methods.

# NOTE: this lab isn't working and can't be completed