+++
weight = 1
toc = true
date = "2019-11-24T05:03:55Z"
title = "Consul Introduction"
+++

Consul provides service discovery, health checks, load balancing, service graph, identity enforcement via TLS, and distributed service configuration management.