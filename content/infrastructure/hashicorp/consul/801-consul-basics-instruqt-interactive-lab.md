+++
toc = true
date = "2019-12-03T12:47:50Z"
title = "Consul Basics Instruqt Interactive Lab"
weight = 801
+++


## Tutorial
https://instruqt.com/hashicorp/tracks/consul-basics

The track is a crash course in Consul operations. You will accomplish the following:
```
Set Up a 3 Node Consul Cluster
Use the Consul UI
Use the Consul CLI
Use the Consul API
Add an App Node to Your Cluster
Test Consul's High Availability
Add Basic Security with Consul's ACLs
```
Once you've learned these fundamentals you can proceed to intermediate and advanced topics like Service Discovery and Consul Connect.

### Starting Consul

```
# /bin/start_consul.sh
Starting HashiCorp Consul in Server Mode...
CMD: nohup consul agent -config-dir=/consul/config > /consul.out &
Log output will appear in consul.out...
Consul server startup complete.



/ # cat /bin/start_consul.sh
#!/bin/sh
echo "Starting HashiCorp Consul in Server Mode..."
sleep 1
echo "CMD: nohup consul agent -config-dir=/consul/config > /consul.out &"
nohup consul agent -config-dir=/consul/config > /consul.out &
echo "Log output will appear in consul.out..."
sleep 1
echo "Consul server startup complete."
```


/consul/config/server.json
```
{
  "datacenter": "dc1",
  "bind_addr": "0.0.0.0",
  "client_addr": "0.0.0.0",
  "data_dir": "/consul/data",
  "log_level": "INFO",
  "node_name": "ConsulServer0",
  "server": true,
  "ui": true,
  "bootstrap_expect": 3,
  "leave_on_terminate": false,
  "skip_leave_on_interrupt": true,
  "rejoin_after_leave": true,
  "retry_join": [
    "consul-server-0:8301",
    "consul-server-1:8301",
    "consul-server-2:8301"
  ]
}
```


The API server endpoint for Consul
```
# echo $CONSUL_HTTP_ADDR
http://127.0.0.1:8500

```

### Consul CLI

All interactions with Consul, whether through the GUI or the command line always have an underlying API call.
```
consul help
consul monitor
(CTRL-C to escape)
consul members
consul operator raft list-peers
consul info
```

### Consul API

Consul has a RESTful API which you can use to interact with Consul. You can see the see Consul API documentation here.
https://www.consul.io/api/index.html

```
curl -s http://127.0.0.1:8500/v1/agent/monitor
(CTRL-C to escape)

curl -s http://127.0.0.1:8500/v1/agent/members | jq

curl -s http://127.0.0.1:8500/v1/status/leader | jq


curl -s http://127.0.0.1:8500/v1/agent/self | jq

```

### Add a Consul agent in client mode 

The Consul agent runs on every node where you want to keep track of services. A node can be a physical server, VM, or container.

The agent tracks information about the node and its associated services. Agents report this information to the Consul servers, where we have a central view of node and service status.


App Server configured to run in `client` mode
```
/consul/config/client.json

{
  "datacenter": "dc1",
  "server": false,
  "bind_addr": "0.0.0.0",
  "client_addr": "0.0.0.0",
  "data_dir": "/consul/data",
  "log_level": "INFO",
  "node_name": "App",
  "ui": true,
  "retry_join": [
    "consul-server-0:8301",
    "consul-server-1:8301",
    "consul-server-2:8301"
  ]
}
```


Run in client mode
```
/ # cat /bin/start_consul.sh
#!/bin/sh
echo "Starting HashiCorp Consul in Client Mode..."
sleep 1
echo "CMD: nohup consul agent -config-dir=/consul/config > /consul.out &"
nohup consul agent -config-dir=/consul/config > /consul.out &
echo "Log output will appear in consul.out..."
sleep 1
echo "Consul client startup complete."
```

Run the Consul startup script in the App tab and join this agent to the cluster.

Look at the node list in the Consul UI or run the `consul members` command to verify that the app server has joined the cluster.

```
/ # /bin/start_consul.sh
Starting HashiCorp Consul in Client Mode...
CMD: nohup consul agent -config-dir=/consul/config > /consul.out &
Log output will appear in consul.out...
Consul client startup complete.


/ # consul members
Node           Address            Status  Type    Build  Protocol  DC   Segment
ConsulServer0  10.96.27.119:8301  alive   server  1.6.2  2         dc1  <all>
ConsulServer1  10.96.29.225:8301  alive   server  1.6.2  2         dc1  <all>
ConsulServer2  10.96.26.192:8301  alive   server  1.6.2  2         dc1  <all>
App            10.96.29.226:8301  alive   client  1.6.2  2         dc1  <default>
/ #
```

### Consul High Availability

Consul servers work together to elect a single leader, which becomes the primary source of truth for the cluster. All updates are forwarded to the cluster leader. If the leader goes down one of the other servers can immediately take its place.

To ensure high availability within the system, we recommend deploying Consul with 3 or 5 server nodes.


We have 3 servers in our lab environment. This means we can lose one server and still have a healthy cluster. You can read more about the Consensus Protocol here.
https://www.consul.io/docs/internals/consensus.html


Let's disable the leader and observe what happens.

First, find your current leader. You can do this from the UI (look for the star), or on the command line:

```
consul operator raft list-peers
```

Go to the tab of your current leader and run the following command to stop the Consul agent:
```
pkill consul
```


If your leader happens to be Consul0 you will temporarily lose access to the UI.

Repeat the above process on the leading server terminal tab, and kill the new leader.

Run the following commands on the last server:

```
consul members
consul operator raft list-peers
consul catalog nodes
```

You'll see error messages like this: `Unexpected response code: 500 (No cluster leader)`

Now that two servers have failed your cluster is no longer functional, all API commands will result in errors until at least one of your other servers comes back online. Let's recover by starting Consul back up. Run the following command on one of the failed nodes:
```
/bin/start_consul.sh
```

Now run `consul members` to verify that the Consul API is responding again.

Bring back your third server with the startup script:
```
/bin/start_consul.sh
```
And run `consul members` one last time. You should see all three server nodes in alive status.

Congratulations, your Consul cluster is healthy again.



```
/ # consul operator raft list-peers
Node           ID                                    Address            State     Voter  RaftProtocol
ConsulServer1  f9b1abd5-6e7a-a7fb-d24c-3a8002ee1aac  10.96.29.225:8300  leader    true   3
ConsulServer0  771ef763-31bf-3ff2-d328-8665d6ac6791  10.96.27.119:8300  follower  true   3
ConsulServer2  bc298eac-afe1-4fc0-15da-7dd374dea05d  10.96.26.192:8300  follower  true   3
/ # consul members
Node           Address            Status  Type    Build  Protocol  DC   Segment
ConsulServer0  10.96.27.119:8301  alive   server  1.6.2  2         dc1  <all>
ConsulServer1  10.96.29.225:8301  alive   server  1.6.2  2         dc1  <all>
ConsulServer2  10.96.26.192:8301  alive   server  1.6.2  2         dc1  <all>
App            10.96.29.226:8301  alive   client  1.6.2  2         dc1  <default>
/ # consul operator raft list-peers
Node           ID                                    Address            State     Voter  RaftProtocol
ConsulServer2  bc298eac-afe1-4fc0-15da-7dd374dea05d  10.96.26.192:8300  leader    true   3
ConsulServer1  f9b1abd5-6e7a-a7fb-d24c-3a8002ee1aac  10.96.29.225:8300  follower  true   3
ConsulServer0  771ef763-31bf-3ff2-d328-8665d6ac6791  10.96.27.119:8300  follower  true   3
/ # consul members
Node           Address            Status  Type    Build  Protocol  DC   Segment
ConsulServer0  10.96.27.119:8301  alive   server  1.6.2  2         dc1  <all>
ConsulServer1  10.96.29.225:8301  failed  server  1.6.2  2         dc1  <all>
ConsulServer2  10.96.26.192:8301  alive   server  1.6.2  2         dc1  <all>
App            10.96.29.226:8301  alive   client  1.6.2  2         dc1  <default>
/ # consul operator raft list-peers
Error getting peers: Failed to retrieve raft configuration: Unexpected response code: 500 (No cluster leader)
/ # consul members
Node           Address            Status  Type    Build  Protocol  DC   Segment
ConsulServer0  10.96.27.119:8301  alive   server  1.6.2  2         dc1  <all>
ConsulServer1  10.96.29.225:8301  failed  server  1.6.2  2         dc1  <all>
ConsulServer2  10.96.26.192:8301  failed  server  1.6.2  2         dc1  <all>
App            10.96.29.226:8301  alive   client  1.6.2  2         dc1  <default>
/ #
```

### Consul ACLs

Consul uses Access Control Lists (ACLs) to secure the UI, API, CLI, service communications, and agent communications. ACLs operate by grouping rules into policies, then associating one or more policies with a token. ACLs are imperative for all Consul production environments.

For a detailed explanation of the ACL system, check out this guide.
https://learn.hashicorp.com/consul/security-networking/production-acls


Let's apply some access controls to our Consul cluster to prevent unauthorized access. In this challenge you'll use the master bootstrap token to access the UI and to enable a policy for your app server.

You may notice that you're locked out of the UI. None of the nodes and services are showing up because of a default deny policy.

Consul ACLs are enabled by running the `consul acl bootstrap` command.

We've already run this command for you and saved the output in the ACL Bootstrap code editor tab. The initial bootstrap token is the SecretID field of this output. This special token is used to configure your cluster and to generate other tokens. Copy the bootstrap token by highlighting it and pressing CTRL-C.

```
/tmp/bootstrap.txt

AccessorID:       7ed9c1f2-7112-b1db-8028-9731dcea0eca
SecretID:         d62f9915-0959-b4b1-3f8c-5d812d673510
Description:      Bootstrap Token (Global Management)
Local:            false
Create Time:      2019-12-03 13:22:36.008834513 +0000 UTC
Policies:
   00000000-0000-0000-0000-000000000001 - global-management
```

To re-enable the UI, select the Consul UI tab, and click on the ACL tab and enter your token into the text field.

On the command line you should set an environment variable with your token. Run the following command on the App tab:

```
/ # export CONSUL_HTTP_TOKEN=d62f9915-0959-b4b1-3f8c-5d812d673510
/ #
```

This lab has a deny by default policy, so your App node will be logging ACL errors. Run the following command to view the ACL errors (CTRL-C to exit):
```
consul monitor

```

The app server is currently blocked from making any consul updates or queries.
```
2019/12/03 13:26:18 [WARN] agent: Coordinate update blocked by ACLs
2019/12/03 13:26:42 [ERR] consul: "Coordinate.Update" RPC failed to server 10.96.27.119:8300: rpc error making call: rpc error making call: Permission denied
2019/12/03 13:26:42 [WARN] agent: Coordinate update blocked by ACLs
2019/12/03 13:27:08 [ERR] consul: "Coordinate.Update" RPC failed to server 10.96.26.192:8300: rpc error making call: Permission denied
2019/12/03 13:27:08 [WARN] agent: Coordinate update blocked by ACLs
```

Let's create a policy to enable limited access for our app node. Run these commands from the App tab.

```

/ # consul acl policy create \
>  -name app \
>  -rules @/consul/policies/app.hcl
ID:           76995eb9-09bc-1020-4b1a-52411953fce4
Name:         app
Description:
Datacenters:
Rules:
# app.hcl
node "App" {
  policy = "write"
}

/ #

```

The contents of /consul/policies/app.hcl
```
# app.hcl
node "App" {
  policy = "write"
}
```


Next, create a token for that policy.

```

/ # consul acl token create -description "app agent token" \
>   -policy-name app
AccessorID:       1771eadb-1c29-e8ee-36d2-cfb28d6962ac
SecretID:         cd5f1c1b-b742-9c82-94d7-978fef4e1454
Description:      app agent token
Local:            false
Create Time:      2019-12-03 13:29:52.54159832 +0000 UTC
Policies:
   76995eb9-09bc-1020-4b1a-52411953fce4 - app
/ #
```


Finally apply the token to make it active.

```
/ # consul acl set-agent-token agent "cd5f1c1b-b742-9c82-94d7-978fef4e1454"
ACL token "agent" set successfully
/ #

```

You can now verify that the App agent logs are no longer logging errors. Nice work!!! You just secured your first Consul agent!

```

consul monitor


2019/12/03 13:30:44 [ERR] consul: "Coordinate.Update" RPC failed to server 10.96.27.119:8300: rpc error making call: rpc error making call: Permission denied
2019/12/03 13:30:44 [WARN] agent: Coordinate update blocked by ACLs
2019/12/03 13:30:59 [INFO] agent: Synced node info
2019/12/03 13:31:04 [INFO] agent: Updated agent's ACL token "agent"
^C
/ # date
Tue Dec  3 13:31:15 UTC 2019
/ #
```

