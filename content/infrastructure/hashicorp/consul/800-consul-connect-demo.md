+++
toc = true
date = "2019-12-02T18:47:50Z"
title = "Consul Connect Demo"
weight = 800
+++

12/2/2019

https://learn.hashicorp.com/consul/getting-started/connect


```
[root@ccca2028 consul.d]# cat socat.json
{
  "service": {
    "name": "socat",
    "port": 8181,
    "connect": { "sidecar_service": {} }
  }
}


[root@ccca2028 consul.d]# cat web.json
{"service": {
    "name": "web",
    "port": 19991,
    "connect": {
      "sidecar_service": {
        "proxy": {
          "upstreams": [{
             "destination_name": "socat",
             "local_bind_port": 9191
          }]
        }
      }
    }
  }
}
[root@ccca2028 consul.d]#


```




```

[root@ccca2028 consul.d]# ps -ef |grep consul
root     126712 121313  0 17:56 pts/3    00:00:00 ./consul agent -dev -enable-local-script-checks -config-dir ./consul.d
root     126768 124574  0 17:56 pts/7    00:00:00 ./consul connect proxy -sidecar-for socat
root     126783 120482  0 17:56 pts/2    00:00:00 ./consul connect proxy -sidecar-for web
root     126874 121993  0 17:57 pts/5    00:00:00 grep --color=auto consul
[root@ccca2028 consul.d]# netstat -anltp |grep 126712
tcp        0      0 127.0.0.1:8300          0.0.0.0:*               LISTEN      126712/./consul
tcp        0      0 127.0.0.1:8301          0.0.0.0:*               LISTEN      126712/./consul
tcp        0      0 127.0.0.1:8302          0.0.0.0:*               LISTEN      126712/./consul
tcp        0      0 127.0.0.1:8500          0.0.0.0:*               LISTEN      126712/./consul
tcp        0      0 127.0.0.1:8502          0.0.0.0:*               LISTEN      126712/./consul
tcp        0      0 127.0.0.1:8600          0.0.0.0:*               LISTEN      126712/./consul
tcp        0      0 127.0.0.1:8500          127.0.0.1:53194         ESTABLISHED 126712/./consul
tcp        0      0 127.0.0.1:8500          127.0.0.1:53188         ESTABLISHED 126712/./consul
tcp        0     12 127.0.0.1:52278         127.0.0.1:8300          ESTABLISHED 126712/./consul
tcp        0      0 127.0.0.1:8500          127.0.0.1:53192         ESTABLISHED 126712/./consul
tcp        0      0 127.0.0.1:8500          127.0.0.1:53210         ESTABLISHED 126712/./consul
tcp        0      0 127.0.0.1:8500          127.0.0.1:53196         ESTABLISHED 126712/./consul
tcp        0      0 127.0.0.1:8300          127.0.0.1:52278         ESTABLISHED 126712/./consul
tcp        0      0 127.0.0.1:8500          127.0.0.1:53186         ESTABLISHED 126712/./consul
tcp        0      0 127.0.0.1:8500          127.0.0.1:53204         ESTABLISHED 126712/./consul
tcp        0      0 127.0.0.1:8500          127.0.0.1:53184         ESTABLISHED 126712/./consul
[root@ccca2028 consul.d]# netstat -anltp |grep 126768
tcp        0      0 0.0.0.0:21000           0.0.0.0:*               LISTEN      126768/./consul
tcp        0      0 127.0.0.1:53188         127.0.0.1:8500          ESTABLISHED 126768/./consul
tcp        0      0 127.0.0.1:53210         127.0.0.1:8500          ESTABLISHED 126768/./consul
tcp        0      0 127.0.0.1:53186         127.0.0.1:8500          ESTABLISHED 126768/./consul
tcp        0      0 127.0.0.1:53184         127.0.0.1:8500          ESTABLISHED 126768/./consul
[root@ccca2028 consul.d]# netstat -anltp |grep 126783
tcp        0      0 127.0.0.1:9191          0.0.0.0:*               LISTEN      126783/./consul
tcp        0      0 0.0.0.0:21001           0.0.0.0:*               LISTEN      126783/./consul
tcp        0      0 127.0.0.1:53192         127.0.0.1:8500          ESTABLISHED 126783/./consul
tcp        0      0 127.0.0.1:53196         127.0.0.1:8500          ESTABLISHED 126783/./consul
tcp        0      0 127.0.0.1:53204         127.0.0.1:8500          ESTABLISHED 126783/./consul
tcp        0      0 127.0.0.1:53194         127.0.0.1:8500          ESTABLISHED 126783/./consul


```


```
[root@ccca2028 consul.d]# dig @127.0.0.1 -p 8600 socat.service.consul

; <<>> DiG 9.9.4-SuSE-9.9.4-73.el7_6 <<>> @127.0.0.1 -p 8600 socat.service.consul
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 41548
;; flags: qr aa rd; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 2
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;socat.service.consul.          IN      A

;; ANSWER SECTION:
socat.service.consul.   0       IN      A       127.0.0.1

;; ADDITIONAL SECTION:
socat.service.consul.   0       IN      TXT     "consul-network-segment="

;; Query time: 0 msec
;; SERVER: 127.0.0.1#8600(127.0.0.1)
;; WHEN: Mon Dec 02 18:02:53 EST 2019
;; MSG SIZE  rcvd: 101




[root@ccca2028 consul.d]# dig @127.0.0.1 -p 8600 web.service.consul

; <<>> DiG 9.9.4-SuSE-9.9.4-73.el7_6 <<>> @127.0.0.1 -p 8600 web.service.consul
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 44245
;; flags: qr aa rd; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 2
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;web.service.consul.            IN      A

;; ANSWER SECTION:
web.service.consul.     0       IN      A       127.0.0.1

;; ADDITIONAL SECTION:
web.service.consul.     0       IN      TXT     "consul-network-segment="

;; Query time: 0 msec
;; SERVER: 127.0.0.1#8600(127.0.0.1)
;; WHEN: Mon Dec 02 18:03:28 EST 2019
;; MSG SIZE  rcvd: 99



```



```
[root@ccca2028 consul.d]# !nc
nc 127.0.0.1 9191
TEST
TEST
^C
[root@ccca2028 consul.d]# nc 127.0.0.1 8181
TEST
TEST
^C

```


```
[root@ccca2028 consul.d]# ../consul intention check web socat
Allowed


[root@ccca2028 consul.d]# ../consul intention create -deny web socat
Created: web => socat (deny)
[root@ccca2028 consul.d]# ../consul intention check web socat
Denied



[root@ccca2028 consul.d]# nc 127.0.0.1 8181
TEst
TEst
^C
[root@ccca2028 consul.d]# nc 127.0.0.1 9191
Testtt

Ncat: Broken pipe.




[root@ccca2028 consul.d]# ../consul intention delete web socat
Intention deleted.
[root@ccca2028 consul.d]# ../consul intention check web socat
Allowed
[root@ccca2028 consul.d]# nc 127.0.0.1 9191
Allowed again
Allowed again
^C

```

NOTE: next step is to try this again in a real cluster - with multiple nodes
NOTE: also with Envoy Proxy instead of the built-in proxy

https://learn.hashicorp.com/consul/datacenter-deploy/day1-deploy-intro
https://learn.hashicorp.com/consul/datacenter-deploy/reference-architecture
