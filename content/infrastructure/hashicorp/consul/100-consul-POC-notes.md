+++
toc = true
date = "2019-12-02T18:47:50Z"
title = "Consul POC Notes"
weight = 100
+++

# consul POC 


## consul POC servers ppprivmgmt.intraxa
```
DCFA2027
DCFA2040
DCFA2043
```

## Copy consul binary to servers
```
scp ~/Downloads/consul_1.6.2_linux_amd64.zip coreimage@dcfa2027.ppprivmgmt.intraxa:/tmp
scp ~/Downloads/consul_1.6.2_linux_amd64.zip coreimage@dcfa2040.ppprivmgmt.intraxa:/tmp
scp ~/Downloads/consul_1.6.2_linux_amd64.zip coreimage@dcfa2043.ppprivmgmt.intraxa:/tmp
ssh coreimage@dcfa2043.ppprivmgmt.intraxa 'free -m'
ssh coreimage@dcfa2040.ppprivmgmt.intraxa 'free -m'
ssh coreimage@dcfa2027.ppprivmgmt.intraxa 'free -m'
```



# Installation

## https://learn.hashicorp.com/consul/datacenter-deploy/deployment-guide
```
cd /usr/local/bin
unzip /tmp/consul_1.6.2_linux_amd64.zip
consul version
consul -autocomplete-install
complete -C /usr/local/bin/consul consul

useradd --system --home /etc/consul.d --shell /bin/false consul

mkdir -p /etc/consul.d
cd /etc/consul.d/
vi server.hcl
vi consul.hcl
chown -R consul:consul /etc/consul.d
chmod 640 /etc/consul.d/*.hcl

mkdir -p /opt/consul
chown -R consul:consul /opt/consul

vi /etc/systemd/system/consul.service
systemctl enable consul
systemctl start consul
systemctl status consul

consul members

consul operator raft list-peers


[root@dcfa2043 consul.d]# consul operator raft list-peers
Node                         ID                                    Address            State     Voter  RaftProtocol
dcfa2040.ppprivmgmt.intraxa  e5424cef-fcaf-087d-70ca-cb8fee418118  10.79.192.82:8300  follower  true   3
dcfa2043.ppprivmgmt.intraxa  6056fa24-d3f9-7dbd-fd46-478362cd5c11  10.79.192.99:8300  follower  true   3
dcfa2027.ppprivmgmt.intraxa  4c2fd11a-43ca-2b2d-d220-f93567bc2dfb  10.79.192.22:8300  leader    true   3


[root@dcfa2027 ~]# consul catalog nodes
Node                         ID        Address       DC
dcfa2027.ppprivmgmt.intraxa  4c2fd11a  10.79.192.22  dcfa
dcfa2040.ppprivmgmt.intraxa  e5424cef  10.79.192.82  dcfa
dcfa2043.ppprivmgmt.intraxa  6056fa24  10.79.192.99  dcfa
```


## Configure access to WEB GUI
Install HAproxy and redirect 3389 to 127.0.0.1:8500

http://dcfa2027.ppprivmgmt.intraxa:3389/ui/dcfa/services


## Configure a client agent
Try DCFA2041.ppprivmgmt.intraxa
or
DCFAI022.ppprivmgmt.intraxa  (OOP Aapache server)

```
scp ~/Downloads/consul_1.6.2_linux_amd64.zip coreimage@dcfai022.ppprivmgmt.intraxa:/tmp

cd /usr/local/bin/
unzip /tmp/consul_1.6.2_linux_amd64.zip
consul version
consul -autocomplete-install
complete -C /usr/local/bin/consul consul
useradd --system --home /etc/consul.d --shell /bin/false consul
id consul
mkdir -p /etc/consul.d
cd /etc/consul.d/
vi consul.hcl
chown -R consul:consul /etc/consul.d
chmod 640 /etc/consul.d/*.hcl
mkdir -p /opt/consul
chown -R consul:consul /opt/consul
vi /etc/systemd/system/consul.service
systemctl enable consul
systemctl start consul
systemctl status consul



[root@dcfai022 consul.d]# consul members
Node                         Address            Status  Type    Build  Protocol  DC    Segment
dcfa2027.ppprivmgmt.intraxa  10.79.192.22:8301  alive   server  1.6.2  2         dcfa  <all>
dcfa2040.ppprivmgmt.intraxa  10.79.192.82:8301  alive   server  1.6.2  2         dcfa  <all>
dcfa2043.ppprivmgmt.intraxa  10.79.192.99:8301  alive   server  1.6.2  2         dcfa  <all>
dcfai022.ppprivmgmt.intraxa  10.79.192.94:8301  alive   client  1.6.2  2         dcfa  <default>

[root@dcfai022 consul.d]# consul operator raft list-peers
Node                         ID                                    Address            State     Voter  RaftProtocol
dcfa2040.ppprivmgmt.intraxa  e5424cef-fcaf-087d-70ca-cb8fee418118  10.79.192.82:8300  follower  true   3
dcfa2043.ppprivmgmt.intraxa  6056fa24-d3f9-7dbd-fd46-478362cd5c11  10.79.192.99:8300  follower  true   3
dcfa2027.ppprivmgmt.intraxa  4c2fd11a-43ca-2b2d-d220-f93567bc2dfb  10.79.192.22:8300  leader    true   3



[root@dcfa2027 ~]# consul catalog nodes
Node                         ID        Address       DC
dcfa2027.ppprivmgmt.intraxa  4c2fd11a  10.79.192.22  dcfa
dcfa2040.ppprivmgmt.intraxa  e5424cef  10.79.192.82  dcfa
dcfa2043.ppprivmgmt.intraxa  6056fa24  10.79.192.99  dcfa
dcfai022.ppprivmgmt.intraxa  b55666aa  10.79.192.94  dcfa
```


## Query Consul DNS for service name

```
[root@dcfai022 consul.d]# dig -p 8600 @127.0.0.1 web-OOP.service.consul

; <<>> DiG 9.9.4-SuSE-9.9.4-72.el7 <<>> -p 8600 @127.0.0.1 web-OOP.service.consul
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 26402
;; flags: qr aa rd; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 2
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;web-OOP.service.consul.                IN      A

;; ANSWER SECTION:
web-OOP.service.consul. 0       IN      A       10.79.192.94

;; ADDITIONAL SECTION:
web-OOP.service.consul. 0       IN      TXT     "consul-network-segment="

;; Query time: 5 msec
;; SERVER: 127.0.0.1#8600(127.0.0.1)
;; WHEN: Thu Dec 26 16:04:11 CST 2019
;; MSG SIZE  rcvd: 103




[root@dcfai022 consul.d]# dig -p 8600 @127.0.0.1 httpd.web-OOP.service.consul

; <<>> DiG 9.9.4-SuSE-9.9.4-72.el7 <<>> -p 8600 @127.0.0.1 httpd.web-OOP.service.consul
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 63527
;; flags: qr aa rd; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 2
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;httpd.web-OOP.service.consul.  IN      A

;; ANSWER SECTION:
httpd.web-OOP.service.consul. 0 IN      A       10.79.192.94

;; ADDITIONAL SECTION:
httpd.web-OOP.service.consul. 0 IN      TXT     "consul-network-segment="

;; Query time: 1 msec
;; SERVER: 127.0.0.1#8600(127.0.0.1)
;; WHEN: Thu Dec 26 16:05:24 CST 2019
;; MSG SIZE  rcvd: 109
```

## Look at Consul Logs 

```
[root@dcfai022 log]# consul monitor
2019/12/26 15:44:53 [WARN] agent: Node name "dcfai022.ppprivmgmt.intraxa" will not be discoverable via DNS due to invalid characters. Valid characters include all alpha-numerics and dashes.
2019/12/26 15:44:53 [INFO] serf: EventMemberJoin: dcfai022.ppprivmgmt.intraxa 10.79.192.94
2019/12/26 15:44:53 [INFO] agent: Started DNS server 127.0.0.1:8600 (udp)
2019/12/26 15:44:53 [INFO] agent: Started DNS server 127.0.0.1:8600 (tcp)
2019/12/26 15:44:53 [INFO] agent: Started HTTP server on 127.0.0.1:8500 (tcp)
2019/12/26 15:44:53 [INFO] agent: started state syncer
2019/12/26 15:44:53 [INFO] agent: Retry join LAN is supported for: aliyun aws azure digitalocean gce k8s mdns os packet scaleway softlayer triton vsphere
2019/12/26 15:44:53 [INFO] agent: Joining LAN cluster...
2019/12/26 15:44:53 [INFO] agent: (LAN) joining: [10.79.192.22]
2019/12/26 15:44:53 [WARN] manager: No servers available
2019/12/26 15:44:53 [ERR] agent: failed to sync remote state: No known Consul servers
2019/12/26 15:44:53 [INFO] serf: EventMemberJoin: dcfa2027.ppprivmgmt.intraxa 10.79.192.22
2019/12/26 15:44:53 [INFO] serf: EventMemberJoin: dcfa2040.ppprivmgmt.intraxa 10.79.192.82
2019/12/26 15:44:53 [INFO] serf: EventMemberJoin: dcfa2043.ppprivmgmt.intraxa 10.79.192.99
2019/12/26 15:44:53 [WARN] memberlist: Refuting a suspect message (from: dcfai022.ppprivmgmt.intraxa)
2019/12/26 15:44:53 [INFO] agent: (LAN) joined: 1
2019/12/26 15:44:53 [INFO] agent: Join LAN completed. Synced with 1 initial agents
2019/12/26 15:44:53 [INFO] consul: adding server dcfa2027.ppprivmgmt.intraxa (Addr: tcp/10.79.192.22:8300) (DC: dcfa)
2019/12/26 15:44:53 [INFO] consul: adding server dcfa2040.ppprivmgmt.intraxa (Addr: tcp/10.79.192.82:8300) (DC: dcfa)
2019/12/26 15:44:53 [INFO] consul: adding server dcfa2043.ppprivmgmt.intraxa (Addr: tcp/10.79.192.99:8300) (DC: dcfa)
2019/12/26 15:44:55 [INFO] agent: Synced service "web-OOP"
2019/12/26 15:45:33 [INFO] agent: Caught signal:  hangup
2019/12/26 15:45:33 [INFO] agent: Reloading configuration...
2019/12/26 15:45:33 [INFO] agent: Synced service "web-OOP"
2019/12/26 15:48:45 [INFO] agent: Caught signal:  hangup
2019/12/26 15:48:45 [INFO] agent: Reloading configuration...
2019/12/26 15:48:45 [INFO] agent: Synced service "web-OOP"
2019/12/26 15:48:45 [INFO] agent: Synced service "ssh"
2019/12/26 15:50:14 [INFO] agent: Caught signal:  hangup
2019/12/26 15:50:14 [INFO] agent: Reloading configuration...
2019/12/26 15:50:14 [INFO] agent: Deregistered service "ssh"
2019/12/26 15:50:14 [INFO] agent: Synced service "web-OOP"
2019/12/26 16:02:07 [INFO] agent: Caught signal:  hangup
2019/12/26 16:02:07 [INFO] agent: Reloading configuration...
2019/12/26 16:02:07 [INFO] agent: Synced service "web-OOP"
^C[root@dcfai022 log]#

```


### Using DNSmasq to help enable DNS queries to Consul.

Append the following three lines to /etc/dnsmasq.conf

```
server=/consul/127.0.0.1#8600
server=10.77.249.21
server=10.77.249.23
```

Enable dnsmasq
```
systemctl enable dnsmasq
systemctl start dnsmasq
```

Update /etc/resolv.conf with:
```
nameserver 127.0.0.1
```

Examples:
```
[root@dcfa2027 etc]# nslookup web-OOP.service.consul
Server:         127.0.0.1
Address:        127.0.0.1#53

Name:   web-OOP.service.consul
Address: 10.79.192.94



[root@dcfa2027 etc]# nslookup dcfa2040.ppprivmgmt.intraxa
Server:         127.0.0.1
Address:        127.0.0.1#53

Non-authoritative answer:
Name:   dcfa2040.ppprivmgmt.intraxa
Address: 10.79.192.82




[root@dcfa2027 etc]# nslookup salt.equitable.com
Server:         127.0.0.1
Address:        127.0.0.1#53

Non-authoritative answer:
salt.equitable.com      canonical name = CCC3D001.equitable.com.
Name:   CCC3D001.equitable.com
Address: 10.77.248.21




[root@dcfa2027 etc]# dig +short web-OOP.service.consul
10.79.192.94

```



## NOTE: Alternative to HAproxy - you can use SSH tunneling to connect to Consul Web GUI
ssh -L 80:127.0.0.1:8500 coreimage@dcfa2043.ppprivmgmt.intraxa -N
then connect via your local browser to http://localhost



# Secure Consul with ACLs
Done with the Vault integration
refer to [Vault POC Notes](../vault/100-vault-POC-notes.md)



# From our laptops
```
jeffrey.liu@USLDNYGO9YLC2Z2 MINGW64 ~/OneDrive - AXA/temp/git/DNS
$ export CONSUL_HTTP_ADDR=http://dcfa2027.ppprivmgmt.intraxa:3389


jeffrey.liu@USLDNYGO9YLC2Z2 MINGW64 ~/OneDrive - AXA/temp/git/DNS
$ export CONSUL_HTTP_TOKEN="46d022e7-bb43-d5f2-ed6a-570cae24e81a"


jeffrey.liu@USLDNYGO9YLC2Z2 MINGW64 ~/OneDrive - AXA/temp/git/DNS
$ ~/Downloads/consul.exe members
Node                         Address            Status  Type    Build  Protocol  DC    Segment
dcfa2027.ppprivmgmt.intraxa  10.79.192.22:8301  alive   server  1.6.2  2         dcfa  <all>
dcfa2040.ppprivmgmt.intraxa  10.79.192.82:8301  alive   server  1.6.2  2         dcfa  <all>
dcfa2043.ppprivmgmt.intraxa  10.79.192.99:8301  alive   server  1.6.2  2         dcfa  <all>
dcfai022.ppprivmgmt.intraxa  10.79.192.94:8301  alive   client  1.6.2  2         dcfa  <default>


jeffrey.liu@USLDNYGO9YLC2Z2 MINGW64 ~/OneDrive - AXA/temp/git/DNS
$ ~/Downloads/consul.exe operator raft list-peers
Node                         ID                                    Address            State     Voter  RaftProtocol
dcfa2043.ppprivmgmt.intraxa  6056fa24-d3f9-7dbd-fd46-478362cd5c11  10.79.192.99:8300  leader    true   3
dcfa2040.ppprivmgmt.intraxa  e5424cef-fcaf-087d-70ca-cb8fee418118  10.79.192.82:8300  follower  true   3
dcfa2027.ppprivmgmt.intraxa  4c2fd11a-43ca-2b2d-d220-f93567bc2dfb  10.79.192.22:8300  follower  true   3
```




# NEXT 

## TODO create health checks  e.g., web url /health



## TODO Next step - check on enabling TLS for agents
1/30/2020
### TODO Consul TLS encryption setup for Agents
https://www.consul.io/docs/agent/encryption.html
There are two separate encryption systems, one for gossip traffic and one for RPC.
Gossip (already enabled?  how to verify?): https://learn.hashicorp.com/consul/security-networking/agent-encryption
TLS agent communication: https://learn.hashicorp.com/consul/security-networking/certificates
NOTE: there's an option to use the Consul's built-in TLS helpers to create a basic CA (internal to Consul).


## TODO Next step - test backups
https://learn.hashicorp.com/consul/datacenter-deploy/backup


## TODO Next step - Windows agents https://learn.hashicorp.com/consul/datacenter-deploy/windows


## TODO Next step - check on upgrade process
https://www.consul.io/docs/upgrading.html


## Next step - test federation across datacenters
https://instruqt.com/hashicorp/tracks/service-mesh-with-consul-hybrid
In this track you leverage Mesh Gateways to connect applications and services between K8s and VMs in remote datacenters.





## Resources
### https://learn.hashicorp.com/consul/getting-started/services
### https://support.hashicorp.com/hc/en-us/articles/115015668287-Where-are-my-Consul-logs-and-how-do-I-access-them
### https://learn.hashicorp.com/consul/security-networking/production-acls

### DNSmasq References
https://learn.hashicorp.com/consul/security-networking/forwarding
https://www.tecmint.com/setup-a-dns-dhcp-server-using-dnsmasq-on-centos-rhel/
https://stackoverflow.com/questions/48820058/consul-dnsmasq-issue
https://docs.gluster.org/en/latest/Administrator%20Guide/Consul/

