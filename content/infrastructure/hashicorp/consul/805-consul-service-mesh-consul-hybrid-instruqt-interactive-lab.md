+++
toc = true
date = "2020-01-01T12:47:50Z"
title = "Consul Service Mesh with Consul Hybrid Instruqt Interactive Lab"
weight = 805
+++


# Connect disparate platforms across datacenters


## Tutorial
https://instruqt.com/hashicorp/tracks/service-mesh-with-consul-hybrid


In this track you leverage Mesh Gateways to connect applications and services between K8s and VMs in remote datacenters.


### Connect your Datacenters

In this track we will connect applications across two separate datacenters, with application components in each. Our lab datacenters will reflect a common heterogeneous environment.

DC1 - Kubernetes & VM Environment (greenfield)
DC2 - VM Environment (legacy)
The two most common problems when connecting datacenters in this pattern are:

Overlapping IP spaces
Firewall complexities
In this track we will show you how Consul Connect and Mesh Gateways can solve this problem.

We need a few minutes to spin up your K8s infrastructure.

Let's take a brief moment to review our environment.

In DC1, we have a VM running a Consul server. We also have a Kubernetes cluster running with a single worker node. This Kubernetes worker node has a Consul agent running on it with a DaemonSet.

We can see this below.

```
consul members
```

```
root@dc1-consul-server:~# consul members
Node            Address              Status  Type    Build  Protocol  DC   Segment
ConsulServer    10.132.0.94:8301     alive   server  1.6.2  2         dc1  <all>
dc1-kubernetes  192.168.52.134:8301  alive   client  1.6.2  2         dc1  <default>
root@dc1-consul-server:~#
```



In DC2, we have a VM running a Consul server. We also have a VM that will run our legacy service. We'll inspect this Consul cluster in the next section.

The lab is preconfigured for connectivity within each Consul DC, but we need to make each datacenter Consul aware of the other datacenter.

Let's do this by connecting our two Consul datacenters with a wan join. This will allow us to manage our workloads across the datacenters.

Note: the lab env helps us this with this resolution of short names for our lab servers.

```
consul join -wan dc2-consul-server
```

```
root@dc1-consul-server:~# consul join -wan dc2-consul-server
Successfully joined cluster by contacting 1 nodes.
```


Now let's check our members output

```
consul members -wan
```

```
root@dc1-consul-server:~# consul members
Node            Address              Status  Type    Build  Protocol  DC   Segment
ConsulServer    10.132.0.94:8301     alive   server  1.6.2  2         dc1  <all>
dc1-kubernetes  192.168.52.134:8301  alive   client  1.6.2  2         dc1  <default>
root@dc1-consul-server:~# consul members -wan
Node              Address           Status  Type    Build  Protocol  DC   Segment
ConsulServer.dc1  10.132.0.94:8302  alive   server  1.6.2  2         dc1  <all>
ConsulServer.dc2  10.132.0.65:8302  alive   server  1.6.2  2         dc2  <all>
root@dc1-consul-server:~#
```

Great! Now that we have federation between our Consul datacenters, we can start deploying our workloads.



### Deploy your Legacy App

We will use our tracing app from the previous track for this lab. As you recall, the app is broken out into five components:

Frontend - Access to our application
API - gRPC API to backend services
Cache - Cache responses for our API
Payments - Process payments
Currency - Do currency lookups for our payments

Our currency service will represent our legacy workload running on-prem in DC2.

Let's take a quick look at Consul in this DC.

```
consul members
```

```
root@dc2-app-server:~# consul members
Node          Address            Status  Type    Build  Protocol  DC   Segment
ConsulServer  10.132.0.65:8301   alive   server  1.6.2  2         dc2  <all>
Currency      10.132.0.106:8301  alive   client  1.6.2  2         dc2  <default>
root@dc2-app-server:~#
```

We have the Currency service running for you on the VM in DC2. You can see the app running in Docker.

```
docker ps -a
```

```
root@dc2-app-server:~# docker ps -a
CONTAINER ID        IMAGE                                 COMMAND               CREATED             STATUS              PORTS               NAMES
25394b80c541        nicholasjackson/fake-service:v0.6.3   "/app/fake-service"   34 seconds ago      Up 32 seconds                           default_currency_1
root@dc2-app-server:~#
```


Let's add a Consul health check for the service, and reload it.

```
cat <<-EOF > /etc/consul.d/currency-service.json
{
  "service": {
    "name": "currency",
    "tags": [
      "production"
    ],
    "port": 9094,
    "check": {
      "id": "http",
      "name": "traffic on port 9094",
      "http": "http://127.0.0.1:9094/health",
      "interval": "10s",
      "timeout": "1s"
    }
  }
}
EOF
```

```
consul reload
```

You should see a healthy service in Consul. We can also test this service locally.

```
curl -s  http://127.0.0.1:9094 | jq
```

```
root@dc2-app-server:~# cat <<-EOF > /etc/consul.d/currency-service.json
> {
>   "service": {
>     "name": "currency",
>     "tags": [
>       "production"
>     ],
>     "port": 9094,
>     "check": {
>       "id": "http",
>       "name": "traffic on port 9094",
>       "http": "http://127.0.0.1:9094/health",
>       "interval": "10s",
>       "timeout": "1s"
>     }
>   }
> }
> EOF
root@dc2-app-server:~#
root@dc2-app-server:~# consul reload
Configuration reload triggered
root@dc2-app-server:~# curl -s  http://127.0.0.1:9094 | jq
{
  "name": "currency",
  "type": "HTTP",
  "start_time": "2020-01-02T15:06:59.705458",
  "end_time": "2020-01-02T15:06:59.705604",
  "duration": "146.231µs",
  "body": "Currency response",
  "code": 200
}
root@dc2-app-server:~#
```


Nice work! Now let's deploy the rest of our application in the other datacenter.



### Deploy the Modern App

Now that we have our legacy service running in DC2, let's deploy the rest of the application in DC1.

We've made a few minor changes to the deployment spec from the last track. You'll notice we've removed the spec file for the `currency` service, as it's already running in DC2.

We've also removed the `sidecar` upstreams from the payments service that calls the currency service, replaced it with a Consul DNS definition instead.

This is a common transition pattern where service discovery aware apps can become service mesh aware in a phased rollout. You can read more about this integration in K8s here.
https://www.consul.io/docs/platform/k8s/dns.html

Check this out in the `payments.yml` deployment file.
```
- name: UPSTREAM_URIS
  value: "http://currency.service.dc2.consul:9094"
```

Let's deploy the rest of the application and test it. Our frontend web component is accessible via NodePort.

```
kubectl apply -f  tracing

sleep 30

kubectl wait --for=condition=Ready $(kubectl get pod --selector=app=web -o name)
kubectl wait --for=condition=Ready $(kubectl get pod --selector=app=api -o name)
kubectl wait --for=condition=Ready $(kubectl get pod --selector=app=cache -o name)
kubectl wait --for=condition=Ready $(kubectl get pod --selector=app=payments -o name)
```


```
root@dc1-kubernetes:~# kubectl apply -f  tracing
deployment.apps/api created
deployment.apps/cache created
deployment.apps/payments created
service/web created
deployment.apps/web created
root@dc1-kubernetes:~#
root@dc1-kubernetes:~# sleep 30
root@dc1-kubernetes:~#
root@dc1-kubernetes:~# kubectl wait --for=condition=Ready $(kubectl get pod --selector=app=web -o name)
pod/web-6c8855c4f5-cxjdt condition met
root@dc1-kubernetes:~# kubectl wait --for=condition=Ready $(kubectl get pod --selector=app=api -o name)
pod/api-5d8f8b46c7-dgvkz condition met
root@dc1-kubernetes:~# kubectl wait --for=condition=Ready $(kubectl get pod --selector=app=cache -o name)
pod/cache-d7bdf4db9-stc9c condition met
root@dc1-kubernetes:~# kubectl wait --for=condition=Ready $(kubectl get pod --selector=app=payments -o name)
pod/payments-9766855f8-4v88w condition met
root@dc1-kubernetes:~#
```



Once all the pods are ready, we can send traffic to the frontend.

```
curl -s localhost:30900 | jq
```

```
root@dc1-kubernetes:~# curl -s localhost:30900 | jq
{
  "name": "web",
  "uri": "/",
  "type": "HTTP",
  "ip_addresses": [
    "192.168.52.138"
  ],
  "start_time": "2020-01-02T15:11:07.632928",
  "end_time": "2020-01-02T15:11:17.672054",
  "duration": "10.039126134s",
  "upstream_calls": [
    {
      "name": "api",
      "uri": "grpc://127.0.0.1:9091",
      "type": "gRPC",
      "ip_addresses": [
        "192.168.52.135"
      ],
      "upstream_calls": [
        {
          "name": "cache",
          "uri": "http://127.0.0.1:9092",
          "type": "HTTP",
          "ip_addresses": [
            "192.168.52.136"
          ],
          "start_time": "2020-01-02T15:11:07.648750",
          "end_time": "2020-01-02T15:11:07.649980",
          "duration": "1.230279ms",
          "body": "Cache response",
          "code": 200
        },
        {
          "name": "payments",
          "uri": "http://127.0.0.1:9093",
          "type": "HTTP",
          "ip_addresses": [
            "192.168.52.137"
          ],
          "start_time": "2020-01-02T15:11:07.661052",
          "end_time": "2020-01-02T15:11:17.666215",
          "duration": "10.005162317s",
          "upstream_calls": [
            {
              "uri": "http://currency.service.dc2.consul:9094",
              "code": -1,
              "error": "Error communicating with upstream service: Get http://currency.service.dc2.consul:9094/: net/http: request canceled while waiting for connection (Client.Timeout exceeded while awaiting headers)"
            }
          ],
          "code": 500,
          "error": "Error processing upstream request: http://127.0.0.1:9093"
        }
      ],
      "code": 13,
      "error": "rpc error: code = Internal desc = Error processing upstream request: http://127.0.0.1:9093"
    }
  ],
  "code": 500
}
root@dc1-kubernetes:~#
```


Hmmm, it looks like something went wrong. You should see a similar timeout message.

```
rpc error: code = Internal desc = Error processing upstream request
```

That error is not particularly helpful. Let's use our tracing infrastructure to run this down in the next assignment.



### Find the Issue

In this section we will investigate the connectivity issues between DCs. We'll wait a few moments for our traces to propagate.

Let's revisit our Jaeger UI and take a look at the latest trace data. You should see a message similar to this in the payments trace. You can get to this message quickly by clicking on the `expand all` button in Jaeger for the trace.

```
"error:Error communicating with upstream service: Get http://currency.service.dc2.consul:9094/: net/http: request canceled while waiting for connection (Client.Timeout exceeded while awaiting headers)"
```

It seems even though we could discover the service in the datacenter, we don't actually have a route to that service, or the network is blocking it.

Let's explore this further with a basic connectivity check between our K8s node and our server running in the other DC. We can use a tool called `nmap` to help us with this verification.

We know that IP is our Currency service running on our DC2 app server, so we can test with the below command.

```
ip=$(getent ahostsv4 dc2-app-server |  awk '{print $1}' | head -1)
nmap -p 9094 $ip
```


```
root@dc1-kubernetes:~# ip=$(getent ahostsv4 dc2-app-server |  awk '{print $1}' | head -1)
root@dc1-kubernetes:~# nmap -p 9094 $ip

Starting Nmap 7.60 ( https://nmap.org ) at 2020-01-02 15:13 UTC
Nmap scan report for p-xjsdurjotcsg-dc2-app-server.c.instruqt-prod.internal (10.132.0.106)
Host is up (0.0013s latency).

PORT     STATE    SERVICE
9094/tcp filtered unknown

Nmap done: 1 IP address (1 host up) scanned in 0.56 seconds
```

It looks like that route is indeed blocked. We can see the status as `filtered`.

Let's look at how Mesh Gateways can help us solve this problem in the next exercise.


### Add the Legacy Sidecar

To make our legacy service work with the mesh gateways, we need to run a proxy next to the service, which we will deploy in this assignment.

Let's start by updating consul with a sidecar service definition.

```
cat <<-EOF > /etc/consul.d/currency-service.json
{
  "service": {
    "name": "currency",
    "tags": [
      "production"
    ],
    "port": 9094,
    "connect": { "sidecar_service": {} },
    "check": {
      "id": "http",
      "name": "traffic on port 9094",
      "http": "http://127.0.0.1:9094/health",
      "interval": "10s",
      "timeout": "1s"
    }
  }
}
EOF

consul reload
```

```
root@dc2-app-server:~# cat <<-EOF > /etc/consul.d/currency-service.json
> {
>   "service": {
>     "name": "currency",
>     "tags": [
>       "production"
>     ],
>     "port": 9094,
>     "connect": { "sidecar_service": {} },
>     "check": {
>       "id": "http",
>       "name": "traffic on port 9094",
>       "http": "http://127.0.0.1:9094/health",
>       "interval": "10s",
>       "timeout": "1s"
>     }
>   }
> }
> EOF
root@dc2-app-server:~#
root@dc2-app-server:~# consul reload
Configuration reload triggered
root@dc2-app-server:~#
```

Now we can start our sidecar.

```
root@dc2-app-server:~# nohup consul connect envoy -sidecar-for=currency > /envoy.out &
[1] 4141
root@dc2-app-server:~# nohup: ignoring input and redirecting stderr to stdout

```

Excellent. Now let's create our gateways.


### Deploy the DC2 Gateway

In the last assignment we discovered our Kubernetes cluster in DC1 was not able to route to our legacy app running in DC2. This is a common problem, and in general these types of routes can be tricky to set up and secure.

In this lab we will deploy a Mesh Gateway in DC2 and establish connectivity between the gateway and our service.

We have introduced a new server that will represent ingress and egress points for DC2. Let's do some basic connectivity checks before we make Consul aware of our gateway nodes.

Let's first check our gateway in DC2 can reach the local app. We'll use some helpful resolution from the lab env to grab the same IP from our Jaeger trace

```
ip=$(getent ahostsv4 dc2-app-server |  awk '{print $1}' | head -1)
nmap -p 9094 $ip
```

```
root@dc2-consul-gateway:~# ip=$(getent ahostsv4 dc2-app-server |  awk '{print $1}' | head -1)
root@dc2-consul-gateway:~# nmap -p 9094 $ip

Starting Nmap 7.60 ( https://nmap.org ) at 2020-01-02 15:16 UTC
Nmap scan report for p-xjsdurjotcsg-dc2-app-server.c.instruqt-prod.internal (10.132.0.106)
Host is up (0.00098s latency).

PORT     STATE SERVICE
9094/tcp open  unknown

Nmap done: 1 IP address (1 host up) scanned in 0.44 seconds
```

It looks like our Gateway server in DC2 has a direct route to our application. We can see this with the state of open. Progress!

Now we can configure the node to be a mesh gateway and bring it into our mesh. There is a Consul agent already running on this machine.

```
local_ipv4=$(curl -s -H "Metadata-Flavor: Google" http://metadata/computeMetadata/v1/instance/network-interfaces/0/ip)
nohup consul connect envoy -mesh-gateway -register \
                 -service "gateway" \
                 -address ${local_ipv4}:443 \
                 -wan-address ${local_ipv4}:443 \
                 -bind-address "public=${local_ipv4}:443" \
                 -admin-bind 127.0.0.1:19000 > /gateway.out &
```

```
root@dc2-consul-gateway:~# local_ipv4=$(curl -s -H "Metadata-Flavor: Google" http://metadata/computeMetadata/v1/instance/network-interfaces/0/ip)
root@dc2-consul-gateway:~# nohup consul connect envoy -mesh-gateway -register \
>                  -service "gateway" \
>                  -address ${local_ipv4}:443 \
>                  -wan-address ${local_ipv4}:443 \
>                  -bind-address "public=${local_ipv4}:443" \
>                  -admin-bind 127.0.0.1:19000 > /gateway.out &
[1] 3290
root@dc2-consul-gateway:~# nohup: ignoring input and redirecting stderr to stdout
```

Now we've got our gateway running we can see we have a listener for this traffic.

```
root@dc2-consul-gateway:~# curl localhost:19000/listeners && echo
["10.132.0.37:443"]
root@dc2-consul-gateway:~#
```


```
root@dc2-consul-gateway:/etc/consul.d# cat client.json
{
  "datacenter": "dc2",
  "server": false,
  "bind_addr": "10.132.0.37",
  "client_addr": "0.0.0.0",
  "data_dir": "/consul/data",
  "log_level": "INFO",
  "node_name": "Gateway",
  "ui": true,
  "connect": {
    "enabled": true
  },
  "enable_central_service_config": true,
  "ports": {
    "grpc": 8502
  },
  "retry_join": [
    "dc2-consul-server:8301"
  ]
}
root@dc2-consul-gateway:/etc/consul.d# consul members
Node          Address            Status  Type    Build  Protocol  DC   Segment
ConsulServer  10.132.0.65:8301   alive   server  1.6.2  2         dc2  <all>
Currency      10.132.0.106:8301  alive   client  1.6.2  2         dc2  <default>
Gateway       10.132.0.37:8301   alive   client  1.6.2  2         dc2  <default>
root@dc2-consul-gateway:/etc/consul.d#
```


Let's repeat this deployment in DC1.


### Deploy the DC1 Gateway

Now that we have a Gateway in DC2 that can talk to our legacy service, we can spin up a gateway in DC1, and connect the two gateways.

First, let's check our connectivity again. We can check this between gateways now that our DC2 gateway is accepting traffic.

Let's check the gateway to the DC2 app server first.

```
ip=$(getent ahostsv4 dc2-app-server |  awk '{print $1}' | head -1)
nmap -p 9094 $ip
```

```
root@dc1-consul-gateway:~# ip=$(getent ahostsv4 dc2-app-server |  awk '{print $1}' | head -1)

root@dc1-consul-gateway:~# nmap -p 9094 $ip

Starting Nmap 7.60 ( https://nmap.org ) at 2020-01-02 15:19 UTC
Nmap scan report for p-xjsdurjotcsg-dc2-app-server.c.instruqt-prod.internal (10.132.0.106)
Host is up (0.0012s latency).

PORT     STATE    SERVICE
9094/tcp filtered unknown

Nmap done: 1 IP address (1 host up) scanned in 0.45 seconds
root@dc1-consul-gateway:~# 
```


Let's check the gateway in DC1 to the gateway in DC2.

```
ip=$(getent ahostsv4 dc2-consul-gateway |  awk '{print $1}' | head -1)
nmap -p 443 $ip
```

```
root@dc1-consul-gateway:~# ip=$(getent ahostsv4 dc2-consul-gateway |  awk '{print $1}' | head -1)
root@dc1-consul-gateway:~# nmap -p 443 $ip

Starting Nmap 7.60 ( https://nmap.org ) at 2020-01-02 15:41 UTC
Nmap scan report for p-xjsdurjotcsg-dc2-consul-gateway.c.instruqt-prod.internal (10.132.0.37)
Host is up (0.0015s latency).

PORT    STATE SERVICE
443/tcp open  https

Nmap done: 1 IP address (1 host up) scanned in 0.45 seconds
root@dc1-consul-gateway:~#
```

Our gateway can reach the other gateway, but not the app server. This is the behavior we expect based on our lab environment. The gateways will handle all the cross DC traffic, so we don't have to worry about it.

Now let's start up the Gateway in DC1. There is a preconfigured Consul agent on this box.

```
local_ipv4=$(curl -s -H "Metadata-Flavor: Google" http://metadata/computeMetadata/v1/instance/network-interfaces/0/ip)
nohup consul connect envoy -mesh-gateway -register \
                 -service "gateway" \
                 -address ${local_ipv4}:443 \
                 -wan-address ${local_ipv4}:443 \
                 -bind-address "public=${local_ipv4}:443" \
                 -admin-bind 127.0.0.1:19000 > /gateway.out &
```

```
root@dc1-consul-gateway:~# nohup consul connect envoy -mesh-gateway -register \
>                  -service "gateway" \
>                  -address ${local_ipv4}:443 \
>                  -wan-address ${local_ipv4}:443 \
>                  -bind-address "public=${local_ipv4}:443" \
>                  -admin-bind 127.0.0.1:19000 > /gateway.out &
[1] 20476
root@dc1-consul-gateway:~# nohup: ignoring input and redirecting stderr to stdout
```

We can now verify our new listener.

```
root@dc1-consul-gateway:~# curl localhost:19000/listeners && echo
["10.132.0.82:443"]
root@dc1-consul-gateway:~#
```

```
root@dc1-consul-gateway:/etc/consul.d# cat client.json
{
  "datacenter": "dc1",
  "server": false,
  "bind_addr": "10.132.0.82",
  "client_addr": "0.0.0.0",
  "data_dir": "/consul/data",
  "log_level": "INFO",
  "node_name": "Gateway",
  "ui": true,
  "connect": {
    "enabled": true
  },
  "enable_central_service_config": true,
  "ports": {
    "grpc": 8502
  },
  "retry_join": [
    "dc1-consul-server:8301"
  ]
}
root@dc1-consul-gateway:/etc/consul.d#


root@dc1-consul-gateway:/etc/consul.d# consul members
Node            Address              Status  Type    Build  Protocol  DC   Segment
ConsulServer    10.132.0.94:8301     alive   server  1.6.2  2         dc1  <all>
Gateway         10.132.0.82:8301     alive   client  1.6.2  2         dc1  <default>
dc1-kubernetes  192.168.52.134:8301  alive   client  1.6.2  2         dc1  <default>
root@dc1-consul-gateway:/etc/consul.d#
```

Now that we have gateway to gateway connectivity, let's fix our app and redeploy it to use the sidecars across gateways.



### Bring it all together

Now that we have gateways to connect our DCs, we can redeploy our application, and route the entire component flow through the sidecars and gateways. We'll inspect some of the traffic through the gateways to validate they are being used, and look specifically at the SNI headers, as well as trace data.

First, let's set up tcpdump to inspect the traffic on our gateways.

On the DC1 gateway, run the following. This filter looks specifically at the SSL handshake hello packet.

```
tcpdump -i ens4 -s 1500 '(tcp[((tcp[12:1] & 0xf0) >> 2)+5:1] = 0x01) and (tcp[((tcp[12:1] & 0xf0) >> 2):1] = 0x16)' -nnXSs0 -ttt
```

On the DC2 gateway, run the following. We'll look at all the packets.

```
tcpdump -i ens4 port 443 -nnXX -vv -A
```

You'll notice the upstream definition has an a datacenter tag appended to the upstream, for dc2. You can see this in the code editor. We've set the gateways up in `local` mode so this will send all cross DC traffic through our gateways.

```
"consul.hashicorp.com/connect-service-upstreams": "currency:9094:dc2"
```

DC1-App COnfig /root/tracing/payments.yml
```
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: payments
  labels:
    app: payments
spec:
  replicas: 1
  selector:
    matchLabels:
      app: payments
  template:
    metadata:
      labels:
        app: payments
      annotations:
        "consul.hashicorp.com/connect-inject": "true"
        "consul.hashicorp.com/connect-service-upstreams": "currency:9094:dc2"
    spec:
      containers:
      - name: payments
        image: "nicholasjackson/fake-service:v0.7.8"
        imagePullPolicy: Always
        ports:
        - containerPort: 9093
        env:
          - name: LISTEN_ADDR
            value: "0.0.0.0:9093"
          - name: UPSTREAM_URIS
            value: "http://127.0.0.1:9094"
          - name: MESSAGE
            value: "Payments response"
          - name: NAME
            value: "payments"
          - name: SERVER_TYPE
            value: "http"
          - name: TRACING_ZIPKIN
            value: "http://10.132.0.38:9411"
          - name:  HTTP_CLIENT_REQUEST_TIMEOUT
            value: "10s"
```


Now we can redeploy our app on the DC1 - K8s tab.

```
kubectl apply -f tracing
sleep 10
kubectl wait --for=condition=Ready $(kubectl get pod --selector=app=payments -o name)
```

```
root@dc1-kubernetes:~# kubectl apply -f tracing
deployment.apps/api unchanged
deployment.apps/cache unchanged
deployment.apps/payments configured
service/web unchanged
deployment.apps/web unchanged
root@dc1-kubernetes:~# sleep 10
root@dc1-kubernetes:~# kubectl wait --for=condition=Ready $(kubectl get pod --selector=app=payments -o name)
pod/payments-66b5b7c488-6wdjg condition met
root@dc1-kubernetes:~#
```

Next, let's simulate some traffic through the DC1 - K8s tab.

```
curl -s localhost:30900 | jq
```

```
root@dc1-kubernetes:~# curl -s localhost:30900 | jq
{
  "name": "web",
  "uri": "/",
  "type": "HTTP",
  "ip_addresses": [
    "192.168.52.138"
  ],
  "start_time": "2020-01-02T15:28:44.301326",
  "end_time": "2020-01-02T15:28:44.337618",
  "duration": "36.292329ms",
  "body": "Hello World",
  "upstream_calls": [
    {
      "name": "api",
      "uri": "grpc://127.0.0.1:9091",
      "type": "gRPC",
      "ip_addresses": [
        "192.168.52.135"
      ],
      "start_time": "2020-01-02T15:28:44.302490",
      "end_time": "2020-01-02T15:28:44.336166",
      "duration": "33.675749ms",
      "body": "API response",
      "upstream_calls": [
        {
          "name": "cache",
          "uri": "http://127.0.0.1:9092",
          "type": "HTTP",
          "ip_addresses": [
            "192.168.52.136"
          ],
          "start_time": "2020-01-02T15:28:44.305074",
          "end_time": "2020-01-02T15:28:44.306276",
          "duration": "1.201917ms",
          "body": "Cache response",
          "code": 200
        },
        {
          "name": "payments",
          "uri": "http://127.0.0.1:9093",
          "type": "HTTP",
          "ip_addresses": [
            "192.168.52.139"
          ],
          "start_time": "2020-01-02T15:28:44.312964",
          "end_time": "2020-01-02T15:28:44.334552",
          "duration": "21.588318ms",
          "body": "Payments response",
          "upstream_calls": [
            {
              "name": "currency",
              "uri": "http://127.0.0.1:9094",
              "type": "HTTP",
              "start_time": "2020-01-02T15:28:44.332139",
              "end_time": "2020-01-02T15:28:44.332275",
              "duration": "135.849µs",
              "body": "Currency response",
              "code": 200
            }
          ],
          "code": 200
        }
      ],
      "code": 0
    }
  ],
  "code": 200
}
root@dc1-kubernetes:~#
```

Awesome. You've fixed the application across DCs!! Go check out the new Jaeger traces!!

If you look closely at the packets in the DC1 - Gateway tab you'll see a a similar value that is not encrypted.


```
currency.default.dc2.internal.<domain>.consul
```

That's the SNI header. It's the only piece of information the gateway can see, and this value allows it to be sent to the correct destination.

The packet is not decrypted until it reaches the last sidecar proxy, so we have end-to-end encryption across over the WAN between our DCs.

Nice work!!! You just ran your first Mesh Gateway workload!!!