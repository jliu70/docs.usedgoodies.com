+++
toc = true
date = "2019-12-03T11:47:50Z"
title = "Consul Service Mesh Instruqt interactive lab"
weight = 804
+++

12/3/2019

https://instruqt.com/hashicorp/tracks/service-mesh-with-consul

Service Mesh with Consul

Evolve from service discovery to service mesh. No more mapping IP addresses and ports across load balancers and firewalls. Consul Connect ensures secure connections between services wherever they run.

This track will build on what you learned in the service discovery track and take our application from service discovery to service mesh. We'll dive into mesh fundamentals and use an Envoy proxy to connect our application to its database.


### Get Into my Sidecar
Create a sidecar service definition for your application proxy

Connect proxies are typically deployed as sidecars to an instance that they handle traffic for. They might be on the same bare metal server, virtual machine, or Kubernetes daemonset. Connect has a pluggable proxy architecture, with awesome first-class support for Envoy. We'll use Envoy as our proxy for the entirety of this workshop.

Visit the Connect docs for more info on proxy integration.
https://www.consul.io/docs/connect/proxies.html


In this challenge, we'll set up a sidecar definition.

![Connect Sidecar](/images/consul/connect_sidecar.png?&classes=border,shadow)


In this challenge we'll add a sidecar service to our existing service definition.

The `sidecar_service` field is a nested service definition where almost any regular service definition field can be set.

All fields in the nested definition are optional, however there are some default settings that make sidecar proxy configuration much simpler.

In orchestrated environments,such as Kubernetes or Nomad, this is highly abstract and can be configured with simple metadata i.e. annotations.

Update the definition by adding the the `connect` block as seen below. Copy and paste what's below over the entire file in the Database Service tab. Use CTRL-S to save the file.

```
{
  "service": {
    "name": "mysql",
    "tags": [
      "database",
      "production"
    ],
    "port": 3306,
    "connect": { "sidecar_service": {} },
    "check": {
      "id": "mysql",
      "name": "MySQL TCP on port 3306",
      "tcp": "localhost:3306",
      "interval": "10s",
      "timeout": "1s"
    }
  }
}
```

Next, reload Consul:
```
consul reload
```

You should see a failing service called `mysql-sidecar-proxy` in Consul. This is expected!

We will start a proxy and register it with Connect in our next challenge.




### Introducing the Envoy Proxy
Run your first Connect sidecar proxy with Envoy

In the last challenge we set up a sidecar service definition for our Envoy proxy. This is the first step in bringing our mesh to life.

The sidecar definition tells Consul to expect a proxy registration for a service, Database, in this example.

Now that Consul is aware that the Database service should run a proxy, we can use the Consul agent to bootstrap the proxy and send it dynamic configuration.

We'll take a deeper look at this configuration later.


Now that we've registered a proxy service in Consul for our MySQL database, let's start the proxy server so the health check will pass.

Consul will bootstrap the proxy with the correct configuration, and bring it into the mesh for us.

We've placed an Envoy binary on this machine for you. Consul will be able to access it from the $PATH.

Go ahead and start the proxy with the following command:

```
nohup consul connect envoy -sidecar-for mysql > /envoy.out &
```

You can verify in the Consul UI or the with the Consul CLI that your proxy health check is now passing.

We can now use the proxy to establish communication between our application and the database!

NOTE: the default port for the Envoy proxy is 21000
```
root@database:~# ss -antlp
State Recv-Q Send-Q  Local Address:Port  Peer Address:Port
LISTEN0      128           0.0.0.0:21000      0.0.0.0:*     users:(("envoy",pid=416,fd=20))
LISTEN0      128           0.0.0.0:22         0.0.0.0:*     users:(("dropbear",pid=61,fd=3))
LISTEN0      128         127.0.0.1:19000      0.0.0.0:*     users:(("envoy",pid=416,fd=8))
LISTEN0      128                 *:15778            *:*     users:(("gotty",pid=44,fd=3))
LISTEN0      128                 *:15779            *:*     users:(("instruqt-agent",pid=49,fd=3))
LISTEN0      70                  *:3306             *:*
LISTEN0      128                 *:8301             *:*     users:(("consul",pid=365,fd=5))
LISTEN0      128                 *:8500             *:*     users:(("consul",pid=365,fd=9))
LISTEN0      128                 *:8502             *:*     users:(("consul",pid=365,fd=10))
LISTEN0      128              [::]:22            [::]:*     users:(("dropbear",pid=61,fd=4))
LISTEN0      128                 *:8600             *:*     users:(("consul",pid=365,fd=7))
```




### Connect Upstream with Envoy

Connect provides service connectivity through upstream definitions. These services could be a database, backend, or any service which another service relies on.

In the previous challenges we set up a sidecar service definition without an upstream definition.

In this assignment we'll modify our sidecar service and add an upstream definition that will allow our application to connect to its database.

We've brought back our application server for this assignment.

It has an empty sidecar_service definition, which you can see in the code editor. Let's modify it below to create connectivity to our database.

Modify the application's sidecar_service definition to add our upstream for the database. You can copy and paste the entire file from below:
```
{
  "service": {
    "name": "wordpress",
    "tags": [
      "wordpress",
      "production"
    ],
    "port": 80,
    "connect": {
      "sidecar_service": {
        "proxy": {
          "upstreams": [
            {
              "destination_name": "mysql",
              "local_bind_port": 3306
            }
          ]
        }
      }
    },
    "check": {
      "id": "wordpress",
      "name": "wordpress TCP on port 80",
      "tcp": "localhost:80",
      "interval": "10s",
      "timeout": "1s"
    }
  }
}
```

Next reload consul
```
consul reload
```


Envoy will create a loopback listener for us to connect to the database on port 3306.

Example:  see how the netcat connection failed prior to `consul reload`
```
root@app:~# nc 127.0.0.1 3306
(UNKNOWN) [127.0.0.1] 3306 (?) : Connection refused
root@app:~# consul reload
Configuration reload triggered
root@app:~# nc 127.0.0.1 3306
n
5.5.5-10.4.8-MariaDB-1:10.4.8+maria~bionicO*DXl:zzu���]Rao.2Ox1)xCmysql_native_password
^C
```


Envoy has an admin interface that listens on port 19000 by default. We can check out our new listener with following command:
```
root@app:~# curl localhost:19000/listeners && echo
["0.0.0.0:21000","127.0.0.1:3306"]
root@app:~#

```

We'll configure our application to use the listener in our next assignment.



### Connect application to the database

In this section will use Envoy to connect the application to the database.

In the last assignment we created an Envoy listener for our database service through a Connect upstream definition.

Let's use that definition to allow our application to connect to the database.

Remember, our listener is configured on localhost, so we can just update our properties file to 127.0.0.1, and establish connectivity.

Go ahead and do this now. Check the Website tab. Our service mesh blog is back online!


wp-config.php
```
<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress');

/** MySQL database username */
define( 'DB_USER', 'root');

/** MySQL database password */
define( 'DB_PASSWORD', 'HashiCorp123');

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1');
```

NOTE: I think I had to do a `consul reload` in order for the website to load



### Consul Connect Intentions
Intentions define access control for services via Consul Connect and are used to control which services may establish connections. You can manage Intentions via the API, CLI, or UI. Intentions are enforced by the proxy on inbound connections.

After verifying the TLS client certificate, the authorize API endpoint is called which verifies the connection. If authorize returns false the connection must be terminated.

Intentions are powerful because they free you from having to manage endless lists of IP address and port mappings. Instead, we can manage traffic in simple terms by what an application is or does.

Let's use intentions to restrict connectivity from our application to the database.

Traffic is denied by default at the start of this assignment, so our application is now broken.
Let's create an allow rule to bring back our application. Copy the following command into the app tab.

```
root@app:~# consul intention check wordpress mysql
Denied

root@app:~# consul intention create -allow wordpress mysql
Created: wordpress => mysql (allow)

root@app:~# consul intention check wordpress mysql
Allowed
```

You can also manage Intentions in the Intentions tab of the Consul UI. Click on the intention, and change it to deny. What happens?

Change it back to allow to restore service. With connectivity from the database restored, your application should be serving traffic once again!!!

We'll dive deeper into how this works in the next challenge.



### Envoy Proxy
In this assignment, we'll take a deeper look at Envoy. We'll focus on three elements that make up the foundation of our mesh.

mTLS - How did Connect and Envoy provide end-to-end encryption between services?
Service Discovery - How was Consul able to get service discovery information to the Envoy proxy for it's upstreams?
Intentions - How were we able to allow or deny traffic based on service identity?
Let's investigate each of these with some easy to get info from Envoy.

Pop Open the Hood

First, let's check out some of the certificate information from our Envoy proxy. You can see some basic cert info in the Envoy UI under certs. 


Envoy UI - certs
```
{
 "certificates": [
  {
   "ca_cert": [
    {
     "path": "\u003cinline\u003e",
     "serial_number": "07",
     "subject_alt_names": [
      {
       "uri": "spiffe://756fec51-175a-670b-0780-7182930a9a23.consul"
      }
     ],
     "days_until_expiration": "3652",
     "valid_from": "2019-12-03T17:18:53Z",
     "expiration_time": "2029-12-03T17:18:53Z"
    }
   ],
   "cert_chain": [
    {
     "path": "\u003cinline\u003e",
     "serial_number": "1f",
     "subject_alt_names": [
      {
       "uri": "spiffe://756fec51-175a-670b-0780-7182930a9a23.consul/ns/default/dc/dc1/svc/wordpress"
      }
     ],
     "days_until_expiration": "2",
     "valid_from": "2019-12-03T17:24:27Z",
     "expiration_time": "2019-12-06T17:24:27Z"
    }
   ]
  },
  {
   "ca_cert": [
    {
     "path": "\u003cinline\u003e",
     "serial_number": "07",
     "subject_alt_names": [
      {
       "uri": "spiffe://756fec51-175a-670b-0780-7182930a9a23.consul"
      }
     ],
     "days_until_expiration": "3652",
     "valid_from": "2019-12-03T17:18:53Z",
     "expiration_time": "2029-12-03T17:18:53Z"
    }
   ],
   "cert_chain": [
    {
     "path": "\u003cinline\u003e",
     "serial_number": "1f",
     "subject_alt_names": [
      {
       "uri": "spiffe://756fec51-175a-670b-0780-7182930a9a23.consul/ns/default/dc/dc1/svc/wordpress"
      }
     ],
     "days_until_expiration": "2",
     "valid_from": "2019-12-03T17:24:27Z",
     "expiration_time": "2019-12-06T17:24:27Z"
    }
   ]
  }
 ]
}
```


We've also grabbed the cert for you and decoded in the code editor.
```
/tmp/crt.txt

Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 31 (0x1f)
        Signature Algorithm: ecdsa-with-SHA256
        Issuer: CN = Consul CA 7
        Validity
            Not Before: Dec  3 17:24:27 2019 GMT
            Not After : Dec  6 17:24:27 2019 GMT
        Subject: CN = wordpress
        Subject Public Key Info:
            Public Key Algorithm: id-ecPublicKey
                Public-Key: (256 bit)
                pub:
                    04:81:47:3d:29:e4:25:68:6b:f5:3c:5e:ca:c1:2f:
                    f6:48:b3:70:6a:44:30:2f:a5:7d:bf:04:84:fa:37:
                    f2:e8:ba:e6:00:85:1f:b9:cb:02:13:50:cf:08:c7:
                    72:20:f2:73:e1:bb:88:0d:e5:dc:49:18:6f:d5:29:
                    81:e3:d7:61:9f
                ASN1 OID: prime256v1
                NIST CURVE: P-256
        X509v3 extensions:
            X509v3 Key Usage: critical
                Digital Signature, Key Encipherment, Data Encipherment, Key Agreement
            X509v3 Extended Key Usage: 
                TLS Web Client Authentication, TLS Web Server Authentication
            X509v3 Basic Constraints: critical
                CA:FALSE
            X509v3 Subject Key Identifier: 
                2E:EC:57:5F:E1:04:86:06:8D:12:AD:BE:A8:E0:E6:26:02:DC:AB:36:3B:51:1E:11:80:9D:3C:CD:03:B1:53:73
            X509v3 Authority Key Identifier: 
                keyid:2E:EC:57:5F:E1:04:86:06:8D:12:AD:BE:A8:E0:E6:26:02:DC:AB:36:3B:51:1E:11:80:9D:3C:CD:03:B1:53:73

            X509v3 Subject Alternative Name: 
                URI:spiffe://756fec51-175a-670b-0780-7182930a9a23.consul/ns/default/dc/dc1/svc/wordpress
    Signature Algorithm: ecdsa-with-SHA256
         30:45:02:20:30:2d:0c:0a:6c:cb:8e:21:28:21:a4:7d:f2:42:
         d4:c3:67:b8:ef:b3:56:5c:1d:3c:2d:42:5e:27:16:d9:13:ca:
         02:21:00:9b:d5:7b:39:e7:b2:6f:fd:ce:62:0c:70:34:86:a9:
         5f:73:e1:98:19:a9:26:dd:01:f3:a4:99:e7:f6:13:e9:3f

```



We can look some basic certificate information from Envoy with the below command.

```
root@app:~# curl -s localhost:19000/certs | jq '.certificates[0].cert_chain[0].subject_alt_names[0].uri'
"spiffe://756fec51-175a-670b-0780-7182930a9a23.consul/ns/default/dc/dc1/svc/wordpress"
root@app:~#

```


Nice! That's the identity for our application in the mesh. We can see the TTL for the certificate

Let's see when that cert expires.
```
root@app:~# curl -s localhost:19000/certs | jq '.certificates[0].cert_chain[0].days_until_expiration'
"2"
root@app:~#

```
Our certificate TTL is very short, 2 days! And as a bonus, it's automatically managed by Consul.


Second, let's check out some of the service discovery information from our Envoy proxy. You can see the this in the Envoy UI under clusters.
```
local_app::default_priority::max_connections::1024
local_app::default_priority::max_pending_requests::1024
local_app::default_priority::max_requests::1024
local_app::default_priority::max_retries::3
local_app::high_priority::max_connections::1024
local_app::high_priority::max_pending_requests::1024
local_app::high_priority::max_requests::1024
local_app::high_priority::max_retries::3
local_app::added_via_api::true
local_app::127.0.0.1:80::cx_active::0
local_app::127.0.0.1:80::cx_connect_fail::42
local_app::127.0.0.1:80::cx_total::87
local_app::127.0.0.1:80::rq_active::0
local_app::127.0.0.1:80::rq_error::0
local_app::127.0.0.1:80::rq_success::0
local_app::127.0.0.1:80::rq_timeout::0
local_app::127.0.0.1:80::rq_total::45
local_app::127.0.0.1:80::health_flags::healthy
local_app::127.0.0.1:80::weight::1
local_app::127.0.0.1:80::region::
local_app::127.0.0.1:80::zone::
local_app::127.0.0.1:80::sub_zone::
local_app::127.0.0.1:80::canary::false
local_app::127.0.0.1:80::success_rate::-1
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::outlier::success_rate_average::-1
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::outlier::success_rate_ejection_threshold::-1
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::default_priority::max_connections::1024
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::default_priority::max_pending_requests::1024
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::default_priority::max_requests::1024
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::default_priority::max_retries::3
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::high_priority::max_connections::1024
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::high_priority::max_pending_requests::1024
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::high_priority::max_requests::1024
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::high_priority::max_retries::3
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::added_via_api::true
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::cx_active::0
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::cx_connect_fail::0
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::cx_total::30
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::rq_active::0
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::rq_error::0
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::rq_success::0
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::rq_timeout::0
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::rq_total::30
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::health_flags::healthy
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::weight::1
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::region::
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::zone::
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::sub_zone::
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::canary::false
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::success_rate::-1
local_agent::default_priority::max_connections::1024
local_agent::default_priority::max_pending_requests::1024
local_agent::default_priority::max_requests::1024
local_agent::default_priority::max_retries::3
local_agent::high_priority::max_connections::1024
local_agent::high_priority::max_pending_requests::1024
local_agent::high_priority::max_requests::1024
local_agent::high_priority::max_retries::3
local_agent::added_via_api::false
local_agent::127.0.0.1:8502::cx_active::1
local_agent::127.0.0.1:8502::cx_connect_fail::0
local_agent::127.0.0.1:8502::cx_total::1
local_agent::127.0.0.1:8502::rq_active::1
local_agent::127.0.0.1:8502::rq_error::0
local_agent::127.0.0.1:8502::rq_success::0
local_agent::127.0.0.1:8502::rq_timeout::0
local_agent::127.0.0.1:8502::rq_total::1
local_agent::127.0.0.1:8502::health_flags::healthy
local_agent::127.0.0.1:8502::weight::1
local_agent::127.0.0.1:8502::region::
local_agent::127.0.0.1:8502::zone::
local_agent::127.0.0.1:8502::sub_zone::
local_agent::127.0.0.1:8502::canary::false
local_agent::127.0.0.1:8502::success_rate::-1
```



We can see the added_via_api::true is set for our database cluster, which means the Consul agent sent this to Envoy via the API.

```
root@app:~# curl -s localhost:19000/clusters | grep  mysql
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::outlier::success_rate_average::-1
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::outlier::success_rate_ejection_threshold::-1
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::default_priority::max_connections::1024
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::default_priority::max_pending_requests::1024
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::default_priority::max_requests::1024
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::default_priority::max_retries::3
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::high_priority::max_connections::1024
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::high_priority::max_pending_requests::1024
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::high_priority::max_requests::1024
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::high_priority::max_retries::3
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::added_via_api::true
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::cx_active::0
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::cx_connect_fail::0
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::cx_total::30
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::rq_active::0
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::rq_error::0
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::rq_success::0
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::rq_timeout::0
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::rq_total::30
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::health_flags::healthy
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::weight::1
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::region::
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::zone::
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::sub_zone::
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::canary::false
mysql.default.dc1.internal.756fec51-175a-670b-0780-7182930a9a23.consul::10.96.27.126:21000::success_rate::-1
root@app:~#
```


We can also validate that the ip address in Envoy matches the ip address in Consul for the database node.

```
root@app:~# curl -s  http://127.0.0.1:8500/v1/catalog/node/Database | jq '.Node.Address'
"10.96.27.126"
root@app:~#
```


Last, we can do some basic intention validation by emulating the API call made from Envoy to the Consul agent.
```
cat /tmp/payload.json

{
  "Target": "mysql",
  "ClientCertURI": "spiffe://756fec51-175a-670b-0780-7182930a9a23.consul/ns/default/dc/dc1/svc/wordpress",
  "ClientCertSerial": "F3:1A:BD:06:CB:FF:7C:C2:BF:91:05:82:33:1D:30:0F:D9:C1:17:F5"
}

```


```
root@app:~# curl -s -X POST -d @/tmp/payload.json http://127.0.0.1:8500/v1/agent/connect/authorize |  jq
{
  "Authorized": true,
  "Reason": "Matched intention: ALLOW default/wordpress => default/mysql (ID: 91fa3fad-d502-efba-d1f8-5882f32110df, Precedence: 9)"
}
root@app:~#

```

That's it!!! Now you're an expert at troubleshooting Connect & Envoy!!!






