+++
toc = true
date = "2019-11-24T12:47:50Z"
title = "Vault Resources"
weight = 900
+++


https://www.vaultproject.io




### 11/24/2019

Introduction to Vault
https://www.youtube.com/watch?v=VYfl-DpZ5wM


## Tutorial
https://learn.hashicorp.com/vault


### Instruqt interactive labs
https://play.instruqt.com/hashicorp/tracks/vault-basics
See [notes on vault basics lab](./800-vault-basics-instruqt-interactive-lab.md)


https://play.instruqt.com/hashicorp/tracks/vault-dynamic-database-credentials

https://play.instruqt.com/hashicorp/tracks/vault-encryption-as-a-service

https://play.instruqt.com/hashicorp/tracks/kubernetes-secrets-with-vault



### Painless password rotation with Vault 
November 27, 2018  https://www.youtube.com/watch?v=-74bFgUzOWo&t=1006
Jan 10, 2019 https://www.hashicorp.com/resources/painless-password-rotation-hashicorp-vault
Slide deck: https://scarolan.github.io/painless-password-rotation/#1
    slide 34: `vault secrets enable -version=2 -path=systemcreds/ kv`
Source code: https://github.com/scarolan/painless-password-rotation

References: 
https://www.hashicorp.com/resources/policies-vault
https://www.vaultproject.io/docs/secrets/kv/kv-v2/

Plugin: https://github.com/sethvargo/vault-secrets-gen
Plugin documentation: https://www.vaultproject.io/docs/internals/plugins/
Generating a SHA256 checksum: https://stackoverflow.com/questions/3358420/generating-a-sha256-from-the-linux-command-line


### TODO Systemd service file for Vault
https://medium.com/hashicorp-engineering/systemd-service-file-for-vault-3e339ff86bc6
### Vault debug
https://medium.com/hashicorp-engineering/vault-debug-7fd2f2f70f38


### TODO Essential Patterns of Vault - Part 1
https://medium.com/hashicorp-engineering/essential-elements-of-vault-part-1-5a64d3de3be8

In its simplest form, the process of using Vault is:
1. Be familiar with the documentation
2. Enable and configure Secret Engines
3. Enable and configure Authentication Methods
4. Define and create policies
5. Authorize by tying policies for accessing secret engines to authentication methods.
6. Do positive and negative tests to prove your configurations allow access to the needed secrets as well as disallow access to unneeded secrets.
In a nutshell, that’s it.

### TODO Essential Patterns of Vault - Part 2
https://medium.com/hashicorp-engineering/essential-patterns-of-vault-part-2-b4d34976f1dc


### Secure Infrastructure Provisioning with Terraform Cloud, Vault and Gitlab CI
https://medium.com/hashicorp-engineering/secure-infrastructure-provisioning-with-terraform-cloud-vault-gitlab-ci-eaeabf7e31a1

