+++
toc = true
date = "2020-01-28T12:47:50Z"
title = "Vault POC notes"
weight = 100
+++

# Vault POC - using Vault with Consul as the backend storage


## Enable Consul ACL

https://learn.hashicorp.com/consul/security-networking/production-acls

The documentation is a little sparse - but the basic overall steps are:
1. Prepare Consul ACL Rules for the policy  https://www.vaultproject.io/docs/configuration/storage/consul/
2. Create the Consul ACL Policy  https://www.consul.io/docs/commands/acl/policy/create.html
3. Create the Consul Token for Vault https://www.consul.io/docs/commands/acl/token/create.html


### First step: Prepare the rules for the Consul ACL policy can save to a file rules.hcl
Reference: https://www.vaultproject.io/docs/configuration/storage/consul/
/path/to/rules.hcl
```
{
  "key_prefix": {
    "vault/": {
      "policy": "write"
    }
  },
  "node_prefix": {
    "": {
      "policy": "write"
    }
  },
  "service": {
    "vault": {
      "policy": "write"
    }
  },
  "agent_prefix": {
    "": {
      "policy": "write"
    }
  },
  "session_prefix": {
    "": {
      "policy": "write"
    }
  }
}
```


### Second step: Create the Consul ACL policy
Reference: https://www.consul.io/docs/commands/acl/policy/create.html
```
[root@dcfa2040 tmp]# consul acl policy create -name "vault" -description "Vault Token" -rules @rules.hcl
ID:           6629a018-8953-80e0-9b89-8cf3c679fc99
Name:         vault
Description:  Vault Token
Datacenters:
Rules:
{
  "key_prefix": {
    "vault/": {
      "policy": "write"
    }
  },
  "node_prefix": {
    "": {
      "policy": "write"
    }
  },
  "service": {
    "vault": {
      "policy": "write"
    }
  },
  "agent_prefix": {
    "": {
      "policy": "write"
    }
  },
  "session_prefix": {
    "": {
      "policy": "write"
    }
  }
}


```


### Third step: create the Consul ACL token for Vault
Reference: https://www.consul.io/docs/commands/acl/token/create.html
```
[root@dcfa2040 tmp]# consul acl token create -description "Vault token" -policy-id 6629a018-8953-80e0-9b89-8cf3c679fc99
AccessorID:       f624fa0b-a53f-5083-9b01-08b3cc7287d0
SecretID:         c1172dc6-444c-5d0c-5c20-f73e0fcfc260
Description:      Vault token
Local:            false
Create Time:      2020-01-29 08:49:22.689796667 -0600 CST
Policies:
   6629a018-8953-80e0-9b89-8cf3c679fc99 - vault
```


#### NOTE: you will need to update the Vault config with the token (SecretID)
/etc/vault.d/vault-no-tls.hcl
```
storage "consul" {
  address = "127.0.0.1:8500"
  path    = "vault/"
  token = "c1172dc6-444c-5d0c-5c20-f73e0fcfc260"
}
listener "tcp" {
  address     = "0.0.0.0:8200"
  tls_disable = "true"
}
ui=true
```



## Initializing Vault

```
[root@dcfa2040 ~]# vault operator init -key-shares=1 -key-threshold=1
Unseal Key 1: +pqglA3r42JxYr2KbMCc9LcDFKPuHIkzeUasHX9pWyE=

Initial Root Token: s.AUVcVzFjN8GuN5IkUGhFKuZz

Vault initialized with 1 key shares and a key threshold of 1. Please securely
distribute the key shares printed above. When the Vault is re-sealed,
restarted, or stopped, you must supply at least 1 of these keys to unseal it
before it can start servicing requests.

Vault does not store the generated master key. Without at least 1 key to
reconstruct the master key, Vault will remain permanently sealed!

It is possible to generate new unseal keys, provided you have a quorum of
existing unseal keys shares. See "vault operator rekey" for more information.
```


## Unsealing Vault
### NOTE: the systemctl startup script does not automatically unseal the vault 

```
[root@dcfa2040 ~]# export VAULT_TOKEN=s.AUVcVzFjN8GuN5IkUGhFKuZz


[root@dcfa2040 ~]# vault operator unseal
Unseal Key (will be hidden):
Key                    Value
---                    -----
Seal Type              shamir
Initialized            true
Sealed                 false
Total Shares           1
Threshold              1
Version                1.3.1
Cluster Name           vault-cluster-f2e2c524
Cluster ID             52a67b5b-209c-a847-78cb-c13b52bf2e55
HA Enabled             true
HA Cluster             n/a
HA Mode                standby
Active Node Address    <none>



[root@dcfa2040 ~]# vault status
Key             Value
---             -----
Seal Type       shamir
Initialized     true
Sealed          false
Total Shares    1
Threshold       1
Version         1.3.1
Cluster Name    vault-cluster-f2e2c524
Cluster ID      52a67b5b-209c-a847-78cb-c13b52bf2e55
HA Enabled      true
HA Cluster      https://10.79.192.82:8201
HA Mode         active


```


## Pre-requisites - setting environment Variables

```
export VAULT_ADDR='http://127.0.0.1:8200'
export VAULT_TOKEN=s.AUVcVzFjN8GuN5IkUGhFKuZz
```


## Eanble KV store in Vault 

```
[root@dcfa2040 ~]# vault secrets enable -version=2 kv
Success! Enabled the kv secrets engine at: kv/

```

### Setting a KV entry in Vault

```
[root@dcfa2040 ~]# vault kv put kv/a-secret value=1234
Key              Value
---              -----
created_time     2020-01-28T17:37:00.578954998Z
deletion_time    n/a
destroyed        false
version          1




[root@dcfa2040 ~]# vault kv get kv/a-secret
====== Metadata ======
Key              Value
---              -----
created_time     2020-01-28T17:37:00.578954998Z
deletion_time    n/a
destroyed        false
version          1

==== Data ====
Key      Value
---      -----
value    1234

```

## Enable userpass authentication 
```
vault auth enable userpass
Success! Enabled userpass auth method at: userpass/

```

### Add user
```
[root@dcfa2040 ~]# vault write auth/userpass/users/atavi password=12345
Success! Data written to: auth/userpass/users/atavi



[root@dcfa2040 ~]# vault login -method=userpass username=atavi password=12345
WARNING! The VAULT_TOKEN environment variable is set! This takes precedence
over the value set by this command. To use the value set by this command,
unset the VAULT_TOKEN environment variable or set it to the token displayed
below.

Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key                    Value
---                    -----
token                  s.LAJ0wPlGoUQQo2J9SdayUymH
token_accessor         2ncYyZJqSi3UEsawqNUHjMYb
token_duration         768h
token_renewable        true
token_policies         ["default"]
identity_policies      []
policies               ["default"]
token_meta_username    atavi

```


### Fresh login: 
```
export VAULT_TOKEN=s.AUVcVzFjN8GuN5IkUGhFKuZz

vault kv get kv/a-secret
====== Metadata ======
Key              Value
---              -----
created_time     2020-01-28T17:37:00.578954998Z
deletion_time    n/a
destroyed        false
version          1

==== Data ====
Key      Value
---      -----
value    1234

```


### User not allowed to access kv/a-secret 
```
[root@dcfa2040 ~]# export VAULT_TOKEN=s.LAJ0wPlGoUQQo2J9SdayUymH
[root@dcfa2040 ~]# vault kv get kv/a-secret
Error making API request.

URL: GET http://127.0.0.1:8200/v1/sys/internal/ui/mounts/kv/a-secret
Code: 403. Errors:

* preflight capability check returned 403, please ensure client's policies grant access to path "kv/a-secret/"

```


### Set Vault ACL policy for user atavi

#### Prepare the rules 
```
[root@dcfa2040 vault.d]# cat atavi-policy.hcl
path "kv/data/atavi/*" {
  capabilities = ["create", "update", "read", "delete"]
}
path "kv/delete/atavi/*" {
  capabilities = ["update"]
}
path "kv/metadata/atavi/*" {
  capabilities = ["list", "read", "delete"]
}
path "kv/destroy/atavi/*" {
  capabilities = ["update"]
}

# Additional access for UI
path "kv/metadata" {
  capabilities = ["list"]
}
```

#### Create the Policy
```
[root@dcfa2040 vault.d]# vault policy write atavi /etc/vault.d/atavi-policy.hcl
Success! Uploaded policy: atavi
[root@dcfa2040 vault.d]#
```

#### Apply the Policy to the user 
```
[root@dcfa2040 vault.d]# vault write auth/userpass/users/atavi/policies policies=atavi
Success! Data written to: auth/userpass/users/atavi/policies
```


#### Vault Policy list
```
[root@dcfa2040 ~]# vault policy list
atavi
default
root
```

#### Vault Policy read
```
[root@dcfa2040 ~]# vault policy read atavi
path "kv/data/atavi/*" {
  capabilities = ["create", "update", "read", "delete"]
}
path "kv/delete/atavi/*" {
  capabilities = ["update"]
}
path "kv/metadata/atavi/*" {
  capabilities = ["list", "read", "delete"]
}
path "kv/destroy/atavi/*" {
  capabilities = ["update"]
}

# Additional access for UI
path "kv/metadata" {
  capabilities = ["list"]
}
```


### Usage examples for atavi
```
vault kv put kv/atavi/weight weight=150

vault kv get kv/atavi/weight




vault kv put kv/atavi/apiKey apiKey=ABCDEFGHIJKLMNOP

vault kv get kv/atavi/age



[root@dcfa2040 vault.d]# vault kv put kv/atavi/apiKey apiKey=ABCDEFGHIJKLMNOP
Key              Value
---              -----
created_time     2020-01-28T18:17:06.170463947Z
deletion_time    n/a
destroyed        false
version          1


[root@dcfa2040 vault.d]# vault kv get kv/atavi/apiKey
====== Metadata ======
Key              Value
---              -----
created_time     2020-01-28T18:17:06.170463947Z
deletion_time    n/a
destroyed        false
version          1

===== Data =====
Key       Value
---       -----
apiKey    ABCDEFGHIJKLMNOP


```


## List Vault authentication 
```
[root@dcfa2040 ~]# vault auth list
Path         Type        Accessor                  Description
----         ----        --------                  -----------
token/       token       auth_token_04e54347       token based credentials
userpass/    userpass    auth_userpass_157ffad0    n/a



[root@dcfa2040 ~]# vault auth list -detailed
Path         Plugin      Accessor                  Default TTL    Max TTL    Token Type         Replication    Seal Wrap    External Entropy Access    Options    Description                UUID
----         ------      --------                  -----------    -------    ----------         -----------    ---------    -----------------------    -------    -----------                ----
token/       token       auth_token_04e54347       system         system     default-service    replicated     false        false                      map[]      token based credentials    575cca48-24a3-6aa4-2e76-fcba78bfd825
userpass/    userpass    auth_userpass_157ffad0    system         system     default-service    replicated     false        false                      map[]      n/a                        039d085d-49e5-ac16-6f2f-07da4f14335f
```


## From our laptops 

You can download the vault binary to your laptop: https://www.vaultproject.io/downloads/
```
jeffrey.liu@USLDNYGO9YLC2Z2 MINGW64 ~/Downloads
$ ./vault.exe version
Vault v1.3.2
```

So from our laptops, connected to the VPN, to access you need to set two Environment Variables:
```
export VAULT_ADDR='http://dcfa2040.ppprivmgmt.intraxa:3389’
export VAULT_TOKEN=s.LAJ0wPlGoUQQo2J9SdayUymH
```


Example:
```
jeffrey.liu@USLDNYGO9YLC2Z2 MINGW64 ~/Downloads
$ ./vault.exe kv get kv/atavi/apiKey
====== Metadata ======
Key              Value
---              -----
created_time     2020-01-28T18:17:06.170463947Z
deletion_time    n/a
destroyed        false
version          1

===== Data =====
Key       Value
---       -----
apiKey    ABCDEFGHIJKLMNOP



jeffrey.liu@USLDNYGO9YLC2Z2 MINGW64 ~/Downloads
$ ./vault.exe kv get kv/atavi/weight
====== Metadata ======
Key              Value
---              -----
created_time     2020-01-28T18:05:25.434701734Z
deletion_time    n/a
destroyed        false
version          1

===== Data =====
Key       Value
---       -----
weight    150
```

## TODO Vault -output-curl-string option
Reference: Solutions Engineering Hangout Vault secrets via API for the REST of Us https://www.youtube.com/watch?v=fSEoiffuYkQ
related blog post: https://medium.com/hashicorp-engineering/taming-the-wild-rest-with-fuse-8bc01fa27341
```
export VAULT_ADDR='http://127.0.0.1:8200'

export VAULT_TOKEN=s.AUVcVzFjN8GuN5IkUGhFKuZz


[root@dcfa2040 ~]# vault kv get kv/atavi/weight
====== Metadata ======
Key              Value
---              -----
created_time     2020-01-28T18:05:25.434701734Z
deletion_time    n/a
destroyed        false
version          1

===== Data =====
Key       Value
---       -----
weight    150


[root@dcfa2040 ~]# vault kv get -output-curl-string kv/atavi/weight
curl -H "X-Vault-Request: true" -H "X-Vault-Token: $(vault print token)" http://127.0.0.1:8200/v1/kv/data/atavi/weight


[root@dcfa2040 ~]# curl -H "X-Vault-Request: true" -H "X-Vault-Token: $(vault print token)" http://127.0.0.1:8200/v1/kv/data/atavi/weight | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   293  100   293    0     0   260k      0 --:--:-- --:--:-- --:--:--  286k
{
  "request_id": "63a742be-301c-a582-7b50-dcdc853d1dd1",
  "lease_id": "",
  "renewable": false,
  "lease_duration": 0,
  "data": {
    "data": {
      "weight": "150"
    },
    "metadata": {
      "created_time": "2020-01-28T18:05:25.434701734Z",
      "deletion_time": "",
      "destroyed": false,
      "version": 1
    }
  },
  "wrap_info": null,
  "warnings": null,
  "auth": null
}



[root@dcfa2040 ~]# vault print token
s.AUVcVzFjN8GuN5IkUGhFKuZz
```


### From our laptops
```
jeffrey.liu@USLDNYGO9YLC2Z2 MINGW64 ~/OneDrive - AXA/temp
$ env |grep VAULT

jeffrey.liu@USLDNYGO9YLC2Z2 MINGW64 ~/OneDrive - AXA/temp
$ export VAULT_ADDR='http://dcfa2040.ppprivmgmt.intraxa:3389'

jeffrey.liu@USLDNYGO9YLC2Z2 MINGW64 ~/OneDrive - AXA/temp
$ export VAULT_TOKEN=s.AUVcVzFjN8GuN5IkUGhFKuZz

jeffrey.liu@USLDNYGO9YLC2Z2 MINGW64 ~/OneDrive - AXA/temp
$ curl -H "X-Vault-Request: true" -H "X-Vault-Token: $(~/Downloads/vault.exe print token)" $VAULT_ADDR/v1/kv/data/atavi/weight | ~/Downloads/jq-win64.exe
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   293  100   293    0     0   2483      0 --:--:-- --:--:-- --:--:--  2525
{
  "request_id": "78b6ad02-bf2c-905f-3c8d-7b715090b4c2",
  "lease_id": "",
  "renewable": false,
  "lease_duration": 0,
  "data": {
    "data": {
      "weight": "150"
    },
    "metadata": {
      "created_time": "2020-01-28T18:05:25.434701734Z",
      "deletion_time": "",
      "destroyed": false,
      "version": 1
    }
  },
  "wrap_info": null,
  "warnings": null,
  "auth": null
}
```

### For login 
```
[root@dcfa2040 ~]# curl -H "X-Vault-Request: true" -H "X-Vault-Token: $(vault print token)" http://127.0.0.1:8200/v1/kv/data/atavi/weight | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    36  100    36    0     0  45056      0 --:--:-- --:--:-- --:--:-- 36000
{
  "errors": [
    "missing client token"
  ]
}



[root@dcfa2040 ~]# export VAULT_ADDR='http://127.0.0.1:8200'
[root@dcfa2040 ~]# vault login -output-curl-string -method=userpass username=atavi
Password (will be hidden):
curl -X PUT -H "X-Vault-Request: true" -d '{"password":"12345"}' http://127.0.0.1:8200/v1/auth/userpass/login/atavi



[root@dcfa2040 ~]# curl -X PUT -H "X-Vault-Request: true" -d '{"password":"12345"}' http://127.0.0.1:8200/v1/auth/userpass/login/atavi
{
  "request_id": "1e2d6e6d-0e26-5a4d-c0b6-766416ee07a2",
  "lease_id": "",
  "renewable": false,
  "lease_duration": 0,
  "data": null,
  "wrap_info": null,
  "warnings": null,
  "auth": {
    "client_token": "s.sQ8SXtGLWdKRoSZRGd2o3IdK",
    "accessor": "73aLt6Wv3VV28FuNaMh4FHZ3",
    "policies": [
      "atavi",
      "default"
    ],
    "token_policies": [
      "atavi",
      "default"
    ],
    "identity_policies": [
      "atavi"
    ],
    "metadata": {
      "username": "atavi"
    },
    "lease_duration": 2764800,
    "renewable": true,
    "entity_id": "2a28784a-8958-9803-a953-4117c7e08cc5",
    "token_type": "service",
    "orphan": true
  }
}




[root@dcfa2040 ~]# curl -H "X-Vault-Request: true" -H "X-Vault-Token: s.RiYjpttSvpBV3frU36LjfUy1" http://127.0.0.1:8200/v1/kv/data/atavi/weight | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   293  100   293    0     0   172k      0 --:--:-- --:--:-- --:--:--  286k
{
  "request_id": "0dfffc5e-dd33-b7dc-604c-ff4ca8a677c2",
  "lease_id": "",
  "renewable": false,
  "lease_duration": 0,
  "data": {
    "data": {
      "weight": "150"
    },
    "metadata": {
      "created_time": "2020-01-28T18:05:25.434701734Z",
      "deletion_time": "",
      "destroyed": false,
      "version": 1
    }
  },
  "wrap_info": null,
  "warnings": null,
  "auth": null
}




[root@dcfa2040 ~]# echo s.RiYjpttSvpBV3frU36LjfUy1 > ~/.vault-token
[root@dcfa2040 ~]# vault print token
s.RiYjpttSvpBV3frU36LjfUy1



[root@dcfa2040 ~]# curl -H "X-Vault-Request: true" -H "X-Vault-Token: $(vault print token)" http://127.0.0.1:8200/v1/kv/data/atavi/weight | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   293  100   293    0     0   188k      0 --:--:-- --:--:-- --:--:--  286k
{
  "request_id": "0beb6664-83dd-09ad-b347-559b30774ed9",
  "lease_id": "",
  "renewable": false,
  "lease_duration": 0,
  "data": {
    "data": {
      "weight": "150"
    },
    "metadata": {
      "created_time": "2020-01-28T18:05:25.434701734Z",
      "deletion_time": "",
      "destroyed": false,
      "version": 1
    }
  },
  "wrap_info": null,
  "warnings": null,
  "auth": null
}

rm ~/.vault-token
```







## Vault and Consul web UI

### Consul UI
http://dcfa2027.ppprivmgmt.intraxa:3389
  NOTE: local HAproxy redirect from 3389 to 8500

### Vault UI
http://dcfa2040.ppprivmgmt.intraxa:3389/
  NOTE: local HAproxy redirect from 3389 to 8200

### NOTE: Alternative to HAproxy - you can use SSH tunneling to connect to Consul Web GUI
ssh -L 80:127.0.0.1:8500 coreimage@dcfa2043.ppprivmgmt.intraxa -N
then connect via your local browser to http://localhost




## NEXT STEPS
1/30/2020
Also see [Consul TLS setup for Agents](../consul/100-consul-POC-notes.md)
### TODO Vault TLS setup
Vault PKI as a CA: https://www.vaultproject.io/docs/secrets/pki/index.html
Step-by-step tutorial on building your own CA: https://learn.hashicorp.com/vault/secrets-management/sm-pki-engine
Setting up Vault to listen via TLS: https://www.vaultproject.io/docs/configuration/listener/tcp/


#### TODO Excellent blog walkthrough setting up PKI as a Service with HashiCorp Vault (1/29/2019)
https://medium.com/hashicorp-engineering/pki-as-a-service-with-hashicorp-vault-a8d075ece9a
#### Another blog post on Certificates automation with Vault and Consul Template
https://medium.com/hashicorp-engineering/certificates-issuing-and-renewal-with-vault-and-consul-template-18e766228dac





## References

https://learn.hashicorp.com/vault#getting-started
https://learn.hashicorp.com/vault/day-one/day-one-path-intro

https://learn.hashicorp.com/vault/day-one/ops-deployment-guide
NOTE: up to a point - then the documentation fails on the ACL API call - so refer to the next URL to continue

https://www.vaultproject.io/docs/configuration/storage/consul/

https://learn.hashicorp.com/vault/day-one/ops-vault-ha-consul

https://learn.hashicorp.com/consul/security-networking/production-acls


https://www.consul.io/docs/commands/acl.html
https://www.consul.io/docs/acl/acl-system.html


https://www.hashicorp.com/resources/policies-vault


https://learn.hashicorp.com/consul/datacenter-deploy/backup
