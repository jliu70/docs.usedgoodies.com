+++
toc = true
date = "2020-01-22T12:47:50Z"
title = "Vault Basics Instruqt Interactive Lab"
weight = 800
+++


https://www.vaultproject.io




## Vault Basics

https://play.instruqt.com/hashicorp/tracks/vault-basics


Learn how to setup and run a Vault server and manage its secrets.


Vault is a tool for securely accessing secrets. A secret is anything that you want to tightly control access to, such as API keys, passwords, or certificates. Vault provides a unified interface to any secret, while providing tight access control and recording a detailed audit log.

Vault tightly controls access to secrets by authenticating against trusted sources of identity such as Active Directory, LDAP, Kubernetes, CloudFoundry, and cloud platforms. Vault enables fine grained authorization of which users and applications are permitted access to secrets.

This track will introduce you to the open source version of Vault, starting with the Vault CLI and running a Vault dev server and moving on to running a Vault production server, enabling the KV secrets engine and Userpass auth method, and using Vault policies to restrict which secrets different users can access.



Vault open source is a command line application that you can download and run from your laptop or virtual workstation.

It is written in Go and runs on macOS, Windows, Linux and other operating systems.

You can always download the latest version of vault here: https://www.vaultproject.io/downloads.html

Installing Vault on your laptop or workstation is easy. You simply download the zip file, unpack it, and place it somewhere in your PATH.

Check out this tutorial for step-by-step instructions: https://learn.hashicorp.com/vault/getting-started/install

We've launched Vault in a Docker container in your Instruqt lab environment so that you don't need to download or install it.


## The Vault CLI

The Vault Command Line Interface (CLI) allows you to interact with Vault servers.

Let's start with some basic vault commands, running them in the "Vault CLI" tab on the left.

Check the version of Vault running on your machine:
vault version
```
/ # vault version
Vault v1.2.3
```

See the list of Vault CLI commands:
vault
```
/ # vault
Usage: vault <command> [args]

Common commands:
    read        Read data and retrieves secrets
    write       Write data, configuration, and secrets
    delete      Delete secrets and configuration
    list        List data or secrets
    login       Authenticate locally
    agent       Start a Vault agent
    server      Start a Vault server
    status      Print seal and HA status
    unwrap      Unwrap a wrapped secret

Other commands:
    audit          Interact with audit devices
    auth           Interact with auth methods
    kv             Interact with Vault's Key-Value storage
    lease          Interact with leases
    namespace      Interact with namespaces
    operator       Perform operator-specific tasks
    path-help      Retrieve API help for paths
    plugin         Interact with Vault plugins and catalog
    policy         Interact with policies
    print          Prints runtime configurations
    secrets        Interact with secrets engines
    ssh            Initiate an SSH session
    token          Interact with tokens
```

Get help for the "vault secrets" command:
vault secrets -h
Note that you can also use the "-help" and "--help" flags instead of "-h".
```
/ # vault secrets -h
Usage: vault secrets <subcommand> [options] [args]

  This command groups subcommands for interacting with Vault's secrets engines.
  Each secret engine behaves differently. Please see the documentation for
  more information.

  List all enabled secrets engines:

      $ vault secrets list

  Enable a new secrets engine:

      $ vault secrets enable database

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    disable    Disable a secret engine
    enable     Enable a secrets engine
    list       List enabled secrets engines
    move       Move a secrets engine to a new path
    tune       Tune a secrets engine configuration
```

Get help for the "vault read" command:
vault read -h
Scroll back up to get a feeling for how you would read secrets from Vault.


## Your First Secret

In this challenge, you will run your first Vault server in "dev" mode and write your first secret to Vault.

See https://www.vaultproject.io/docs/concepts/dev-server.html for more on Vault's "dev" mode.

You will use the default KV v2 secrets engine that Vault automatically creates in "dev" mode. It is used to store multiple versions of static secrets.

See https://www.vaultproject.io/docs/secrets/kv/kv-v2.html for more on the KV v2 secrets engine.

Vault servers can be run in "dev" and "production" modes. You can see this by getting help for the "vault server" CLI command:
vault server -h
If you do run this, scroll back up to see information about the two modes.

Let's run a Vault dev server on the "Vault Dev Server" tab. The simpest command to do this would be "vault server -dev", but we want to set the initial root token to "root" and make the Vault server bind to all IP addresses. So, please run this instead:
```
vault server -dev -dev-listen-address=0.0.0.0:8200 -dev-root-token-id=root
```

Log into the Vault UI with the token "root".

On the "Vault CLI" tab, write a secret to the KV v2 secrets engine that the Vault dev server automatically mounted:
vault kv put secret/my-first-secret age=<age>
where <age> is your age.

Alternatively, you could create this secret in the Vault UI by selecting the "secret/" KV v2 secrets engine on the "Secrets" tab, clicking the "Create secret +" button, specifying "my-first-secret" as the path, "age" as the secret's first key, and your age as the corresponding value, and then finally clicking the "Save" button.

In the Vault UI, select the "secret/" KV v2 secrets engine, select the "my-first-secret" secret, and click the eye icon to see your age.

If you lied about your age, you can correct it in the Vault UI by clicking the "Create new version +" button, or with the Vault CLI by repeating the "vault kv put" command with your real age. Don't worry, nobody else can see it!


## The Vault API

In this challenge, you will use the Vault HTTP API to check the status of the Vault server you started in the last challenge.

You will also use the Vault HTTP API to read your age from your "my-first-secret" secret.

See https://www.vaultproject.io/api/overview for more on the Vault HTTP API.

In this challenge, you will use the Vault HTTP API.

On the "Vault CLI" tab, retrive the health of the Vault server by running this command:
```
curl http://localhost:8200/v1/sys/health | jq
```

```
/ # curl http://localhost:8200/v1/sys/health | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   294  100   294    0     0  98000      0 --:--:-- --:--:-- --:--:-- 98000
{
  "initialized": true,
  "sealed": false,
  "standby": false,
  "performance_standby": false,
  "replication_performance_mode": "disabled",
  "replication_dr_mode": "disabled",
  "server_time_utc": 1579872944,
  "version": "1.2.3",
  "cluster_name": "vault-cluster-06aac99c",
  "cluster_id": "ebcc213d-acb5-8905-15ee-5476eee7eeef"
}
/ #
```


That should bring back a nicely formatted JSON document indicating that your server is initialized and unsealed.

Now, retrieve your "my-first-secret" secret with this command (on a single line):
```
curl --header "X-Vault-Token: root" http://localhost:8200/v1/secret/data/my-first-secret | jq
```

This should bring back a JSON document showing your age and metadata about the secret including its version.

```
/ # curl --header "X-Vault-Token: root" http://localhost:8200/v1/secret/data/my-first-secret | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   289  100   289    0     0   141k      0 --:--:-- --:--:-- --:--:--  141k
{
  "request_id": "e427dad0-3a2b-90f4-8a20-6bfa2881d366",
  "lease_id": "",
  "renewable": false,
  "lease_duration": 0,
  "data": {
    "data": {
      "age": "33"
    },
    "metadata": {
      "created_time": "2020-01-24T13:34:19.172590703Z",
      "deletion_time": "",
      "destroyed": false,
      "version": 2
    }
  },
  "wrap_info": null,
  "warnings": null,
  "auth": null
}
/ #
```


## Run a Production Server

In this challenge, you will run a Vault server in "production" mode, initialize it, and unseal it. The server will get its configuration from a file that you will edit before starting it.

See https://www.vaultproject.io/docs/configuration/index.html for information on configuring a Vault server.

See https://www.vaultproject.io/docs/commands/operator/init.html for information on initializing a Vault server.

See https://www.vaultproject.io/docs/concepts/seal.html for information on the sealing and unsealing of Vault servers.

This lab will guide you through initializing and unsealing your Vault server with the Vault CLI. But, it is also possible to do both of these in the Vault UI.

Let's run a production Vault sever.

A production Vault server gets its configuration from a configuration file. Open the vault-config.hcl configuration file on the "Vault Configuration" tab to see what your server will use.

On the "Vault Server" tab, run a Vault server that uses the configuration file:
```
vault server -config=/vault/config/vault-config.hcl
```

```
/vault/config/vault-config.hcl

listener "tcp" {
 address = "0.0.0.0:8200"
 tls_disable = 1
}
storage "file" {
  path = "/vault/file"
}
disable_mlock = true
api_addr = "http://localhost:8200"
ui=true

```

Since that tab is now running the Vault server, you'll run the rest of the CLI commands on the "Vault CLI" tab.

Initialize the new server, indicating that you want to use one unseal key:
```
vault operator init -key-shares=1 -key-threshold=1
```
This gives you back an unseal key and an initial root token. Please save these for further use within this track.
```
/ # vault operator init -key-shares=1 -key-threshold=1
Unseal Key 1: BCAADCSfAFRpL3HrikWFf5zqCYsnHXFoFHWcVgNdKLM=

Initial Root Token: s.OLQgC1SNNOQu6t8lhgUBA0e8

Vault initialized with 1 key shares and a key threshold of 1. Please securely
distribute the key shares printed above. When the Vault is re-sealed,
restarted, or stopped, you must supply at least 1 of these keys to unseal it
before it can start servicing requests.

Vault does not store the generated master key. Without at least 1 key to
reconstruct the master key, Vault will remain permanently sealed!

It is possible to generate new unseal keys, provided you have a quorum of
existing unseal keys shares. See "vault operator rekey" for more information.
/ #
```


In order to use most Vault commands, you need to set the "VAULT_TOKEN" environment variable, using the initial root token that the "init" command returned:
```
export VAULT_TOKEN=<root_token>
 
 # export VAULT_TOKEN=s.OLQgC1SNNOQu6t8lhgUBA0e8
```

You next need to unseal your Vault server, providing the unseal key that the "init" command returned:
vault operator unseal
This will return the status of the server which should show that "Initialized" is "true" and that "Sealed` is "false".
```
/ # vault operator unseal
Unseal Key (will be hidden):
Key             Value
---             -----
Seal Type       shamir
Initialized     true
Sealed          false
Total Shares    1
Threshold       1
Version         1.2.3
Cluster Name    vault-cluster-b20af728
Cluster ID      6374dd2f-4350-c6c3-937c-9f501f0a3dfd
HA Enabled      false
/ #
```

To check the status of your Vault server at any time, you can run the "vault status" command. If it shows that "Sealed" is "true", re-run the "vault operator unseal" command.

```
/ # vault status
Key             Value
---             -----
Seal Type       shamir
Initialized     true
Sealed          false
Total Shares    1
Threshold       1
Version         1.2.3
Cluster Name    vault-cluster-b20af728
Cluster ID      6374dd2f-4350-c6c3-937c-9f501f0a3dfd
HA Enabled      false
/ #
```


Finally, log into the Vault UI with your root token. If you have problems, double-check that you ran all of the above commands.

**Don't forget to save your root token and unseal key!**



## Use the KV V2 Secrets Engine

In this challenge, you will mount an instance of Vault's KV v2 secrets engine on the Vault production server you started in the previous challenge.

You will also write a secret, change its value in the Vault UI, and then see that you can still inspect its original value since the KV v2 secrets engine maintains multiple versions of secrets.

See https://www.vaultproject.io/docs/secrets/index.html to learn about Vault secrets engines.

See https://www.vaultproject.io/docs/secrets/kv/index.html to learn about Vault's KV secrets engine. You will use version 2 of this engine which can retain multiple versions of any secret.

See https://www.vaultproject.io/docs/commands/secrets/enable.html for information about the vault secrets enable command.


Now that you have a Vault production server running (in the background), you can mount a secrets engine and write secrets to it.

First, you'll need to export your VAULT_TOKEN environment variable again, using the initial root token that the "init" command returned in the previous challenge:
```
export VAULT_TOKEN=<root_token>
export VAULT_TOKEN=s.OLQgC1SNNOQu6t8lhgUBA0e8
```

Alternatively, you can use your <up_arrow> key to scroll back through your command history until you see your previous export of VAULT_TOKEN.

Now, you can mount an instance of the KV v2 secrets engine on the default path, "kv":
```
vault secrets enable -version=2 kv
Success! Enabled the kv secrets engine at: kv/
```

Next, write a secret to your new secrets engine:
```

vault kv put kv/a-secret value=1234
Key              Value
---              -----
created_time     2020-01-24T13:44:21.311873997Z
deletion_time    n/a
destroyed        false
version          1
/ #
```

Feel free to specify your own secret number instead of 1234.

Login to the Vault UI with your root token and verify that the secret "a-secret" under the "kv" secrets engine has the value you provided by clicking its eye icon.

Change the value in the UI by clicking the "Create new version +" button, typing a new value in the field with the dots, and clicking the "Save" button. Click the eye icon for the secret to validate the change.

Still in the Vault UI, select the History menu for the secret, select "Version 1", and click the eye icon again to see the original value.


## Use the Userpass Auth Method

Now that you have a running production Vault server with a KV v2 secrets engine mounted, it's time to learn how to authenticate users.

In this challenge, you'll mount and use the Userpass auth method which allows users to authenticate to Vault with usernames and passwords managed by Vault.

You'll also start to learn about Vault policies that restrict which secrets different users and applications can read and write. Note that Vault policies are "deny by default"; this means a token can only read or write secrets if explicitly given permission to do so by one of the policies attached to it.

See https://www.vaultproject.io/docs/auth/index.html to learn about Vault auth methods.

See https://www.vaultproject.io/docs/auth/userpass.html to learn about Vault's Userpass auth method.



Now that you have a running production Vault server with a KV v2 secrets engine mounted, it's time to learn how to authenticate users.

First, you'll need to export your VAULT_TOKEN environment variable again:
```
export VAULT_TOKEN=<root_token>
/ # export VAULT_TOKEN=s.OLQgC1SNNOQu6t8lhgUBA0e8
```

Alternatively, you can use your <up_arrow> key to scroll back through your command history until you see your previous export of VAULT_TOKEN.

Now, enable the Userpass auth method:
```
vault auth enable userpass
Success! Enabled userpass auth method at: userpass/
```

Next, add yourself as a Vault user without any policies:
```
vault write auth/userpass/users/<name> password=<pwd>

/ # vault write auth/userpass/users/jliu password=12345
Success! Data written to: auth/userpass/users/jliu
```

Be sure to specify an actual username for <name> and a password for <pwd> without the angle brackets.

Now, you can sign into the Vault UI by selecting the Username method and providing your username and password.

You can also login with the Vault CLI:
```
vault login -method=userpass username=<name> password=<pwd>

/ # vault login -method=userpass username=jliu password=12345
WARNING! The VAULT_TOKEN environment variable is set! This takes precedence
over the value set by this command. To use the value set by this command,
unset the VAULT_TOKEN environment variable or set it to the token displayed
below.

Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key                    Value
---                    -----
token                  s.vFOjjZaHr37obmobg8S5f8eT
token_accessor         l1mRTHHkxPLq8aE0zMitCjLj
token_duration         768h
token_renewable        true
token_policies         ["default"]
identity_policies      []
policies               ["default"]
token_meta_username    jliu
/ #
```

Both of these login methods give you a Vault token with Vault's default policy that grants some very limited capabilities. A yellow warning message tells us that we currently have the VAULT_TOKEN environment variable set and that we should either unset it or set it to the new token. Let's unset it:
```
unset VAULT_TOKEN
```

To confirm that your new token is being used, run this command:
```
vault token lookup

/ # vault token lookup
Key                 Value
---                 -----
accessor            l1mRTHHkxPLq8aE0zMitCjLj
creation_time       1579873721
creation_ttl        768h
display_name        userpass-jliu
entity_id           278e7d21-1db4-b0ad-7986-a6e87954941f
expire_time         2020-02-25T13:48:41.464481595Z
explicit_max_ttl    0s
id                  s.vFOjjZaHr37obmobg8S5f8eT
issue_time          2020-01-24T13:48:41.464481131Z
meta                map[username:jliu]
num_uses            0
orphan              true
path                auth/userpass/login/jliu
policies            [default]
renewable           true
ttl                 767h59m2s
type                service
/ #
```

You will see that the display_name of the current token is "userpass-<name>" where <name> is your username and that the only policy listed for the token is the "default" policy.

Try to read the secret you wrote to the KV v2 secrets engine in the last challenge:
```
vault kv get kv/a-secret

/ # vault kv get kv/a-secret
Error making API request.

URL: GET http://localhost:8200/v1/sys/internal/ui/mounts/kv/a-secret
Code: 403. Errors:

* preflight capability check returned 403, please ensure client's policies grant access to path "kv/a-secret/"
/ #
```

You will get an error message because your token is not authorized to read any secrets yet. That is because Vault policies are "deny by default", meaning that a token can only read or write a secret if it is explicitly given permission to do so by one of its policies.

In the next challenge, you'll add a policy to your username that will allow you to read and write some secrets.


## Use Vault Policies

In this challenge, you'll define Vault policies to give two different users access to different secrets within Vault.

See https://www.vaultproject.io/docs/concepts/policies.html to learn about Vault policies.


Now that you have the Userpass authentication method configured, you can add policies to give different users access to different secrets.

First, you'll need to export your VAULT_TOKEN environment variable again either by typing the command or by scrolling back through your history:
```
export VAULT_TOKEN=<root_token>

/ # export VAULT_TOKEN=s.OLQgC1SNNOQu6t8lhgUBA0e8
```

You already created a username for yourself to use with the Userpass auth method. Now, create a second user with the same command you used before, selecting a different username and password:
```
vault write auth/userpass/users/<name> password=<pwd>

/ # vault write auth/userpass/users/jliu2 password=12345
Success! Data written to: auth/userpass/users/jliu2
```

Next, edit the two policies on the "Vault Policies" tab, user-1-policy.hcl and user-2-policy.hcl. 

```
/vault/policies/user-1-policy.hcl
path "kv/data/<user>/*" {
  capabilities = ["create", "update", "read", "delete"]
}
path "kv/delete/<user>/*" {
  capabilities = ["update"]
}
path "kv/metadata/<user>/*" {
  capabilities = ["list", "read", "delete"]
}
path "kv/destroy/<user>/*" {
  capabilities = ["update"]
}

# Additional access for UI
path "kv/metadata" {
  capabilities = ["list"]
}
```

```
/vault/policies/user-2-policy.hcl
path "kv/data/<user>/*" {
  capabilities = ["create", "update", "read", "delete"]
}
path "kv/delete/<user>/*" {
  capabilities = ["update"]
}
path "kv/metadata/<user>/*" {
  capabilities = ["list", "read", "delete"]
}
path "kv/destroy/<user>/*" {
  capabilities = ["update"]
}

# Additional access for UI
path "kv/metadata" {
  capabilities = ["list"]
}
```



In user-1-policy.hcl, set all occurences of <user> to your own username. In user-2-policy.hcl, set all occurences of <user> to the username of the user you just added.

Save both files.



```
/vault/policies/user-1-policy.hcl
path "kv/data/jliu/*" {
  capabilities = ["create", "update", "read", "delete"]
}
path "kv/delete/jliu/*" {
  capabilities = ["update"]
}
path "kv/metadata/jliu/*" {
  capabilities = ["list", "read", "delete"]
}
path "kv/destroy/jliu/*" {
  capabilities = ["update"]
}

# Additional access for UI
path "kv/metadata" {
  capabilities = ["list"]
}
```

```
/vault/policies/user-2-policy.hcl
path "kv/data/jliu2/*" {
  capabilities = ["create", "update", "read", "delete"]
}
path "kv/delete/jliu2/*" {
  capabilities = ["update"]
}
path "kv/metadata/jliu2/*" {
  capabilities = ["list", "read", "delete"]
}
path "kv/destroy/jliu2/*" {
  capabilities = ["update"]
}

# Additional access for UI
path "kv/metadata" {
  capabilities = ["list"]
}
```


Next, you are going to add the policies to the Vault server:
```
vault policy write <user_1> /vault/policies/user-1-policy.hcl
vault policy write <user_2> /vault/policies/user-2-policy.hcl

/ # vault policy write jliu /vault/policies/user-1-policy.hcl
Success! Uploaded policy: jliu
/ # vault policy write jliu2 /vault/policies/user-2-policy.hcl
Success! Uploaded policy: jliu2

```
replacing <user_1> and <user_2> with the usernames you are using.



Now, you can assign the new policies to the users by updating the policies assigned to the users:
```
vault write auth/userpass/users/<user_1>/policies policies=<user_1>
vault write auth/userpass/users/<user_2>/policies policies=<user_2>
```
again replacing <user_1> and <user_2> with the usernames you are using.

```
/ # vault write auth/userpass/users/jliu/policies policies=jliu
Success! Data written to: auth/userpass/users/jliu/policies
/ # vault write auth/userpass/users/jliu2/policies policies=jliu2
Success! Data written to: auth/userpass/users/jliu2/policies
/ #
```


Now, let's see what happens when you log into the Vault UI as the two different users.

Log in as yourself (the first user you created). Remember to specify the login method as "Userpass".

Click on the kv secrets engine. You should see the secret you previously created called "a-secret". But if you select it, you'll see a "Not Authorized" message.

Click on the "kv" breadcrumb to go back to the previous screen.

Click the "Create secret +" button, enter <user>/age for the path, "age" for the key in the "Version data" section of the screen, and your age for the value associated with that key. Be sure to replace <user> with the username of the logged in user. Then click the "Save" button. You should be allowed to do this.

Logout and log back in as the second user. Try to access the first user's secret. You should not be able to.

Now, while still logged in as the second user, repeat the above steps to create a secret, specifying the second user's name for <user> in the path. You should be allowed to do this.

If you would like to see what happens when using the Vault CLI, you can run unset VAULT_TOKEN, login as each user with vault login -method=userpass username=<user> password=<pwd>, and then try commands like the following:
```
vault kv get kv/<user>/age
vault kv put kv/<user>/weight weight=150
```

These will succeed when <user> matches the username of the logged in user and fail when it does not.

```
/ # unset VAULT_TOKEN
/ # vault login -method=userpass username=jliu password=12345
Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key                    Value
---                    -----
token                  s.nhVcZcVyCVGaypQrRogyVQIb
token_accessor         aTFQq6jV5JVtEIN4U0RfpQAe
token_duration         768h
token_renewable        true
token_policies         ["default" "jliu"]
identity_policies      []
policies               ["default" "jliu"]
token_meta_username    jliu
/ # vault kv get kv/jliu/age
====== Metadata ======
Key              Value
---              -----
created_time     2020-01-24T14:05:18.267317484Z
deletion_time    n/a
destroyed        false
version          1

=== Data ===
Key    Value
---    -----
age    49


/ # vault kv put kv/jliu/weight weight=150
Key              Value
---              -----
created_time     2020-01-24T14:08:44.31488132Z
deletion_time    n/a
destroyed        false
version          1
/ #


/ # vault kv get kv/jliu2/age
Error reading kv/data/jliu2/age: Error making API request.

URL: GET http://localhost:8200/v1/kv/data/jliu2/age
Code: 403. Errors:

* 1 error occurred:
        * permission denied


/ #
```

The bottom line for this challenge is that Vault protects each user's secrets from other users.

Congratulations on finishing the entire track!
