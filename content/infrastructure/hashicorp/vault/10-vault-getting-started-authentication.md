+++
weight = 10
toc = true
date = "2019-11-24T05:03:55Z"
title = "Vault Getting Started Authentication"
+++

# Vault Authentication

https://learn.hashicorp.com/vault/getting-started/authentication


## Tokens

Token authentication is enabled by default in Vault and cannot be disabled. When you start a dev server with vault server -dev, it prints your root token. The root token is the initial access token to configure Vault. It has root privileges, so it can perform any operation within Vault.

You can create more tokens:
```
vault token create

[coreimage@dcfa2040 ~]$ vault token create
Key                  Value
---                  -----
token                s.i3CUXmb7D3Ynt2Ijt9XkczoU
token_accessor       KJ7kYa3R48lPDr5FpK9ItyWb
token_duration       ∞
token_renewable      false
token_policies       ["root"]
identity_policies    []
policies             ["root"]

```

By default, this will create a child token of your current token that inherits all the same policies. The "child" concept here is important: tokens always have a parent, and when that parent token is revoked, children can also be revoked all in one operation. This makes it easy when removing access for a user, to remove access for all sub-tokens that user created as well.

To authenticate with a token:

```
[coreimage@dcfa2040 ~]$ vault login s.i3CUXmb7D3Ynt2Ijt9XkczoU
WARNING! The VAULT_TOKEN environment variable is set! This takes precedence
over the value set by this command. To use the value set by this command,
unset the VAULT_TOKEN environment variable or set it to the token displayed
below.

Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key                  Value
---                  -----
token                s.i3CUXmb7D3Ynt2Ijt9XkczoU
token_accessor       KJ7kYa3R48lPDr5FpK9ItyWb
token_duration       ∞
token_renewable      false
token_policies       ["root"]
identity_policies    []
policies             ["root"]
```


This authenticates with Vault. It will verify your token and let you know what access policies the token is associated with.

After a token is created, you can revoke it:

```
[coreimage@dcfa2040 ~]$ vault token revoke s.i3CUXmb7D3Ynt2Ijt9XkczoU
Success! Revoked token (if it existed)
```

In a previous section, we used the vault lease revoke command. This command is only used for revoking leases. For revoking tokens, use vault token revoke.

Log back in with root token:
```
[coreimage@dcfa2040 ~]$ vault login $VAULT_TOKEN
WARNING! The VAULT_TOKEN environment variable is set! This takes precedence
over the value set by this command. To use the value set by this command,
unset the VAULT_TOKEN environment variable or set it to the token displayed
below.

Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

Key                  Value
---                  -----
token                s.t5tD0aA02rSb0YhZypem7jkp
token_accessor       x1opMmm8CbIfm3iVHx4HB9Iw
token_duration       ∞
token_renewable      false
token_policies       ["root"]
identity_policies    []
policies             ["root"]
```

### Recommended Patterns

In practice, operators should not use the token create command to generate Vault tokens for users or machines. Instead, those users or machines should authenticate to Vault using any of Vault's configured auth methods such as GitHub, LDAP, AppRole, etc. For legacy applications which cannot generate their own token, operators may need to create a token in advance. Auth methods are discussed in more detail in the next section.



## Auth Methods

Vault supports many auth methods, but they must be enabled before use. Auth methods give you flexibility. Enabling and configuring auth methods are typically performed by a Vault operator or security team. 

```
vault auth list
```

Similarly, you can ask for help information about any CLI auth method, even if it is not enabled:

```
[coreimage@dcfa2040 ~]$ vault auth help userpass
Usage: vault login -method=userpass [CONFIG K=V...]

  The userpass auth method allows users to authenticate using Vault's
  internal user database.

  If MFA is enabled, a "method" and/or "passcode" may be required depending on
  the MFA method. To check which MFA is in use, run:

      $ vault read auth/<mount>/mfa_config

  Authenticate as "sally":

      $ vault login -method=userpass username=sally
      Password (will be hidden):

  Authenticate as "bob":

      $ vault login -method=userpass username=bob password=password

Configuration:

  method=<string>
      MFA method.

  passcode=<string>
      MFA OTP/passcode.

  password=<string>
      Password to use for authentication. If not provided, the CLI will prompt
      for this on stdin.

  username=<string>
      Username to use for authentication.
```


### Enable internal userpass auth
```

[coreimage@dcfa2040 ~]$ vault auth enable userpass
Success! Enabled userpass auth method at: userpass/


[coreimage@dcfa2040 ~]$ vault auth list
Path         Type        Accessor                  Description
----         ----        --------                  -----------
token/       token       auth_token_ee8a11d7       token based credentials
userpass/    userpass    auth_userpass_c27c67a0    n/a

```



# Policies
https://learn.hashicorp.com/vault/getting-started/policies

See more details of implementation at [Vault POC](./100-vault-POC-notes.md)



# Deploy
https://learn.hashicorp.com/vault/getting-started/deploy

## Configuring Vault

Vault is configured using HCL files.  The configuration file for Vault is relatively simple:
```
storage "consul" {
  address = "127.0.0.1:8500"
  path    = "vault/"
}

listener "tcp" {
 address     = "127.0.0.1:8200"
 tls_disable = 1
}
```

Do not worry about getting the exact policy format correct. Vault includes a command that will format the policy automatically according to specification. It also reports on any syntax errors.
```
vault policy fmt my-policy.hcl
```

## Starting the Server

With the configuration in place, starting the server is simple, as shown below. Modify the -config flag to point to the proper path where you saved the configuration above.

```
vault server -config=config.hcl
```


## Initializing the Vault

Initialization is the process configuring the Vault. This only happens once when the server is started against a new backend that has never been used with Vault before. When running in HA mode, this happens once per cluster, not per server.

During initialization, the encryption keys are generated, unseal keys are created, and the initial root token is setup. To initialize Vault use vault operator init. This is an unauthenticated request, but it only works on brand new Vaults with no data:

```
vault operator init
```

Initialization outputs two incredibly important pieces of information: the unseal keys and the initial root token. This is the only time ever that all of this data is known by Vault, and also the only time that the unseal keys should ever be so close together.

For the purpose of this getting started guide, save all of these keys somewhere, and continue. In a real deployment scenario, you would never save these keys together. Instead, you would likely use Vault's PGP and Keybase.io support to encrypt each of these keys with the users' PGP keys. This prevents one single person from having all the unseal keys. Please see the documentation on using PGP, GPG, and Keybase for more information.



## Seal/Unseal

Every initialized Vault server starts in the sealed state. From the configuration, Vault can access the physical storage, but it can't read any of it because it doesn't know how to decrypt it. The process of teaching Vault how to decrypt the data is known as unsealing the Vault.

Unsealing has to happen every time Vault starts. It can be done via the API and via the command line. To unseal the Vault, you must have the threshold number of unseal keys. In the output above, notice that the "key threshold" is 3. This means that to unseal the Vault, you need 3 of the 5 keys that were generated.

> Note: Vault does not store any of the unseal key shards. Vault uses an algorithm known as Shamir's Secret Sharing to split the master key into shards. Only with the threshold number of keys can it be reconstructed and your data finally accessed.


Begin unsealing the Vault:

```
vault operator unseal
```

After pasting in a valid key and confirming, you'll see that the Vault is still sealed, but progress is made. Vault knows it has 1 key out of 3. Due to the nature of the algorithm, Vault doesn't know if it has the correct key until the threshold is reached.

Also notice that the unseal process is stateful. You can go to another computer, use vault operator unseal, and as long as it's pointing to the same server, that other computer can continue the unseal process. This is incredibly important to the design of the unseal process: multiple people with multiple keys are required to unseal the Vault. The Vault can be unsealed from multiple computers and the keys should never be together. A single malicious operator does not have enough keys to be malicious.

Continue with vault operator unseal to complete unsealing the Vault. To unseal the vault you must use three different keys, the same key repeated will not work. As you use keys, as long as they are correct, you should soon see output like this:

```
$ vault operator unseal

Unseal Key (will be hidden):
# ...

$ vault operator unseal

Unseal Key (will be hidden):
# ...
```


When the value for Sealed changes to false, the Vault is unsealed:

```
Key                    Value
---                    -----
Seal Type              shamir
Initialized            true
Sealed                 false  <--
Total Shares           5
Threshold              3
Version                1.0.2
Cluster Name           vault-cluster-d0db8b4f
Cluster ID             fcb8c4c9-b1d7-e83a-3876-a8abf13430cf
HA Enabled             true
HA Cluster             n/a
HA Mode                standby
Active Node Address    <none>

```


Feel free to play around with entering invalid keys, keys in different orders, etc. in order to understand the unseal process.

Finally, authenticate as the initial root token (it was included in the output with the unseal keys):
```
vault login s.xxxxxxx

```

As a root user, you can reseal the Vault with vault operator seal. A single operator is allowed to do this. This lets a single operator lock down the Vault in an emergency without consulting other operators.

When the Vault is sealed again, it clears all of its state (including the encryption key) from memory. The Vault is secure and locked down from access.


### Web UI
#### Non-Dev Servers

The Vault UI is not activated by default. To activate the UI, set the ui configuration option in the Vault server configuration.

#### Web UI Wizard
Vault UI has a built-in guide to navigate you through the common steps to operate various Vault features.


# Using HTTP APIs with Authentication

All of Vault's capabilities are accessible via the HTTP API in addition to the CLI. In fact, most calls from the CLI actually invoke the HTTP API. In some cases, Vault features are not available via the CLI and can only be accessed via the HTTP API.

Once you have started the Vault server, you can use curl or any other http client to make API calls. For example, if you started the Vault server in dev mode, you could validate the initialization status like this:
```
$ curl http://127.0.0.1:8200/v1/sys/init
```

This will return a JSON response:

```
{
  "initialized": true
}
```

## Accessing Secrets via the REST APis

Machines that need access to information stored in Vault will most likely access Vault via its REST API. For example, if a machine were using AppRole for authentication, the application would first authenticate to Vault which would return a Vault API token. The application would use that token for future communication with Vault.

For the purpose of this guide, we will use the following configuration which disables TLS and uses a file-based backend. TLS is disabled here only for example purposes; it should never be disabled in production.

```
backend "file" {
  path = "vault"
}

listener "tcp" {
  tls_disable = 1
}
```

Save this file on disk as `config.hcl` and then start the Vault server
```
$ vault server -config=config.hcl
```

At this point, we can use Vault's API for all our interactions. For example, we can initialize Vault like this:
```
$ curl \
    --request POST \
    --data '{"secret_shares": 1, "secret_threshold": 1}' \
    http://127.0.0.1:8200/v1/sys/init | jq
```

The response should be JSON and looks something like this:
```
{
  "keys": [
    "ff27b63de46b77faabba1f4fa6ef44c948e4d6f2ea21f960d6aab0eb0f4e1391"
  ],
  "keys_base64": [
    "/ye2PeRrd/qruh9Ppu9EyUjk1vLqIflg1qqw6w9OE5E="
  ],
  "root_token": "s.Ga5jyNq6kNfRMVQk2LY1j9iu"
}
```

This response contains our initial root token. It also includes the unseal key. You can use the unseal key to unseal the Vault and use the root token perform other requests in Vault that require authentication.

To make this guide easy to copy-and-paste, we will be using the environment variable $VAULT_TOKEN to store the root token. You can export this Vault token in your current shell like this:
```
$ export VAULT_TOKEN="s.Ga5jyNq6kNfRMVQk2LY1j9iu"
```

Using the unseal key (not the root token) from above, you can unseal the Vault via the HTTP API:
```
$ curl \
    --request POST \
    --data '{"key": "/ye2PeRrd/qruh9Ppu9EyUjk1vLqIflg1qqw6w9OE5E="}' \
    http://127.0.0.1:8200/v1/sys/unseal | jq
```

Note that you should replace /ye2PeRrd/qru... with the generated key from your output. This will return a JSON response:
```
{
  "type": "shamir",
  "initialized": true,
  "sealed": false,
  "t": 1,
  "n": 1,
  "progress": 0,
  "nonce": "",
  "version": "1.1.2",
  "migration": false,
  "cluster_name": "vault-cluster-e5c75412",
  "cluster_id": "7060bafe-dd35-be24-1cb9-5a351e912df1",
  "recovery_seal": false
}
```

Now any of the available auth methods can be enabled and configured. For the purposes of this guide lets enable AppRole authentication.

Start by enabling the AppRole authentication.
```
$ curl \
    --header "X-Vault-Token: $VAULT_TOKEN" \
    --request POST \
    --data '{"type": "approle"}' \
    http://127.0.0.1:8200/v1/sys/auth/approle
```

Continue onward at the official site: https://learn.hashicorp.com/vault/getting-started/apis










# Day 1 - Operations and Production Deployment
https://learn.hashicorp.com/vault?track=day-one#day-one

    https://learn.hashicorp.com/vault/operations/ops-reference-architecture
    https://learn.hashicorp.com/vault/day-one/ops-deployment-guide
        Download Vault
        Install Vault
            sudo setcap cap_ipc_lock=+ep /usr/local/bin/vault
            sudo useradd --system --home /etc/vault.d --shell /bin/false vault
        Configure systemd
            sudo touch /etc/systemd/system/vault.service
```
[Unit]
Description="HashiCorp Vault - A tool for managing secrets"
Documentation=https://www.vaultproject.io/docs/
Requires=network-online.target
After=network-online.target
ConditionFileNotEmpty=/etc/vault.d/vault.hcl
StartLimitIntervalSec=60
StartLimitBurst=3

[Service]
User=vault
Group=vault
ProtectSystem=full
ProtectHome=read-only
PrivateTmp=yes
PrivateDevices=yes
SecureBits=keep-caps
AmbientCapabilities=CAP_IPC_LOCK
Capabilities=CAP_IPC_LOCK+ep
CapabilityBoundingSet=CAP_SYSLOG CAP_IPC_LOCK
NoNewPrivileges=yes
ExecStart=/usr/local/bin/vault server -config=/etc/vault.d/vault.hcl
ExecReload=/bin/kill --signal HUP $MAINPID
KillMode=process
KillSignal=SIGINT
Restart=on-failure
RestartSec=5
TimeoutStopSec=30
StartLimitInterval=60
StartLimitIntervalSec=60
StartLimitBurst=3
LimitNOFILE=65536
LimitMEMLOCK=infinity

[Install]
WantedBy=multi-user.target
```
        Configure Consul
            export CONSUL_ADDR="http://127.0.0.1:8500"
            Update consul configuration file (/etc/consul.d/consul.hcl) on all Consul Servers to allow ACLs
```
acl {
  enabled = true
  default_policy = "deny"
  down_policy = "extend-cache"
}
```
systemctl restart consul

        https://www.consul.io/docs/commands/acl.html
        https://learn.hashicorp.com/consul/security-networking/production-acls#bootstrapping-acls

```
[root@dcfa2027 ~]# consul acl bootstrap
AccessorID:       d8f09536-28fa-be5e-9d8f-c0752041e971
SecretID:         46d022e7-bb43-d5f2-ed6a-570cae24e81a
Description:      Bootstrap Token (Global Management)
Local:            false
Create Time:      2020-01-22 09:49:11.946579126 -0600 CST
Policies:
   00000000-0000-0000-0000-000000000001 - global-management
```
This will return a Consul management token that should be saved as this will be needed in all subsequent Consul API calls.

```
export CONSUL_TOKEN="46d022e7-bb43-d5f2-ed6a-570cae24e81a"

```

Generate the Consul ACL agent token
```
curl  --request PUT  --header "X-Consul-Token: ${CONSUL_TOKEN}" \
      --data '{"Name": "Agent Token", \
         "Type": "client", \
         "Rules": \
            "node \"\" { policy = \"write\" \} \
            service \"\" { policy = \"read\" }"
        }' \
        ${CONSUL_ADDR}/v1/acl/create
```


This will return a Consul agent token.
```
[root@dcfa2040 ~]# env |grep CONSUL
CONSUL_HTTP_TOKEN=46d022e7-bb43-d5f2-ed6a-570cae24e81a
CONSUL_TOKEN=46d022e7-bb43-d5f2-ed6a-570cae24e81a
CONSUL_ADDR=http://127.0.0.1:8500


[root@dcfa2040 ~]# curl  --request PUT  --header "X-Consul-Token: ${CONSUL_TOKEN}"       --data '{"Name": "Agent Token", \
         "Type": "client", \
         "Rules": \
            "node \"\" { policy = \"write\" \} \
            service \"\" { policy = \"read\" }"
        }'         ${CONSUL_ADDR}/v1/acl/create
Request decode failed: invalid character '\\' looking for beginning of object key string[root@dcfa2040 ~]#
[root@dcfa2040 ~]#



[root@dcfa2040 ~]# curl  --request PUT  --header "X-Consul-Token: ${CONSUL_TOKEN}" --data '{"Name": "Agent Token", "Type": "client", "Rules": "node "" { policy = "write" } service "" { policy = "read" }" }' ${CONSUL_ADDR}/v1/acl/create
Request decode failed: invalid character '"' after object key:value pair


```



## 1/24/2020
This doc may have the work round to the API call error by setting the config for Consul in the /etc/consul.d/consul.hcl config file:
https://www.vaultproject.io/docs/configuration/storage/consul/

https://blog.8bitzen.com/posts/05-11-2018-vault-error-server-gave-http-response-to-https-client/
export VAULT_ADDR='http://127.0.0.1:8200'

https://learn.hashicorp.com/vault/operations/ops-vault-ha-consul

https://www.opcito.com/blogs/how-to-set-up-high-availability-vault-with-consul-backend/

https://gist.github.com/scarolan/2bce7e32ef708476ac0f954ce996630b




See more at [Vault POC](./100-vault-POC-notes.md)


Update the Consul configuration file (/etc/consul.d/consul.hcl) on all Consul servers and agents by adding these lines in the acl stanza
```
tokens {
  Agent = "[TOKEN from step 6]"
}
```


https://learn.hashicorp.com/vault/day-one/production-hardening

