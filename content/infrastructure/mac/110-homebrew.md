+++
date = "2017-03-17T05:04:01Z"
title = "110 homebrew"
weight = 110
toc = true
+++

## 2019
So as of 2019 I'm no longer using home brew.  There are specific arguments and reasons for this.  





Homebrew calls itself `The missing package manager for Mac OS`

https://brew.sh/

https://brew.sh/blog/

## Installation

### Pre-requisite 
Important to install `Command Line Tools` from `Xcode`

#### Is Xcode Already Installed?
```
$ xcode-select -p
/Library/Developer/CommandLineTools
```

To install from the CLI
```
$ xcode-select --install

```

Verify
```
$ xcode-select -p
/Library/Developer/CommandLineTools

$ gcc --version
Configured with: --prefix=/Library/Developer/CommandLineTools/usr --with-gxx-include-dir=/usr/include/c++/4.2.1
Apple LLVM version 9.0.0 (clang-900.0.39.2)
Target: x86_64-apple-darwin17.3.0
Thread model: posix
InstalledDir: /Library/Developer/CommandLineTools/usr/bin
```

### Install Homebrew
Visit website: 
https://brew.sh/

Follow the directions on the home page.

## Homebrew Docs

https://docs.brew.sh


http://stackoverflow.com/questions/15860297/how-can-i-get-more-info-about-a-brew-formula-before-installing


http://ricostacruz.com/cheatsheets/homebrew.html


```
$ brew help
Example usage:
  brew search [TEXT|/REGEX/]
  brew (info|home|options) [FORMULA...]
  brew install FORMULA...
  brew update
  brew upgrade [FORMULA...]
  brew uninstall FORMULA...
  brew list [FORMULA...]

Troubleshooting:
  brew config
  brew doctor
  brew install -vd FORMULA
```


http://docs.brew.sh/
http://docs.brew.sh/FAQ.html

```
List all installed formulae.

jliu@JEFFREYs-MBP ~ $ brew ls
exiftool	jq		libevent	oniguruma	openssl		readline	s3cmd		socat		tmux
jliu@JEFFREYs-MBP ~ $ brew ls -l
total 0
drwxr-xr-x  4 jliu  admin  136 Jan 29 10:36 exiftool
drwxr-xr-x  4 jliu  admin  136 Jan 29 10:36 jq
drwxr-xr-x  3 jliu  admin  102 Jul 19  2016 libevent
drwxr-xr-x  4 jliu  admin  136 Jan 29 10:36 oniguruma
drwxr-xr-x  4 jliu  admin  136 Jan 29 10:36 openssl
drwxr-xr-x  4 jliu  admin  136 Jan 29 10:36 readline
drwxr-xr-x  3 jliu  admin  102 Jun 25  2016 s3cmd
drwxr-xr-x  4 jliu  admin  136 Jan 29 10:36 socat
drwxr-xr-x  4 jliu  admin  136 Jan 29 10:36 tmux

$ brew ls --versions
exiftool 10.15 10.36
jq 1.5_1 1.5_2
libevent 2.0.22
oniguruma 6.0.0 6.1.3
openssl 1.0.2h_1 1.0.2k
readline 6.3.8 7.0.1
s3cmd 1.6.1
socat 1.7.3.1 1.7.3.2
tmux 2.2 2.3_2

```

### List any packages which need to be updated
```
brew outdated
```

### Update a package
```
brew upgrade <formula>
```

### Cleanup
Homebrew keeps older versions of packages around in case you want to roll back.  
Cleanup will remove the older verions if you don't need them.

https://wilsonmar.github.io/macos-homebrew/
### To see which files would be removed as no longer needed
```
brew cleanup -n   # dry-run
brew cleanup
```


### How much space does Homebrew take up?
https://wilsonmar.github.io/macos-homebrew/
```

$ brew info
9 kegs, 4,133 files, 61.5MB

$ brew info —all
```

### How much space does each Homebrew keg take up?
```
for i in $(brew ls); do  brew info $i|grep files; done
```



### List all files in the Homebrew prefix not installed by Homebrew.
```
brew list, ls --unbrewed
```

### List all formulas with their version numbers
```
brew list --versions
```


### Information on specific home brew package
```
$ brew info exiftool
exiftool: stable 10.40 (bottled)
Perl lib for reading and writing EXIF metadata
http://www.sno.phy.queensu.ca/~phil/exiftool/index.html
/usr/local/Cellar/exiftool/10.15 (202 files, 11.6MB)
  Poured from bottle on 2016-06-20 at 11:43:34
/usr/local/Cellar/exiftool/10.36 (208 files, 12MB) *
  Poured from bottle on 2017-01-29 at 10:36:20
From: https://github.com/Homebrew/homebrew-core/blob/master/Formula/exiftool.rb
```


### Brew tap adds repos not in the Homebrew master repo from inside a larger package.
```
brew tap
```


### Information on how large a home brew package is before downloading
https://github.com/Homebrew/legacy-homebrew/issues/45088
```
brew info vault
brew info mongodb
brew info kubectl
brew info kompose

  504  brew info mongodb
  513  brew info --json=v1 mongodb | jq .
  523  curl -I -L "$(brew info --json=v1 mongodb | jq -r '.[0].bottle.stable.files.high_sierra.url')"

Example:
curl -I -L "$(brew info --json=v1 mongodb | jq -r '.[0].bottle.stable.files.high_sierra.url')"
HTTP/1.1 200 OK
Server: nginx
Date: Fri, 22 Dec 2017 19:08:55 GMT
Content-Type: application/gzip
Content-Length: 97830292
```  

### Update Home brew  
```
brew update
```
### Download and update all software packages installed
```
brew upgrade
```

### mac   sw_vers
http://osxdaily.com/2007/04/23/get-system-information-from-the-command-line/
```
jliu@JEFFREYs-MBP ~/stuff $ sw_vers 
ProductName:	Mac OS X
ProductVersion:	10.13
BuildVersion:	17A405
```


[Homebrew Cheat Sheet][link-1]


[link-1]:https://devhints.io/homebrew

