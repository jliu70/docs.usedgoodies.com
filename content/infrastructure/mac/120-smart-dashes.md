+++
date = "2019-11-18T22:04:01Z"
title = "120 smart dashes"
weight = 120
toc = true
+++


1/17/2019
Apple Notes 
disable smart dashes -- 

https://superuser.com/questions/555628/how-to-stop-mac-to-convert-typing-double-dash-to-emdash
Notes -> Edit -> Substitutions -> Smart Dashes


One of the more annoying things which Notes does is to have features like smart dashes which auto-correct dashes into emdash.  This was a source of particular pain when I was using the laptop remotely and didn't really see they conversion which was causing some weird issues with a command I was running with `doctl`.  



