+++
date = "2017-04-18T13:46:39Z"
toc = true
weight = 100
title = "100 Mac CLI tools"
+++



## Diagram of what the standard icons for command, option-alt, ctrl keys look like.

![Mac Icons to Keys Legend](/images/mac/Mac_OS_Icons_to_Keys_Legend.png?&classes=border,shadow)


## Command-Arrow keys to jump around a document
#### These work in many Mac apps (Safari, Notes, and textEdit, Visual Studio Code)
```
  Command-Up    - Top of Document
  Commmand-Down - Bottom of Document
  Command-Left  - Beginning of line
  Command-Right - End of line
```
![Mac Command Arrow shortcuts](/images/mac/Mac_OS_Command_Arrow_shortcuts.png?&classes=border,shadow)

