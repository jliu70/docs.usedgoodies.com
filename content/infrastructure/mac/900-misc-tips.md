+++
date = "2017-04-18T13:46:39Z"
toc = true
weight = 900
title = "900 Mac Misc Tips"
+++



## prep your mac for software development
https://medium.com/my-name-is-midori/how-to-prepare-your-fresh-mac-for-software-development-b841c05db18

## Decline of the macbook pro
https://news.ycombinator.com/item?id=17266492



### 12/10/2016 - Safari browser not able to download Gmail attachments
https://productforums.google.com/forum/#!topic/inbox/Kb1xXXTLaY8
I have discovered something that seems to fix the problem -  whenever I open Safari browser I go to VIEW in the menu-bar, then scroll down to RELOAD WITH PLUGINS: I am then able to download email attachments. HOWEVER, Safari doesn't seem to be able to remember this, and I have to go to VIEW + RELOAD WITH PLUGINS every time I open Safari up


## After OS upgrade you may receive an error when running git or gcc
```
xcrun: error: invalid active developer path (/Library/Developer/CommandLineTools), missing xcrun at: /Library/Developer/CommandLineTools/usr/bin/xcrun
```
### Fix
```
xcode-select --install
```
This will download and install xcode developer tools and fix the problem. The problem is that one needs to explicitly agree to the license agreement. 


