+++
date = "2017-12-16T13:46:39Z"
toc = true
weight = 300
title = "300 Apple Notes"
+++


https://discussions.apple.com/thread/7453639
https://www.alfredforum.com/topic/3055-finding-built-in-notesapp-notes/


## Where does Apple Notes store it’s files? What format are they in?
### reference:  https://news.ycombinator.com/item?id=15815040

They're stored in an sqlite file at ~/Library/Containers/com.apple.Notes/Data/Library/Notes/NotesV7.storedata (version number might be different.)
The data stored all over the place in the DB.
For example, you can extract HTML body of a note with the following query: "select ZHTMLSTRING from ZNOTEBODY".
 	

Yep. They probably use Core Data (which is a layer on top of SQLite) internally instead of straight SQLite, but either way it ends up on disk as an SQLite file. A lot of standard Apple apps do this.



Useful tip (that I didn't know until recently) is you can also access your notes via any modern browser at https://www.icloud.com/
I recently moved to a Windows machine as my day-to-day home machine and thought I'd have to abandon Apple Notes (which I use on my phone and my work Macbook) but the web interface is basically identical to the apps.

The web interface actually lets you do one thing you can't on iOS - create nested folders.


> Apple has my data, and I need to trust them.
You don't need to let Apple have your data to use Notes. Notes syncs through a standard IMAP account.
If you have your own domain, you can just create a notes@example.com e-mail account and Apple Notes will happily sync all of its data through that. That's what I do.





## How to export Apple Notes to plain text files (for backup or version control, etc)
Reference:  https://www.reddit.com/r/bearapp/comments/6sc98u/how_to_export_notes_from_apples_notes_app_as/
http://falcon.star-lord.me/exporter/. I've used it and it works.
Software developer also makes Another note taking app  Falcon  http://falcon.star-lord.me/#features

NOTE: Exporter works well, but it does NOT decrypt locked notes, nor does it handle anything outside of text (e.g, images).  Exporter also does not preserve sub-folder structure -- it places all sub-folders in the root directory.




