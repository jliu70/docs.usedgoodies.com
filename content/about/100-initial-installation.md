+++
toc = true
date = "2017-03-08T19:03:03Z"
title = "Initial Installation From Scratch"
weight = 100
+++


> Discover what this Hugo theme is all about and the core-concepts behind it.
> **Hugo-learn-theme** is a theme for Hugo, a fast and modern static website engine written in Go. Where Hugo is often used for blogs, this theme is fully designed for **technical documentation**.

Learn more about this theme from the [official documentation][link-1]


### Getting Started

It is very simple and straight forward to setup a site from scratch.  Just follow the [simple instructions.][link-2]

{{% notice warning %}}
Pre-requisites:  Git and Hugo
Hugo needs to be installed in order to view/generate the static site.  Info on Hugo can be found on the "Initial Installation" page.
{{% /notice %}}




## Brief overview/summary from the above instructions:

### Install Hugo
Follow the [steps listed on the official documentation][link-3] to install Hugo.  
(Typically you will just be downloading a single Go binary for your platform.)

### Create your Hugo site
```
hugo new site <new_project>
```

### Install theme
Once you have your Hugo site, you then install the theme.

```
cd /path/to/new_project/themes
git clone https://github.com/matcornic/hugo-theme-learn.git

```

{{% notice note %}}
If you are using git to version control your hugo site, then it is recommended that you use git submodule to install the hugo theme.
{{% /notice %}}

#### Install theme with git submodule
```
$ git submodule add https://github.com/matcornic/hugo-theme-learn.git themes/hugo-theme-learn
Adding existing repo at 'themes/hugo-theme-learn' to the index
```

#### Clone missing submodules, and checkout commits
```
git submodule update --init --recursive
```

#### Submodule status
```
$ git submodule status
 fcca8a21f1000bdb0de5db80d396ea30dc15f53e themes/hugo-theme-learn (heads/master)
```


{{% notice info %}}
For more information about git submodules see the "git submodule" page under the "git" section.
{{% /notice %}}


### Basic configuration
Recommend to update your `config.toml` with the following parameter:
```
theme = "Hugo-theme-learn"
```

### Create your first Chapter page
```
cd /path/to/new_project/
hugo new --kind chapter chapterName/index.md
```

### Create your first Content pages
```
hugo new chapterName/index.md
hugo new chapterName/first-content.md
hugo new chapterName/second-content/index.md
```

### Launching the website
Hugo can run a local copy of your website which is good for editing/previewing your work before you publish.

```
hugo server -D
```

Point your web browser to `http://localhost:1313/`  or `http://localhost:1313/chapterName`



[link-1]:http://matcornic.github.io/hugo-learn-doc/basics/what-is-this-hugo-theme/
[link-2]:http://matcornic.github.io/hugo-learn-doc/basics/installation/
[link-3]:https://gohugo.io/overview/quickstart/
