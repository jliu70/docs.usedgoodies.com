+++
toc = true
date = "2017-03-02T12:47:50Z"
title = "Usage - brief overview"
weight = 200
+++



## Frequently Used Commands


### Adding a new Chapter
```
hugo new --kind chapter chapterName/index.md

```

Optional: remove the `prev` statement for each Chapter index.md


### Adding new content
```
hugo new chapterName/first-content.md
hugo new chapterName/second-content/index.md
hugo new chapterName/second-content/second-content-1.md
```

### Setting the content order
Optional: You can order the content pages by assigning a weight to within the [Front Matter][link-1] for each content page.  
For example, `100` `200` `300` etc.
```
weight = 100
```

### Setting the menu items order

Update your `config.toml` with the following parameter:
```
[params]
  menu = [ "docker", "docker-swarm", "amazon-web-services", "kubernetes", "git", "about" ]

```


### Images

{{% notice info %}}
Optional: set the width, border, shadow, etc.
{{% /notice %}}

#### Local Images
```
![Chapter page](/images/amazon-web-services/aws-signup.png?width=50%&classes=border,shadow)

```

{{% notice tip %}}
Recommended to place images in sub-directory with similar (if not identical) name as chapterName.
{{% /notice %}}


#### Remote Images

```
![Example image](https://i0.wp.com/blog.docker.com/wp-content/uploads/lifecycle.png?w=2280&ssl=1)

```

[link-1]:https://gohugo.io/content/front-matter/
