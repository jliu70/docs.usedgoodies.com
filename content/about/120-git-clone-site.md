+++
title = "git clone this site"
weight = 120
toc = true
date = "2017-03-08T23:28:27Z"
+++

## General Steps

To get a working local copy, perform the following steps.

{{% notice warning %}}
Pre-requisites:  Git and Hugo 
Hugo needs to be installed in order to view/generate the static site.  Info on Hugo can be found on the "Initial Installation" page.
{{% /notice %}}



### Git Clone

```
git clone https://gitlab.com/jliu70/docs.usedgoodies.com.git
```

#### Verify hugo-theme-learn theme with git submodule
```
$ cd docs.usedgoodies.com
$ git submodule status
 35bd9113b8d55a7525bfcb604755afb90c5e4918 ../../themes/hugo-theme-learn (2.0.0-18-g35bd911)
```
#### If missing submodules you can configure
```
$ git submodule add https://github.com/matcornic/hugo-theme-learn.git  themes/hugo-theme-learn
```

#### Clone missing submodules, and checkout commits
```
git submodule update --init --recursive
```

#### Submodule status
```
$ git submodule status
 35bd9113b8d55a7525bfcb604755afb90c5e4918 ../../themes/hugo-theme-learn (2.0.0-18-g35bd911)
```

{{% notice info %}}
For more information about git submodules see the "git submodule" page under the "git" section.
{{% /notice %}}
