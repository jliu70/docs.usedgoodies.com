+++
toc = true
date = "2017-03-08T19:03:03Z"
title = "Why This Site"
weight = 1
+++


### Why not just reference the official Documentation for *X* ?
Depending on the project, the official documentation can be very good or very bad.  For some projects the vast amount of information is overwhelming and it can be difficult to know where to start.  The content on this site started out as my personal notes and content which I used to present to coworkers and others.  

#### Basic requirements
- Plain text based
  - Open format which will be forever supported, easily placed under version control
- Simple to setup
- Simple to use
- Content accessible from multiple devices

#### Bonus requirements
- Self-hosted
- Easy to collaborate with others


### Why not a wiki?
There are many wiki solutions out there.  Typically a wiki offers a better organizational experience with links between notes, better navigational side bar.
Wikis are also quick and easy to use.
However, most wiki solutions do not lend themselves to be easily shareable.  Some are web-based (e.g., Media wiki), but they are typically complicated to setup and/or store data in a database (and thus not plain text).
Wiki solutions are also not always available with version control, or if available, usually takes alot more effort to enable.  By treating your documentation the same as code, it makes it easier for people to share, submit pull requests for updates.

https://opensource.com/business/15/4/git-and-github-open-source-documentation-reviews
https://justwriteclick.com/2015/05/19/treat-docs-like-code-doc-bugs-and-issues/
http://idratherbewriting.com/2016/07/15/transforming-documentation-processes-docs-as-code/



### Why not a note taking app?
Note taking apps are great for personal use.  
Note taking apps are usually very easy to setup and to use.  
Typically the content within note taking apps are not easily shareable with others.  
Most note taking apps lock you in with a proprietary data format.  As a result, [version control is also often difficult or not possible.][link-1]

There are also various formats that note taking apps support.  

- One end of the spectrum: individual text files
	- Pros: forever accessible - use any editor
	- Cons: can lead to directories/folder sprawl and makes it harder to find stuff  - can't always see hierarchy.
- Other end of the spectrum: one big database
	- Pros: easier to see hierarchy with the tree view
	- Cons: not as easily accessible outside of the platform, not easily placed under version control


### Why not a blog?
Presenting technical documentation via a blog format does not work well.  
Blogs are time based.  A blog post is a snapshot in time.  Typically it is cumbersome to return to a past blog post to update.  
As blogs are sorted by date/time, the best organization we can achieve is to apply tags to blog posts.  
Blogs are ideal for making quick point in time observations or tips.  
Blogs are also ideal for expressing your opinions or to offer a review.


### What about readthedocs.org?
I prefer a self-hosted option, but if you don't care for that, then readthedocs.org is a great alternative.  
https://docs.readthedocs.io/en/latest/getting_started.html



### What this site is built with
> **Hugo-learn-theme** is a theme for [Hugo][link-2], a fast and modern static website engine written in Go.
Where Hugo is often used for blogs, this theme is fully designed for **technical documentation**.

Both [Hugo][link-2] and [Hugo-learn-theme][link-3] are extremely simple to setup and to use.





[link-1]:https://www.martinfowler.com/articles/apple-notes-restore.html
[link-2]:https://gohugo.io
[link-3]:http://matcornic.github.io/hugo-learn-doc/basics/what-is-this-hugo-theme/
