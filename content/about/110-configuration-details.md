+++
toc = true
date = "2017-03-04T20:51:49Z"
title = " - Configuration Details"
weight = 110
+++

### Configuration Details
#### Custom Logo
`/layouts/partials/logo.html` will override `/themes/hugo-learn-theme/layouts/partials/logo.html`

#### Custom Home Page
`/layouts/index.html` will override `/themes/hugo-learn-theme/layouts/index.html`


#### Add 'Edit this page' link
Update your `config.tolm` with the following parameters:
```
[params]
  editURL = "https://gitlab.com/jliu70/docs.usedgoodies.com/edit/master/content/"
  description = "Documentation"
  author = "Jeff Liu"

```
