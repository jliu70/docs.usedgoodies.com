hugo-lanyon-blog
============

Template repo for a blog built with Hugo.


# Some possible future posts:



## Expand section on Cloudfront
11/10/2016
Expand section on AWS Cloudfront - including invalidation - first 1000 objects are free within a month


## Expand section on IAM
11/10/2016
Based off webinar - slideshare http://www.slideshare.net/AmazonWebServices/introduction-to-three-aws-security-services-november-2016-webinar-series
Recording should be sent via email within two days
NOTE: also see screen captures saved to 192.241.169.43:aws-iam-16.zip


## Docker in production - a blog retort
11/3/2016
http://patrobinson.github.io/2016/11/05/docker-in-production/


## Add portainer.io blurb to Docker post
11/3/2016
Seems like a convenient GUI to common Docker commands.  It touts itself as a Rancher competitor / replacment, but I don't agree that it even comes close.
http://portainer.io/
https://portainer.readthedocs.io/en/latest/deployment.html
https://github.com/portainer/portainer

12/30/2016 - portainer review https://media-glass.es/portainer-the-ui-for-docker-d067f6335f23#.ldriqv9ar


## add a usedgoodies.com blog post on Git Pitch
Git Pitch integration availability was announced by Gitlab on 10/3/2016 https://about.gitlab.com/2016/10/03/gitpitch-slideshow-presentations-for-developers-on-gitlab/
It is similar to RemarkJS and provides an additional SaaS model to auto-generate your slides with the option to download in PDF format.




## add a post comparing the features of various options on presenting information
Should I use a hugo blog?   Should I use Git Pitch?   Should I just use github /docs convention?
https://github.com/blog/2233-publish-your-project-documentation-with-github-pages

### Blog format
Pros: asynchronous communication, allows for discussion threads (via disqus or discourse)
Cons: harder to author, generate finished product, and typically requires web hosting
    Hugo blog - for generic blog posts - can be used for documentation with discussions to allow for user feed back, Hugo blog can work offline!
    Presentation medium
        Github /docs convention
            https://github.com/blog/2233-publish-your-project-documentation-with-github-pages
        Github Pages
        Gitlab Pages

### Slide Deck / Presentation format
    Powerpoint/Keynote
        Pros: easy to write content - Powerpoint comes with WYSIWYG editor (using Git Pitch requires more effort), great for presentations or in person discussions
        Cons: effort in summarization during generation of slide content - typically a blog can be more verbose and descriptive, powerpoint not efficient to track in git (version control), content typically not as easy to share (probably through slideshare or PDF?)
    Git Pitch
        Pros: easy to write plaintext markdown - just one PITCHME.md markdown file
        Cons: more effort in summarizatoin during generation of slide content - typically a blog can be more verbose and descriptive. Git Pitch (and other markdown based presentation formats) is not as easy to use as Powerpoint especially formatting with diagrams/screen shots.


### Zim wiki
    Pros: plain text format with simple text formatting -- no proprietary format lock in -- self-hosted -- can work offline.  Can attach files and diagrams (no layout formatting).  Great for personal notes or reference tracking.
    Cons: Not as easy to share - users typically will need to install zim wiki software (python based), or view plain text files, but without context/linking/navigation.  Having TODOs in zim wiki is a simple to intially author, but makes it harder to track or follow -- perhaps better to use Gitlab's new issue board?

#### Side note: migrate Task List from zim wiki to gitlab issue board.   Add info on viewing gitlab issues from the CLI?  Cons: can't work offline?


#### 11/23/2016 Interesting commercial product  https://zenkit.com
Allows you to create a task list, which can morph in real time to kanban board, or to a spreadsheet table, or to a calendar, or to a mind map.
##### NOTE: gitlab issues already offer 'list' view as well as kanban board view.  Has milestones to set due dates.  Lacks table view, calendar view, or mind map.
##### 12/8/2016 - registered for free account and tried it out.  Very nice - except that it is not open or self-hosted.  Much better than gitlab issue boards for the purposes of tracking project items.
Cons: can't work offline!!


### 12/15/2016 Apple Notes
https://discussions.apple.com/thread/7430385?start=0&tstart=0
http://www.makeuseof.com/tag/10-tips-get-apple-notes-os-x/
    Pros: Free and is part of Apple's ecosystem.  iCloud sync across devices.  Can work offline.
    Cons: Not plain text, although there are tools you can use to export from database to plain text.   No tag support as of 12/2016 (just topical folders).  
NOTE: Maybe I should use Notes for research related items to keep the zim wiki uncluttered with just 'reference' and 'permanent notes'.  







## Apple watch 2
http://arstechnica.com/gadgets/2016/09/apple-watch-series-2-review-the-best-of-a-fitness-tracker-in-smartwatch-form/






## mac os virtualization product on xhyve
https://veertu.com/


## Docker
### 12/11/2016 - Learning Docker eBook  (Packt pub - free eBook)  9781784397937-LEARNING_DOCKER.pdf

http://www.infoworld.com/article/3138035/data-center/how-docker-changes-application-monitoring.html

https://github.com/timgrossmann/StorageSystem

### 11/22/2016 Docker Survey 2016 http://blog.calsoftinc.com/industry_insights/docker-survey-2016

## 11/28/2016 Dockerize MySQL at Uber https://eng.uber.com/dockerizing-mysql/

https://medium.com/lucjuggery/discovering-docker-datacenter-cf0daccddc41#.slykm8tj1

https://docs.docker.com/engine/swarm/key-concepts/
http://blog.alexellis.io/microservice-swarm-mode/
https://www.digitalocean.com/community/tutorials/how-to-deploy-a-node-js-and-mongodb-application-with-rancher-on-ubuntu-14-04

https://diogomonica.com/2016/11/19/increasing-attacker-cost-using-immutable-infrastructure/

http://blog.arungupta.me/docker-container-anti-patterns/

http://servicesblog.redhat.com/2016/08/11/automating-the-provisioning-and-configuration-of-red-hat-mobile-application-platform/



https://anchore.com/blog/is-docker-more-secure/
https://anchore.com/blog/anchore-1-0-fast-pace-innovation-meets-production-deployment-confidence/
https://anchore.com/blog/
https://anchore.com


http://blog.kubernetes.io/2016/09/cloud-native-application-interfaces.html
http://ducky.cloud/blog/infrastructure_discovery_with_etcd


# 12/1/2016 http://www.infoworld.com/article/3145696/application-development/docker-for-aws-whos-it-really-for.html


# 12/8/2016 - Docker acquires Infinit - persistent storage for Docker - see email from Docker Weekly #165  - also need to add this as something to look at  
# 12/7/2016  Docker most popular infrastructure is VM  http://searchitoperations.techtarget.com/feature/VMs-prove-most-popular-Docker-infrastructure-for-now


# 11/22/2016 ECS and Vault https://www.kickstarter.com/backing-and-hacking/ecs-and-vault-shhhhh-i-have-a-secret
# 9/22/2016  Vault http://chairnerd.seatgeek.com/secret-management-with-vault/


# 11/30/2016 - Let's encrypt - review this and probably try it out for usedgoodies.com?  https://gist.github.com/mapmeld/a9bcac46d1f486f81664814a799e5897
# 12/1/2016 - created a new shell script which should help list the expiration dates for all the certificates



# 11/25/2016 - Common web app architectures https://www.digitalocean.com/community/tutorials/5-common-server-setups-for-your-web-application


## 11/30/2016 - Kontena reaches 1.0   http://blog.kontena.io/kontena-1-0/
### http://blog.blackducksoftware.com/kontena-developing-a-friendly-container-platform
### how it compares to kubernetes and rancher etc   https://www.kontena.io/docs/faq

## 11/25/2016 production kubernestes cluster on Digital Ocean https://5pi.de/2016/11/20/15-producation-grade-kubernetes-cluster/


### 12/7/2016 - various info on kubernetes, swarm, devops
https://www.upcloud.com/blog/docker-swarm-vs-kubernetes/
https://www.upcloud.com/blog/kubernetes-changing-devops/
https://www.upcloud.com/blog/the-rise-of-coreos/
https://www.upcloud.com/blog/bringing-high-performance-to-containers-and-microservices-kontena/


## 12/13/2016 https://medium.com/google-cloud/local-django-on-kubernetes-with-minikube-89f5ad100378#.yzn03hs1g


## 11/26/2016 - Cloud computing price comparison chart http://welcome.linode.com/pricing-comparison/
https://thehftguy.wordpress.com/2016/11/18/google-cloud-is-50-cheaper-than-aws/






## Microsoft loves linux
https://blogs.msdn.microsoft.com/wsl/2016/10/19/windows-and-ubuntu-interoperability/
https://msdn.microsoft.com/en-us/commandline/wsl/interop
https://msdn.microsoft.com/en-us/commandline/wsl/faq


## microsoft devops
https://channel9.msdn.com/Series/DevOps-Fundamentals
http://devopsassessment.azurewebsites.net/
http://stories.visualstudio.com/devops/
http://www.itproguy.com/how-microsoft-does-agile-devops-resources/
https://channel9.msdn.com/Shows/DevOps-Dimension
https://mva.microsoft.com/training-topics/devops#!lang=1033
http://www.itproguy.com/how-to-enable-devops-practices-with-partsunlimited-apps/


## Microsoft SQL Server for Linux
https://blogs.technet.microsoft.com/dataplatforminsider/2016/11/16/announcing-the-next-generation-of-databases-and-data-lakes-from-microsoft/

## Microsoft Visual Studio for Mac
https://www.visualstudio.com/vs/visual-studio-mac/


## Azure Container Registry
https://azure.microsoft.com/en-us/campaigns/container-registry/






## 11/25/2016 - AWS QuickSight - analogous to Microsoft BI  https://aws.amazon.com/about-aws/whats-new/2016/11/amazon-quicksight-now-generally-available/

## AWS Organizations - centrally manage multiple AWS accounts:  https://aws.amazon.com/blogs/security/announcing-aws-organizations-centrally-manage-multiple-aws-accounts/

## 11/30/2016 - AWS lightsail begins simple Virtual servers - competition for Digital Ocean
https://techcrunch.com/2016/11/30/aws-announces-virtual-private-servers-starting-at-5-a-month/
https://amazonlightsail.com
## 12/8/2016 https://blog.containership.io/amazon-lightsail-vs.-digitalocean

## Devops - https://blog.containership.io/devops-is-not-about-automation-tools-but-theyre-a-prerequisite


## 11/30/2016 - AWS Athena - SQL query data stored in S3 https://aws.amazon.com/athena/


## 11/25/2016 - AWS Lambda  https://aws.amazon.com/blogs/aws/new-for-aws-lambda-environment-variables-and-serverless-application-model/



## 12/2/2016 - Scott Lowe live blog from AWS re:invent
http://blog.scottlowe.org/2016/12/01/aws-reinvent-keynote-werner-vogels/
http://blog.scottlowe.org/2016/11/30/automating-cloud-mgmt-deployment/
http://blog.scottlowe.org/2016/11/29/getting-bang-buck-ec2/
http://blog.scottlowe.org/2016/11/29/hybrid-bridging-gap-to-cloud/
http://blog.scottlowe.org/2016/10/23/managing-aws-infrastructure-ansible/


https://aws.amazon.com/blogs/aws/aws-x-ray-see-inside-of-your-distributed-application/
https://aws.amazon.com/batch/
https://aws.amazon.com/blogs/aws/amazon-pinpoint-hit-your-targets-with-aws/
https://aws.amazon.com/blogs/aws/aws-shield-protect-your-applications-from-ddos-attacks/
https://aws.amazon.com/shield/
https://aws.amazon.com/blogs/aws/aws-codebuild-fully-managed-build-service/
https://aws.amazon.com/codebuild/
https://aws.amazon.com/xray/
https://aws.amazon.com/blogs/aws/new-aws-step-functions-build-distributed-applications-using-visual-workflows/
https://aws.amazon.com/about-aws/whats-new/2016/12/aws-config-now-integrates-with-amazon-ec2-systems-manager-to-provide-continuous-monitoring-and-governance-of-software-on-your-ec2-instances-and-on-premises-systems
https://blox.github.io


## AWS Re:invent 12/9/2016 https://gist.github.com/stevenringo/5f0f9cc7b329dbaa76f495a6af8241e9
https://www.chrisanthropic.com/sending-mail-ses-route53-dkim-spf-dmarc/
https://aws.amazon.com/lex/







## git learning
https://www.shortcutfoo.com/app/dojos/git
### git 2.11 - abbreviated SHA-1 names and other stuff
https://github.com/blog/2288-git-2-11-has-been-released





# minecraft PE 1.0
https://androidcommunity.com/minecraft-pocket-edition-presents-update-1-0-the-ender-is-near-20161114/








### https://static.land

# 11/28/2016 - http://laszlo.nu/2016/11/25/s3-static-site.html
I should expand upon this blog post - by looking into using AWS IAM for Let's Encrypt SSL certificate.  
Typical blog post -- it starts out strong and then fizzles out.









## 11/30/2016 - Raspberry Pi - security update -  no longer enables SSH by default
https://www.raspberrypi.org/blog/a-security-update-for-raspbian-pixel/







https://peteris.rocks/blog/htop/
https://jorge.fbarr.net/2014/01/19/introduction-to-strace/
http://developers.redhat.com/blog/2016/10/28/what-comes-after-iptables-its-successor-of-course-nftables/





## 12/1/2016 - Question mark in blue folder on MacOS terminal title bar.  http://apple.stackexchange.com/questions/75751/why-does-the-folder-icon-in-my-terminal-have-a-question-mark-overlaid









# 12/9/2016 - Openshift
What is OpenShift Origin?
OpenShift Origin is the open source upstream project that powers OpenShift, Red Hat’s container application platform.

https://www.openshift.org/
Try Out the Latest Origin Release
Run Origin in a Container:   https://docs.openshift.org/latest/getting_started/administrators.html#running-in-a-docker-container
Run the All-in-One VM:  https://www.openshift.org/vm/








### 12/11/2016 - Blockchain in a nutshell for beginners  http://blockgeeks.com/guides/what-is-bitcoin-a-step-by-step-guide/



### 12/11/2016   https://about.gitlab.com/2016/12/05/building-gitlab-office-hours/



### 12/11/2016 Kubernetes https://github.com/kelseyhightower/konfd


http://thenewstack.io/cloudfabrix-launches-macaw-new-microservices-platform/


#### 12/11/2016 - automation of openstack with stacki and ansible
https://www.ansible.com/webinars-training/automation-of-openstack-clusters-stacki-ansible







## Slack
http://thenextweb.com/apps/2015/02/05/13-cool-things-might-not-know-can-slack/
https://get.slack.help/hc/en-us/articles/202026038-Slackbot-personal-assistant-and-helpful-bot-




### 9/20/2016 https://www.youtube.com/watch?v=1Gtmca4lCcQ
https://www.youtube.com/watch?v=JKsHhXwqDqM








https://github.com/rusher81572/picluster

12/11/2016
http://blog.alexellis.io/hardened-raspberry-pi-nas/
http://blog.alexellis.io/environmental-monitoring-dashboard/
http://blog.alexellis.io/christmas-iot-tree/
http://blog.alexellis.io/get-started-with-docker-on-64-bit-arm/




http://blog.alexellis.io/scale-docker-swarm-on-aws/
http://blog.alexellis.io/3-steps-to-msbuild-with-docker/
http://blog.alexellis.io/keeping-shipping-your-blog/

http://blog.alexellis.io/continuous-integration-docker-windows-containers/
http://blog.alexellis.io/docker-does-sql2016-aspnet/
http://blog.alexellis.io/run-iis-asp-net-on-windows-10-with-docker/

http://blog.alexellis.io/test-drive-healthcheck/







#### 12/11/2016  git@github.com:chrissnell/chrissnell.com.git  - built with Hugo
https://chrissnell.com/post/ipv6-detection-for-static-sites/


https://news.ycombinator.com/item?id=13153031
https://about.gitlab.com/2016/12/11/proposed-server-purchase-for-gitlab-com/
Chris Snell • 7 hours ago
I lead a technical operations team that moved our infrastructure from public cloud (~400 instances) to private cloud (~55 physical servers) and finally, to Kubernetes (6 physical servers). We actually run a mix of Kubernetes and OpenStack, putting apps and services in Kube and all data storage in OpenStack. I've done extensive testing with Ceph and while it adds flexibility, you're not going to be able to touch the I/O performance of bare metal local disks for database use. For storage, I like to keep it simple. I rely on the Linux OS running on standard tried-and-true filesystems (ext4 and ZFS) and build redundancy at the software layer.

We're currently hosting on bare metal at Rackspace--they manage hardware replacements and we manage everything from the BIOS on up. I'm currently evaluating moving to colocation and it's likely that we'll initiate this in 2017.

We've been running on Kubernetes for over a year now and we've learned a lot about what it takes to run it well. We've built an automated deployment pipeline to build and quickly deploy all of our apps in Docker containers. My team has built a completely self-serve architecture where developers can configure their own custom testing, deployment, metrics collection, monitoring, and load balancing without having to interface with my team. They do this by packaging configuration for all of these things in YAML files that get parsed by the deployment pipeline.






Google search:  nodejs markdown editor
https://scotch.io/tutorials/building-a-real-time-markdown-viewer



# docker swarm
http://searchsoa.techtarget.com/feature/Orchestrate-and-manage-containers-using-the-Docker-Swarm-service


# docker container trends for 2017?
https://www.sdxcentral.com/articles/news/6-docker-containers-themes-track-2017/2016/12/
