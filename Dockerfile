# http://zhezhang.co/2015/10/hugo-docker-and-daocloud/
# https://github.com/yunspace/docker-alpine-hugo/blob/master/Dockerfile
# docker build -t hugo .
# docker images
# cd /path/to/hugo; docker run --rm -ti -p 1313:1313 hugo 
FROM alpine:3.7
MAINTAINER Jeff Liu (https://github.com/jliu70)

ENV HUGO_VERSION 0.32.3
ENV HUGO_BINARY hugo_${HUGO_VERSION}_Linux-64bit.tar.gz
WORKDIR /tmp

RUN apk --update add git wget py-pygments && rm -rf /var/cache/apk/*  \
  && wget --no-check-certificate https://github.com/spf13/hugo/releases/download/v${HUGO_VERSION}/${HUGO_BINARY} \
  && tar -xzf ${HUGO_BINARY} \
  && mv /tmp/hugo /usr/local/bin/hugo \
  && apk del wget \ 
  && rm -rf /var/cache/apk/* \
  && rm -rf /tmp/* \
  && chmod 1777 /tmp \
  && adduser -D hugo \
  &&  mkdir -p /srv/hugo
#COPY . /srv/hugo

WORKDIR /srv/hugo
EXPOSE 1313
USER hugo
#CMD ["hugo", "--buildDrafts", "server", "--bind=0.0.0.0", "--baseUrl=http://usedgoodies.com/", "--port=80", "--appendPort=false"]
#ENTRYPOINT ["hugo"]
#CMD ["--buildDrafts", "server", "--bind=0.0.0.0", "--port=1313"]
#CMD ["/bin/sleep", "100000000"]


# docker run --rm -ti -p 1313:1313 -v $(pwd):/srv/hugo hugo sh
# inside hugo container:  /srv/hugo $ hugo --buildDrafts --bind=0.0.0.0 server

# Alternative:  docker-compose 
#   see docker-compose.yml
# docker-compose up

